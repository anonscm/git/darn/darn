\documentclass[a4paper,titlepage]{article}

\usepackage{t1enc}
\usepackage[pdftex]{graphicx}
\usepackage{times}
\usepackage{float}

\title{Darn v1.0\\ 
{\Large A weighted constraint solver for RNA motif localization}\\
User manual}
\author{INRA Toulouse, France}
\date{February 10, 2010} 


\begin{document}
{\fbox{
\begin{minipage}{1\textwidth}
\maketitle
\end{minipage}
}}

\tableofcontents

\newpage

\section{Presentation}
Darn is a RNA motif search tool. It finds all the subsequences that match a specified motif on some input genomic sequence(s).\\
\\
Darn uses a weighted constraint solver to localize the portions of a genomic sequence that match a motif. The motif is expressed in a specific language (see section 5). Some motif descriptors are provided with the software for FMN, Lysine, RNaseP, SAM, snoRNA C/D-box and tRNA search.\\
\\
The predictions could be available in GFF, FASTA and other formats (see section 4).\\
\\
Darn can be use on line at this address: \texttt\scriptsize{{http://carlit.toulouse.inra.fr/Darn}}.\\
\\
Darn was developped by Matthias Zytnicki during his PhD in the BIA lab at INRA Toulouse, France.\\
\\
Darn is written in C++ for Linux systems and is distributed under the GNU GPL licence.
The current version is 1.0 (February 10, 2010).\\
\\
If you are satisfied to use it, please cite \textit{Darn - A Weighted Constraint Solver for RNA Motif Localization}. M. Zytnicki, C. Gaspin, T. Schiex. Constraints, Volume 13 (2008).\\



\section{Install}
From archive package \texttt{darn-1.0.tar.gz} only \textbf{gcc}, \textbf{g++} and \textbf{makedepend} are required on the computer.\\ 
\\ 
First, download the software.\\ 
Go to this Internet page \texttt\scriptsize{{https://mulcyber.toulouse.inra.fr/frs/?group\_id=15}} .\\
Download the current file \texttt{darn-1.0.tar.gz} .\\
\\
Then go to the directory containing the file.\\
Execute the following commands:\\
\texttt{
\hspace*{1cm}tar xzvf darn-1.0.tar.gz\\
\hspace*{1cm}cd darn\\
\hspace*{1cm}make\\
}
\\
To be able to call the software from everywhere, define an environment variable called DARN\_PATH. \\
If \texttt{<path>} is the absolute path of \texttt{darn} directory, execute the command:\\
\hspace*{1cm}\texttt{setenv DARN\_PATH <path>/src} if you use tcsh shell\\
\hspace*{1cm}\texttt{export DARN\_PATH=<path>/src} if you use bash shell\\
To avoid to type this command in each user session, add it to the login shell script in your home directory.
Using tcsh script, the file is called \texttt{.tcshrc}. 
Using bash script, the file is called \texttt{.bashrc}. 



\section{Run example}
After the installation, go to the \texttt{src} directory and type the following command:\\
{\footnotesize 
\begin{verbatim}
darn -s ../example/sample.fa -d ../descriptor/tRNA_Bacteria.des -n B -f P
\end{verbatim}
} 
\noindent
You should have the following output.

{\footnotesize 
\begin{verbatim}
Darn_Pred_1	DARN	.	9979	10050	0	+	.
GTCCCCTTCGTCTAGAGGCCCAGGACACCGCCCTTTCACGGCGGTAACAGGGGTTCGAATCCCCTAGGGGAC
(((((((..(((...........))).(((((.......)))))....(((((.......))))))))))))
Darn_Pred_2	DARN	.	17079	17159	0	+	.
GGTGGGGTTCCCGAGCGGCCAAAGGGAGCAGACTGTAAATCTGCCGTCACAGACTTCGAAGGTTCGAATCCTTCCCCCACC
(((((((..(((...........))).(((((.......))))).............(((((.......))))))))))))
Darn_Pred_3	DARN	.	17280	17350	0	+	.
GCGGGCATCGTATAATGGCTATTACCTCAGCCTTCCAAGCTGATGATGCGGGTTCGATTCCCGCTGCCCGC
(((((((..(((..........))).(((((.......)))))....(((((.......))))))))))))
Darn_Pred_4	DARN	.	51381	51452	0	+	.
GTCCCCTTCGTCTAGAGGCCCAGGACACCGCCCTTTCACGGCGGTAACAGGGGTTCGAATCCCCTAGGGGAC
(((((((..(((...........))).(((((.......)))))....(((((.......)))))))))))) 
\end{verbatim}
} 


\section{Arguments description}

The syntax to call Darn is the following.\\
\\
\texttt{darn -d file1 -s file2 [-o outputfile] [-n F|R|B]}\\
\hspace*{1cm}\texttt{[-x file3 [-v]]}\\
\hspace*{1cm}\texttt{[-f S|G|P|F] [-t] [-c] [-u] [-e]}\\
\hspace*{1cm}\texttt{[[-w] [-y drawdirectory] [-z webdrawdirectory]]}\\
\hspace*{1cm}\texttt{[-V[V[V]]] [-a time]}\\
\\
Main options\\
\hspace*{1cm}	\texttt{-d file1}: the file that contains the descriptor\\
\hspace*{1cm}	\texttt{-s file2}: the file that contains the main stem\\
\hspace*{1cm}	\texttt{-o outputfile}: a file to write the solutions (default: standard output)\\
\hspace*{1cm}	\texttt{-n F(default)|R|B}: search on the forward(F)/reverse(R)/both(B) strand(s)\\
\hspace*{4cm} on the main sequence\\
\hspace*{1cm}	\texttt{-x file3}: the file that contains the duplex stem sequence\\
\hspace*{1cm}	\texttt{-v}: search from 3' to 5' on the duplex sequence\\
\\
Output options\\
\hspace*{1cm}	\texttt{-f S(default)|G|P|F}: print solutions with specific(S), GFF(G), easy \\
\hspace*{4cm}parsable(P), FASTA(F) format\\
\hspace*{1cm}	\texttt{-t}: print the secondary structure (only in specific output format)\\
\hspace*{1cm}	\texttt{-c}: print solutions with lower cost first (solution score not given in FASTA output format)\\
\hspace*{1cm}	\texttt{-u}: print all dominated solutions\\
\hspace*{1cm}	\texttt{-e}: print the costs given by the constraints (only in specific output format)\\
\\
Web output options\\
\hspace*{1cm}	\texttt{-w}: output solutions using HTML markups\\
\hspace*{1cm}	\texttt{-y drawdirectory}: draw the solution in the given directory\\
\hspace*{1cm}	\texttt{-z webdrawdirectory}: WWW address to the directory containing the drawings\\
\\
Verbosity\\
\hspace*{1cm}	\texttt{-V}: verbose search\\
\hspace*{1cm}	\texttt{-VV}: very verbose search\\
\hspace*{1cm}	\texttt{-VVV}: very, very verbose search\\
\hspace*{1cm}	\texttt{-VVVV}: verbose search for software development\\
\\
Miscellanous\\
\hspace*{1cm}	\texttt{-a n}: alarm, stop after 'n' seconds\\


\section{Motif description syntax}
Darn searches in the input sequence(s) all the subsequences that match a motif. This motif has to be described in the following specific language allowing to describe the general shape of a non-protein-coding RNA through constraints. \\ 

\subsection{General shape}
Here is the skeleton of a descriptor: \\
\texttt{\fbox{
\begin{minipage}{1\textwidth}
{\scriptsize TOP\_VALUE = n \\
POSITIONS X\_VAR = $ X_{min}..X_{max} $, Y\_VAR = $ Y_{min}..Y_{max} $ \\
\# cost functions here }
\end{minipage}
}}\\ \\
The first line gives the overall number of errors the candidate may have, with respect to the signature. This number is \texttt{n}. \\
On the second line, the \texttt{X} positions refer to positions in the main sequence, whereas the \texttt{Y} positions refer to positions in the target sequence, if any. Thus, if you do not need any target sequence, drop the \texttt{Y\_VAR = $ Y_{min}..Y_{max} $} part. This line creates \texttt{$ X_{max} - X_{min} + 1 $} (ranging from \texttt{$ X_{X_{min}} $} to \texttt{$ X_{X_{max}} $}) positions on the main sequence, and \texttt{$ Y_{max} - Y_{min} + 1 $} on the target sequence. \\

\subsection{List of available cost functions} 
Here are some examples involving every supported cost function. All these functions use a parameter model, which is set to soft by default. We will detail its meaning in subsection 5.4. \\
\\
\textbf{PATTERN cost function}\\
\texttt{\fbox{
\begin{minipage}{1\textwidth}
{\scriptsize PATTERN[word="ACGU",errors=1,model=soft](X1,X2) }
\end{minipage}
}}\\ 
Look for the word \texttt{ACGU} starting at position \texttt{X1} and ending at position \texttt{X2}, with one possible error. You may use the letters \texttt{A}, \texttt{C}, \texttt{G}, \texttt{U} (or \texttt{T}), \texttt{N} or any ambiguous nucleotide (namely \texttt{B}, \texttt{D}, etc.) to describe the word. Possible errors are insertions, deletions and substitutions. \\
\\
\textbf{SPACER cost function}\\
\texttt{\fbox{
\begin{minipage}{1\textwidth}
{\scriptsize SPACER[length=15..20..30..35,costs=1..5,model=soft](X1,X2) }
\end{minipage}
}}\\ 
If the distance between positions \texttt{X1} and \texttt{X2} is not less than 20, and not greater than 30, then give a cost of 1. If the distance between positions \texttt{X1} and \texttt{X2} is less than 15, or greater than 35, then give a cost of 5. Otherwise, it gives a cost between 1 and 5. The spacer cost profile looks like this: 
\begin{figure}[H]
  \begin{center}
    \includegraphics[height=3.5cm]{UserManual_images/spacerCost}
  \end{center}
\end{figure}

\hspace*{-0.6cm}
\textbf{COMPOSITION cost function}\\
\texttt{\fbox{
\begin{minipage}{1\textwidth}
{\scriptsize COMPOSITION[nucleotides="CG",threshold>=60..80,costs=1..5,model=soft](X1,X2) }
\end{minipage}
}}\\ \\
If the (G+C)\% between positions \texttt{X1} and \texttt{X2} is not less than 80\%, then the given cost is 1. If it is not greater than 60\%, then the given cost is 5. Otherwise, the cost is between 1 and 5. The composition cost profile looks like this:
\begin{figure}[H]
  \begin{center}
    \includegraphics[height=4cm]{UserManual_images/compositionCost}
  \end{center}
\end{figure}

\hspace*{-0.6cm}
\textbf{HELIX cost function}\\
\texttt{\fbox{
\begin{minipage}{1\textwidth}
{\scriptsize HELIX[stem=5..7,loop=4..10,errors=1,model=soft](X1,X2,X3,X4) }
\end{minipage}
}}\\ 
Set the presence of an helix formed by a stem between positions \texttt{X1} and \texttt{X2} on the one hand, and a stem between positions \texttt{X3} and \texttt{X4} on the other hand. The stem should contain not less than 5 base pairings, and not more than 7 base pairings. The loop should contain not less than 4 nucleotides, and not more than 10 nucleotides. One substitution is allowed in the stem. If you also want to allow insertions and deletions, add \texttt{indels=yes} to the list of parameters. Only canonic interactions (i.e. \texttt{A-U} and \texttt{G-C}) are allowed. If you want to allow wobble interactions (i.e. \texttt{G-U}), add \texttt{wobble=yes} to the list of parameters. \\
\\
\textbf{REPETITION cost function}\\
\texttt{\fbox{
\begin{minipage}{1\textwidth}
{\scriptsize REPETITION[size=5..7,distance=4..10,errors=1,model=soft](X1,X2,X3,X4) }
\end{minipage}
}}\\ 
Set that the subsequence between positions \texttt{X1} and \texttt{X2} is repeated between positions \texttt{X3} and \texttt{X4}. The \texttt{size} parameter give the size of the repeated subsequence, and the \texttt{distance} parameter gives the distance between the two subsequences. The number of allowed differences between the subsequences is given by the parameter \texttt{error}, and the optional parameter \texttt{indels} states whether indels are allowed. \\
\\
\newpage
\noindent
\textbf{PAIR cost function}\\
\texttt{\fbox{
\begin{minipage}{1\textwidth}
{\scriptsize PAIR[interaction=WATSON\_CRICK,interaction=SUGAR,orientation=TRANS,\\
\hspace*{6cm} family=2,model=soft](X1,X2) }
\end{minipage}
}}\\ 
Set a non canonic pairing between the nucleotides at positions \texttt{X1} and \texttt{X2}. The nucleotide at position \texttt{X1} interacts with its Watson-Crick side ; the nucleotide at position X2 interacts with its Sugar side. This is a \textit{trans} interaction, in the geometric family 2, as defined by Leontis \textit{et al}. If you wish to accept all geometric families, given two sides and an orientation, write \texttt{family=all}. \\
\\
\textbf{DUPLEX cost function}\\
\texttt{\fbox{
\begin{minipage}{1\textwidth}
{\scriptsize DUPLEX[errors=1,model=soft](X1,X2,Y1,Y2) }
\end{minipage}
}}\\ 
Set the presence of a duplex formed by a stem between positions \texttt{X1} and \texttt{X2} on the main sequence, and a stem between positions \texttt{Y1} and \texttt{Y2} on the target sequence. The duplex may contain a substitution, an insertion, or a deletion. Notice that it is a time consuming constraint, so set the distance between \texttt{X1} and \texttt{X2}, and between \texttt{Y1} and \texttt{Y2}, and do not use high numbers of errors. If you want to allow wobble interactions, add \texttt{wobble=yes} to the list of parameters. 

\subsection{Changing the costs}
For the \texttt{PATTERN}, \texttt{HELIX}, \texttt{DUPLEX} and \texttt{REPETITION} cost functions, you can also change the costs given by the functions. For example, if you write: \\
\texttt{\fbox{
\begin{minipage}{1\textwidth}
{\scriptsize PATTERN[word="ACGU",errors=1,model=soft](X1,X2) \{0,2\} }
\end{minipage}
}}\\
then the cost given for no error is 0 (as usual). If one error is found, a cost of 2 is given. 

\subsection{The \texttt{model} parameter}
The \texttt{PATTERN, HELIX, DUPLEX} and \texttt{REPETITION} functions all have an \texttt{errors} parameter. If the number of errors found is greater than the value of \texttt{errors}, then the element of structure is not accepted. \\
However, if you use \texttt{model=optional}, then every cost greater than \texttt{errors} will be set to \texttt{errors}.  \\
If you use \texttt{model=hard}, then every cost greater than \texttt{errors} will not be accepted (like the \texttt{soft} option), but any cost not greater than \texttt{errors} will be set to zero. \\
The syntax of the functions \texttt{SPACER} and \texttt{COMPOSITION} with \texttt{model=hard} changes a bit. The \texttt{SPACER} only uses two lengths (the minimum and the maximum lengths), and the \texttt{COMPOSITION} only uses one threshold. Both of them do not use the \texttt{costs} parameter. 

\subsection{Example }
Here is a little example, describing an H/ACA box sRNA \\
\texttt{
\fbox{
\begin{minipage}{1\textwidth}
{\scriptsize 
\# an H/ACA box sRNA \\
TOP\_VALUE = 3 \\
VARIABLES X\_VAR = 1...18, Y\_VAR = 1...4 \\
\\
PATTERN[word="RA",err=0,model=hard](X7,X8) \\
PATTERN[word="GA",err=1,model=optional](X7,X8) \\
PATTERN[word="RUGA",err=0,model=hard](X9,X10) \\
PATTERN[word="ANA",err=0,model=hard](X17,X18) \\
PATTERN[word="NUNN",err=0,model=hard](Y2,Y3) \\
\\
HELIX[stem=7..10,loop=30..65,err=1,model=soft](X1,X2,X15,X16) \\
\\
DUPLEX[err=1,model=soft](X3,X4,Y1,Y2) \\
DUPLEX[err=1,model=soft](X13,X14,Y3,Y4) \\
COMPOSITION[nucleotides="GC",threshold>=75..100\%,costs=0..2,model=soft](X5,X6) \\
COMPOSITION[nucleotides="GC",threshold>=70..100\%,costs=0..2,model=soft](X5,X6) \\
COMPOSITION[nucleotides="GC",threshold>=65..100\%,costs=0..2,model=soft](X11,X12) \\
\\
SPACER[length=0..8,model=hard](X2,X3) \\
SPACER[length=3..6,model=hard](X3,X4)\\ 
SPACER[length=1..1,model=hard](X4,X5) \\
SPACER[length=6..8,model=hard](X5,X6) \\
SPACER[length=1..1,model=hard](X6,X7) \\
SPACER[length=3..30,model=hard](X8,X9) \\
SPACER[length=1..1,model=hard](X10,X11) \\
SPACER[length=4..8,model=hard](X10,X12) \\
SPACER[length=1..1,model=hard](X12,X13) \\
SPACER[length=3..6,model=hard](X13,X14) \\
SPACER[length=0..8,model=hard](X14,X15) \\
SPACER[length=0..1,model=hard](X16,X17) \\
SPACER[length=3..6,model=hard](Y1,Y2) \\
SPACER[length=3..6,model=hard](Y3,Y4) \\
SPACER[length=10..12,model=hard](Y1,Y4) 
}
\end{minipage}
}}\\ 


\section{FAQ}

\textbf{How to report a bug or a feature required ?}\\
Use the Bug and Feature Tracker of the project. \\
So go to the following page\\ \texttt{\scriptsize{https://mulcyber.toulouse.inra.fr/tracker/?atid=528\&group\_id=15\&func=browse}} .\\
Click on the 'New' link, fill the description frame and submit the request.\\
\\ 
\textbf{I can't access directly pages related to Darn.}\\
Darn is hosted by a software Forge \texttt{mulcyber.toulouse.inra.fr} with secure access. 
It is necessary to accept the certificate to access this Forge pages, for example the Download page.\\
\\
\textbf{Error message: \texttt{Error! Cannot open file "" for cost values!}}\\
By default, Darn is looking for a file \texttt{costs.dat} in the current directory.\\
To be able to call Darn from everywhere, define an environment variable \texttt{DARN\_PATH}.\\
Using tcsh shell, type: \texttt{setenv DARN\_PATH src\_directory}\\
Using bash shell, type: \texttt{export DARN\_PATH=src\_directory}\\
To avoid to type this command in each session, add it to the apropriate script.\\
Using tcsh shell, add the line in the \texttt{\textasciitilde /.tcshrc} file.\\
Using bash shell, add the line in the \texttt{\textasciitilde /.bashrc} file.\\


\end{document}
