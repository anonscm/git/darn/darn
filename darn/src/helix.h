// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef HELIX_H_INCLUDED
#define HELIX_H_INCLUDED

#include "helixCell.h"
#include "values.h"
#include "softConstraint.h"


/**
 * The Helix class stands for an helix cost profile. It stores the computed results in a map which uses Quadruplet.
 */
class Helix : public SoftConstraint {

protected:
  /**
   * the maximum length of the helix
   */
  unsigned int helixLength;
  /**
   * the matrix, allocated once, used to perform the computations
   */
  HelixCell ****matrix;
  /**
   * interaction values
   */
  InteractionValues *interactionValues;
  /**
   * compute the minimum cost of the constraint, between the specified bounds (ask to computeResult)
   * @param lb1 the lower bound of the first variable
   * @param ub1 the upper bound of the first variable
   * @param lb2 the lower bound of the second variable
   * @param ub2 the upper bound of the second variable
   * @param lb3 the lower bound of the third variable
   * @param ub3 the upper bound of the third variable
   * @param lb4 the lower bound of the fourth variable
   * @param ub4 the upper bound of the fourth variable
   * @return the minimum
   */
  virtual int getMinCost (int lb1, int ub1, int lb2, int ub2, int lb3, int ub3, int lb4, int ub4);
  /**
   * compute the cost of the helix recursively, storing each step
   * @param i1 the lower bound of the first stem
   * @param i2 the upper bound of the first stem
   * @param i3 the lower bound of the second stem
   * @param i4 the upper bound of the second stem
   * @return the result
   */
  int computeResult (int i1, int i2, int i3, int i4);
  /**
   * compute the minimum cost of the constraint, between the specified bounds
   * @param lb1 the lower bound of the first variable
   * @param ub1 the upper bound of the first variable
   * @param lb2 the lower bound of the second variable
   * @param ub2 the upper bound of the second variable
   * @param lb3 the lower bound of the third variable
   * @param ub3 the upper bound of the third variable
   * @param lb4 the lower bound of the fourth variable
   * @param ub4 the upper bound of the fourth variable
   * @return the minimum
   */
  int computeResult (int lb1, int ub1, int lb2, int ub2, int lb3, int ub3, int lb4, int ub4);

public:
  /**
   * the constructor
   * @param v1 the first variable (the lower bound of the first stem)
   * @param v2 the second variable (the upper bound of the first stem)
   * @param v3 the third variable (the lower bound of the second stem)
   * @param v4 the fourth variable (the upper bound of the second stem)
   * @param i the index of this constraint
   * @param o whether the helix is optional
   * @param hl the maximum length of the helix
   * @param e2c the convertor from errors to costs
   * @param iv the interaction values
   */
  Helix (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int i, bool o, int hl, ErrorsToCosts *e2c, InteractionValues *iv);
  /**
   * get the minimum of the function cost
   * @return the minimum
   */
  virtual int getMinCost ();
  /**
   * @pre The variable @a v should be discretized
   *
   * get the minimum of the function cost, having that the given variable has been assigned to the given value
   * @param v the given v
   * @param val the given value
   * @return the minimum
   */
  virtual int getMinCost (Variable *v, int val);
  /**
   * @pre The variable @a v should be not discretized
   *
   * compute the minimum cost of the constraint, having that the specified variable is assigned to the lowest bound
   * @param v the specified variable
   * @return the minimum
   */
  virtual int getMinLbCost (Variable *v);
  /**
   * @pre The variable @a v should be not discretized
   *
   * compute the minimum cost of the constraint, having that the specified variable is assigned to the upper bound
   * @param v the specified variable
   * @return the minimum
   */
  virtual int getMinUbCost (Variable *v);
  /**
   * compute the cost of the support
   * @param s the support
   * @return the cost of the support
   */
  virtual int getCost (Support *s);
  /**
   * get the contribution of this constraint to the global cost when all variables are assigned
   * @param values the values of the involved variables
   * @return the cost of this constraint
   */
  virtual int getFinalCost (int *values);
};

#endif
