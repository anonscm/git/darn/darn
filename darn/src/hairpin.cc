// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "hairpin.h"


Hairpin::Hairpin (Variable *v1, Variable *v2, int i, int ml, int mml, bool o, ErrorsToCosts *e2c, InteractionValues *iv) : SoftConstraint(i, e2c, o, 2, v1, v2) {
  simple = false;
  minLength = max2(ml, HAIRPINMINLENGTH);
  maxLength = min2(mml, HAIRPINMAXLENGTH);
  iv = interactionValues;

  // build and set init values to the matrix
  matrix = new int*[maxLength+1];
  for (int i = 0; i <= maxLength; i++) {
    matrix[i] = new int[maxLength+1];
    if (i+static_cast<int>(LOOPMINLENGTH)-2 < maxLength+1) {
      matrix[i][i+static_cast<int>(LOOPMINLENGTH)-2] = interactionValues->getJunctionCost();
      if (i+static_cast<int>(LOOPMINLENGTH)-1 < maxLength+1) {
        matrix[i][i+static_cast<int>(LOOPMINLENGTH)-1] = interactionValues->getJunctionCost();
      }
    }
  }
  hardness = minLength;
  name = "hairpin";
}

Hairpin::~Hairpin () {}

int Hairpin::getMinCost () {
  return getMinCost(unaryConstraints[0]->getLb(), unaryConstraints[0]->getUb(), unaryConstraints[1]->getLb(), unaryConstraints[1]->getUb());
}

int Hairpin::getMinCost (Variable *var, int val) {
  int i, lb1, ub1, lb2, ub2;
  i = getVariableIndex(var);
  lb1 = getVariable(0)->getUnaryConstraint()->getLb();
  ub1 = getVariable(0)->getUnaryConstraint()->getUb();
  lb2 = getVariable(1)->getUnaryConstraint()->getLb();
  ub2 = getVariable(1)->getUnaryConstraint()->getUb();
  switch (i) {
    case 0:
      lb1 = val;
      ub1 = val+1;
      break;
    case 1:
      lb2 = val;
      ub2 = val+1;
      break;
  }
  return getMinCost (lb1, ub1, lb2, ub2);
}

int Hairpin::getMinLbCost (Variable *var) {
  int i, lb1, ub1, lb2, ub2;
  i = getVariableIndex(var);
  lb1 = getVariable(0)->getUnaryConstraint()->getLb();
  ub1 = getVariable(0)->getUnaryConstraint()->getUb();
  lb2 = getVariable(1)->getUnaryConstraint()->getLb();
  ub2 = getVariable(1)->getUnaryConstraint()->getUb();
  switch (i) {
    case 0:
      ub1 = lb1+1;
      break;
    case 1:
      ub2 = lb2+1;
      break;
  }
  return getMinCost (lb1, ub1, lb2, ub2);
}

int Hairpin::getMinUbCost (Variable *var) {
  int i, lb1, ub1, lb2, ub2;
  i = getVariableIndex(var);
  lb1 = getVariable(0)->getUnaryConstraint()->getLb();
  ub1 = getVariable(0)->getUnaryConstraint()->getUb();
  lb2 = getVariable(1)->getUnaryConstraint()->getLb();
  ub2 = getVariable(1)->getUnaryConstraint()->getUb();
  switch (i) {
    case 0:
      lb1 = ub1-1;
      break;
    case 1:
      lb2 = ub2-1;
      break;
  }
  return getMinCost (lb1, ub1, lb2, ub2);
}

int Hairpin::getMinCost (int lb1, int ub1, int lb2, int ub2) {
  int start1 = max2(lb1, lb2-(maxLength+1));
  int stop1 = min2(ub1-1, (ub2-1)-(minLength+1));
  int start2, stop2;
  DomainIterator di1 = getVariable(0)->getUnaryConstraint()->getDomainIterator();
  int mem = getMem(), mem1, mem2;
  int min = getMaxCost()+1;
  int value;
  currentSupport->unset();

      // check all the possible values of the first variable
  for (di1.setTo(start1); *di1 <= stop1; ++di1) {
    mem1 = getMemory(variables[0], *di1);

    start2 = max2(lb2, *di1+(minLength+1));
    stop2 = min2(ub2-1, *di1+(maxLength+1));
    DomainIterator di2 = getVariable(1)->getUnaryConstraint()->getDomainIterator();

        // knowing the value of the first variable, check the values of the second variable
    for (di2.setTo(start2); *di2 <= stop2; ++di2) {
      mem2 = getMemory(variables[1], *di2);

      value = getMinCost(*di1, (*di2)+1) - mem1 - mem2 - mem;
      if (value < min) {
        currentSupport->setSupport(0, *di1);
        currentSupport->setSupport(1, *di2);
        min = value;
      }
    }
  }
  return min;
}

int Hairpin::getMinCost (int i1, int i2) {
  int l = (i2+1) - i1;
  int d;
  int match, value, min;
  int startJunction, stopJunction, junctionValue;
  Nucleotide *n1 = sequence->getNucleotide(i1);
  Nucleotide *n2 = sequence->getNucleotide(i2-1);

  if ((i2-i1 > 2 * static_cast<int>(HELIXMAXLENGTH) + static_cast<int>(LOOPMAXLENGTH)) || (i2-i1 < 2 * static_cast<int>(HELIXMINLENGTH) + static_cast<int>(HELIXMINLENGTH))) {
    return getMaxCost()+1;
  }

  // make the first and the last nucleotide match
  if (interactionValues->getMatchCost(n1, n2) < interactionValues->getInsertionCost()) {
    return getMaxCost()+1;
  }

  // Nussinov algorithm
  for (d = LOOPMINLENGTH; d < l; d++) {
    min = getMaxCost()+1;
    for (int i = 0; i < l-d; i++) {
      int j = i + d;
      match = interactionValues->getMatchCost(sequence->getNucleotide(i1+i), sequence->getNucleotide(i1+j));
      
      // compute the cost of the junction
      startJunction = i + LOOPMINLENGTH + 1;
      stopJunction = j - LOOPMINLENGTH - 1;
      junctionValue = getMaxCost()+1;
      for (int k = startJunction; k+1 <= stopJunction; k++) {
        if (junctionValue > matrix[i][k] + matrix[k+1][j]) {
          junctionValue = matrix[i][k] + matrix[k+1][j];
        }
      }

      value = min4(matrix[i+1][j-1]+match, matrix[i][j-1]+interactionValues->getInsertionCost(), matrix[i+1][j]+interactionValues->getInsertionCost(), junctionValue+interactionValues->getJunctionCost());
      matrix[i][j] = value;
      if (min > errorsToCosts->getCostFor(value)) {
        min = errorsToCosts->getCostFor(value);
      }
    }
    if (min > getMaxCost()-interactionValues->getJunctionCost()) {
      return getMaxCost()+1;
    }
  }
  return errorsToCosts->getCostFor(matrix[0][l-1]-interactionValues->getJunctionCost());
}

int Hairpin::getCost (Support *s) {
  int val1 = s->getSupport(0);
  int val2 = s->getSupport(1);
  return (getMinCost(val1, val2+1) - getMemory(variables[0], val1) - getMemory(variables[1], val2) - getMem());
}

int Hairpin::getFinalCost (int *values) {
  int val1, val2;
  val1 = values[0];
  val2 = values[1];
  return min2(getMaxCost()+1, getMinCost(val1, val2));
}
