// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef UNCOST_H_INCLUDED
#define UNCOST_H_INCLUDED

#include "common.h"
#include "cost.h"


/**
 * The UnaryConstraint class implements all the mecanism for storing the modifications of the unary costs.
 */
class UnaryCost : public Cost {

protected:
  
public:
  /**
   * the constructor, by default the cost profile is not discretized
   * @param i the index of the constrain (related to @a index)
   */
  UnaryCost (int i);
  /**
   * @pre the cost profile is discretized
   * 
   * get the cost of an element of the domain
   * @param i the element
   * @return the cost
   */
  virtual int getMemProfile (int i);
  /**
   * @pre the cost profile is not discretized
   *
   * @return the cost of the lower bound
   */
  virtual int getLbMem ();
  /**
   * @pre the cost profile is not discretized
   *
   * @return the cost of the upper bound
   */
  virtual int getUbMem ();
  /**
   * @pre the cost profile is discretized
   *
   * set the cost of an element of the domain
   * @param i the element
   * @param v the new cost
   */
  virtual void setMemProfile (int i, int v);
  /**
   * @pre the cost profile is not discretized
   *
   * set the cost of the lower bound
   * @param l the new cost
   */
  virtual void setLbMem (int l);
  /**
   * @pre the cost profile is not discretized
   *
   * set the cost of the upper bound
   * @param u the new cost
   */
  virtual void setUbMem (int u);
  /**
   * @pre the cost profile is not discretized
   *
   * discretize the cost profile
   * @param cl the lower bound of the cost profile
   * @param cu the upper bound of the cost profile
   */
  virtual void setDiscretized (int cl, int cu);
  /**
   * discretize the cost profile with a domain size of 1
   * @param val the only element of the domain
   * @param c the value the element takes
   */
  virtual void assign (int val, int c);
};

#endif
