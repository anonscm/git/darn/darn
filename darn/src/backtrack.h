// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef BACKTRACK_H_INCLUDED
#define BACKTRACK_H_INCLUDED

#include "variableContainer.h"
#include "variable.h"


/**
 * Provide interface for backtracking
 */
class Backtrack {

public:
  
  /**
   * constructor
   */
  Backtrack () {}

  /**
   * empty destructor
   */
  virtual ~Backtrack () {}

  /**
   * prepare the class for a new search
   */
  virtual void reset () = 0;
  /**
   * notify the assignment of the variable @a var
   * @param var the assigned variable
   */
	virtual void notifyAssignment (int var) = 0;
  /**
   * notify the assignment of the variable @a var
   */
	virtual void notifyBacktrack () = 0;
  /**
   * whether we can backtrack more
   */
  virtual bool doMoreBacktrack () = 0;
};



/**
 * The most simple backtrack
 */
class SimpleBacktrack : public Backtrack {

public:
  
  /**
   * constructor
   */
  SimpleBacktrack () {}

  /**
   * empty destructor
   */
  virtual ~SimpleBacktrack () {}

  /**
   * prepare the class for a new search
   */
  virtual void reset () {}
  /**
   * notify the assignment of the variable @a var
   * @param var the assigned variable
   */
	virtual void notifyAssignment (int var) {}
  /**
   * notify the assignment of the variable @a var
   */
	virtual void notifyBacktrack () {}
  /**
   * whether we can backtrack more
   */
  virtual bool doMoreBacktrack () {
    return false;
  }
};


/**
 * A simple graph based backtrack
 */
class SimpleGraphBasedBacktrack : public Backtrack {

  /**
   * The neighborhood of the backtracked variable
   */
  class Neighborhood {

  protected:
    /**
     * the number of variables in the network
     */
    int nbVariables;
    /**
     * the indices of the neighborhood
     */
    int *neighborhood;
    /**
     * the number of variable in the neighborhood of the backtracked variable
     */
    int nbNeighborhood;

  public:
    /**
     * constructor
     * @param nv the number of variables in the network
     */
    Neighborhood (int nv) : nbVariables(nv) {
      neighborhood = new int[nbVariables];
      nbNeighborhood = 0;
    }

    /**
     * destructor
     */
    ~Neighborhood () {
      /*
      delete[] neighborhood;
      */
    }
    
    /**
     * free the neighborhood
     */
    void free () {
      for (int i = 0; i < nbVariables; i++) {
        neighborhood[i] = UNDEFINED;
      }
      nbNeighborhood = 0;
    }

    /**
     * check whether @a var is in the currently computed neighborhood
     * @a var a variable
     * @return true, if @a var is in the currently computed neighborhood
     */
    bool isIn (int var) {
      for (int i = 0; i < nbNeighborhood; i++) {
        if (neighborhood[i] == var) {
          return true;
        }
      }
      return false;
    }

    /**
     * add @a var to the currently computed neighborhood
     * @a var a variable
     */
    void add (int var) {
      neighborhood[nbNeighborhood] = var;
      nbNeighborhood++;
    }
  };

protected:
  /**
   * the number of variables in the network
   */
  int nbVariables;
  /**
   * the set of the assigned variables
   */
  VariableContainer *variableContainer;
  /**
   * the neighborhood of the backtracked variable
   */
  Neighborhood *neighborhood;

  /**
   * check whether @a nextVariable is in the neighborhood of @a backtrackedVariable
   * @param backtrackedVariable the variable that has been backtracked
   * @param nextVariable the last assigned variable
   * @return true, if @a nextVariable is in the neighborhood of @a backtrackedVariable
   */
  bool isInNeighborhood (int backtrackedVariable, int nextVariable) {
    stack<int> toBeChecked;
    AbstractConstraint *ac;
    int variableIndex, neighborIndex;
    bool noForwardChecking;

    // reset neighborhood vector
    neighborhood->free();

    // base case of the search
    toBeChecked.push(backtrackedVariable);
    neighborhood->add(backtrackedVariable);

    // search
    while (!toBeChecked.empty()) {
      // pop a variable
      variableIndex = toBeChecked.top();
      toBeChecked.pop();

      // check all the hard constraint involving this variable
      for (int i = 0; i < variables[variableIndex]->getNbHardConstraints(); i++) {
        ac = variables[variableIndex]->getHardConstraint(i);

        // trick for the duplex, which does not enforce forward checking
        noForwardChecking = ((! ac->getShortName().compare("hard duplex")) && (ac->getVariable(0)->getUnaryConstraint()->isAssigned()) && (ac->getVariable(1)->getUnaryConstraint()->isAssigned()));

        // get all the neighbors of the variable
        for (int j = 0; j < ac->getNbVariables(); j++) {
          neighborIndex = ac->getVariable(j)->getIndex();
          if (!neighborhood->isIn(neighborIndex)) {
            if (neighborIndex == nextVariable) {
              return true;
            }
            if ((noForwardChecking) && (! variables[neighborIndex]->getUnaryConstraint()->isAssigned())) {
              toBeChecked.push(neighborIndex);
              neighborhood->add(neighborIndex);
            }
          }
        }
      }

      // check all the soft constraint involving this variable
      for (int i = 0; i < variables[variableIndex]->getNbSoftConstraints(); i++) {
        ac = variables[variableIndex]->getSoftConstraint(i);

        // trick for the duplex, which does not enforce forward checking
        noForwardChecking = ((! ac->getShortName().compare("duplex")) && (ac->getVariable(0)->getUnaryConstraint()->isAssigned()) && (ac->getVariable(1)->getUnaryConstraint()->isAssigned()));

        // get all the neighbors of the variable
        for (int j = 0; j < ac->getNbVariables(); j++) {
          neighborIndex = ac->getVariable(j)->getIndex();
          if (!neighborhood->isIn(neighborIndex)) {
            if (neighborIndex == nextVariable) {
              return true;
            }
            if ((noForwardChecking) || (! variables[neighborIndex]->getUnaryConstraint()->isAssigned())) {
              toBeChecked.push(neighborIndex);
              neighborhood->add(neighborIndex);
            }
          }
        }
      }
    }
    return false;
  }

public:
  
  /**
   * constructor
   */
  SimpleGraphBasedBacktrack (int nv, VariableContainer *vc) : nbVariables(nv) {
    variableContainer = vc;
    neighborhood = new Neighborhood(nv);
  }

  /**
   * empty destructor
   */
  virtual ~SimpleGraphBasedBacktrack () {
    /*
    delete neighborhood;
    */
  }

  /**
   * prepare the class for a new search
   */
  virtual void reset () {
    neighborhood->free();
  }
  /**
   * notify the assignment of the variable @a var
   * @param var the assigned variable
   */
	virtual void notifyAssignment (int var) {}
  /**
   * notify the assignment of the variable @a var
   */
	virtual void notifyBacktrack () {}
  /**
   * whether we can backtrack more
   */
  virtual bool doMoreBacktrack () {
    int nbAssignedVariables = variableContainer->getNbAssignedVariables();
    int nextVariable;
    int lastBacktrackedVariable;

    if (nbAssignedVariables <= 1) {
      return false;
    }
    lastBacktrackedVariable = variableContainer->getAssignedVariable(nbAssignedVariables-1);
    nextVariable = variableContainer->getAssignedVariable(nbAssignedVariables-2);

    return (!isInNeighborhood(lastBacktrackedVariable, nextVariable));

  }
};

#endif

