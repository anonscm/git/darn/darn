// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef  NET_H_INCLUDED
#define  NET_H_INCLUDED

#include "common.h"
#include "values.h"
#include "errorsToCosts.h"
#include "top.h"
#include "variableContainer.h"
#include "variable.h"
#include "hardConstraint.h"
#include "hardPattern.h"
#include "hardPattern2.h"
#include "hardDistance.h"
#include "hardComposition.h"
#include "hardNonCanonicPair.h"
#include "hardHairpin.h"
#include "hardHelix.h"
#include "hardHelixSimple.h"
#include "hardRepetition.h"
#include "hardDuplex.h"
#include "defaultUnarySoftConstraint.h"
#include "pattern.h"
#include "pattern2.h"
#include "distance.h"
#include "composition.h"
#include "nonCanonicPair.h"
#include "hairpin.h"
#include "helix.h"
#include "helixSimple.h"
#include "repetition.h"
#include "duplex.h"
#include "variableOrderer.h"
#include "solutionRecorder.h"
#include "backtrack.h"


/**
 * The NetWork class.
 * It processes the search.
 */
class Network {
  
private:
  /**
   * the total number of variables in the network
   */
  int nbVariables;
  /**
   * the variables
   */
  DefaultUnarySoftConstraint** unaryConstraints;
  /**
   * the variable container (stores the indices of the unassigned variables)
   */
  VariableContainer *variableContainer;
  /**
   * the variable orderer
   */
  VariableOrderer *variableOrderer;
  /**
   * the number of nodes explored by the network
   */
  int exploredNodes;
  /**
   * whether the current scanning goes from left to right
   */
  bool firstWay;
  /**
   * the solutions of the problem
   */
  SolutionRecorder *sRecorder;
  /**
   * the backtrack procedure
   */
  Backtrack *backtrack;
  /**
   * total number of undominated solutions
   */
  int nbUndominatedSolutions;

public:
  /**
   * constructor
   * @param nv the number of variables
   * @param mCost is the top
   */
  Network (int nv, int mCost);
  /**
   * destructor
   */
  ~Network ();
  /**
   * prepare the network for a new search
   */
  void reset ();
  /**
   * reverse the main sequence
   */
  void reverseSequence();
  /**
   * create a new variable
   * @param name the name of the variable
   * @param s the sequence it is on
   * @return the new variable
   */
  Variable *makeVariable (string name, Sequence *s);
  /**
   * add a soft constraint
   * @param c the new soft constraint
   */
  void addSoftConstraint (SoftConstraint *c);
  /**
   * create and add a UnaryConstraint
   * @param v the Variable constrained by the UnaryConstraint
   * @return the new UnaryConstraint
   */
  DefaultUnarySoftConstraint* makeUnaryConstraint (Variable *v);
  /**
   * create and add a Pattern constraint
   * @param v the Variable constrained by the Pattern
   * @param p the pattern
   * @param e2c the convertor from errors to costs
   * @param o whether the pattern is optional
   * @return the new Pattern
   */
  Pattern* makePattern (Variable *v, string p, ErrorsToCosts *e2c, bool o = false);
  /**
   * create and add a Pattern 2 constraint involving two variables
   * @param v1 the first Variable constrained by the Pattern2
   * @param v2 the second Variable constrained by the Pattern2
   * @param p the pattern
   * @param e2c the convertor from errors to costs
   * @param o whether the pattern is optional
   * @return the new Pattern
   */
  Pattern2* makePattern2 (Variable *v1, Variable *v2, string p, ErrorsToCosts *e2c, bool o = false);
  /**
   * create and add a Distance constraint
   * @param v1 the first Variable constrained by the Distance
   * @param v2 the second Variable constrained by the Distance
   * @param d1 the first distance parameter for the Distance
   * @param d2 the second distance parameter for the Distance
   * @param d3 the third distance parameter for the Distance
   * @param d4 the fourth distance parameter for the Distance
   * @param c1 the minimum cost paid by the constraint
   * @param e2c the convertor from errors to costs
   * @param o whether the distance is optional
   * @return the new Distance
   */
  Distance* makeDistance (Variable *v1, Variable *v2, int d1, int d2, int d3, int d4, int c1, ErrorsToCosts *e2c, bool o = false);
  /**
   * create and add a Composition constraint
   * @param v1 the start position of the considered interval
   * @param v2 the end position of the considered interval
   * @param n1 the first nucleotide considered
   * @param n2 the first nucleotide considered
   * @param t1 the first threshold
   * @param t2 the second threshold
   * @param r the relation with respect to the thresholds
   * @param mc the minimum cost given by the constraint
   * @param e2c the convertor from errors to costs
   * @param o whether the pattern is optional
   * @return the new Composition
   */
  Composition* makeComposition (Variable *v1, Variable *v2, Nucleotide *n1, Nucleotide *n2, int t1, int t2, Relation r, int mc, ErrorsToCosts *e2c, bool o = false);
  /**
   * create and add a non canoning Pairing constraint
   * @param v1 the first Variable constrained by the Distance
   * @param v2 the second Variable constrained by the Distance
   * @param int1 the interaction concerning the first nucleotide
   * @param int2 the interaction concerning the second nucleotide
   * @param ori the orientation of the nucleotides
   * @param f the isostericity family
   * @param e2c the convertor from errors to costs
   * @param o whether the constraint is optional
   * @return the new NonCanonicPair
   */
  NonCanonicPair* makeNonCanonicPair (Variable *v1, Variable *v2, Interaction int1, Interaction int2, Orientation ori, int f, ErrorsToCosts *e2c, bool o = false);
  /**
   * create and add a Hairpin constraint
   * @param v1 the first Variable constrained by the Hairpin
   * @param v2 the second Variable constrained by the Hairpin
   * @param ml the minimum length of the hairpin
   * @param mml the maximum length of the hairpin
   * @param e2c the convertor from errors to costs
   * @param w whether wobble interactions are allowed
   * @param o whether the hairpin is optional
   * @return the new Hairpin 
   */
  Hairpin* makeHairpin (Variable *v1, Variable *v2, int ml, int mml, ErrorsToCosts *e2c, bool w, bool o = false);
  /**
   * create and add an Helix constraint
   * @param v1 the first Variable constrained by the Helix
   * @param v2 the second Variable constrained by the Helix
   * @param v3 the third Variable constrained by the Helix
   * @param v4 the fourth Variable constrained by the Helix
   * @param hl the maximum length of the helix
   * @param e2c the convertor from errors to costs
   * @param w whether wobble interactions are allowed
   * @param o whether the helix is optional
   * @return the new Helix 
   */
  Helix* makeHelix (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int hl, ErrorsToCosts *e2c, bool w, bool o = false);
  /**
   * create and add a SimpleHelix constraint
   * @param v1 the first Variable constrained by the Helix
   * @param v2 the second Variable constrained by the Helix
   * @param v3 the third Variable constrained by the Helix
   * @param v4 the fourth Variable constrained by the Helix
   * @param msl the minimum length of a stem
   * @param mmsl the maximum length of a stem
   * @param mll the minimum length of the loop
   * @param mmll the minimum length of the loop
   * @param e2c the convertor from errors to costs
   * @param w whether wobble interactions are allowed
   * @param insert whether insertions are allowed
   * @param o whether the helix is optional
   * @return the new SimpleHelix 
   */
  HelixSimple* makeHelixSimple (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int msl, int mmsl, int mll, int mmll, ErrorsToCosts *e2c, bool w, bool insert = false, bool o = false);
  /**
   * create and add a Repetition hard constraint
   * @param v1 the first Variable constrained by the Helix
   * @param v2 the second Variable constrained by the Helix
   * @param v3 the third Variable constrained by the Helix
   * @param v4 the fourth Variable constrained by the Helix
   * @param ms the minimum size of the sequences
   * @param mms the maximum size of the sequences
   * @param md the minimum distance between the sequences
   * @param mmd the maximum distance between the sequences
   * @param e2c the convertor from errors to costs
   * @param insert whether insertions are allowed
   * @param o whether the repetition is optional
   * @return the new HardRepetition 
   */
  Repetition* makeRepetition (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int ms, int mms, int md, int mmd, ErrorsToCosts *e2c, bool insert = false, bool o = false);
  /**
   * create and add a Duplex constraint
   * @param v1 the first Variable constrained by the Helix
   * @param v2 the second Variable constrained by the Helix
   * @param v3 the third Variable constrained by the Helix
   * @param v4 the fourth Variable constrained by the Helix
   * @param e2c the convertor from errors to costs
   * @param s the sequence where the duplex is
   * @param w whether wobble interactions are allowed
   * @param o whether the duplex is optional
   * @return the new Duplex 
   */
  Duplex* makeDuplex (Variable *v1, Variable *v2, Variable *v3, Variable *v4, ErrorsToCosts *e2c, Sequence *s, bool w, bool o = false);
  /**
   * create and add a Pattern hard constraint involving one variable
   * @param v the Variable constrained by the Pattern
   * @param p the pattern
   * @param mc the cost paid by the constraint
   * @return the new HardPattern
   */
  HardPattern* makeHardPattern (Variable *v, string p, int mc);
  /**
   * create and add a Pattern hard constraint involving two variables
   * @param v1 the Variable that represents the beginning of the word
   * @param v2 the Variable that represents the end of the word
   * @param p the pattern
   * @param mc the cost paid by the constraint
   * @return the new HardPattern
   */
  HardPattern2* makeHardPattern2 (Variable *v1, Variable *v2, string p, int mc);
  /**
   * create and add a Distance hard constraint
   * @param v1 the first Variable constrained by the Distance
   * @param v2 the second Variable constrained by the Distance
   * @param d1 the first distance parameter for the Distance
   * @param d2 the second distance parameter for the Distance
   * @return the new HardDistance
   */
  HardDistance* makeHardDistance (Variable *v1, Variable *v2, int d1, int d2);
  /**
   * create and add a Composition hard constraint
   * @param v1 the first Variable constrained by the Composition
   * @param v2 the second Variable constrained by the Composition
   * @param n1 the first nucleotide to be observed
   * @param n2 the second nucleotide to be observed
   * @param t the threshold (in percentage)
   * @param r the relation between the observed composition and the given one ("not less than", or "not greater than")
   * @return the new HardComposition
   */
  HardComposition* makeHardComposition (Variable *v1, Variable *v2, Nucleotide *n1, Nucleotide *n2, int t, Relation r);
  /**
   * create and add a non canoning Pairing hard constraint
   * @param v1 the first Variable constrained by the Distance
   * @param v2 the second Variable constrained by the Distance
   * @param i1 the interaction concerning the first nucleotide
   * @param i2 the interaction concerning the second nucleotide
   * @param o the orientation of the nucleotides
   * @param f the isostericity family
   * @param mc the maximum cost allowed
   * @return the new HardNonCanonicPair
   */
  HardNonCanonicPair* makeHardNonCanonicPair (Variable *v1, Variable *v2, Interaction i1, Interaction i2, Orientation o, int f, int mc);
  /**
   * create and add an Helix hard constraint
   * @param v1 the first Variable constrained by the Helix
   * @param v2 the second Variable constrained by the Helix
   * @param v3 the third Variable constrained by the Helix
   * @param v4 the fourth Variable constrained by the Helix
   * @param hl the maximum length of the helix
   * @param mc the minimum cost paid by the constraint
   * @param w whether wobble interactions are allowed
   * @return the new HardHelix 
   */
  HardHelix* makeHardHelix (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int hl, int mc, bool w);
  /**
   * create and add a SimpleHelix hard constraint
   * @param v1 the first Variable constrained by the Helix
   * @param v2 the second Variable constrained by the Helix
   * @param v3 the third Variable constrained by the Helix
   * @param v4 the fourth Variable constrained by the Helix
   * @param msl the minimum length of a stem
   * @param mmsl the maximum length of a stem
   * @param mll the minimum length of the loop
   * @param mmll the minimum length of the loop
   * @param mc the cost to be paid if no helix is found
   * @param w whether wobble interactions are allowed
   * @param insert whether insertion costs are allowed
   * @return the new HardSimpleHelix 
   */
  HardHelixSimple* makeHardHelixSimple (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int msl, int mmsl, int mll, int mmll, int mc, bool w, bool insert = false);
  /**
   * create and add a Repetition hard constraint
   * @param v1 the first Variable constrained by the Helix
   * @param v2 the second Variable constrained by the Helix
   * @param v3 the third Variable constrained by the Helix
   * @param v4 the fourth Variable constrained by the Helix
   * @param ms the minimum size of the sequences
   * @param mms the maximum size of the sequences
   * @param md the minimum distance between the sequences
   * @param mmd the maximum distance between the sequences
   * @param mc the cost to be paid if no helix is found
   * @param insert whether insertions are allowed
   * @return the new HardRepetition 
   */
  HardRepetition* makeHardRepetition (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int ms, int mms, int md, int mmd, int mc, bool insert = false);
  /**
   * create and add a Hairpin hard constraint
   * @param v1 the first Variable constrained by the Hairpin
   * @param v2 the second Variable constrained by the Hairpin
   * @param ml the minimum length of the hairpin
   * @param mml the maximum length of the hairpin
   * @param mc the minimum cost paid by the constraint
   * @param w whether wobble interactions are allowed
   * @return the new Hairpin 
   */
  HardHairpin* makeHardHairpin (Variable *v1, Variable *v2, int ml, int mml, int mc, bool w);
  /**
   * create and add a Duplex hard constraint
   * @param v1 the first Variable constrained by the Helix
   * @param v2 the second Variable constrained by the Helix
   * @param v3 the third Variable constrained by the Helix
   * @param v4 the fourth Variable constrained by the Helix
   * @param mc the cost to be paid if no helix is found
   * @param s the sequence where the duplex is
   * @param w whether wobble interactions are allowed
   * @return the new HardDuplex 
   */
  HardDuplex* makeHardDuplex (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int mc, Sequence *s, bool w);
  /**
   * add a hard constraint
   * @param c the new hard constraint
   */
  void addHardConstraint (HardConstraint *c);
  /**
   * yield to an equivalent WCSP which is AAC*
   */
  void setAAC ();
  /**
   * start a preleminary AAC* and then explore(0)
   */
  void explore ();
  /**
   * recursive function that explores the tree and launch at each node AAC*
   * @param depth the current depth of the tree
   */
  void explore (int depth);
  /**
   * print the solutions of the current search
   */
  void printSolutions ();
  /**
   * get the total number of undominated solutions
   * @return the number of solutions
   */
  int getNbUndominatedSolutions ();

  /**
   * stack all the elements in the revision stacks
   */
  void setAllRevision ();
  /**
   * unstack all the elements in the revision stacks
   */
  void clearAllRevision ();

  /**
   * make the UnaryConstraint, NaryConstraint and zero arity constraint save the current values
   */
  void save ();
  /**
   * make the UnaryConstraint, NaryConstraint and zero arity constraint restore the current values
   */
  void restore ();
};


#endif
