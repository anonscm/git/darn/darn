// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef VALUES_H_INCLUDED
#define VALUES_H_INCLUDED

#include "common.h"
#include "sequence.h"

/**
 * possible sides of a nucleotide that can interact with another nucleotide
 */
typedef enum {WATSON_CRICK, HOOGSTEEN, SUGAR_EDGE} Interaction;
/**
 * possible orientations of two nucleotides with respect to each other
 */
typedef enum {CIS, TRANS} Orientation;



/**
 * Store the set of values concerning matching problems
 */
class MatchingValues {
  /**
   * the substitution cost matrix (A, C, G, T, N, X)
   */
  int substitution[NBEXTENDEDNUCLEOTIDES][NBEXTENDEDNUCLEOTIDES];
  /**
   * the deletion cost (unused yet)
   */
  int deletion;
  /**
   * the insertion cost
   */
  int insertion;
  /*
   * the maximum among substitution, deletion and insertion
   */
  int maximum;

  /**
   * allow class ValuesReader to fill the values of this class
   */
  friend class ValuesReader;

public:

  /**
   * accessor to @a substitution
   * @param n1 the first nucleotide
   * @param n2 the second nucleotide
   * @return the substitution cost
   */
  int getSubstitutionCost(Nucleotide *n1, Nucleotide *n2) {
    return substitution[(int) n1->getCode()][(int) n2->getCode()];
  }
  /**
   * accessor to @a deletion
   * @return the deletion cost
   */
  int getDeletionCost () {
    return deletion;
  }
  /**
   * accessor to @a insertion
   * @return the insertion cost
   */
  int getInsertionCost () {
    return insertion;
  }
  /**
   * accessor to @a maximum
   * @return the maximum cost
   */
  int getMaxCost () {
    return maximum;
  }
};


/**
 * Store the set of values concerning interaction problems
 */
class InteractionValues {
  /**
   * the match cost matrix (A, C, G, T, N, X)
   */
  int matches[NBNUCLEOTIDES][NBNUCLEOTIDES];
  /**
   * the deletion cost (unused yet)
   */
  int deletion;
  /**
   * the insertion cost
   */
  int insertion;
  /**
   * the junction cost
   */
  int junction;
  /*
   * the maximum among substitution, deletion and insertion
   */
  int maximum;
  /**
   * the cost paid to have a shorter helix than the one specified
   */
  int helixLength;

  /**
   * allow class ValuesReader to fill the values of this class
   */
  friend class ValuesReader;

public:
  /**
   * accessor to @a matches
   * @param n1 the first nucleotide
   * @param n2 the second nucleotide
   * @return the match cost
   */
  int getMatchCost(Nucleotide *n1, Nucleotide *n2) {
    return matches[(int) n1->getCode()][(int) n2->getCode()];
  }
  /**
   * accessor to @a deletion
   * @return the deletion cost
   */
  int getDeletionCost () {
    return deletion;
  }
  /**
   * accessor to @a insertion
   * @return the insertion cost
   */
  int getInsertionCost () {
    return insertion;
  }
  /**
   * accessor to @a junction
   * @return the junction cost
   */
  int getJunctionCost () {
    return junction;
  }
  /**
   * accessor to @a maximum
   * @return the maximum cost
   */
  int getMaxCost () {
    return maximum;
  }
  /**
   * accessor to @a helixLength
   * @return the cost penalty for a short helix
   */
  int getHelixLengthCost () {
    return helixLength;
  }

};


/**
 * Store the set of values concerning isostericity problems
 */
class IsostericityValues {
  /**
   * the non-canonic match cost matrix
   */
  bool matches[NBINTERACTIONS][NBINTERACTIONS][NBORIENTATIONS][NBNUCLEOTIDES][NBNUCLEOTIDES][NBFAMILIES+1];
  /**
   * the maximum non-canonic match cost
   */
  int notInFamily;

  /**
   * allow class ValuesReader to fill the values of this class
   */
  friend class ValuesReader;

public:

  /**
   * accessor to @a matches
   * @return the match cost
   */
  bool getMatchCost (Interaction i1, Interaction i2, Orientation o, Nucleotide *n1, Nucleotide *n2, int family) {
    return matches[i1][i2][o][(int) n1->getCode()][(int) n2->getCode()][family];
  }
  /**
   * accessor to @a notInFamily
   * @return the cost given if the matching is not in the given family
   */
  int getNotInFamilyValue () {
    return notInFamily;
  }
};


/**
 * This class reads and stores the costs values.
 */
class ValuesReader {

private:
  /**
   * the set of values concerning matching problems
   */
  static MatchingValues *matchingValues;
  /**
   * the set of values concerning interaction problems
   *  (where only canonic interactions are allowed)
   */
  static InteractionValues *interactionValues;
  /**
   * the set of values concerning interaction problems
   *  (where canonic interactions and wobble are allowed)
   */
  static InteractionValues *interactionValuesWithWobble;
  /**
   * the set of values concerning isostericity problems
   */
  static IsostericityValues *isostericityValues;

  /**
   * skip the heading white of a character array
   * @param text the text
   * @param maxLength the size of the text
   * @return a pointer to the first word
   */
  static char* skipWhite (char *text, int maxLength) {
    for (int i = 0; i < maxLength; i++) {
      if ((text[i] != ' ') && (text[i] != '\t')) {
        return text+i;
      }
    }
    return text+maxLength;
  }
  /**
   * skip the first word of a character array
   * @param text the text
   * @param maxLength the size of the text
   * @return a pointer to the first white character
   */
  static char* skipWord (char *text, int maxLength) {
    for (int i = 0; i < maxLength; i++) {
      if ((text[i] == ' ') || (text[i] == '\t')) {
        return text+i;
      }
    }
    return text+maxLength;
  }
  /**
   * find the position of a character in an array (NOT_FOUND if not found)
   * @param c the character to be found
   * @param text the text
   * @param maxLength the size of the text
   * @return the position of the character
   */
  static int findChar (char c, char *text, int maxLength) {
    for (int i = 0; i < maxLength; i++) {
      if ((text[i] == '\r') || (text[i] == '\n') || (text[i] == '\0')) {
        return NOT_FOUND;
      }
      if (text[i] == c) {
        return i;
      }
    }
    return NOT_FOUND;
  }
  /**
   * find the position of a character in an word (NOT_FOUND if not found)
   * @param c the character to be found
   * @param text the text
   * @param maxLength the size of the text
   * @return the position of the character
   */
  static int findCharInWord (char c, char *text, int maxLength) {
    for (int i = 0; i < maxLength; i++) {
      if ((text[i] == ' ') || (text[i] == '\r') || (text[i] == '\n') || (text[i] == '\0')) {
        return NOT_FOUND;
      }
      if (text[i] == c) {
        return i;
      }
    }
    return NOT_FOUND;
  }
  /**
   * copy some caracters
   * @param from the source of the copy
   * @param to the destination of the copy
   * @param until the number of characters to be copied
   */
  static void strcpy (char *from, char *to, int until) {
    int i;
    for (i = 0; i < until; i++) {
      to[i] = from[i];
    }
    to[i] = 0;
  }
  /**
   * copy the first word
   * @param from the source of the copy (where the word is)
   * @param to the destination of the copy
   * @param until the size of the first array
   */
  static void getFirstWord (char *from, char *to, int until) {
    for (int i = 0; i < until; i++) {
      to[i] = from[i];
      if ((to[i] == ' ') || (to[i] == '\t') || (to[i] == '\n') || (to[i] == 0)) {
        to[i] = 0;
        return;
      }
    }
  }
  /**
   * fill the matrix with the input stream
   * @param fileStream the input stream
   * @param matrix the matrix to be filled
   */
  static void fillMatrix (ifstream &fileStream, int matrix[NBNUCLEOTIDES][NBNUCLEOTIDES]) {
    char buffer[256], value[256], *nextWord;
    fileStream.getline (buffer, 255);
    for (unsigned int i = 0; i < NBNUCLEOTIDES; i++) {
      fileStream.getline (buffer, 255);
      nextWord = skipWhite(buffer, 255);
      for (unsigned int j = 0; j < NBNUCLEOTIDES; j++) {
        nextWord = skipWord(nextWord, 255);
        nextWord = skipWhite(nextWord, 255);
        getFirstWord(nextWord, value, 255);
        matrix[i][j] = atoi(value);
      }
    }
  }
  /**
   * fill the isostericity matrix with the input stream
   * @param fileStream the input stream
   * @param matrix the matrix to be filled
   */
  static void fillNcMatrix (ifstream &fileStream, bool matrix[NBNUCLEOTIDES][NBNUCLEOTIDES][NBFAMILIES+1]) {
    char buffer[256], strValue[256], word[256], *nextWord, *nextValue;
    int index, value;
    fileStream.getline (buffer, 255);
    for (unsigned int i = 0; i < NBNUCLEOTIDES; i++) {
      fileStream.getline (buffer, 255);
      nextWord = skipWhite(buffer, 255);
      for (unsigned int j = 0; j < NBNUCLEOTIDES; j++) {
        for (unsigned int k = 0; k <= NBFAMILIES; k++) {
          matrix[i][j][k] = false;
        }
        nextWord = skipWord(nextWord, 255);
        nextWord = skipWhite(nextWord, 255);
        getFirstWord(nextWord, word, 255);
        index = findCharInWord(',', word, 255);
        nextValue = word;
        while (index != NOT_FOUND) {
          strcpy(nextValue, strValue, index);
          value = atoi(strValue);
          if (value != 0) {
            matrix[i][j][value] = true;
            matrix[i][j][0] = true;
          }
          nextValue += index+1;
          index = findCharInWord(',', nextValue, 255);
        }
        getFirstWord(nextValue, strValue, 255);
        value = atoi(strValue);
        if (value != 0) {
          matrix[i][j][value] = true;
          matrix[i][j][0] = true;
        }
      }
    }
  }
  /**
   * copy the transpose of the isostericity matrix (since isostericity matrices are not symetric)
   * @param from the matrix to be copied
   * @param to the matrix to be filled
   */
  static void copyTransposeMatrix (bool from[NBNUCLEOTIDES][NBNUCLEOTIDES][NBFAMILIES+1], bool to[NBNUCLEOTIDES][NBNUCLEOTIDES][NBFAMILIES+1]) {
    for (unsigned int i = 0; i < NBNUCLEOTIDES; i++) {
      for (unsigned int j = 0; j < NBNUCLEOTIDES; j++) {
        for (unsigned int k = 0; k <= NBFAMILIES; k++) {
          to[i][j][k] = from[j][i][k];
        }
      }
    }
  }
  /**
   * find the file containing the costs
   * @param fileName the name of the file
   * @param path return the path if the path exists, NULL otherwise
   */
  static void findFile (const char fileName[], char path[]) {
    char fullFileName[256];
    char *envPath;
    int pos = 0;
    ifstream fileStream;

    // check in current directory
    fileStream.open(fileName);
    if (fileStream.is_open()) {
      fileStream.close();
      ::strcpy(path, fileName);
      return;
    }
    fileStream.close();

    // check in DARN_PATH directory
    if ((envPath = getenv("DARN_PATH")) != NULL) {
      sprintf(fullFileName, "%s/%s", envPath, fileName);
      fileStream.open(fullFileName);
      if (fileStream.is_open()) {
        fileStream.close();
        ::strcpy(path, fullFileName);
        return;
      }
    }
    fileStream.close();

    // check in PATH directory
    if ((envPath = getenv("PATH")) != NULL) {
      while ((pos = findCharInWord (':', envPath, 256)) != NOT_FOUND) {
        strcpy(envPath, fullFileName, pos);
        printf ("\tfullfilename: %s\n", fullFileName);
        strcat(fullFileName, "/");
        strcat(fullFileName, fileName);
        fileStream.open(fullFileName);
        if (fileStream.is_open()) {
          fileStream.close();
          ::strcpy(path, fullFileName);
          return;
        }
        envPath += pos + 1;
      }
    }
    fileStream.close();

    // nothing found
    path = NULL;
    return;
  }

public:
  /**
   * read the costs
   * @param fileName the file where the costs should be
   */
  static void read (const char fileName[]) {
    char path[256], buffer[256], name[256], value[256], *nextWord;
    int position;

    findFile(fileName, path);
    if (path == NULL) {
      cerr << "Error: Cannot open file " << string(fileName) << " for cost values ! Please update the environment variable DARN_PATH to this file." <<endl;
      exit(1);
    }

    ifstream fileStream(path);
    if (!fileStream.is_open()) {
      cerr << "Error! Cannot open file " << string(fileName) << " for cost values !" <<endl;
      exit(1);
    }

    matchingValues = new MatchingValues();
    interactionValues = new InteractionValues();
    interactionValuesWithWobble = new InteractionValues();
    isostericityValues = new IsostericityValues(); 
    
    while (!fileStream.eof()) {
      fileStream.getline (buffer, 255);
      if (buffer[0] != '#') {
        position = findChar(':', buffer, 255);
        if (position != -1) {
          strcpy(buffer, name, position);
          if (!strcmp(name, "match costs")) {
            fillMatrix(fileStream, interactionValues->matches);
          }
          else if (!strcmp(name, "match costs with wobble")) {
            fillMatrix(fileStream, interactionValuesWithWobble->matches);
          }
          else if (!strcmp(name, "Watson-Crick -> Watson-Crick (cis)")) {
            fillNcMatrix(fileStream, isostericityValues->matches[WATSON_CRICK][WATSON_CRICK][CIS]);
          }
          else if (!strcmp(name, "Watson-Crick -> Watson-Crick (trans)")) {
            fillNcMatrix(fileStream, isostericityValues->matches[WATSON_CRICK][WATSON_CRICK][TRANS]);
          }
          else if (!strcmp(name, "Watson-Crick -> Hoogsten (cis)")) {
            fillNcMatrix(fileStream, isostericityValues->matches[WATSON_CRICK][HOOGSTEEN][CIS]);
            copyTransposeMatrix(isostericityValues->matches[WATSON_CRICK][HOOGSTEEN][CIS], isostericityValues->matches[HOOGSTEEN][WATSON_CRICK][CIS]);
          }
          else if (!strcmp(name, "Watson-Crick -> Hoogsten (trans)")) {
            fillNcMatrix(fileStream, isostericityValues->matches[WATSON_CRICK][HOOGSTEEN][TRANS]);
            copyTransposeMatrix(isostericityValues->matches[WATSON_CRICK][HOOGSTEEN][TRANS], isostericityValues->matches[HOOGSTEEN][WATSON_CRICK][TRANS]);
          }
          else if (!strcmp(name, "Watson-Crick -> Sugar-Edge (cis)")) {
            fillNcMatrix(fileStream, isostericityValues->matches[WATSON_CRICK][SUGAR_EDGE][CIS]);
            copyTransposeMatrix(isostericityValues->matches[WATSON_CRICK][SUGAR_EDGE][CIS], isostericityValues->matches[SUGAR_EDGE][WATSON_CRICK][CIS]);
          }
          else if (!strcmp(name, "Watson-Crick -> Sugar-Edge (trans)")) {
            fillNcMatrix(fileStream, isostericityValues->matches[WATSON_CRICK][SUGAR_EDGE][TRANS]);
            copyTransposeMatrix(isostericityValues->matches[WATSON_CRICK][SUGAR_EDGE][TRANS], isostericityValues->matches[SUGAR_EDGE][WATSON_CRICK][TRANS]);
          }
          else if (!strcmp(name, "Hoogsten -> Hoogsten (cis)")) {
            fillNcMatrix(fileStream, isostericityValues->matches[HOOGSTEEN][HOOGSTEEN][CIS]);
          }
          else if (!strcmp(name, "Hoogsten -> Hoogsten (trans)")) {
            fillNcMatrix(fileStream, isostericityValues->matches[HOOGSTEEN][HOOGSTEEN][TRANS]);
          }
          else if (!strcmp(name, "Hoogsten -> Sugar-Edge (cis)")) {
            fillNcMatrix(fileStream, isostericityValues->matches[HOOGSTEEN][SUGAR_EDGE][CIS]);
            copyTransposeMatrix(isostericityValues->matches[HOOGSTEEN][SUGAR_EDGE][CIS], isostericityValues->matches[SUGAR_EDGE][HOOGSTEEN][CIS]);
          }
          else if (!strcmp(name, "Hoogsten -> Sugar-Edge (trans)")) {
            fillNcMatrix(fileStream, isostericityValues->matches[HOOGSTEEN][SUGAR_EDGE][TRANS]);
            copyTransposeMatrix(isostericityValues->matches[HOOGSTEEN][SUGAR_EDGE][TRANS], isostericityValues->matches[SUGAR_EDGE][HOOGSTEEN][TRANS]);
          }
          else if (!strcmp(name, "Sugar-Edge -> Sugar-Edge (cis)")) {
            fillNcMatrix(fileStream, isostericityValues->matches[SUGAR_EDGE][SUGAR_EDGE][CIS]);
          }
          else if (!strcmp(name, "Sugar-Edge -> Sugar-Edge (trans)")) {
            fillNcMatrix(fileStream, isostericityValues->matches[SUGAR_EDGE][SUGAR_EDGE][TRANS]);
          }
          else if (!strcmp(name, "substitution costs")) {
            fileStream.getline (buffer, 255);
            for (unsigned int i = 0; i < NBEXTENDEDNUCLEOTIDES; i++) {
              fileStream.getline (buffer, 255);
              nextWord = skipWhite(buffer, 255);
              for (unsigned int j = 0; j < NBEXTENDEDNUCLEOTIDES; j++) {
                nextWord = skipWord(nextWord, 255);
                nextWord = skipWhite(nextWord, 255);
                getFirstWord(nextWord, value, 255);
                matchingValues->substitution[i][j] = atoi(value);
              }
            }
          }
          else if (!strcmp(name, "deletion cost")) {
            nextWord = skipWhite(buffer + position + 1, 255-position);
            getFirstWord(nextWord, value, 255-position-1);
            interactionValuesWithWobble->deletion = interactionValues->deletion = matchingValues->deletion = atoi(value);
          }
          else if (!strcmp(name, "insertion cost")) {
            nextWord = skipWhite(buffer + position + 1, 255-position);
            getFirstWord(nextWord, value, 255-position-1);
            interactionValuesWithWobble->insertion = interactionValues->insertion = matchingValues->insertion = atoi(value);
          }
          else if (!strcmp(name, "junction cost")) {
            nextWord = skipWhite(buffer + position + 1, 255-position);
            getFirstWord(nextWord, value, 255-position-1);
            interactionValuesWithWobble->junction = interactionValues->junction = atoi(value);
          }
          else if (!strcmp(name, "helix length cost")) {
            nextWord = skipWhite(buffer + position + 1, 255-position);
            getFirstWord(nextWord, value, 255-position-1);
            interactionValuesWithWobble->helixLength = interactionValues->helixLength = atoi(value);
          }
          else if (!strcmp(name, "not in family cost")) {
            nextWord = skipWhite(buffer + position + 1, 255-position);
            getFirstWord(nextWord, value, 255-position-1);
            isostericityValues->notInFamily = atoi(value);
          }
        }
      }
    }
    interactionValuesWithWobble->maximum = interactionValues->maximum = matchingValues->maximum = max2(matchingValues->deletion, matchingValues->insertion);
    for (unsigned int i = 0; i < NBEXTENDEDNUCLEOTIDES; i++) {
      for (unsigned int j = 0; j < NBEXTENDEDNUCLEOTIDES; j++) {
        interactionValuesWithWobble->maximum = interactionValues->maximum = matchingValues->maximum = max2(matchingValues->maximum, matchingValues->substitution[i][j]);
      }
    }
    trace(10, "\tFile \"" + string(fileName) + "\" red.");
  }

  /**
   * accessor to @a matchingValues
   * @return the set of values concerning matching problems
   */
  static MatchingValues *getMatchingValues () {
    return matchingValues;
  }
  /**
   * accessor to @a interactionValues
   * @return the set of values concerning interaction problems
   *  (where only canonic interactions are allowed)
   */
  static InteractionValues *getInteractionValues () {
    return interactionValues;
  }
  /**
   * accessor to @a interactionValuesWithWobble
   * @return the set of values concerning interaction problems
   *  (where canonic interactions and wobble are allowed)
   */
  static InteractionValues *getInteractionValuesWithWobble () {
    return interactionValuesWithWobble;
  }
  /**
   * accessor to @a isostericityValues
   * @return the set of values concerning isostericity problems
   */
  static IsostericityValues *getIsostericityValues () {
    return isostericityValues;
  }

};

#endif
