// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef HCONS_H_INCLUDED
#define HCONS_H_INCLUDED

#include "abstractHardConstraint.h"
#include "defaultUnarySoftConstraint.h"


/**
 * Provide advanced to to model hard constraints
 */
class HardConstraint : public AbstractHardConstraint {

protected:

/**
 * The unary constraints of the variables involved in ths constraint.
 */
  DefaultUnarySoftConstraint **unaryConstraints;


public:

  /**
   * The constructor
   * @param i the index
   * @param mc the threshold
   * @param nv the number of variables
   * @param v the first variable
   */
  HardConstraint (int i, int mc, int nv, Variable *v ...);
  /**
   * destructor
   */
  ~HardConstraint ();

  /**
   * prepare the constraint for a new search
   */
  virtual void reset ();

  /**
   * Accessor to an element of @a unaryConstraints
   * @param i the index of the unary constraint
   * @return a unary constraint
   */
  DefaultUnarySoftConstraint *getUnaryConstraint (int i);
};

#endif
