// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef PARAM_H_INCLUDED
#define PARAM_H_INCLUDED

#include <limits.h>

#include "common.h"
#include "sequence.h"
#include "fileReader.h"


/**
 * Write an help message
 */
void writeHelpMessage () {
  cout << "darn -d file1 -s file2 [-o outputfile] [-n F|R|B] [-x file3 [-v]]"<< endl;
  cout << "      [-f S|G|P|F] [-t] [-c] [-u] [-e]" << endl;
  cout << "      [[-w] [-y drawdirectory] [-z webdrawdirectory]]" << endl;
  cout << "      [-V[V[V]]] [-a time]" << endl;
  cout << endl;
  cout << "Main options" << endl;
  cout << "\t-d file1: the file that contains the descriptor" << endl;
  cout << "\t-s file2: the file that contains the main stem" << endl;
  cout << "\t-o outputfile: a file where to write the solutions (default: standard output)" << endl;
  cout << "\t-n F(default)|R|B: search on the forward(F)/reverse(R)/both(B) strand(s) on the main sequence" << endl;
  cout << "\t-x file3: the file that contains the duplex stem sequence" << endl;
  cout << "\t-v: search from 3' to 5' on the duplex sequence" << endl;
  cout << endl;
  cout << "Output options" << endl;
  cout << "\t-f S(default)|G|P|F: print solutions with specific(S), GFF(G), easy parsable(P), FASTA(F) format" << endl;
  cout << "\t-t: print the secondary structure (only in specific output format)" << endl;
  cout << "\t-c: print solutions with lower cost first (solution score not given in FASTA output format)" << endl;
  cout << "\t-u: print all dominated solutions" << endl;
  cout << "\t-e: print the costs given by the constraints (only in specific output format)" << endl;
  cout << endl;
  cout << "Web output options" << endl;
  cout << "\t-w: output solutions using HTML markups" << endl;
  cout << "\t-y drawdirectory: draw the solution in the given directory" << endl;
  cout << "\t-z webdrawdirectory: WWW address to the directory containing the drawings" << endl;
  cout << endl;
  cout << "Verbosity" << endl;
  cout << "\t-V: verbose search" << endl;
  cout << "\t-VV: very verbose search" << endl;
  cout << "\t-VVV: very, very verbose search" << endl;
  cout << endl;
  cout << "Miscellanous" << endl;
  cout << "\t-a n: alarm, stop after n seconds" << endl;
  cout << endl;
}

/**
 * Set the default parameters
 */
static void setDefault () {
  outputStream = &cout;
  sequenceBoth = false;
  sequenceReverse = false;
  sequenceComplement = false;
  duplexReverse = false;
  duplexComplement = false;
  printDominatedSolutions = false;
  gffFormat = false;
  parserFormat = false;
  fastaFormat = false;
  orderedByCost = false;
  explanation = false;
  displayStructure = false;
  webDisplay = false;
  drawDirectory = NULL;
  webDrawDirectory = NULL;
  traceLevel = 1;
  sequence = NULL;
  for (unsigned int i = 0; i < NBMAXDUPLEXSEQUENCES; i++) {
    dupSequences[i] = NULL;
  }

}

/**
 * Set the descriptor file name
 * @param fileName the name of the file where the descriptor should be
 */
static void setDescriptorFileName (char fileName[]) {
  descriptorFileName = fileName;
}

/**
 * Set the main sequence
 * @param fileName the name of the file where the sequence should be
 */
static void setSequence (char fileName[]) {
  sequenceFileReader = new FileReader(string(fileName));
  sequenceFileReader->setSequence(0, sequence, sequenceBoth, sequenceReverse, sequenceComplement);
}

/**
 * Set the output to a given file (default is the standard output)
 * @param fileName the name of the file where the program should write in
 */
static void setOutputFile (char fileName[]) {
  outputStream = new ofstream(fileName);
}

/**
 * Set the duplex sequences (optionnal)
 * @param fileName the name of the file where the sequences should be
 * @param nbDupSequences the number of duplex sequences
 */
static void setDupSequenceFile (char *fileName[], int nbDupSequences) {
  FileReader fr = FileReader(string(*fileName));
  fr.setSequence(0, dupSequences[0], false, duplexReverse, duplexComplement);
}

/**
 * Read the command line
 * @param argc the number of parameters in the command line
 * @param argv the parameter
 * @return true if the program should exit
 */
bool readParameters (int argc, char *argv[]) {
  int argRed = 1;
  char *descriptorFile = NULL;
  char *outputFile = NULL;
  char *sequenceFile = NULL;
  char **dupSequenceFile = NULL;
  nbDupSequences = 0;
  char *arg_char = NULL;

  if (argc <= 2) {
    writeHelpMessage();
    return true;
  }
  setDefault();
  while (argRed < argc) {
    if (!strcmp(argv[argRed], "-h") || (!strcmp(argv[argRed], "--help"))) {
      writeHelpMessage();
      return true;
    }
    if (!strcmp(argv[argRed], "-d")) {
      if (argRed+1 < argc) {
        descriptorFile = argv[argRed+1];
        argRed += 2; }
      else {
        cerr << "Error: '-d' argument must be followed by a motif descriptor file !" << endl; exit(1);}
    }
    else if (!strcmp(argv[argRed], "-s")) {
      if (argRed+1 < argc) {
        sequenceFile = argv[argRed+1];
        argRed += 2; }
      else {
        cerr << "Error: '-s' argument must be followed by a sequence file !" << endl; exit(1);}
    }
    else if (!strcmp(argv[argRed], "-o")) {
      if (argRed+1 < argc) {
        outputFile = argv[argRed+1];
        argRed += 2; }
      else {
        cerr << "Error: '-o' argument must be followed by an output file !" << endl; exit(1);}
    }
    else if (!strcmp(argv[argRed], "-n")) {
      if (argRed+1 < argc) {
        arg_char = argv[argRed+1];
        argRed += 2;
        if (!strcmp(arg_char, "B")) 
          sequenceBoth = true; 
        else if (!strcmp(arg_char, "R")) {
          sequenceReverse = true;
          sequenceComplement = true; }
        else if (strcmp(arg_char, "F")) {
            cerr << "Error: The '-n' argument must be followed by 'B' for both, 'R' for reverse or 'F' for forward (default value)." << endl; exit(1); }
      } else {
        cerr << "Error: '-n' argument must be followed by a letter !" << endl; exit(1);}
    }
    else if (!strcmp(argv[argRed], "-f")) {
      if (argRed+1 < argc) {
        arg_char = argv[argRed+1];
        argRed += 2;
        if (!strcmp(arg_char, "G")) 
          gffFormat = true;
        else if (!strcmp(arg_char,"P")) {
          gffFormat = true;
          parserFormat = true; }
        else if (!strcmp(arg_char, "F")) 
          fastaFormat = true;
        else if (strcmp(arg_char, "S")) {
            cerr << "Error: The '-f' argument must be followed by 'G' for GFF, 'P' for easy parsable, 'F' for FASTA or 'S' for specific (default value)." << endl; exit(1); }
      } else {
          cerr << "Error: '-f' argument must be followed by a letter !" << endl; exit(1); } 
    }
    else if (!strcmp(argv[argRed], "-x")) {
      dupSequenceFile = argv + (argRed+1);
      for (argRed++; argRed < argc; argRed++) {
        if (argv[argRed][0] == '-') {
          break;
        }
        nbDupSequences++;
      }
    }
    else if (!strcmp(argv[argRed], "-v")) {
      duplexReverse = true;
      argRed += 1;
    }
    else if (!strcmp(argv[argRed], "-u")) {
      printDominatedSolutions = true;
      argRed += 1;
    }
    else if (!strcmp(argv[argRed], "-c")) {
      orderedByCost = true;
      argRed += 1;
    }
    else if (!strcmp(argv[argRed], "-e")) {
      explanation = true;
      argRed += 1;
    }
    else if (!strcmp(argv[argRed], "-t")) {
      displayStructure = true;
      argRed += 1;
    }
    else if (!strcmp(argv[argRed], "-w")) {
      webDisplay = true;
      argRed += 1;
    }
    else if (!strcmp(argv[argRed], "-y")) {
      if (argRed+1 < argc) {
        drawDirectory = new char[strlen(argv[argRed+1])+1];
        strcpy(drawDirectory, argv[argRed+1]);
        argRed += 2; }
      else {
        cerr << "Error: '-y' argument must be followed by <drawdirectory> !" << endl; exit(1); } 
    }
    else if (!strcmp(argv[argRed], "-z")) {
      if (argRed+1 < argc) {
        webDrawDirectory = new char[strlen(argv[argRed+1])+1];
        strcpy(webDrawDirectory, argv[argRed+1]);
        argRed += 2; }
      else {
        cerr << "Error: '-z' argument must be followed by <webdrawdirectory> !" << endl; exit(1); } 
    }
    else if (!strcmp(argv[argRed], "-V")) {
      traceLevel = 2;
      argRed += 1;
    }
    else if (!strcmp(argv[argRed], "-VV")) {
      traceLevel = 3;
      argRed += 1;
    }
    else if (!strcmp(argv[argRed], "-VVV")) {
      traceLevel = 11;
      argRed += 1;
    }
    else if (!strcmp(argv[argRed], "-VVVV")) {
      traceLevel = 101;
      argRed += 1;
    }
    else if (!strcmp(argv[argRed], "-a")) {
      if (argRed+1 < argc) {
        int i = atoi(argv[argRed+1]);
        argRed += 2;
        if (i>0 && INT_MAX)
          alarm(i);
        else {
          cerr << "Error: '-a' argument must be followed by a number of second (integer) !" << endl; exit(1); } }
      else {
        cerr << "Error: '-a' argument must be followed by an integer !" << endl; exit(1); } 
    }
    else {
      cerr << "Error: Cannot understand parameter '" << argv[argRed] << "'!" << endl;
      exit(1);
    }
  }
  if (descriptorFile != NULL) {
    setDescriptorFileName(descriptorFile);
  }
  else {
    cerr << "Error: The descriptor file is not set!" << endl;
    exit(1);
  }
  if (outputFile != NULL) {
    setOutputFile(outputFile);
  }
  if (sequenceFile != NULL) {
    setSequence(sequenceFile);
  }
  else {
    cerr << "Error: The sequence file is not set!" << endl;
    exit(1);
  }
  if (dupSequenceFile != NULL) {
    setDupSequenceFile(dupSequenceFile, nbDupSequences);
  }
  trace(10, "Parameters red.");
  return false;
}

#endif
