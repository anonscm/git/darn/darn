// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef HPATT_H_INCLUDED
#define HPATT_H_INCLUDED

#include "values.h"
#include "hardConstraint.h"


/**
 * This class stands for an distance hard constraint.
 */
class HardPattern : public HardConstraint {

protected:
  
  /**
   * the sequence where the pattern should be found
   */
  Sequence* sequence;
  /**
   * the pattern to be compared with the sequence
   */
  Sequence* pattern;
  /**
   * a matrix, allocated once, used to perform the computations
   */
  int *matrix;
  /**
   * matching values
   */
  MatchingValues *matchingValues;
  /**
   * check if the the text beginning at @a i1 is a approximation of the pattern
   * @param i1 the index of the first letter of the text
   * @return true if the pattern and the sub-text are alike
   */
  bool isConsistent (int i1);

public:
  /**
   * the constructor
   * @param v1 the variable constrained by this pattern
   * @param i the index of this constraint
   * @param s the pattern
   * @param mc the maximum number of mismatch/insertion/deletion
   * @param mv the matching values
   */
  HardPattern (Variable *v1, int i, string s, int mc, MatchingValues *mv);
  /**
   * revision of the constraint, when no information is given on what led to the revision
   */
  virtual void revise ();
  /**
   * revision of the constraint, due to a given variable
   * @param v the index of the given variable
   */
  virtual void revise (int v);
  /**
   * revision of the constraint, when the lower bound of a variable has changed
   * @param v the index of the given variable
   */
  virtual void reviseFromLb (int v);
  /**
   * revision of the constraint, when the upper bound of a variable has changed
   * @param v the given variable
   */
  virtual void reviseFromUb (int v);
  /**
   * revision of the constraint, when the upper bound of a variable has been assigned
   * @param v the given variable
   */
  virtual void reviseFromAssignment (int v);
  /**
   * verfiy that the support is still valid
   * @param s the support
   */
  virtual bool getConsistency (Support *s);
  /**
   * whether one should print a space before or after the variable @a var
   *  (useful for printing the results)
   * @param var the index of the variable
   * @param place 0: before, 1: after
   * @return true, if a space should be printed
   */
  virtual bool printSpace (int var, int place);
};

#endif
