// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef HAIR_H_INCLUDED
#define HAIR_H_INCLUDED

#include "values.h"
#include "softConstraint.h"


/**
 * The Helix class stands for an helix cost profile. It stores the computed results in a map which uses Quadruplet.
 */
class Hairpin : public SoftConstraint {

protected:
  /**
   * the minimum size of the hairpin
   */
  int minLength;
  /**
   * the maximum size of the hairpin
   */
  int maxLength;
  /**
   * a matrix, allocated once, used to perform the computations
   */
  int **matrix;
  /**
   * interaction values
   */
  InteractionValues *interactionValues;

  /**
   * computes the minimum cost of the function, given the bounds of the variables
   * @param i1 the lower bound of the first stem
   * @param i2 the upper bound of the second stem
   * @return the minimum
   */
  int getMinCost (int i1, int i2);
  /**
   * compute the minimum cost of the constraint, between the specified bounds
   * @param lb1 the lower bound of the first variable
   * @param ub1 the upper bound of the first variable
   * @param lb2 the lower bound of the second variable
   * @param ub2 the upper bound of the second variable
   * @return the minimum
   */
  int getMinCost (int lb1, int ub1, int lb2, int ub2);

public:
  /**
   * the constructor
   * @param v1 the first variable (the lower bound of the first stem)
   * @param v2 the second variable (the upper bound of the second stem)
   * @param o whether the hairpin is optional
   * @param i the index of this constraint
   * @param ml the minimum length of the hairpin
   * @param mml the maximum length of the hairpin
   * @param e2c the convertor from errors to costs
   * @param iv the interaction values
   */
  Hairpin (Variable *v1, Variable *v2, int i, int ml, int mml, bool o, ErrorsToCosts *e2c, InteractionValues *iv);
  /**
   * empty destructor
   */
  virtual ~Hairpin ();
  /**
   * get the minimum of the function cost
   * @return the minimum
   */
  virtual int getMinCost ();
  /**
   * @pre The variable @a v should be discretized
   *
   * get the minimum of the function cost, having that the given variable has been instanciated to the given value
   * @param v the given v
   * @param val the given value
   * @return the minimum
   */
  virtual int getMinCost (Variable *v, int val);
  /**
   * @pre The variable @a v should be not discretized
   *
   * compute the minimum cost of the constraint, having that the specified variable is instanciated to the lowest bound
   * @param v the specified variable
   * @return the minimum
   */
  virtual int getMinLbCost (Variable *v);
  /**
   * @pre The variable @a v should be not discretized
   *
   * compute the minimum cost of the constraint, having that the specified variable is instanciated to the upper bound
   * @param v the specified variable
   * @return the minimum
   */
  virtual int getMinUbCost (Variable *v);
  /**
   * compute the cost of the given support
   * @param s the given support
   * @return the cost
   */
  virtual int getCost (Support *s);
  /**
   * get the contribution of this constraint to the global cost when all variables are assigned
   * @param values the values of the involved variables
   * @return the cost of this constraint
   */
  virtual int getFinalCost (int *values);
};

#endif
