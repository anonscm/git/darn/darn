// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef TOOLS_H_INCLUDED
#define TOOLS_H_INCLUDED

#include "common.h"
#include "values.h"
#include "sequence.h"


/**
 * This file provides with advanced algorithmic tools
 */


/**
 * This function computes the matching score between two strings (in the edit distance fashion).
 * It uses a non optimized version of the dynamical programming.
 * @param s1 the first string
 * @param s2 the second string
 * @param direct whether both strands have the same orientation
 * @param interactionValues the interaction costs
 * @return the score
 */
static inline int getMatchCost (Sequence *s1, Sequence *s2, InteractionValues *interactionValues, bool direct = true) {
  int **matrix;
  int l1, l2;
  int val1, val2, val3;
  Nucleotide *n1, *n2;
  l1 = s1->getLength();
  l2 = s2->getLength();
  if (l1 == 0) {
    return l2*interactionValues->getInsertionCost();
  }
  if (l2 == 0) {
    return l1*interactionValues->getInsertionCost();
  }
  matrix = new int*[l1+1];
  for (int i = 0; i < l1+1; i++) {
    matrix[i] = new int[l2+1];
    matrix[i][0] = i * interactionValues->getInsertionCost();
  }
  for (int j = 1; j < l2+1; j++) {
    matrix[0][j] = j * interactionValues->getInsertionCost();
  }
  for (int i = 1; i < l1+1; i++) {
    n1 = s1->getNucleotide(i-1);
    for (int j = 1; j < l2+1; j++) {
      if (direct) {
        n2 = s2->getNucleotide(j-1);
      }
      else {
        n2 = s2->getNucleotide(l2-j);
      }
      val1 = matrix[i-1][j-1] + interactionValues->getMatchCost(n1, n2);
      val2 = matrix[i][j-1] + interactionValues->getInsertionCost();
      val3 = matrix[i-1][j] + interactionValues->getInsertionCost();
      matrix[i][j] = min3(val1, val2, val3);
    }
  }
  val1 = matrix[l1][l2];
  for (int i = 0; i < l1+1; i++) {
    delete[] matrix[i];
  }
  delete[] matrix;
  return val1;
}

/**
 * This function computes the edit distance between two strings.
 * It uses a non optimized version of the dynamical programming.
 * @param s1 the first string
 * @param s2 the second string
 * @param matchingValues the matching costs
 * @return the distance
 */
static inline int getEditDistance (Sequence *s1, Sequence *s2, MatchingValues *matchingValues) {
  int **matrix;
  int l1, l2;
  int val1, val2, val3;
  Nucleotide *n1, *n2;
  l1 = s1->getLength();
  l2 = s2->getLength();
  if (l1 == 0) {
    return l2 * matchingValues->getInsertionCost();
  }
  if (l2 == 0) {
    return l1 * matchingValues->getInsertionCost();
  }
  matrix = new int*[l1+1];
  for (int i = 0; i < l1+1; i++) {
    matrix[i] = new int[l2+1];
    matrix[i][0] = i * matchingValues->getInsertionCost();
  }
  for (int j = 1; j < l2+1; j++) {
    matrix[0][j] = j * matchingValues->getInsertionCost();
  }
  for (int i = 1; i < l1+1; i++) {
    n1 = s1->getNucleotide(i-1);
    for (int j = 1; j < l2+1; j++) {
      n2 = s2->getNucleotide(j-1);
      val1 = matrix[i-1][j-1] + matchingValues->getSubstitutionCost(n1, n2);
      val2 = matrix[i][j-1] + matchingValues->getInsertionCost();
      val3 = matrix[i-1][j] + matchingValues->getInsertionCost();
      matrix[i][j] = min3(val1, val2, val3);
    }
  }
  val1 = matrix[l1][l2];
  for (int i = 0; i < l1+1; i++) {
    delete[] matrix[i];
  }
  delete[] matrix;
  return val1;
}

/**
 * This function computes the edit distance between two strings
 *    -- however, the matching between the first letters and the last letters should be substitutions
 * It uses a non optimized version of the dynamical programming.
 * @param s1 the first string
 * @param s2 the second string
 * @param matchingValues the matching costs
 * @return the distance
 */
static inline int getEditSubstitutionDistance (Sequence *s1, Sequence *s2, MatchingValues *matchingValues) {
  int **matrix;
  int l1, l2;
  int val1, val2, val3;
  Nucleotide *n1, *n2;
  l1 = s1->getLength();
  l2 = s2->getLength();
  if (l1 == 0) {
    return l2 * matchingValues->getInsertionCost();
  }
  if (l2 == 0) {
    return l1 * matchingValues->getInsertionCost();
  }
  /*
  matrix = new int*[l1];
  for (int i = 1; i < l1; i++) {
    matrix[i] = new int[l2];
    matrix[i][1] = i * matchingValues->getInsertionCost();
  }
  for (int j = 1; j < l2; j++) {
    matrix[1][j] = j * matchingValues->getInsertionCost();
  }
  for (int i = 2; i < l1; i++) {
    n1 = s1->getNucleotide(i-1);
    for (int j = 2; j < l2; j++) {
      n2 = s2->getNucleotide(j-1);
      val1 = matrix[i-1][j-1] + matchingValues->getSubstitutionCost(n1, n2);
      val2 = matrix[i][j-1] + matchingValues->getInsertionCost();
      val3 = matrix[i-1][j] + matchingValues->getInsertionCost();
      matrix[i][j] = min3(val1, val2, val3);
    }
  }
  val1 = matrix[l1-1][l2-1];
  for (int i = 1; i < l1; i++) {
    delete[] matrix[i];
  }
  delete[] matrix;
  n1 = s1->getNucleotide(0);
  n2 = s2->getNucleotide(0);
  val1 += matchingValues->getSubstitutionCost(n1, n2);
  n1 = s1->getNucleotide(l1-1);
  n2 = s2->getNucleotide(l2-1);
  val1 += matchingValues->getSubstitutionCost(n1, n2);
  return val1;
  */
  matrix = new int*[l1];
  for (int i = 0; i < l1; i++) {
    matrix[i] = new int[l2];
    matrix[i][0] = 10000;
  }
  matrix[0][0] = 0;
  for (int j = 1; j < l2; j++) {
    matrix[0][j] = 10000;
  }
  for (int i = 1; i < l1; i++) {
    n1 = s1->getNucleotide(i-1);
    for (int j = 1; j < l2; j++) {
      n2 = s2->getNucleotide(j-1);
      val1 = matrix[i-1][j-1] + matchingValues->getSubstitutionCost(n1, n2);
      val2 = matrix[i][j-1] + matchingValues->getInsertionCost();
      val3 = matrix[i-1][j] + matchingValues->getInsertionCost();
      matrix[i][j] = min3(val1, val2, val3);
    }
  }
  val1 = matrix[l1-1][l2-1];
  for (int i = 1; i < l1; i++) {
    delete[] matrix[i];
  }
  delete[] matrix;
  n1 = s1->getNucleotide(l1-1);
  n2 = s2->getNucleotide(l2-1);
  val1 += matchingValues->getSubstitutionCost(n1, n2);
  return val1;
}

/**
 * This function computes the edit distance between a short string and a substring of a longer string.
 * It uses a non optimized version of the dynamical programming.
 * @param s1 the short string
 * @param s2 the long string
 * @param matchingValues the matching costs
 * @return the distance
 */
static inline int getEditDistanceFactor (Sequence *s1, Sequence *s2, MatchingValues *matchingValues) {
  int **matrix;
  int l1, l2;
  int val1, val2, val3, min;
  Nucleotide *n1, *n2;
  l1 = s1->getLength();
  l2 = s2->getLength();
  if (l1 == 0) {
    return l2 * matchingValues->getInsertionCost();
  }
  if (l2 == 0) {
    return l1 * matchingValues->getInsertionCost();
  }
  matrix = new int*[l1+1];
  for (int i = 0; i < l1+1; i++) {
    matrix[i] = new int[l2+1];
    matrix[i][0] = i * matchingValues->getInsertionCost();
  }
  for (int j = 1; j < l2+1; j++) {
    matrix[0][j] = j * matchingValues->getInsertionCost();
  }
  min = matrix[l1][0];
  for (int j = 1; j < l2+1; j++) {
    n2 = s2->getNucleotide(j-1);
    for (int i = 1; i < l1+1; i++) {
      n1 = s1->getNucleotide(i-1);
      val1 = matrix[i-1][j-1] + matchingValues->getSubstitutionCost(n1, n2);
      val2 = matrix[i][j-1] + matchingValues->getInsertionCost();
      val3 = matrix[i-1][j] + matchingValues->getInsertionCost();
      matrix[i][j] = min3(val1, val2, val3);
    }
    min = min2(min, matrix[l1][j]);
  }
  for (int i = 0; i < l1+1; i++) {
    delete[] matrix[i];
  }
  delete[] matrix;
  return min;
}

#endif
