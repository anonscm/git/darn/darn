 /* Darn: RNA Motif Localization
    Copyright (C) 2010  INRA

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>. */

%{
#include "common.h"
#include "values.h"
#include "network.h"

int yylex(void);
int yyerror(char *msg);
int messageErrorMissingParameter(const char constraint[], const char arg[]);
int messageErrorMissingVariable(const char constraint[], int arg);

/**
 * number of possible constraint parameters
 */
static const int MAX_ARGS = 27;
/**
 * maximum number of constraint variables
 */
static const int MAX_VAR_ARGS = 4;

/**
 * set of possible types of constraint parameters
 */
typedef enum {STRING, INTEGER, BOOLEAN, INTERACTION, ORIENTATION, RELATION, MODEL} ConstraintParameterType;


/**
 * set of possible models for a function
 */
typedef enum {hardModel, softModel, optionalModel} Model;
/**
 * the name of the previous models
 */
const char* modelNames[] = {"hard", "soft", "optional"};

/**
 * structure recording the value of a parameter
 */
union ConstraintParameter {
  /**
   * a string
   */
  char *stringValue;
  /**
   * an integer
   */
  int integerValue;
  /**
   * a boolean
   */
  bool booleanValue;
  /**
   * an interaction (for a non-canonic interaction constraint)
   */
  Interaction interactionValue;
  /**
   * an orientation (for a non-canonic interaction constraint)
   */
  Orientation orientationValue;
  /**
   * a greater than / less than relation (for the threshold of a composition constraint)
   */
  Relation relationValue;
  /**
   * the model of a function
   */
  Model modelValue;
};

/**
 * name of the constraint parameters
 */
typedef enum {MODEL_A, WORD_A, ERRORS_A, MINSTEM_A, MAXSTEM_A, MINLOOP_A, MAXLOOP_A, MINLENGTH_A, MAXLENGTH_A, MINSOFTLENGTH_A, MAXSOFTLENGTH_A, MINCOSTS_A, MAXCOSTS_A, INDELS_A, WOBBLE_A, INTERACTION1_A, INTERACTION2_A, ORIENTATION_A, NUCLEOTIDES_A, THRESHOLD1_A, THRESHOLD2_A, RELATION_A, FAMILY_A, MINSIZE_A, MAXSIZE_A, MINDISTANCE_A, MAXDISTANCE_A} ConstraintParameterName;

/**
 * types of the constraint parameters (related to @a ConstraintParameterName)
 */
ConstraintParameterType types[MAX_ARGS] = {MODEL, STRING, INTEGER, INTEGER, INTEGER, INTEGER, INTEGER, INTEGER, INTEGER, INTEGER, INTEGER, INTEGER, INTEGER, BOOLEAN, BOOLEAN, INTERACTION, INTERACTION, ORIENTATION, STRING, INTEGER, INTEGER, RELATION, INTEGER, INTEGER, INTEGER, INTEGER, INTEGER};

/**
 * structure recording all the parameters of a constraint
 */
struct ConstraintParameters {
  /**
   * the values of the possible parameters
   */
  ConstraintParameter parameters[MAX_ARGS];
  /**
   * whether a parameter have been given by the user
   */
  bool marks[MAX_ARGS];

  /**
   * Constructor
   */
  ConstraintParameters () {
    for (int i = 0; i < MAX_ARGS; i++) {
      marks[i] = false;
    }
  }

  /**
   * set the value of a parameter
   * @param cp the value of the parameter
   * @param cpn the name of the parameter
   */
  void addElement (ConstraintParameter cp, ConstraintParameterName cpn) {
    switch (types[cpn]) {
      case STRING:
        parameters[cpn].stringValue = cp.stringValue;
        break;
      case BOOLEAN:
        parameters[cpn].booleanValue = cp.booleanValue;
        break;
      case INTEGER:
        parameters[cpn].integerValue = cp.integerValue;
        break;
      case INTERACTION:
        parameters[cpn].interactionValue = cp.interactionValue;
        break;
      case ORIENTATION:
        parameters[cpn].orientationValue = cp.orientationValue;
        break;
      case RELATION:
        parameters[cpn].relationValue = cp.relationValue;
        break;
      case MODEL:
        parameters[cpn].modelValue = cp.modelValue;
        break;
    }
    marks[cpn] = true;
  }

  /**
   * get the value of a parameter
   * @param cpn the name of the parameter
   * @return the value of the parameter
   */
  ConstraintParameter getElement (ConstraintParameterName cpn) {
    return parameters[cpn];
  }

  /**
   * check whether the value of a parameter has been given
   * @param cpn the name of the parameter
   * @return true, if the value has been given
   */
  bool isSet (ConstraintParameterName cpn) {
    return (marks[cpn]);
  }

  /**
   * erase all the values
   */
  void clear () {
    for (int i = 0; i < MAX_ARGS; i++) {
      marks[i] = false;
    }
  }
};

/**
 * structure recording all the variables of a constraint
 */
struct ConstraintVariables {
  /**
   * the set of variables
   */
  Variable **constraintVariables;
  /**
   * the number of variable given by the user
   */
  int nbConstraintVariables;

  /**
   * Constructor
   */
  ConstraintVariables () {
    constraintVariables = new Variable*[MAX_VAR_ARGS];
    nbConstraintVariables = 0;
  }

  /**
   * set a variable to the constraint
   * @param v the variable
   */
  void addElement (Variable *v) {
    constraintVariables[nbConstraintVariables] = v;
    nbConstraintVariables++;
  }

  /**
   * get a variable
   * @param i the index of the variable
   */
  Variable *getElement (int i) {
    return constraintVariables[i];
  }

  /**
   * check whether a variable has been given
   * @param i the index of the parameter
   * @return true, if the variable has been given
   */
  bool isSet (int i) {
    return (nbConstraintVariables > i);
  }

  /**
   * erase all the variables
   */
  void clear () {
    nbConstraintVariables = 0;
  }
};

/**
 * structure recording the costs of a soft constraint
 */
struct SoftConstraintCosts {
  /**
   * the set of costs
   */
  int *costs;
  /**
   * the number of costs given by the user
   */
  int nbCosts;
  /**
   * the maximum cost
   */
  int maxCost;
  /**
   * a string containing the content
   */
  char content[255];

  /**
   * Constructor
   */
  SoftConstraintCosts (int mc) : maxCost(mc) {
    costs = new int[maxCost+1];
    nbCosts = 0;
    content[0] = '\0';
  }

  /**
   * set a cost to the constraint
   * @param c the variable
   */
  void addElement (int c) {
    char tmp[255];
    if (nbCosts > maxCost+1) {
      cerr << "Error: Trying to access an out of range cost!" <<endl;
      exit(1);
    }
    costs[nbCosts] = c;
    nbCosts++;
    if (nbCosts == 1) {
      sprintf(content, " {");
    }
    else {
      content[strlen(content)-1] = '\0';
      strcat(content, ", ");
    }
    strcpy(tmp, content);
    sprintf(content, "%s%i", tmp, c);
    strcat(content, "}");
  }

  /**
   * get the costs
   * @return the costs
   */
  int *getCosts () {
    if (isSet()) {
      return costs;
    }
    return NULL;
  }

  /**
   * get the number of costs given by the user
   * @return the number of costs
   */ 
  int getNbCosts () {
    return nbCosts;
  }

  /**
   * check whether costs have been given
   * @return true, if the variable has been given
   */
  bool isSet () {
    return (nbCosts > 0);
  }

  /**
   * erase all the costs
   */
  void clear () {
    nbCosts = 0;
    content[0] = '\0';
  }

  /**
   * print the current content
   * @return a string
   */
  char *print () {
    return content;
  }
};

/**
 * an explicit name for the parameters
 */
const char *constraintParameterComments[] = {"model", "word", "errors", "minimum stem length", "maximum stem length", "minimum loop length", "maximum loop length", "minimum length", "maximum length", "minimum soft length", "maximum soft length", "minimum cost", "maximum cost", "insertions/deletions accepted", "wobble accepted", "first interaction", "second interaction", "orientation", "nucleotides", "first threshold", "second threshold", "relation","isostericity family"};

/**
 * the set of possible variable for a constraint
 */
ConstraintVariables constraintVariables;
/**
 * the set of possible parameters for a constraint
 */
ConstraintParameters constraintParameters;
/**
 * a value of parameter (global variable because heavily used)
 */
ConstraintParameter constraintParameter;
/**
 * the set of costs for a soft constraint
 */
SoftConstraintCosts *constraintCosts;


/**
 * check whether the variables have been provided by the user
 * @param constraintName an explicit name for the constraint
 * @param nbConstraintVariables the number of variables used by the constraint
 */
void checkParameters (const char *constraintName, int nbConstraintVariables) {
  for (int i = 0; i < nbConstraintVariables; i++) {
    if (!constraintVariables.isSet(i)) {
      messageErrorMissingVariable(constraintName, i+1);
      break;
    }
  }
}

/**
 * check whether the variables and the parameters have been provided by the user
 * @param constraintName an explicit name for the constraint
 * @param nbConstraintVariables the number of variables used by the constraint
 * @param nbConstraintParameters the number of parameters used by the constraint
 * @param cpn0 the first parameter
 */
void checkParameters (const char *constraintName, int nbConstraintVariables, int nbConstraintParameters, ConstraintParameterName cpn0 ...) {
  ConstraintParameterName cpn;
  va_list l;
  va_start(l, cpn0);

  for (int i = 0; i < nbConstraintVariables; i++) {
    if (!constraintVariables.isSet(i)) {
      messageErrorMissingVariable(constraintName, i+1);
      break;
    }
  }
  for (int i = 1; i < nbConstraintParameters; i++) {
    cpn = (ConstraintParameterName) va_arg(l, int);
    if (!constraintParameters.isSet(cpn)) {
      messageErrorMissingParameter(constraintName, constraintParameterComments[cpn]);
    }
  }
  va_end(l);
}

/**
 * number of variables of the network
 */
int nbVars;
/**
 * maximum cost
 */
int maxCost = 1;
/**
 * index of the first S variable (on the main stem)
 */
int firstSVarIndex;
/**
 * index of the last S variable (on the main stem)
 */
int lastSVarIndex;
/**
 * number of S variables (on the main stem)
 */
int nbSVars;
/**
 * index of the first T variable (on the target stem)
 */
int firstTVarIndex;
/**
 * index of the last T variable (on the target stem)
 */
int lastTVarIndex;
/**
 * number of T variables (on the target stem)
 */
int nbTVars;

%}

%locations
 
%union {
    char *word;
    int value;
}

%token <value>  NUMBER
%token <value>  SVAR
%token <value>  TVAR
%token <word>   WORD
%token          MAX_COST
%token          DECL_VARIABLE DECL_CONTENT DECL_PATTERN DECL_PATTERN2 DECL_SPACER DECL_COMPOSITION DECL_HELIX_SIMPLE DECL_HELIX DECL_REPETITION DECL_DUPLEX DECL_NONCANONICPAIR DECL_HAIRPIN DECL_SVAR DECL_TVAR
%token          PARAM_MODEL PARAM_WORD PARAM_NUCLEOTIDES PARAM_ERRORS PARAM_MINSTEM PARAM_MAXSTEM PARAM_STEM PARAM_MINLOOP PARAM_MAXLOOP PARAM_LOOP PARAM_MINLENGTH PARAM_MAXLENGTH PARAM_MINSOFTLENGTH PARAM_MAXSOFTLENGTH PARAM_LENGTH PARAM_MINCOSTS PARAM_MAXCOSTS PARAM_COSTS PARAM_INDELS PARAM_WOBBLE PARAM_INTERACTION PARAM_ORIENTATION PARAM_THRESHOLD PARAM_RELATION PARAM_FAMILY PARAM_SIZE PARAM_DISTANCE
%token          VALUE_TRUE VALUE_FALSE VALUE_ALL
%token          INTERACTION_WATSON_CRICK INTERACTION_HOOGSTEEN INTERACTION_SUGAR_EDGE
%token          ORIENTATION_CIS ORIENTATION_TRANS
%token          RELATION_NOT_LESS_THAN RELATION_NOT_GREATER_THAN
%token          MODEL_HARD MODEL_SOFT MODEL_OPTIONAL
%token          DOTS COMMA EQUAL
%token          LEFT_BRACKET RIGHT_BRACKET
%token          LEFT_PAR RIGHT_PAR LEFT_BRA RIGHT_BRA
%token          QUOTE
%token          PERCENT
%token          END_LINE

%type  <value>  variable

%right          LEFT_BRA

%start program

%%

program:
  end_line overall_cost_declaration variable_declaration function_declarations
  ;

overall_cost_declaration:
    MAX_COST EQUAL NUMBER end_line
          {
            maxCost = $3;
            constraintCosts = new SoftConstraintCosts(NBMAXERRORS);
          }
  ;
variable_declaration:
    DECL_VARIABLE DECL_SVAR EQUAL NUMBER DOTS NUMBER end_line
          {
            char str[10];
            firstSVarIndex = $4;
            lastSVarIndex = $6;
            firstTVarIndex = 0;
            lastTVarIndex = 0;
            nbSVars = lastSVarIndex - firstSVarIndex + 1;
            nbTVars = 0;
            nbVars = nbSVars;
            network = new Network(nbVars, maxCost);
            for (int i = firstSVarIndex; i <= lastSVarIndex; i++) {
              sprintf(str, "X%i", i);
              network->makeVariable(string(str), sequence);
            }
          }
  | DECL_VARIABLE DECL_SVAR EQUAL NUMBER DOTS NUMBER COMMA DECL_TVAR EQUAL NUMBER DOTS NUMBER end_line
          {
            char str[10];
            firstSVarIndex = $4;
            lastSVarIndex = $6;
            firstTVarIndex = $10;
            lastTVarIndex = $12;
            nbSVars = lastSVarIndex - firstSVarIndex + 1;
            nbTVars = lastTVarIndex - firstTVarIndex + 1;
            nbVars = nbSVars + nbTVars;

            if (nbDupSequences == 0) {
              trace(0, "Error! The duplex file is not set!");
              exit(1);
            }

            network = new Network(nbVars, maxCost);
            for (int i = firstSVarIndex; i <= lastSVarIndex; i++) {
              sprintf(str, "X%i", i);
              network->makeVariable(string(str), sequence);
            }
            for (int i = firstTVarIndex; i <= lastTVarIndex; i++) {
              sprintf(str, "Y%i", i);
              network->makeVariable(string(str), dupSequences[0]);
            }
          }
  | DECL_SVAR NUMBER DOTS NUMBER end_line
          {
            char str[10];
            firstSVarIndex = $2;
            lastSVarIndex = $4;
            firstTVarIndex = 0;
            lastTVarIndex = 0;
            nbSVars = lastSVarIndex - firstSVarIndex + 1;
            nbTVars = 0;
            nbVars = nbSVars;
            network = new Network(nbVars, maxCost);
            for (int i = firstSVarIndex; i <= lastSVarIndex; i++) {
              sprintf(str, "X%i", i);
              network->makeVariable(string(str), sequence);
            }
          }
  | DECL_SVAR NUMBER DOTS NUMBER end_line DECL_TVAR NUMBER DOTS NUMBER end_line
          {
            char str[10];
            firstSVarIndex = $2;
            lastSVarIndex = $4;
            firstTVarIndex = $7;
            lastTVarIndex = $9;
            nbSVars = lastSVarIndex - firstSVarIndex + 1;
            nbTVars = lastTVarIndex - firstTVarIndex + 1;
            nbVars = nbSVars + nbTVars;

            if (nbDupSequences == 0) {
              trace(0, "Error! The duplex file is not set!");
              exit(1);
            }

            network = new Network(nbVars, maxCost);
            for (int i = firstSVarIndex; i <= lastSVarIndex; i++) {
              sprintf(str, "X%i", i);
              network->makeVariable(string(str), sequence);
            }
            for (int i = firstTVarIndex; i <= lastTVarIndex; i++) {
              sprintf(str, "Y%i", i);
              network->makeVariable(string(str), dupSequences[0]);
            }
          }
  ;

function_declarations:
      /* empty */
  | function_declaration function_declarations
  ;

function_declaration:
    function end_line
          {
            constraintVariables.clear();
            constraintParameters.clear();
            constraintCosts->clear();
          }
  ;

function:
    pattern
  | spacer
  | composition
  | noncanonicpair
  | hairpin
  | helix
  | helix_simple
  | repetition
  | duplex
  ;

pattern:
    DECL_PATTERN parameters costs
          {
            Variable *v1, *v2;
            char *word;
            int errors = 0;
            Model model = hardModel;
            const char *constraintName = "pattern constraint (2)";
            checkParameters(constraintName, 2, 1, WORD_A);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            word = constraintParameters.getElement(WORD_A).stringValue;
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(ERRORS_A)) {
              errors = constraintParameters.getElement(ERRORS_A).integerValue;
            }
            if (model != optionalModel) {
              network->makeHardDistance(v1, v2, strlen(word)-1-errors, strlen(word)-1+errors);
              network->makeHardPattern2(v1, v2, word, errors);
            }
            if (model != hardModel) {
              network->makePattern2(v1, v2, word, new ErrorsToCosts(errors, constraintCosts->getCosts()));
            }
            char mes[255];
            sprintf(mes, "\tCreating pattern (2) [%s, %i, %s] (%s, %s)%s", word, errors, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          }
  ;

spacer:
    DECL_SPACER parameters
          {
            Variable *v1, *v2;
            int minLength, maxLength;
            int minSoftLength, maxSoftLength;
            int minCosts = 0;
            int maxCosts = maxCost;
            Model model = hardModel;
            const char *constraintName = "spacer";
            checkParameters(constraintName, 2, 2, MINLENGTH_A, MAXLENGTH_A);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            minLength = constraintParameters.getElement(MINLENGTH_A).integerValue;
            maxLength = constraintParameters.getElement(MAXLENGTH_A).integerValue;
            minSoftLength = minLength;
            maxSoftLength = maxLength;
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(MINCOSTS_A)) {
              minCosts = constraintParameters.getElement(MINCOSTS_A).integerValue;
            }
            if (constraintParameters.isSet(MAXCOSTS_A)) {
              maxCosts = constraintParameters.getElement(MAXCOSTS_A).integerValue;
            }
            if (constraintParameters.isSet(MINSOFTLENGTH_A)) {
              minSoftLength = constraintParameters.getElement(MINSOFTLENGTH_A).integerValue;
            }
            if (constraintParameters.isSet(MAXSOFTLENGTH_A)) {
              maxSoftLength = constraintParameters.getElement(MAXSOFTLENGTH_A).integerValue;
            }
            if (model != optionalModel) {
              network->makeHardDistance(v1, v2, minLength, maxLength);
            }
            if (model != hardModel) {
              network->makeDistance(v1, v2, minLength, minSoftLength, maxSoftLength, maxLength, minCosts, new ErrorsToCosts(maxCosts, constraintCosts->getCosts()));
            }
            char mes[255];
            sprintf(mes, "\tCreating spacer [%i, %i, %i, %i, %i, %i, %s] (%s, %s)%s", minLength, minSoftLength, maxSoftLength, maxLength, minCosts, maxCosts, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          }
  ;

composition:
    DECL_COMPOSITION parameters
          {
            Variable *v1, *v2;
            int t1 = 0;
            int t2 = 100;
            int c1 = 0;
            int c2 = maxCost;
            char *nuc;
            Nucleotide *n1 = nucleotideC;
            Nucleotide *n2 = nucleotideG;
            Relation r = NOT_LESS_THAN;
            Model model = hardModel;
            const char *constraintName = "composition";
            checkParameters(constraintName, 2, 1, THRESHOLD1_A);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(RELATION_A)) {
              r = constraintParameters.getElement(RELATION_A).relationValue;
            }
            if (constraintParameters.isSet(NUCLEOTIDES_A)) {
              nuc = constraintParameters.getElement(NUCLEOTIDES_A).stringValue;
              n1 = Nucleotide::getNucleotide(nuc[0]);
              n2 = Nucleotide::getNucleotide(nuc[1]);
            }
            if (constraintParameters.isSet(THRESHOLD1_A)) {
              t1 = constraintParameters.getElement(THRESHOLD1_A).integerValue;
            }
            if (constraintParameters.isSet(THRESHOLD2_A)) {
              t2 = constraintParameters.getElement(THRESHOLD2_A).integerValue;
            }
            else {
              t2 = t1;
            }
            if (constraintParameters.isSet(MINCOSTS_A)) {
              c1 = constraintParameters.getElement(MINCOSTS_A).integerValue;
            }
            if (constraintParameters.isSet(MAXCOSTS_A)) {
              c2 = constraintParameters.getElement(MAXCOSTS_A).integerValue;
            }
            if (model != optionalModel) {
              if (r == NOT_LESS_THAN) {
                network->makeHardComposition(v1, v2, n1, n2, t1, r);
              }
              else {
                network->makeHardComposition(v1, v2, n1, n2, t2, r);
              }
            }
            if (model != hardModel) {
              network->makeComposition(v1, v2, n1, n2, t1, t2, r, c1, new ErrorsToCosts(c2, constraintCosts->getCosts()));

            }
            char mes[255];
            sprintf(mes, "\tCreating composition [%c, %c, %s, %i, %i, %i, %i, %s] (%s, %s)%s", n1->print(), n2->print(), ((r == NOT_LESS_THAN)? " >= ": " <= "), t1, t2, c1, c2, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          }
  ;

noncanonicpair:
    DECL_NONCANONICPAIR parameters
          {
            Variable *v1, *v2;
            Interaction i1, i2;
            Orientation o;
            int mc = maxCost;
            int family = 0;
            Model model = hardModel;
            const char *constraintName = "non canonic pairing";
            checkParameters(constraintName, 2, 3, INTERACTION1_A, INTERACTION2_A, ORIENTATION_A);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            i1 = constraintParameters.getElement(INTERACTION1_A).interactionValue;
            i1 = constraintParameters.getElement(INTERACTION2_A).interactionValue;
            o = constraintParameters.getElement(ORIENTATION_A).orientationValue;
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(MAXCOSTS_A)) {
              mc = constraintParameters.getElement(MAXCOSTS_A).integerValue;
            }
            i1 = constraintParameters.getElement(INTERACTION1_A).interactionValue;
            i2 = constraintParameters.getElement(INTERACTION2_A).interactionValue;
            o = constraintParameters.getElement(ORIENTATION_A).orientationValue;
            if (constraintParameters.isSet(FAMILY_A)) {
              family = constraintParameters.getElement(FAMILY_A).integerValue;
            }
            if (model != optionalModel) {
              network->makeHardNonCanonicPair(v1, v2, i1, i2, o, family, mc);
            }
            if (model != hardModel) {
              network->makeNonCanonicPair(v1, v2, i1, i2, o, family, new ErrorsToCosts(mc, constraintCosts->getCosts()));
            }
            char mes[255];
            sprintf(mes, "\tCreating non canonic pair [%i, %i, %i, %i, %i, %s] (%s, %s)%s", i1, i2, o, family, mc, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          }
  ;

hairpin:
    DECL_HAIRPIN parameters
          {
            Variable *v1, *v2;
            int minLength, maxLength;
            int errors = 0;
            Model model = hardModel;
            bool wobble = false;
            const char *constraintName = "hairpin";
            checkParameters(constraintName, 2, 2, MINLENGTH_A, MAXLENGTH_A);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(ERRORS_A)) {
              errors = constraintParameters.getElement(ERRORS_A).integerValue;
            }
            if (constraintParameters.isSet(WOBBLE_A)) {
              wobble = constraintParameters.getElement(WOBBLE_A).booleanValue;
            }
            minLength = constraintParameters.getElement(MINLENGTH_A).integerValue;
            maxLength = constraintParameters.getElement(MAXLENGTH_A).integerValue;
            if (model != optionalModel) {
              network->makeHardHairpin(v1, v2, minLength, maxLength, errors, wobble);
            }
            if (model != hardModel) {
              network->makeHairpin(v1, v2, minLength, maxLength, new ErrorsToCosts(errors, constraintCosts->getCosts()), wobble);
            }
            char mes[255];
            sprintf(mes, "\tCreating folding [%i, %i, %i, %i, %s] (%s, %s)%s", minLength, maxLength, wobble, errors, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          }
  ;
  
helix_simple:
    DECL_HELIX_SIMPLE parameters costs
          {
            Variable *v1, *v2, *v3, *v4;
            int minStem, maxStem;
            int minLoop, maxLoop;
            int errors = 0;
            Model model = hardModel;
            bool indels = false;
            bool wobble = false;
            const char *constraintName = "simple helix";
            checkParameters(constraintName, 4, 4, MINSTEM_A, MAXSTEM_A, MINLOOP_A, MAXLOOP_A);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            v3 = constraintVariables.getElement(2);
            v4 = constraintVariables.getElement(3);
            minStem = constraintParameters.getElement(MINSTEM_A).integerValue;
            maxStem = constraintParameters.getElement(MAXSTEM_A).integerValue;
            minLoop = constraintParameters.getElement(MINLOOP_A).integerValue;
            maxLoop = constraintParameters.getElement(MAXLOOP_A).integerValue;
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(ERRORS_A)) {
              errors = constraintParameters.getElement(ERRORS_A).integerValue;
            }
            if (constraintParameters.isSet(INDELS_A)) {
              indels = constraintParameters.getElement(INDELS_A).booleanValue;
            }
            if (constraintParameters.isSet(WOBBLE_A)) {
              wobble = constraintParameters.getElement(WOBBLE_A).booleanValue;
            }
            if (model != optionalModel) {
              network->makeHardDistance(v1, v2, minStem-1, maxStem-1);
              network->makeHardDistance(v2, v3, minLoop+1, maxLoop+1);
              network->makeHardDistance(v3, v4, minStem-1, maxStem-1);
              network->makeHardHelixSimple(v1, v2, v3, v4, minStem, maxStem, minLoop, maxLoop, errors, wobble, indels);
            }
            if (model != hardModel) {
              network->makeHelixSimple(v1, v2, v3, v4, minStem, maxStem, minLoop, maxLoop, new ErrorsToCosts(errors, constraintCosts->getCosts()), wobble, indels);
              
            }
            char mes[255];
            sprintf(mes, "\tCreating simple helix [%i, %i, %i, %i, %i, %i, %i, %s] (%s, %s, %s, %s)%s", minStem, maxStem, minLoop, maxLoop, errors, wobble, indels, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), v3->getName().c_str(), v4->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          }
  ;

repetition:
    DECL_REPETITION parameters costs
          {
            Variable *v1, *v2, *v3, *v4;
            int errors = 0;
            Model model = hardModel;
            bool indels = false;
            const char *constraintName = "repetition";
            int minSize, maxSize;
            int minDistance, maxDistance;
            checkParameters(constraintName, 4, 4, MINSIZE_A, MAXSIZE_A, MINDISTANCE_A, MAXDISTANCE_A);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            v3 = constraintVariables.getElement(2);
            v4 = constraintVariables.getElement(3);
            minSize = constraintParameters.getElement(MINSIZE_A).integerValue;
            maxSize = constraintParameters.getElement(MAXSIZE_A).integerValue;
            minDistance = constraintParameters.getElement(MINDISTANCE_A).integerValue;
            maxDistance = constraintParameters.getElement(MAXDISTANCE_A).integerValue;
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(ERRORS_A)) {
              errors = constraintParameters.getElement(ERRORS_A).integerValue;
            }
            if (constraintParameters.isSet(INDELS_A)) {
              indels = constraintParameters.getElement(INDELS_A).booleanValue;
            }
            if (model != optionalModel) {
              network->makeHardDistance(v1, v2, minSize-1, maxSize-1);
              network->makeHardDistance(v2, v3, minDistance+1, maxDistance+1);
              network->makeHardDistance(v3, v4, minSize-1, maxSize-1);
              network->makeHardRepetition(v1, v2, v3, v4, minSize, maxSize, minDistance, maxDistance, errors, indels);
            }
            if (model != hardModel) {
              network->makeRepetition(v1, v2, v3, v4, minSize, maxSize, minDistance, maxDistance, new ErrorsToCosts(errors, constraintCosts->getCosts()), indels);
            }
            char mes[255];
            sprintf(mes, "\tCreating repetition [%i, %i, %i, %i, %i, %i, %s] (%s, %s, %s, %s)%s", minSize, maxSize, minDistance, maxDistance, errors, indels, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), v3->getName().c_str(), v4->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          }
  ;

helix:
    DECL_HELIX parameters costs
          {
            Variable *v1, *v2, *v3, *v4;
            int errors = 0;
            Model model = hardModel;
            bool wobble = false;
            const char *constraintName = "helix";
            int maxStem;
            checkParameters(constraintName, 4, 1, MAXSTEM_A);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            v3 = constraintVariables.getElement(2);
            v4 = constraintVariables.getElement(3);
            maxStem = constraintParameters.getElement(MAXSTEM_A).integerValue;
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(ERRORS_A)) {
              errors = constraintParameters.getElement(ERRORS_A).integerValue;
            }
            if (constraintParameters.isSet(WOBBLE_A)) {
              wobble = constraintParameters.getElement(WOBBLE_A).booleanValue;
            }
            if (model != optionalModel) {
              network->makeHardHelix(v1, v2, v3, v4, maxStem, errors, wobble);
            }
            if (model != hardModel) {
              network->makeHelix(v1, v2, v3, v4, maxStem, new ErrorsToCosts(errors, constraintCosts->getCosts()), wobble);
            }
            char mes[255];
            sprintf(mes, "\tCreating helix [%i, %i, %i, %s] (%s, %s, %s, %s)%s", maxStem, wobble, errors, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), v3->getName().c_str(), v4->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          }
  ;
  
duplex:
    DECL_DUPLEX parameters costs
          {
            Variable *v1, *v2, *v3, *v4;
            int errors = 0;
            Model model = hardModel;
            bool wobble = false;
            const char *constraintName = "duplex";
            checkParameters(constraintName, 4);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            v3 = constraintVariables.getElement(2);
            v4 = constraintVariables.getElement(3);
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(ERRORS_A)) {
              errors = constraintParameters.getElement(ERRORS_A).modelValue;
            }
            if (constraintParameters.isSet(WOBBLE_A)) {
              wobble = constraintParameters.getElement(WOBBLE_A).booleanValue;
            }
            if (model != optionalModel) {
              network->makeHardDuplex(v1, v2, v3, v4, errors, dupSequences[0], wobble);
            }
            if (model != hardModel) {
              network->makeDuplex(v1, v2, v3, v4, new ErrorsToCosts(errors, constraintCosts->getCosts()), dupSequences[0], wobble);
            }
            char mes[255];
            sprintf(mes, "\tCreating duplex [%i, %i, %s] (%s, %s, %s, %s)%s", wobble, errors, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), v3->getName().c_str(), v4->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          }
  ;
  
parameters:
    LEFT_BRACKET parameters_no_bracket RIGHT_BRACKET
  | LEFT_BRACKET parameters_no_bracket RIGHT_BRACKET LEFT_PAR parameters_no_bracket RIGHT_PAR
  ;

parameters_no_bracket:
    parameter 
  | parameter COMMA parameters_no_bracket
  ;

parameter:
    variable_parameter
  | model_parameter
  | word_parameter
  | errors_parameter
  | minstem_parameter
  | maxstem_parameter
  | minmaxstem_parameter
  | minloop_parameter
  | maxloop_parameter
  | minmaxloop_parameter
  | minlength_parameter
  | maxlength_parameter
  | minmaxlength_parameter
  | mincosts_parameter
  | maxcosts_parameter
  | minmaxcosts_parameter
  | indels_parameter
  | wobble_parameter
  | interaction_parameter
  | orientation_parameter
  | family_parameter
  | nucleotides_parameter
  | threshold_parameter
  | sizes_parameter
  | distances_parameter
  | default_parameter
  ;

variable_parameter:
    variable
          {
            constraintVariables.addElement(variables[$1]);
          }
  ;
variable:
    SVAR
          {
            $$ = $1 - firstSVarIndex;
          }
  | TVAR
          {
            $$ = $1 + nbSVars - firstTVarIndex;
          }
  ;

model_parameter:
    PARAM_MODEL EQUAL MODEL_HARD
          {
            constraintParameter.modelValue = hardModel;
            constraintParameters.addElement(constraintParameter, MODEL_A);
          }
  | PARAM_MODEL EQUAL MODEL_SOFT
          {
            constraintParameter.modelValue = softModel;
            constraintParameters.addElement(constraintParameter, MODEL_A);
          }
  | PARAM_MODEL EQUAL MODEL_OPTIONAL
          {
            constraintParameter.modelValue = optionalModel;
            constraintParameters.addElement(constraintParameter, MODEL_A);
          }
  ;

word_parameter:
    PARAM_WORD EQUAL WORD
          {
            constraintParameter.stringValue = $3;
            constraintParameters.addElement(constraintParameter, WORD_A);
          }
  | PARAM_WORD EQUAL QUOTE WORD QUOTE
          {
            constraintParameter.stringValue = $4;
            constraintParameters.addElement(constraintParameter, WORD_A);
          }
  ;

errors_parameter:
    PARAM_ERRORS EQUAL NUMBER
          {
            if (!constraintParameters.isSet(ERRORS_A)) {
              constraintParameter.integerValue = $3;
            }
            else {
              constraintParameter.integerValue = $3 + constraintParameters.getElement(ERRORS_A).integerValue;
            }
            constraintParameters.addElement(constraintParameter, ERRORS_A);
          }
  ;

minstem_parameter:
    PARAM_MINSTEM EQUAL NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MINSTEM_A);
          }
  ;

maxstem_parameter:
    PARAM_MAXSTEM EQUAL NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MAXSTEM_A);
          }
  ;

minmaxstem_parameter:
    PARAM_STEM EQUAL NUMBER DOTS NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MINSTEM_A);
            constraintParameter.integerValue = $5;
            constraintParameters.addElement(constraintParameter, MAXSTEM_A);
          }
  | PARAM_STEM EQUAL NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MINSTEM_A);
            constraintParameters.addElement(constraintParameter, MAXSTEM_A);
          }
  ;

minloop_parameter:
    PARAM_MINLOOP EQUAL NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MINLOOP_A);
          }
  ;

maxloop_parameter:
    PARAM_MAXLOOP EQUAL NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MAXLOOP_A);
          }
  ;

minmaxloop_parameter:
    PARAM_LOOP EQUAL NUMBER DOTS NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MINLOOP_A);
            constraintParameter.integerValue = $5;
            constraintParameters.addElement(constraintParameter, MAXLOOP_A);
          }
  | PARAM_LOOP EQUAL NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MINLOOP_A);
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MAXLOOP_A);
          }
  ;

minlength_parameter:
    PARAM_MINLENGTH EQUAL NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MINLENGTH_A);
          }
  ;

maxlength_parameter:
    PARAM_MAXLENGTH EQUAL NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MAXLENGTH_A);
          }
  ;

minmaxlength_parameter:
    PARAM_LENGTH EQUAL NUMBER DOTS NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MINLENGTH_A);
            constraintParameter.integerValue = $5;
            constraintParameters.addElement(constraintParameter, MAXLENGTH_A);
          }
  | PARAM_LENGTH EQUAL NUMBER DOTS NUMBER DOTS NUMBER DOTS NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MINLENGTH_A);
            constraintParameter.integerValue = $5;
            constraintParameters.addElement(constraintParameter, MINSOFTLENGTH_A);
            constraintParameter.integerValue = $7;
            constraintParameters.addElement(constraintParameter, MAXSOFTLENGTH_A);
            constraintParameter.integerValue = $9;
            constraintParameters.addElement(constraintParameter, MAXLENGTH_A);
          }
  | PARAM_LENGTH EQUAL NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MINLENGTH_A);
            constraintParameters.addElement(constraintParameter, MINSOFTLENGTH_A);
            constraintParameters.addElement(constraintParameter, MAXSOFTLENGTH_A);
            constraintParameters.addElement(constraintParameter, MAXLENGTH_A);
          }
  ;

mincosts_parameter:
    PARAM_MINCOSTS EQUAL NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MINCOSTS_A);
          }
  ;

maxcosts_parameter:
    PARAM_MAXCOSTS EQUAL NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MAXCOSTS_A);
          }
  ;

minmaxcosts_parameter:
    PARAM_COSTS EQUAL NUMBER DOTS NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MINCOSTS_A);
            constraintParameter.integerValue = $5;
            constraintParameters.addElement(constraintParameter, MAXCOSTS_A);
          }
  | PARAM_COSTS EQUAL NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MINCOSTS_A);
            constraintParameters.addElement(constraintParameter, MAXCOSTS_A);
          }
  ;

indels_parameter:
    PARAM_INDELS EQUAL VALUE_TRUE
          {
            constraintParameter.booleanValue = true;
            constraintParameters.addElement(constraintParameter, INDELS_A);
          }
  | PARAM_INDELS EQUAL VALUE_FALSE
          {
            constraintParameter.booleanValue = false;
            constraintParameters.addElement(constraintParameter, INDELS_A);
          }
  ;

wobble_parameter:
    PARAM_WOBBLE EQUAL VALUE_TRUE
          {
            constraintParameter.integerValue = true;
            constraintParameters.addElement(constraintParameter, WOBBLE_A);
          }
  | PARAM_WOBBLE EQUAL VALUE_FALSE
          {
            constraintParameter.integerValue = false;
            constraintParameters.addElement(constraintParameter, WOBBLE_A);
          }
  ;

interaction_parameter:
    PARAM_INTERACTION EQUAL INTERACTION_WATSON_CRICK
          {
            constraintParameter.interactionValue = WATSON_CRICK;
            if (!constraintParameters.isSet(INTERACTION1_A)) {
              constraintParameters.addElement(constraintParameter, INTERACTION1_A);
            }
            else {
              constraintParameters.addElement(constraintParameter, INTERACTION2_A);
            }
          }
  | PARAM_INTERACTION EQUAL INTERACTION_HOOGSTEEN
          {
            constraintParameter.interactionValue = HOOGSTEEN;
            if (!constraintParameters.isSet(INTERACTION1_A)) {
              constraintParameters.addElement(constraintParameter, INTERACTION1_A);
            }
            else {
              constraintParameters.addElement(constraintParameter, INTERACTION2_A);
            }
          }
  | PARAM_INTERACTION EQUAL INTERACTION_SUGAR_EDGE
          {
            constraintParameter.interactionValue = SUGAR_EDGE;
            if (!constraintParameters.isSet(INTERACTION1_A)) {
              constraintParameters.addElement(constraintParameter, INTERACTION1_A);
            }
            else {
              constraintParameters.addElement(constraintParameter, INTERACTION2_A);
            }
          }
  ;

orientation_parameter:
    PARAM_ORIENTATION EQUAL ORIENTATION_CIS
          {
            constraintParameter.orientationValue = CIS;
            constraintParameters.addElement(constraintParameter, ORIENTATION_A);
          }
  | PARAM_ORIENTATION EQUAL ORIENTATION_TRANS
          {
            constraintParameter.orientationValue = TRANS;
            constraintParameters.addElement(constraintParameter, ORIENTATION_A);
          }
  ;

family_parameter:
    PARAM_FAMILY EQUAL NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, FAMILY_A);
          }
  | PARAM_FAMILY EQUAL VALUE_ALL
          {
            constraintParameter.integerValue = 0;
            constraintParameters.addElement(constraintParameter, FAMILY_A);
          }
  ;

nucleotides_parameter:
    PARAM_NUCLEOTIDES EQUAL WORD
          {
            constraintParameter.stringValue = $3;
            constraintParameters.addElement(constraintParameter, NUCLEOTIDES_A);
          }
  | PARAM_NUCLEOTIDES EQUAL QUOTE WORD QUOTE
          {
            constraintParameter.stringValue = $4;
            constraintParameters.addElement(constraintParameter, NUCLEOTIDES_A);
          }
  ;

threshold_parameter:
    PARAM_THRESHOLD RELATION_NOT_LESS_THAN NUMBER
          {
            constraintParameter.relationValue = NOT_LESS_THAN;
            constraintParameters.addElement(constraintParameter, RELATION_A);
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, THRESHOLD1_A);
          }
  | PARAM_THRESHOLD RELATION_NOT_LESS_THAN NUMBER PERCENT
          {
            constraintParameter.relationValue = NOT_LESS_THAN;
            constraintParameters.addElement(constraintParameter, RELATION_A);
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, THRESHOLD1_A);
          }
  | PARAM_THRESHOLD RELATION_NOT_GREATER_THAN NUMBER
          {
            constraintParameter.relationValue = NOT_GREATER_THAN;
            constraintParameters.addElement(constraintParameter, RELATION_A);
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, THRESHOLD1_A);
          }
  | PARAM_THRESHOLD RELATION_NOT_GREATER_THAN NUMBER PERCENT
          {
            constraintParameter.relationValue = NOT_GREATER_THAN;
            constraintParameters.addElement(constraintParameter, RELATION_A);
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, THRESHOLD1_A);
          }
  | PARAM_THRESHOLD RELATION_NOT_LESS_THAN NUMBER DOTS NUMBER
          {
            constraintParameter.relationValue = NOT_LESS_THAN;
            constraintParameters.addElement(constraintParameter, RELATION_A);
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, THRESHOLD1_A);
            constraintParameter.integerValue = $5;
            constraintParameters.addElement(constraintParameter, THRESHOLD2_A);
          }
  | PARAM_THRESHOLD RELATION_NOT_LESS_THAN NUMBER DOTS NUMBER PERCENT
          {
            constraintParameter.relationValue = NOT_LESS_THAN;
            constraintParameters.addElement(constraintParameter, RELATION_A);
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, THRESHOLD1_A);
            constraintParameter.integerValue = $5;
            constraintParameters.addElement(constraintParameter, THRESHOLD2_A);
          }
  | PARAM_THRESHOLD RELATION_NOT_GREATER_THAN NUMBER DOTS NUMBER
          {
            constraintParameter.relationValue = NOT_GREATER_THAN;
            constraintParameters.addElement(constraintParameter, RELATION_A);
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, THRESHOLD1_A);
            constraintParameter.integerValue = $5;
            constraintParameters.addElement(constraintParameter, THRESHOLD2_A);
          }
  | PARAM_THRESHOLD RELATION_NOT_GREATER_THAN NUMBER DOTS NUMBER PERCENT
          {
            constraintParameter.relationValue = NOT_GREATER_THAN;
            constraintParameters.addElement(constraintParameter, RELATION_A);
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, THRESHOLD1_A);
            constraintParameter.integerValue = $5;
            constraintParameters.addElement(constraintParameter, THRESHOLD2_A);
          }
  ;

sizes_parameter:
    PARAM_SIZE EQUAL NUMBER DOTS NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MINSIZE_A);
            constraintParameter.integerValue = $5;
            constraintParameters.addElement(constraintParameter, MAXSIZE_A);
          }
  ;

distances_parameter:
    PARAM_DISTANCE EQUAL NUMBER DOTS NUMBER
          {
            constraintParameter.integerValue = $3;
            constraintParameters.addElement(constraintParameter, MINDISTANCE_A);
            constraintParameter.integerValue = $5;
            constraintParameters.addElement(constraintParameter, MAXDISTANCE_A);
          }
  ;

default_parameter:
    WORD
          {
            char str[521];
            sprintf(str, "Error! Do not understand '%s'!", $1);
            yyerror((char*) str);
          }
  | WORD EQUAL NUMBER
          {
            char str[521];
            sprintf(str, "Error! Do not understand '%s'!", $1);
            yyerror((char*) str);
          }
  | WORD EQUAL NUMBER DOTS NUMBER
          {
            char str[521];
            sprintf(str, "Error! Do not understand '%s'!", $1);
            yyerror((char*) str);
          }
  | WORD EQUAL NUMBER DOTS NUMBER DOTS NUMBER DOTS NUMBER
          {
            char str[521];
            sprintf(str, "Error! Do not understand '%s'!", $1);
            yyerror((char*) str);
          }
  | WORD EQUAL WORD
          {
            char str[521];
            sprintf(str, "Error! Do not understand '%s'!", $1);
            yyerror((char*) str);
          }
  ;

costs:
        /* empty */
  | LEFT_BRA cost_declarations RIGHT_BRA
  ;

cost_declarations:
    cost_declaration
  | cost_declarations COMMA cost_declaration
  ;

cost_declaration:
  NUMBER
          {
            constraintCosts->addElement($1);
          }
  ;

end_line:
      /* empty */
  | END_LINE end_line
  ;


%%

int messageErrorMissingParameter(const char constraint[], const char arg[]) {
  char str[512];
  sprintf(str, "Error! Missing '%s' argument in %s", arg, constraint);
  return yyerror((char*) str);
}

int messageErrorMissingVariable(const char constraint[], int arg) {
  char str[512];
  sprintf(str, "Error! Missing %ith variable in %s", arg, constraint);
  return yyerror((char*) str);
}

int	yyerror (char msg[]) {
  char str[512];
  sprintf(str, "Parse error in the descriptor: %s.\n", msg);
	trace(0, str);
	exit(1);
	return false;
}
