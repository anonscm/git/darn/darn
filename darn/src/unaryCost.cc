// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "unaryCost.h"


UnaryCost::UnaryCost (int i) : Cost (i, 1) {}

int UnaryCost::getMemProfile (int i) {
  return Cost::getMemProfile(0, i);
}

int UnaryCost::getLbMem () {
  return Cost::getLbMem(0);
}

int UnaryCost::getUbMem () {
  return Cost::getUbMem(0);
}

void UnaryCost::setMemProfile (int i, int v) {
  Cost::setMemProfile(0, i, v);
}

void UnaryCost::setLbMem (int l) {
  Cost::setLbMem(0, l);
}

void UnaryCost::setUbMem (int u) {
  Cost::setUbMem(0, u);
}

void UnaryCost::setDiscretized (int l, int u) {
  if (discretized[0]) return;
  lb[0] = l;
  ub[0] = u;
  for (int i = 1; i < ub[0]-lb[0]-1; i++) {
    memProf[0][i] = mem;
  }
  if (u == l+1) {
    memProf[0][0] = mem + lbMem[0];
  }
  else {
    memProf[0][0] = mem + lbMem[0];
    memProf[0][ub[0]-lb[0]-1] = mem + ubMem[0];
  }
  discretized[0] = true;
}

void UnaryCost::assign (int val, int c) {
  if (discretized[0]) return;
  lb[0] = val;
  ub[0] = val+1;
  memProf[0][0] = c;
  discretized[0] = true;
}
