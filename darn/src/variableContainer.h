// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef  VC_H_INCLUDED
#define  VC_H_INCLUDED


#include "common.h"
#include "variableIterator.h"
#include "variable.h"


/**
 * This class stores the index of the variables of a problem.
 * It provides with easy tools to assign and backtrack.
 */
class VariableContainer {
  
private:
  /**
   * the number of variables in the problem
   */
  int nbVariables;
  /**
   * the arrays of variables (in fact, the DefaultUnarySoftConstraint)
   */
  bool *isPresent;
  /**
   * an array containing the assigned variables (used as a stack)
   */
  int *assignedVariables;
  /**
   * the stack pointer of the previous array
   */
  int assignedVariablesPointer;
  /**
   * the last backtracked variable
   */
  int lastBacktrackedVariable;

public:
  /**
   * the constructor
   * @param nv the number of variables
   */
  VariableContainer (int nv) : nbVariables(nv) {
    isPresent = new bool[nbVariables];
    assignedVariables = new int[nbVariables];
    for (int i = 0; i < nbVariables; i++) {
      isPresent[i] = true;
    }
    assignedVariablesPointer = 0;
    lastBacktrackedVariable = UNDEFINED;
  }

  /**
   * destructor
   */
  ~VariableContainer () {
    /*
    delete[] isPresent;
    delete[] assignedVariables;
    */
  }

  /**
   * prepare the container for a new search
   */
  void reset () {
    for (int i = 0; i < nbVariables; i++) {
      isPresent[i] = true;
    }
    assignedVariablesPointer = 0;
    lastBacktrackedVariable = UNDEFINED;
  }

  /**
   * Simulate an assignment
   * @param v the assigned variable
   */
  void assign (int v) {
    if (assignedVariablesPointer >= nbVariables) {
      trace(1, "Error! Trying to assign too many variables!");
    }
    assignedVariables[assignedVariablesPointer] = v;
    isPresent[v] = false;
    assignedVariablesPointer++;
    lastBacktrackedVariable = UNDEFINED;
  }

  /**
   * Simulate a backtrack
   */
  void backtrack () {
    int assignedVariable;
    if (assignedVariablesPointer <= 0) {
      trace(1, "Error! Trying to backtrack whereas no variable is assigned!");
    }
    assignedVariablesPointer--;
    lastBacktrackedVariable = assignedVariables[assignedVariablesPointer];
    assignedVariable = assignedVariables[assignedVariablesPointer];
    isPresent[assignedVariable] = true;
  }

  /**
   * Get the number of assigned variables
   * @return the number of assigned variables
   */
  int getNbAssignedVariables () {
    return assignedVariablesPointer;
  }

  /**
   * Get the @a i th assigned variable
   * @param i the index of the wanted variable, sorted from the first assigned variable, to the last
   * @return the index of the @a i th assigned variable
   */
  int getAssignedVariable (int i) {
    if ((i < 0) || (i >= assignedVariablesPointer)) {
      trace(1, "Error! Trying to get an unassigned variable!");
    }
    return assignedVariables[i];
  }

  /**
   * Get the last backtracked variable
   * @return the index of the last backtracked variable
   */
  int getLastBacktrackedVariable () {
    return lastBacktrackedVariable;
  }

  /**
   * Get a variable iterator
   * @warning Should be freed by the delete function!
   * @return the variable iterator
   */
  VariableIterator *getIterator () {
    return (new VariableIterator(nbVariables, isPresent));
  }

  /**
   * print the current state of the variable container (namely, the assigned variables)
   * @return a string
   */
  string print () {
    string s;
    for (int i = 0; i < assignedVariablesPointer; i++) {
      s += variables[assignedVariables[i]]->getName() + " ";
    }
    return s;
  }
};

#endif
