// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef NCOST_H_INCLUDED
#define NCOST_H_INCLUDED

#include "common.h"
#include "cost.h"


/**
 * The NaryConstraint class implements all the mecanism for storing the modifications of the nary costs.
 */
class NaryCost : public Cost {

protected:
  
public:
  /**
   * the constructor, by default all the memory profiles are not discretized
   * @param i the index of the constrain (related to @a index)
   * @param nv the number of variables
   */
  NaryCost (int i, int nv);
  /**
   * creates modify the bounds and eventually the memory profile of a variable
   * @param i the index of the variable
   * @param val the value the variable is assigned to
   * @param c the cost the value takes
   */
  virtual void instanciate (int i, int val, int c);
  /**
   * @pre the specified variable is not discretized
   * 
   * creates the memory profile of a variable
   * @param i the index of the variable
   * @param lb its lower bound
   * @param ub its upper bound
   */
  virtual void setDiscretized (int i, int lb, int ub);
};

#endif
