// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "hardHelixSimple.h"

HardHelixSimple::HardHelixSimple (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int i, int msl, int mmsl, int mll, int mmll, int mc, InteractionValues *iv, bool insert) : HardConstraint (i, mc, 4, v1, v2, v3, v4), minStemLength(msl), maxStemLength(mmsl), minLoopLength(mll), maxLoopLength(mmll), insertion(insert) {
  simple = false;
  matrix = new int[maxStemLength+1];
  hardness = minStemLength;
  interactionValues = iv;
  name = "hard helix";
}

int *HardHelixSimple::isConsistent (int var, int val, int *values) {
  int *result;
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  int lb3 = unaryConstraints[2]->getLb();
  int ub3 = unaryConstraints[2]->getUb();
  int lb4 = unaryConstraints[3]->getLb();
  int ub4 = unaryConstraints[3]->getUb();
  switch (var) {
    case 0:
      lb1 = val;
      ub1 = val+1;
      break;
    case 1:
      lb2 = val;
      ub2 = val+1;
      break;
    case 2:
      lb3 = val;
      ub3 = val+1;
      break;
    case 3:
      lb4 = val;
      ub4 = val+1;
      break;
  }
  result = isConsistent(lb1, ub1, lb2, ub2, lb3, ub3, lb4, ub4, values);
  return result;
}

int *HardHelixSimple::isConsistent (int lb1, int ub1, int lb2, int ub2, int lb3, int ub3, int lb4, int ub4, int *values) {
  bool isASupport = false;
  int startI, stopI, startJ, stopJ, startK, stopK, startL, stopL;
  int match, value, val1, val2, val3, previous;

  // find all possible values for the 1st variable
  startI = max4(lb1, lb2-(maxStemLength-1), lb3-(maxStemLength-1+maxLoopLength+1), lb4-(2*(maxStemLength-1)+maxLoopLength+1));
  stopI = min4(ub1-1, ub2-1-(minStemLength-1), ub3-1-(minStemLength-1+minLoopLength+1), ub4-1-(2*(minStemLength-1)+minLoopLength+1));

  // scan the possibles values for the first variable
  for (int i = startI; i <= stopI; i++) {

    // find all possible values for the last variable
    startL = max4(i+2*(minStemLength-1)+(minLoopLength+1), lb2+(minStemLength-1+minLoopLength+1), lb3+(minStemLength-1), lb4);
    stopL = min4(i+(2*(maxStemLength-1)+maxLoopLength+1), ub2-1+(maxStemLength-1+maxLoopLength+1), ub3-1+(maxStemLength-1), ub4-1);

    // scan the possibles values for the last variable
    for (int l = startL; l <= stopL; l++) {

      // find all possible values for the second variable
      startJ = max4(i+minStemLength-1, lb2, lb3-(maxLoopLength+1), l-(maxStemLength-1+maxLoopLength+1));
      stopJ = min4(i+maxStemLength-1, ub2-1, ub3-1-(minLoopLength+1), l-(minStemLength-1+minLoopLength+1));
      // find all possible values for the third variable
      startK = max4(i+minStemLength-1+minLoopLength+1, lb2+minLoopLength+1, lb3, l-(maxStemLength-1));
      stopK = min4(i+maxStemLength-1+maxLoopLength+1, ub2-1+maxLoopLength+1, ub3-1, l-(minStemLength-1));

      // if errors are allowed, with possible insertions
      if (insertion) {

        // set the base cases of the dynamic programming matrix
        for (int col = 0; col <= min2(l-startK+1, maxCost / interactionValues->getInsertionCost()); col++) {
          matrix[col] = col * interactionValues->getInsertionCost();
        }

        // fill in the matrix
        for (int lin = 1; lin <= stopJ-i+1; lin++) {
          if (lin - maxCost / interactionValues->getInsertionCost() > 1) {
            previous = matrix[lin - maxCost / interactionValues->getInsertionCost() - 1];
            matrix[lin - maxCost / interactionValues->getInsertionCost() - 1] = maxCost + 1;
          }
          else {
            previous = (lin-1) * interactionValues->getInsertionCost();
            matrix[0] = lin * interactionValues->getInsertionCost();
            isASupport = true;
          }

          for (int col = max2(1, lin-maxCost/interactionValues->getInsertionCost()); col <= min2(l-startK+1, lin+maxCost/interactionValues->getInsertionCost()); col++) {
            match = interactionValues->getMatchCost(sequence->getNucleotide(i+(lin-1)), sequence->getNucleotide(l-(col-1)));
            val1 = previous + match;
            if (col == lin+maxCost/interactionValues->getInsertionCost()) {
              val2 = maxCost+1;
            }
            else {
              val2 = matrix[col] + interactionValues->getInsertionCost();
            }
            val3 = matrix[col-1] + interactionValues->getInsertionCost();
            value = min3(val1, val2, val3);
            previous = matrix[col];
            matrix[col] = value;

            if (value <= maxCost) {
              // if a support has been found
              if ((i + lin-1 >= startJ) && (l-(col-1) <= stopK) && ((l-(col-1))-(i+(lin-1)) >= minLoopLength+1) && ((l-(col-1))-(i+(lin-1)) <= maxLoopLength+1)) {
                values[0] = i;
                values[1] = i+(lin-1);
                values[2] = l-(col-1);
                values[3] = l;
                return values;
              }
              // a score in the line was less than maxCost
              isASupport = true;
            }
          }
          // if no score in the line was less than maxCost
          if (!isASupport) {
            // select another value for the second variable
            break;
          }
        }
      }

      // if errors are allowed, with no insertion
      else {
        value = 0;

        // scan the possible values for the second and third variables
        for (int lin = 0, col = 0; ((i+lin <= stopJ) && (l-col >= startK)); lin++, col++) {
          match = interactionValues->getMatchCost(sequence->getNucleotide(i+lin), sequence->getNucleotide(l-col));
          value += match;
          if (value > maxCost) {
            break;
          }
          // if we have found a support
          if ((value <= maxCost) && (i + lin >= startJ) && (l-col <= stopK) && ((l-col)-(i+lin) >= minLoopLength+1) && ((l-col)-(i+lin) <= maxLoopLength+1)) {
            if (value <= maxCost) {
              if (values != NULL) {
                values[0] = i;
                values[1] = i+lin;
                values[2] = l-col;
                values[3] = l;
              }
              return values;
            }
          }
        }
      }
    }
  }
  return NULL;
}

void HardHelixSimple::revise () {
  for (int i = 0; i < 4; i++) {
    revise(i);
  }
}

void HardHelixSimple::revise (int v) {
  if ((!unaryConstraints[0]->isAssigned()) && (!unaryConstraints[1]->isAssigned()) && (!unaryConstraints[2]->isAssigned()) && ((!unaryConstraints[3]->isAssigned()))) {
    return;
  }
  reviseFromLb(v);
  reviseFromUb(v);
}

void HardHelixSimple::reviseFromLb (int v) {
  int ub = unaryConstraints[v]->getUb();
  int length, buffer[4], *values;
  DomainIterator di = unaryConstraints[v]->getDomainIterator();
  if (checkLbSupport(v)) {
    return;
  }
  lbSupports[v]->unset();

  for (int i = v; i < 3; i++) {
    length = ((i == 1)? minLoopLength+1: minStemLength-1);
    if (unaryConstraints[i+1]->getLb() - unaryConstraints[i]->getLb() < length) {
      unaryConstraints[i+1]->setLb(unaryConstraints[i]->getLb()+length);
    }
  }
  for (int i = v; i > 0; i--) {
    length = ((i == 2)? maxLoopLength+1: maxStemLength-1);
    if (unaryConstraints[i]->getLb() - unaryConstraints[i-1]->getLb() > length) {
      unaryConstraints[i-1]->setLb(unaryConstraints[i]->getLb()-length);
    }
  }
  for (; *di < ub; ++di) {
    values = isConsistent(v, *di, buffer);
    if (values != NULL) {
      for (int i = 0; i < 4; i++) {
        lbSupports[v]->setSupport(i, values[i]);
      }
      unaryConstraints[v]->setLb(*di);
      return;
    }
  }
  unaryConstraints[v]->setLb(ub);
}

void HardHelixSimple::reviseFromUb (int v) {
  int lb = unaryConstraints[v]->getLb();
  int length, buffer[4], *values;
  DomainIterator di = unaryConstraints[v]->getReverseDomainIterator();
  if (checkUbSupport(v)) {
    return;
  }
  ubSupports[v]->unset();

  for (int i = v; i > 0; i--) {
    length = ((i == 2)? minLoopLength+1: minStemLength-1);
    if (unaryConstraints[i]->getUb() - unaryConstraints[i-1]->getUb() < length) {
      unaryConstraints[i-1]->setUb(unaryConstraints[i]->getUb()-length);
    }
  }
  for (int i = v; i < 3; i++) {
    length = ((i == 1)? maxLoopLength+1: maxStemLength-1);
    if (unaryConstraints[i+1]->getUb() - unaryConstraints[i]->getUb() > length) {
      unaryConstraints[i+1]->setUb(unaryConstraints[i]->getUb()+length);
    }
  }
  for (; *di >= lb; --di) {
    values = isConsistent(v, *di, buffer);
    if (values != NULL) {
      for (int i = 0; i < 4; i++) {
        ubSupports[v]->setSupport(i, values[i]);
      }
      unaryConstraints[v]->setUb(*di+1);
      return;
    }
  }
  unaryConstraints[v]->setUb(lb);
}

void HardHelixSimple::reviseFromAssignment (int v) {
  int lb = unaryConstraints[v]->getLb();
  int length, buffer[4], *values;
  if ((checkLbSupport(v)) || (checkUbSupport(v))) {
    return;
  }
  lbSupports[v]->unset();
  ubSupports[v]->unset();
  for (int i = v; i < 3; i++) {
    length = ((i == 1)? minLoopLength+1: minStemLength-1);
    if (unaryConstraints[i+1]->getLb() - unaryConstraints[i]->getLb() < length) {
      unaryConstraints[i+1]->setLb(unaryConstraints[i]->getLb()+length);
    }
  }
  for (int i = v; i > 0; i--) {
    length = ((i == 2)? maxLoopLength+1: maxStemLength-1);
    if (unaryConstraints[i]->getLb() - unaryConstraints[i-1]->getLb() > length) {
      unaryConstraints[i-1]->setLb(unaryConstraints[i]->getLb()-length);
    }
  }
  for (int i = v; i > 0; i--) {
    length = ((i == 2)? minLoopLength+1: minStemLength-1);
    if (unaryConstraints[i]->getUb() - unaryConstraints[i-1]->getUb() < length) {
      unaryConstraints[i-1]->setUb(unaryConstraints[i]->getUb()-length);
    }
  }
  for (int i = v; i < 3; i++) {
    length = ((i == 1)? maxLoopLength+1: maxStemLength-1);
    if (unaryConstraints[i+1]->getUb() - unaryConstraints[i]->getUb() > length) {
      unaryConstraints[i+1]->setUb(unaryConstraints[i]->getUb()+length);
    }
  }
  values = isConsistent(v, lb, buffer);
  if (values != NULL) {
    for (int i = 0; i < 4; i++) {
      lbSupports[v]->setSupport(i, values[i]);
    }
    return;
  }
  unaryConstraints[v]->setUb(lb);
}

bool HardHelixSimple::getConsistency (Support *s) {
  int val1 = s->getSupport(0);
  int val2 = s->getSupport(1);
  int val3 = s->getSupport(2);
  int val4 = s->getSupport(3);
  int buffer[4], *values;
  values = isConsistent(val1, val1+1, val2, val2+1, val3, val3+1, val4, val4+1, buffer);
  return (values != NULL);
}

bool HardHelixSimple::printSpace (int var, int place) {
  return (((var == 0) && (place == 0)) || ((var == 1) && (place == 1)) || ((var == 2) && (place == 0)) || ((var == 3) && (place == 1)));
}

void HardHelixSimple::getTrace (int start, int *values, bool **backtrace) {
  int m[maxStemLength+1][maxStemLength+1];
  int l1, l2;
  int val1, val2, val3;
  Nucleotide *n1, *n2;
  l1 = values[1] - values[0] + 1;
  l2 = values[3] - values[2] + 1;

  if (l1 <= 0) {
    return;
  }
  if (l2 <= 0) {
    return;
  }

  // fill the dynamic programming matrix
  for (int i = 0; i <= l1; i++) {
    m[i][0] = i * interactionValues->getInsertionCost();
  }
  for (int j = 1; j <= l2; j++) {
    m[0][j] = j * interactionValues->getInsertionCost();
  }
  for (int i = 1; i <= l1; i++) {
    n1 = sequence->getNucleotide(values[0] + (i-1));
    for (int j = 1; j <= l2; j++) {
      n2 = sequence->getNucleotide(values[3] - (j-1));
      val1 = m[i-1][j-1] + interactionValues->getMatchCost(n1, n2);
      val2 = m[i][j-1] + interactionValues->getInsertionCost();
      val3 = m[i-1][j] + interactionValues->getInsertionCost();
      m[i][j] = min3(val1, val2, val3);
    }
  }

  // retrieve the trace
  int i = l1;
  int j = l2;
  while ((i != 0) || (j != 0)) {
    if ((i > 0)? (m[i][j] == m[i-1][j] + interactionValues->getInsertionCost()): false) {
      i--;
    }
    else if ((j > 0)? (m[i][j] == m[i][j-1] + interactionValues->getInsertionCost()): false) {
      j--;
    }
    else {
      if (m[i][j] == m[i-1][j-1]) {
        backtrace[values[0] + (i-1) - start][values[3] - (j-1) - start] = true;
      }
      i--;
      j--;
    }
  }
}

