// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef ABSSCONS_H_INCLUDED
#define ABSSCONS_H_INCLUDED

#include "abstractConstraint.h"
#include "top.h"
#include "cost.h"
#include "errorsToCosts.h"


/**
 * Provide the minimum features to model a soft constraint.
 * Contains basic cost propagations (with no correlation with other constraints),
 *    supports, safe backtracking features.
 */
class AbstractSoftConstraint : public AbstractConstraint {

protected:
  /**
   * the Cost that stores the information (mem) about this constraint
   */
  Cost *cost;

  /**
   * a support currently used by the constraint
   */
  Support *currentSupport;

  /**
   * a mapping from errors to costs
   */
  ErrorsToCosts *errorsToCosts;


public:

  /**
   * constructor
   * @param i the index
   * @param e2c a mapping from errors to costs
   * @param nv the number of variables
   */
  AbstractSoftConstraint (int i, ErrorsToCosts *e2c, int nv = 0);

  /**
   * destructor
   */
  virtual ~AbstractSoftConstraint ();

  /**
   * prepare the constraint for a new search
   */
  virtual void reset ();

  /**
   * notify a variable has been assigned to NaryCost
   * @param v the variable that has been assigned
   * @param val the value the variable is assigned to
   */
  virtual void assign (Variable *v, int val);
  /**
   * notify a variable has been discretized to NaryCost
   * @param v the variable that has been discretized
   */
  virtual void setDiscretized (Variable *v);
  /**
   * get the memory of the cost -- what has been projected directly to the zero arity constraint (ask to cost)
   * @return the cost
   */
	virtual int getMem ();
  /**
   * set the memory of the cost -- what has been projected directly to the zero arity constraint (ask to cost)
   * @return the new cost
   */
	virtual void setMem (int m);
  /**
   * increase the memory of the cost -- what has been projected directly to the zero arity constraint (ask to cost)
   * @return the increase
   */
	virtual void incMem (int m);
  /**
   * decrease the memory of the cost -- what has been projected directly to the zero arity constraint (ask to cost)
   * @return the decrease
   */
	virtual void decMem (int m);
  /**
   * @pre the specified variable is discretized
   * 
   * get a memory of the cost -- what has been projected from the constraint to an element of a variable (ask to Cost)
   * @param v the variable
   * @param val the element of the variable
   * @return the cost
   */
  virtual int getMemProfile (Variable *v, int val);
  /**
   * @pre the specified variable is discretized
   * 
   * set a memory of the cost -- what has been projected from the constraint to an element of a variable (ask to Cost)
   * @param v the variable
   * @param value the element of the variable
   * @param m the new memory
   */
  virtual void setMemProfile (Variable *v, int value, int m);
  /**
   * @pre the specified variable is discretized
   * 
   * increase a memory of the cost -- what has been projected from the constraint to an element of a variable (ask to Cost)
   * @param v the variable
   * @param value the element of the variable
   * @param m the increase
   */
  virtual void incMemProfile (Variable *v, int value, int m);
  /**
   * @pre the specified variable is discretized
   * 
   * decrease a memory of the cost -- what has been projected from the constraint to an element of a variable (ask to Cost)
   * @param v the variable
   * @param value the element of the variable
   * @param m the decrease
   */
  virtual void decMemProfile (Variable *v, int value, int m);
  /**
   * @pre the specified variable is not discretized
   *
   * get a memory of the cost -- what has been projected from the constraint to the lower bound of a variable (ask to Cost)
   * @param v the variable
   * @return the cost
   */
  virtual int getLbMem (Variable *v);
  /**
   * @pre the specified variable is not discretized
   *
   * get a memory of the cost -- what has been projected from the constraint to the upper bound of a variable (ask to Cost)
   * @param v the variable
   * @return the cost
   */
  virtual int getUbMem (Variable *v);
  /**
   * @warning Much slower than getMemProfile or getLbMem, should be used when the representation of the domain is not know.
   * 
   * get the memory projection of the given variable at the given value -- can manage with both discretized and undiscretized representations
   * @param v the index of the variable
   * @param val the value of the variable
   * @return the memory projection
   */
  virtual int getMemory (Variable *v, int val);
  /**
   * @pre the specified variable is not discretized
   *
   * set a memory of the cost -- what has been projected from the constraint to the lower bound of a variable (ask to Cost)
   * @param v the variable
   * @param m the new cost
   */
  virtual void setLbMem (Variable *v, int m);
  /**
   * @pre the specified variable is not discretized
   *
   * set a memory of the cost -- what has been projected from the constraint to the upper bound of a variable (ask to Cost)
   * @param v the variable
   * @param m the new cost
   */
  virtual void setUbMem (Variable *v, int m);
  /**
   * @pre the specified variable is not discretized
   *
   * increase a memory of the cost -- what has been projected from the constraint to the lower bound of a variable (ask to Cost)
   * @param v the variable
   * @param m the increase
   */
  virtual void incLbMem (Variable *v, int m);
  /**
   * @pre the specified variable is not discretized
   *
   * increase a memory of the cost -- what has been projected from the constraint to the upper bound of a variable (ask to Cost)
   * @param v the variable
   * @param m the increase
   */
  virtual void incUbMem (Variable *v, int m);
  /**
   * @pre the specified variable is not discretized
   *
   * decrease a memory of the cost -- what has been projected from the constraint to the lower bound of a variable (ask to Cost)
   * @param v the variable
   * @param m the decrease
   */
  virtual void decLbMem (Variable *v, int m);
  /**
   * @pre the specified variable is not discretized
   *
   * decrease a memory of the cost -- what has been projected from the constraint to the upper bound of a variable (ask to Cost)
   * @param v the variable
   * @param m the decrease
   */
  virtual void decUbMem (Variable *v, int m);
  /**
   * @pre the specified variable is not discretized
   *
   * set a memory of the cost to 0 -- what has been projected from the constraint to the lower bound of a variable (ask to Cost)
   * @param v the variable
   */
  virtual void resetLbMem (Variable *v);
  /**
   * @pre the specified variable is not discretized
   *
   * set a memory of the cost to 0 -- what has been projected from the constraint to the upper bound of a variable (ask to Cost)
   * @param v the variable
   */
  virtual void resetUbMem (Variable *v);
	/**
	 * get the minimum cost of this function (by default, @a mem to make it compliant with the zero arity constraint)
	 * @return the minimum
	 */
	virtual int getMinCost ();
  /**
   * @warning To be overloaded by any nary constraint
   * 
   * @pre The variable @ v should be discretized
   *
   * compute the minimum cost of the constraint, having that the specified variable is assigned to the specified element
   * @param v the specified variable
   * @param val the specified element
   * @return the minimum, 0 by default in this class
   */
  virtual int getMinCost (Variable *v, int val);
  /**
   * @warning To be overloaded by any nary constraint
   * 
   * @pre The variable @a v should be not discretized
   *
   * compute the minimum cost of the constraint, having that the specified variable is assigned to the lower bound
   * @param v the specified variable
   * @return the minimum, 0 by default in this class
   */
  virtual int getMinLbCost (Variable *v);
  /**
   * @warning To be overloaded by any nary constraint
   * 
   * @pre The variable @a v should be not discretized
   *
   * compute the minimum cost of the constraint, having that the specified variable is assigned to the upper bound
   * @param v the specified variable
   * @return the minimum, 0 by default in this class
   */
  virtual int getMinUbCost (Variable *v);
	/**
   * get the maximum cost of the constraint
	 * @return the maximum cost
	 */
	virtual int getMaxCost () const;
	/**
	 * whether the constraint is unconsistent (by default, @a mem is not less than the maximum cost), could be overloaded to work more accurately
	 * @return whether the constraint is unconsistent
	 */
	virtual bool isUnconsistent ();
  
  /**
   * get the current support
   * @return the support
   */
  virtual Support *getCurrentSupport ();
  /**
   * get the support of the zero-inversibility of the constraint (ask to Cost)
   * @return the support
   */
  virtual Support *getZeroSupport ();
  /**
   * get the support of the lower bound of a given variable (ask to Cost)
   * @param v the variable
   * @return the support
   */
  virtual Support *getLbSupport (Variable *v);
  /**
   * get the support of the upper bound of a given variable (ask to Cost)
   * @param v the variable
   * @return the support
   */
  virtual Support *getUbSupport (Variable *v);
  /**
   * get the support of a value of a given variable (ask to Cost)
   * @param v the variable
   * @param i the value
   * @return the support
   */
  virtual Support *getSupport (Variable *v, int i);
  /**
   * get the cost of the given support
   * @param s the support
   * @return the cost
   */
  virtual int getCost (Support *s);

  /**
   * check if the support previously computed is still valid (the variables' values are still between the bounds)
   * @param s the previously computed support
   * @return whether the support is still valid
   */
  virtual bool checkSupport (Support *s);

  /**
   * Get the maximum amount of cost projected on a unary constraint
   * @param var the variable on which the cost is projected
   * @return the cost
   */
  virtual int getMaxProjected (Variable *var);

  /**
   * get the contribution of this constraint to the global cost when all variables are assigned
   * @param values the values of the involved variables
   * @return the cost of this constraint
   */
  virtual int getFinalCost (int *values);
  /**
   * print the name of the constraint and its state
   * @return a string containing @a mem
   */
  virtual string print ();

  /**
   * make the Cost save the current values (@a mem)
   */
  virtual void save ();
  /**
   * make the Cost restore the current values (@a mem)
   */
  virtual void restore ();

  /**
   * the bounded sum (in a bounded interval structure)
   * @param c1 the first element of the sum
   * @param c2 the second element of the sum
   * @return the bounded sum
   */
  inline int sumCost (const int c1, const int c2) const {
    return min2(c1 + c2, getMaxCost()+1);
  }

  /**
   * the bounded difference (in a bounded interval structure)
   * @param c1 the first element of the difference
   * @param c2 the second element of the difference
   * @return the bounded difference
   */
  inline int diffCost (const int c1, const int c2) const {
    char msg[256];
    if ((c1 < 0) || (c2 < 0)) {
      sprintf(msg, "Error! Trying to subtract two costs, and one of them is negative (%i or %i)!", c1, c2);
      trace(1, msg);
    }
    if ((c1 < c2) && (c1 <= getMaxCost())) {
      sprintf(msg, "Error! Trying to get the difference between %i and %i (maxCost is %i)!", c1, c2, getMaxCost());
      trace(1, msg);
    }
    return ((c1 >= getMaxCost()+1)? getMaxCost()+1: c1-c2);
  }
};

#endif

