// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef REVISION_H_INCLUDED
#define REVISION_H_INCLUDED


/**
 * This class provides tools to handle revision mechanisms: stacking element, popping, cleaning the stack. An element is supposed to be an integer, ranging from 0 to s-1, where s is the size of the stack.
 * However there is no order in this so-called stack (no FIFO nor FILO).
 */
class Revision {

private:
  /**
   * the size of the stack
   */
  int size;
  /**
   * whether an element is present
   */
  bool *isPresent;
  /**
   * where to store additionnal valus attached to an element
   */
  int *values;
  /**
   * the cursor of the stack (works in a ring fashion)
   */
  int currentElement;
  /**
   * the number of elements currently stacked
   */
  int nbElements;


public:
  /**
   * constructor
   * @param s the size of the stack
   */
  Revision (int s);

  /**
   * add an element to the stack, possibly with an attached value
   * @param i the element
   * @param v the attached value
   */
  void addElement (int i, int v = -1);
  /**
   * get the element pointed by the cursor
   * @return the element
   */
  int getCurrentElement ();
  /**
   * get the value attached with the element pointed by the cursor
   * @return the value
   */
  int getCurrentValue ();
  /**
   * move the cursor to the next element in the stack
   */
  void getNext ();
  /**
   * remove the element pointed by the cursor
   */
  void removeCurrentElement ();
  /**
   * check whether there is no element left in the stack
   * @return true if there is no element left
   */
  bool isEmpty ();
  /**
   * stack all the elements
   */
  void setAll ();
  /**
   * unstack all the elements
   */
  void clear ();
};


#endif
