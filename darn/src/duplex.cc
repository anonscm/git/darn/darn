// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "duplex.h"


Duplex::Duplex (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int i, bool o, ErrorsToCosts *e2c, Sequence *s, InteractionValues *iv) : SoftConstraint(i, e2c, o, 4, v1, v2, v3, v4) {
  simple = false;
  interactionValues = iv;
  suffixArray = new SuffixArray(s, interactionValues);
  name = "duplex";
}

int Duplex::getMinCost () {
  return 0;
}

int Duplex::getMinCost (Variable *var, int val) {
  int i, lb1, lb2, lb3, ub3, lb4, ub4, result;
  if ((!unaryConstraints[0]->isAssigned()) || (!unaryConstraints[1]->isAssigned())) {
    return 0;
  }
  currentSupport->unset();
  i = getVariableIndex(var);
  lb1 = getVariable(0)->getUnaryConstraint()->getLb();
  lb2 = getVariable(1)->getUnaryConstraint()->getLb();
  lb3 = getVariable(2)->getUnaryConstraint()->getLb();
  ub3 = getVariable(2)->getUnaryConstraint()->getUb();
  lb4 = getVariable(3)->getUnaryConstraint()->getLb();
  ub4 = getVariable(3)->getUnaryConstraint()->getUb();
  switch (i) {
    case 2:
      lb3 = val;
      ub3 = val+1;
      break;
    case 3:
      lb4 = val;
      ub4 = val+1;
      break;
  }
  result = computeResult(lb1, lb2, lb3, ub3, lb4, ub4);
  return result;
}

int Duplex::getMinLbCost (Variable *var) {
  if ((!unaryConstraints[0]->isAssigned()) || (!unaryConstraints[1]->isAssigned())) {
    return 0;
  }
  //return min2(getMaxCost()-getLbMem(var)+1, getMinCost(var, var->getUnaryConstraint()->getLb()));
  return getMinCost(var, var->getUnaryConstraint()->getLb());
}

int Duplex::getMinUbCost (Variable *var) {
  if ((!unaryConstraints[0]->isAssigned()) || (!unaryConstraints[1]->isAssigned())) {
    return 0;
  }
  //return min2(getMaxCost()-getUbMem(var)+1, getMinCost(var, var->getUnaryConstraint()->getUb()-1));
  return getMinCost(var, var->getUnaryConstraint()->getUb()-1);
}

int Duplex::computeResult (int lb1, int lb2, int lb3, int ub3, int lb4, int ub4) {
  int min = getMaxCost()+1;
  int value = 0, valueLb3 = 0, valueUb3 = 0, valueLb4 = 0, valueUb4 = 0, valueLbLb = 0, valueLbUb = 0, valueUbLb = 0, valueUbUb = 0;
  int cost;
  int mem1, mem2, mem3, mem4, lbMem3, ubMem3, lbMem4, ubMem4, mem, currentMem;
  int startI, stopI, startJ, stopJ, startK, stopK, startL, stopL;
  Sequence *s;
  currentSupport->unset();

  if ((lb3 > ub3) || (lb4 > ub4)) {
    return getMaxCost()+1;
  }
  
  if ((ub4-1) - lb3 + (errorsToCosts->getMaxErrors()+1)*interactionValues->getMaxCost() < lb4 - (ub3-1) - (errorsToCosts->getMaxErrors()+1)*interactionValues->getMaxCost()) {
    return getMaxCost()+1;
  }
  if (lb4 - (ub3-1) - (errorsToCosts->getMaxErrors()+1)*interactionValues->getMaxCost() > (ub4-1) - lb3 + (errorsToCosts->getMaxErrors()+1)*interactionValues->getMaxCost()) {
    return getMaxCost()+1;
  }

  startI = max2(lb1, lb2 - ((ub4-1) - lb3) - (errorsToCosts->getMaxErrors()+1)*interactionValues->getMaxCost());
  stopI = min2(lb1, lb2 - (lb4 - (ub3-1)) + (errorsToCosts->getMaxErrors()+1)*interactionValues->getMaxCost());
  if (startI > stopI) {
    return getMaxCost()+1;
  }
  startJ = max3(lb2, lb1-1, lb1 + (lb4 - (ub3-1)) - (errorsToCosts->getMaxErrors()+1)*interactionValues->getMaxCost())+1;
  stopJ = min2(lb2, lb1 + ((ub4-1) - lb3) + (errorsToCosts->getMaxErrors()+1)*interactionValues->getMaxCost())+1;
  if (startJ > stopJ) {
    return getMaxCost()+1;
  }

  if (lb1 > lb2) {
    return getMaxCost()+1;
  }

  if ((!unaryConstraints[0]->isAssigned()) || (!unaryConstraints[1]->isAssigned())) {
    return 0;
  }
  mem = getMem();
  mem1 = getMemory(variables[0], lb1);
  mem2 = getMemory(variables[1], lb2);
  currentMem = mem + mem1 + mem2;
  if (currentMem > getMaxCost()) {
    return getMaxCost()+1;
  }

  s = sequence->getSubSequence(lb1, lb2-lb1+1);

  if ((ub3-lb3 == 1) && (ub4 - lb4 == 1)) {
    mem3 = getMemory(variables[2], lb3);
    mem4 = getMemory(variables[3], lb4);
    currentMem = mem + mem1 + mem2 + mem3 + mem4;
    if (currentMem > getMaxCost()) {
      delete s;
      return getMaxCost()+1;
    }
    value = suffixArray->lookForApproximate(s, lb3, lb4+1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-currentMem));
    cost = errorsToCosts->getCostFor(value);
    if (cost+currentMem <= getMaxCost()) {
      currentSupport->setSupport(0, lb1);
      currentSupport->setSupport(1, lb2);
      currentSupport->setSupport(2, lb3);
      currentSupport->setSupport(3, lb4);
      delete s;
      return cost-currentMem;
    }
    else {
      delete s;
      return getMaxCost()+1;
    }
  }

  if ((!unaryConstraints[2]->isDiscretized()) && (!unaryConstraints[3]->isDiscretized())) {
    lbMem3 = getMemory(variables[2], lb3);
    ubMem3 = getMemory(variables[2], ub3-1);
    lbMem4 = getMemory(variables[3], lb4);
    ubMem4 = getMemory(variables[3], ub4-1);
    min = getMaxCost()+1;

    if ((ub3-lb3 > 2) && (ub4-lb4 > 2)) {
      value = suffixArray->lookForApproximate(s, lb3+1, ub3-2, lb4+2, ub4-1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-currentMem));
      cost = errorsToCosts->getCostFor(value);
      if (cost+currentMem <= getMaxCost()) {
        min = cost - currentMem;
        currentSupport->unset();
        if (min == 0) {
          delete s;
          return 0;
        }
      }
    }
    if (ub4-lb4 > 2) {
      if (currentMem + lbMem3 <= getMaxCost()) {
        valueLb3 = suffixArray->lookForApproximate(s, lb3, lb3, lb4+2, ub4-1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-(lbMem3+currentMem)));
        cost = errorsToCosts->getCostFor(valueLb3);
        if ((cost+lbMem3+currentMem <= getMaxCost()) && (cost-(lbMem3+currentMem) < min)) {
          min = cost-(lbMem3+currentMem);
          currentSupport->unset();
          if (min == 0) {
            delete s;
            return 0;
          }
        }
      }
      if (currentMem + ubMem3 <= getMaxCost()) {
        valueUb3 = suffixArray->lookForApproximate(s, ub3-1, ub3-1, lb4+2, ub4-1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-(ubMem3+currentMem)));
        cost = errorsToCosts->getCostFor(valueUb3);
        if ((cost+ubMem3+currentMem <= getMaxCost()) && (cost-(ubMem3+currentMem) < min)) {
          min = cost-(ubMem3+currentMem);
          currentSupport->unset();
          if (min == 0) {
            delete s;
            return 0;
          }
        }
      }
    }
    if (ub3-lb3 > 2) {
      if (currentMem + lbMem4 <= getMaxCost()) {
        valueLb4 = suffixArray->lookForApproximate(s, lb3+1, ub3-2, lb4+1, lb4+1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-(lbMem4+currentMem)));
        cost = errorsToCosts->getCostFor(valueLb4);
        if ((cost+lbMem4+currentMem <= getMaxCost()) && (cost-(lbMem4+currentMem) < min)) {
          min = cost-(lbMem4+currentMem);
          currentSupport->unset();
          if (min == 0) {
            delete s;
            return 0;
          }
        }
      }
      if (currentMem + ubMem4 <= getMaxCost()) {
        valueUb4 = suffixArray->lookForApproximate(s, lb3+1, ub3-2, ub4, ub4, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-(ubMem4+currentMem)));
        cost = errorsToCosts->getCostFor(valueUb4);
        if ((cost+ubMem4+currentMem <= getMaxCost()) && (cost-(ubMem4+currentMem) < min)) {
          min = cost-(ubMem4+currentMem);
          currentSupport->unset();
          if (min == 0) {
            delete s;
            return 0;
          }
        }
      }
    }

    if (currentMem + lbMem3 + lbMem4 <= getMaxCost()) {
      valueLbLb = suffixArray->lookForApproximate(s, lb3, lb4+1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-(lbMem3+lbMem4+currentMem)));
      cost = errorsToCosts->getCostFor(valueLbLb);
      if ((cost+lbMem3+lbMem4+currentMem <= getMaxCost()) && (cost-(lbMem3+lbMem4+currentMem) < min)) {
        min = cost-(lbMem3+lbMem4+currentMem);
        currentSupport->setSupport(0, lb1);
        currentSupport->setSupport(1, lb2);
        currentSupport->setSupport(2, lb3);
        currentSupport->setSupport(3, lb4);
        if (min == 0) {
          delete s;
          return 0;
        }
      }
    }
    if (currentMem + lbMem3 + ubMem4 <= getMaxCost()) {
      valueLbUb = suffixArray->lookForApproximate(s, lb3, ub4, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-(lbMem3+ubMem4+currentMem)));
      cost = errorsToCosts->getCostFor(valueLbUb);
      if ((cost+lbMem3+ubMem4+currentMem <= getMaxCost()) && (cost-(lbMem3+ubMem4+currentMem) < min)) {
        min = cost-(lbMem3+ubMem4+currentMem);
        currentSupport->setSupport(0, lb1);
        currentSupport->setSupport(1, lb2);
        currentSupport->setSupport(2, lb3);
        currentSupport->setSupport(3, ub4-1);
        if (min == 0) {
          delete s;
          return 0;
        }
      }
    }
    if (currentMem + ubMem3 + lbMem4 <= getMaxCost()) {
      valueUbLb = suffixArray->lookForApproximate(s, ub3-1, lb4+1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-(ubMem3+lbMem4+currentMem)));
      cost = errorsToCosts->getCostFor(valueUbLb);
      if ((cost+ubMem3+lbMem4+currentMem <= getMaxCost()) && (cost-(ubMem3+lbMem4+currentMem) < min)) {
        min = cost-(ubMem3+lbMem4+currentMem);
        currentSupport->setSupport(0, lb1);
        currentSupport->setSupport(1, lb2);
        currentSupport->setSupport(2, ub3-1);
        currentSupport->setSupport(3, lb4);
        if (min == 0) {
          delete s;
          return 0;
        }
      }
    }
    if (currentMem + ubMem3 + ubMem4 + currentMem <= getMaxCost()) {
      valueUbUb = suffixArray->lookForApproximate(s, ub3-1, ub4, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-(ubMem3+ubMem4+currentMem)));
      cost = errorsToCosts->getCostFor(valueUbUb);
      if ((cost+ubMem3+ubMem4+currentMem <= getMaxCost()) && (cost-(ubMem3+ubMem4+currentMem) < min)) {
        min = cost-(ubMem3+ubMem4+currentMem);
        currentSupport->setSupport(0, lb1);
        currentSupport->setSupport(1, lb2);
        currentSupport->setSupport(2, ub3-1);
        currentSupport->setSupport(3, ub4-1);
        if (min == 0) {
          delete s;
          return 0;
        }
      }
    }
    delete s;
    return min;
  }
  if ((unaryConstraints[2]->isDiscretized()) && (!unaryConstraints[3]->isDiscretized())) {
    min = getMaxCost()+1;
    if (ub4-lb4 == 1) {
      lbMem4 = getMemory(variables[3], lb4);
      DomainIterator di3 = unaryConstraints[2]->getDomainIterator();
      for (int i; *di3 < ub3; ++di3) {
        i = *di3;
        if (i >= lb3) {
          mem3 = getMemory(variables[2], i);
          currentMem = mem + mem1 + mem2 + mem3 + lbMem4;
          if (currentMem <= getMaxCost()) {
            valueLb4 = suffixArray->lookForApproximate(s, i, lb4+1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-currentMem));
            cost = errorsToCosts->getCostFor(valueLb4);
            if ((cost+currentMem <= getMaxCost()) && (cost-currentMem < min)) {
              min = cost-currentMem;
              currentSupport->setSupport(0, lb1);
              currentSupport->setSupport(1, lb2);
              currentSupport->setSupport(2, i);
              currentSupport->setSupport(3, lb4);
              if (min == 0) {
                delete s;
                return 0;
              }
            }
          }
        }
      }
      delete s;
      return min;
    }
    if (ub4-lb4 == 2) {
      lbMem4 = getMemory(variables[3], lb4);
      ubMem4 = getMemory(variables[3], ub4-1);
      DomainIterator di3 = unaryConstraints[2]->getDomainIterator();
      for (int i; *di3 < ub3; ++di3) {
        i = *di3;
        if (i >= lb3) {
          mem3 = getMemory(variables[2], i);
          currentMem = mem + mem1 + mem2 + mem3 + lbMem4;
          if (currentMem <= getMaxCost()) {
            valueLb4 = suffixArray->lookForApproximate(s, i, lb4+1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-currentMem));
            cost = errorsToCosts->getCostFor(valueLb4);
            if ((cost+currentMem <= getMaxCost()) && (cost-currentMem < min)) {
              min = cost-currentMem;
              currentSupport->setSupport(0, lb1);
              currentSupport->setSupport(1, lb2);
              currentSupport->setSupport(2, i);
              currentSupport->setSupport(3, lb4);
              if (min == 0) {
                delete s;
                return 0;
              }
            }
          }
          currentMem = mem + mem1 + mem2 + mem3 + ubMem4;
          if (currentMem <= getMaxCost()) {
            valueUb4 = suffixArray->lookForApproximate(s, i, ub4, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-currentMem));
            cost = errorsToCosts->getCostFor(valueUb4);
            if ((cost+currentMem <= getMaxCost()) && (cost-currentMem < min)) {
              min = cost-currentMem;
              currentSupport->setSupport(0, lb1);
              currentSupport->setSupport(1, lb2);
              currentSupport->setSupport(2, i);
              currentSupport->setSupport(3, ub4-1);
              if (min == 0) {
                delete s;
                return 0;
              }
            }
          }
        }
      }
      delete s;
      return min;
    }
    lbMem4 = getMemory(variables[3], lb4);
    ubMem4 = getMemory(variables[3], ub4-1);
    DomainIterator di3 = unaryConstraints[2]->getDomainIterator();
    for (int i; *di3 < ub3; ++di3) {
      i = *di3;
      if (i >= lb3) {
        mem3 = getMemory(variables[2], i);
        currentMem = mem + mem1 + mem2 + mem3 + lbMem4;
        if (currentMem <= getMaxCost()) {
          valueLb4 = suffixArray->lookForApproximate(s, i, lb4+1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-currentMem));
          cost = errorsToCosts->getCostFor(valueLb4);
          if ((cost+currentMem <= getMaxCost()) && (cost-currentMem < min)) {
            min = cost-currentMem;
            currentSupport->setSupport(0, lb1);
            currentSupport->setSupport(1, lb2);
            currentSupport->setSupport(2, i);
            currentSupport->setSupport(3, lb4);
            if (min == 0) {
              delete s;
              return 0;
            }
          }
        }
        currentMem = mem + mem1 + mem2 + mem3 + ubMem4;
        if (currentMem <= getMaxCost()) {
          valueUb4 = suffixArray->lookForApproximate(s, i, ub4, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-currentMem));
          cost = errorsToCosts->getCostFor(valueUb4);
          if ((cost+currentMem <= getMaxCost()) && (cost-currentMem < min)) {
            min = cost-currentMem;
            currentSupport->setSupport(0, lb1);
            currentSupport->setSupport(1, lb2);
            currentSupport->setSupport(2, i);
            currentSupport->setSupport(3, ub4-1);
            if (min == 0) {
              delete s;
              return 0;
            }
          }
        }
        currentMem = mem + mem1 + mem2 + mem3;
        if (currentMem <= getMaxCost()) {
          value = suffixArray->lookForApproximate(s, i, i, lb4+2, ub4-1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-currentMem));
          cost = errorsToCosts->getCostFor(value);
          if ((cost+currentMem <= getMaxCost()) && (cost-currentMem < min)) {
            min = cost-currentMem;
            currentSupport->unset();
            if (min == 0) {
              delete s;
              return 0;
            }
          }
        }
      }
    }
    delete s;
    return min;
  }
  if ((!unaryConstraints[2]->isDiscretized()) && (unaryConstraints[3]->isDiscretized())) {
    min = getMaxCost()+1;
    if (ub3-lb3 == 1) {
      lbMem3 = getMemory(variables[2], lb3);
      DomainIterator di4 = unaryConstraints[3]->getDomainIterator();
      for (int i; *di4 < ub4; ++di4) {
        i = *di4;
        if (i >= lb4) {
          mem4 = getMemory(variables[3], i);
          currentMem = mem + mem1 + mem2 + lbMem3 + mem4;
          if (currentMem <= getMaxCost()) {
            valueLb3 = suffixArray->lookForApproximate(s, lb3, i+1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-currentMem));
            cost = errorsToCosts->getCostFor(valueLb3);
            if ((cost+currentMem <= getMaxCost()) && (cost-currentMem < min)) {
              min = cost-currentMem;
              currentSupport->setSupport(0, lb1);
              currentSupport->setSupport(1, lb2);
              currentSupport->setSupport(2, lb3);
              currentSupport->setSupport(3, i);
              if (min == 0) {
                delete s;
                return 0;
              }
            }
          }
        }
      }
      delete s;
      return min;
    }
    if (ub3-lb3 == 2) {
      lbMem3 = getMemory(variables[2], lb3);
      ubMem3 = getMemory(variables[2], ub3-1);
      DomainIterator di4 = unaryConstraints[3]->getDomainIterator();
      for (int i; *di4 < ub4; ++di4) {
        i = *di4;
        if (i >= lb4) {
          mem4 = getMemory(variables[3], i);
          currentMem = mem + mem1 + mem2 + lbMem3 + mem4;
          if (currentMem <= getMaxCost()) {
            valueLb3 = suffixArray->lookForApproximate(s, lb3, i+1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-currentMem));
            cost = errorsToCosts->getCostFor(valueLb3);
            if ((cost+currentMem <= getMaxCost()) && (cost-currentMem < min)) {
              min = cost-currentMem;
              currentSupport->setSupport(0, lb1);
              currentSupport->setSupport(1, lb2);
              currentSupport->setSupport(2, lb3);
              currentSupport->setSupport(3, i);
              if (min == 0) {
                delete s;
                return 0;
              }
            }
          }
          currentMem = mem + mem1 + mem2 + ubMem3 + mem4;
          if (currentMem <= getMaxCost()) {
            valueUb3 = suffixArray->lookForApproximate(s, ub3-1, i+1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-currentMem));
            cost = errorsToCosts->getCostFor(valueUb3);
            if ((cost+currentMem <= getMaxCost()) && (cost-currentMem < min)) {
              min = cost-currentMem;
              currentSupport->setSupport(0, lb1);
              currentSupport->setSupport(1, lb2);
              currentSupport->setSupport(2, ub3-1);
              currentSupport->setSupport(3, i);
              if (min == 0) {
                delete s;
                return 0;
              }
            }
          }
        }
      }
      delete s;
      return min;
    }
    lbMem3 = getMemory(variables[2], lb3);
    ubMem3 = getMemory(variables[2], ub3-1);
    DomainIterator di4 = unaryConstraints[3]->getDomainIterator();
    for (int i; *di4 < ub4; ++di4) {
      i = *di4;
      if (i >= lb4) {
        mem4 = getMemory(variables[3], i);
        currentMem = mem + mem1 + mem2 + lbMem3 + mem4;
        if (currentMem <= getMaxCost()) {
          valueLb3 = suffixArray->lookForApproximate(s, lb3, i+1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-currentMem));
          cost = errorsToCosts->getCostFor(valueLb3);
          if ((cost+currentMem <= getMaxCost()) && (cost-currentMem < min)) {
            min = cost-currentMem;
            currentSupport->setSupport(0, lb1);
            currentSupport->setSupport(1, lb2);
            currentSupport->setSupport(2, lb3);
            currentSupport->setSupport(3, i);
            if (min == 0) {
              delete s;
              return 0;
            }
          }
        }
        currentMem = mem + mem1 + mem2 + ubMem3 + mem4;
        if (currentMem <= getMaxCost()) {
          valueUb3 = suffixArray->lookForApproximate(s, ub3-1, i+1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-currentMem));
          cost = errorsToCosts->getCostFor(valueUb3);
          if ((cost+currentMem <= getMaxCost()) && (cost-currentMem < min)) {
            min = cost-currentMem;
            currentSupport->setSupport(0, lb1);
            currentSupport->setSupport(1, lb2);
            currentSupport->setSupport(2, ub3-1);
            currentSupport->setSupport(3, i);
            if (min == 0) {
              delete s;
              return 0;
            }
          }
        }
        currentMem = mem + mem1 + mem2 + mem4;
        if (currentMem <= getMaxCost()) {
          value = suffixArray->lookForApproximate(s, lb3+1, ub3-2, i+1, i+1, errorsToCosts->getErrorsNotGreaterThan(getMaxCost()-currentMem));
          cost = errorsToCosts->getCostFor(value);
          if ((cost+currentMem <= getMaxCost()) && (cost-currentMem < min)) {
            min = cost-currentMem;
            currentSupport->unset();
            if (min == 0) {
              delete s;
              return 0;
            }
          }
        }
      }
    }
    delete s;
    return min;
  }

  s = sequence->getSubSequence(lb1, lb2);

  startK = max2(lb3, lb4 - (lb2 - lb1) - errorsToCosts->getMaxErrors());
  stopK = min2(ub3-1, (ub4-1) - (lb2 - lb1) + errorsToCosts->getMaxErrors());
  for (int k = startK; k <= stopK; k++) {
    mem3 = getMemProfile(variables[2], k);
    if (mem + mem1 + mem2 + mem3 > getMaxCost()) {
      continue;
    }

    startL = max3(lb4, k-1, k + (lb2 - lb1) - errorsToCosts->getMaxErrors())+1;
    stopL = min2(ub4-1, k + (lb2 - lb1) + errorsToCosts->getMaxErrors())+1;
    for (int l = startL; l <= stopL; l++) {
      mem4 = getMemProfile(variables[3], l-1);
      if (mem + mem1 + mem2 + mem3 + mem4 > getMaxCost()) {
        continue;
      }
      value = suffixArray->lookForApproximate(s, k, k, l, l, errorsToCosts->getErrorsNotGreaterThan(min+mem1+mem2+mem3+mem4+mem-1));
      cost = errorsToCosts->getCostFor(value);
      if (cost <= getMaxCost()) { 
        cost -= mem1+mem2+mem3+mem4+mem;
        if (cost < min) {
          min = cost;
          currentSupport->setSupport(0, lb1);
          currentSupport->setSupport(1, lb2);
          currentSupport->setSupport(2, k);
          currentSupport->setSupport(3, l-1);
        }
        if (min == 0) {
          delete s;
          return 0;
        }
      }
    }
  }
  delete s;
  return min;
}

int Duplex::getCost (Support *s) {
  int val1 = s->getSupport(0);
  int val2 = s->getSupport(1);
  int val3 = s->getSupport(2);
  int val4 = s->getSupport(3);
  return computeResult(val1, val2, val3, val3+1, val4, val4+1);
}

int Duplex::getFinalCost (int *values) {
  Sequence *s;
  int val;
  int val1 = values[0];
  int val2 = values[1];
  int val3 = values[2];
  int val4 = values[3];
  s = sequence->getSubSequence(val1, val2-val1+1);
  val = min2(getMaxCost()+1, errorsToCosts->getCostFor(suffixArray->lookForApproximate(s, val3, val4+1, errorsToCosts->getMaxErrors())));
  delete s;
  return val;
}
