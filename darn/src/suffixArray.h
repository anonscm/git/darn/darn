// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef SUFFARRAY_H_INCLUDED
#define SUFFARRAY_H_INCLUDED

#include "values.h"
#include "support.h"


/**
 * This simulates a stack of integer pairs with a fixed size array
 */
class FixedSizeStack {
private:
  /**
   * the size
   */
  unsigned int size;
  /**
   * the elements
   */
  Pair **elements;
  /**
   * the cursor (starts at 0)
   */
  unsigned int cursor;

public:
  /**
   * constructor
   * @param s the size of the stack
   */
  FixedSizeStack (unsigned int s) : size(s), cursor(0) {
    elements = new Pair*[size];
    for (unsigned int i = 0; i < size; i++) {
      elements[i] = new Pair();
    }
  }

  /**
   * push a new element in the stack
   * @param f the first integer
   * @param s the second integer
   */
  void push (int f, int s) {
    elements[cursor]->first = f;
    elements[cursor]->second = s;
    cursor++;
    if (cursor >= size) {
      trace(1, "Error! The stack has now an underflow!");
    }
  }

  /**
   * push a new element in the stack
   * @param s the new element
   */
  void push (Pair *s) {
    elements[cursor]->first = s->first;
    elements[cursor]->second = s->second;
    cursor++;
    if (cursor >= size) {
      trace(1, "Error! The stack has now an underflow!");
    }
  }

  /**
   * get the last element stacked
   * @return the top element
   */
  Pair *top () {
    if (cursor == 0) {
      trace(1, "Error! An empty stack is being red!");
    }
    return elements[cursor-1];
  }

  /**
   * forget the last element stacked
   */
  void pop () {
    if (cursor == 0) {
      trace(1, "Error! The stack has now an underflow!");
    }
    cursor--;
  }

  /**
   * forget all elements
   */
  void clear () {
    cursor = 0;
  }

  /**
   * check whether the stack is empty
   * @return true if the stack is empty
   */
  bool empty () {
    return (cursor == 0);
  }
};



/**
 * This is a l-interval of a suffix array
 */
struct Interval {
  /**
   * the lcp of the interval
   */
  int lcp;
  /**
   * the lower bound of the interval
   */
  int bot;
  /**
   * the upper bound of the interval
   */
  int top;
  /**
   * constructor
   * @param l the lcp
   * @param b the lower bound
   * @param t the upper bound
   */
  Interval (int l, int b, int t) : lcp(l), bot(b), top(t) {}
  /**
   * print a trace of the interval
   */
  void print () {cout << bot << " --\t(" << lcp << ")\t-- " << top << endl;}
};

/**
 * This is a l-interval of a suffix array, plus several values used by the search algorithm to store a configuration
 */
struct StoredInterval {
  /**
   * the lcp of the interval
   */
  int lcp;
  /**
   * the lower bound of the interval
   */
  int bot;
  /**
   * the upper bound of the interval
   */
  int top;
  /**
   * the number of characters of the word that are matched
   */
  int patternIndex;
  /**
   * the number of errors found so far
   */
  int nbErrors;
  /**
   * constructor
   * @param l the lcp
   * @param b the lower bound
   * @param t the upper bound
   * @param p the number of characters of the word that are matched
   * @param n the number of errors
   */
  StoredInterval (int b, int t, int l, int p, int n) : lcp(l), bot(b), top(t), patternIndex(p), nbErrors(n) {}
};

/**
 * An element of the suffix array
 */
struct Entry {
  /**
   * index where the suffix starts
   */
  int suf;
  /**
   * lowest common prefix
   */
  int lcp;
  /**
   * value used to find the next sibling l-interval
   */
  int next;
  /**
   * value used to find the first child l-interval
   */
  int up;
  /**
   * value used to find the first child l-interval
   */
  int down;
  /**
   * first index of where the suffix starts
   */
  int minIndex;
  /**
   * last index of where the suffix starts
   */
  int maxIndex;
  /**
   * number of letters of the word matched during the last exploration of the l-interval
   */
  int patternSize;
  /**
   * number of letters of the word matched during the last exploration of the leaf
   */
  int singlePatternSize;
  /**
   * number of errors found during the last exploration of the l-interval
   */
  int nbErrors;
  /**
   * number of errors found during the last exploration of the leaf
   */
  int singleNbErrors;
  /**
   * timestamp for the values related to the l-interval
   */
  int runs;
  /**
   * timestamp for the values related to the leaf
   */
  int singleRuns;
};


/**
 * The class implement a suffix array and several methods to find a word with errors
 */
class SuffixArray {

protected:
  /**
   * the suffix array itself
   */
  Entry** suffixArray;
  /**
   * the sequence used
   */
  Sequence *sequence;
  /**
   * the size of the sequence (and the suffix array)
   */
  unsigned int size;
  /**
   * a timestamp
   */
  int runs;
  /**
   * the maximum size of the words used (lcp and duplex)
   */
  int maxSize;
  /**
   * the matrix used to compute dynamic programming costs
   */
  int *matrix;
  /**
   * a stack that stores the candidates of @a getCandidates
   */
  FixedSizeStack *fsStack;
  /**
   * the interaction values
   */
  InteractionValues *interactionValues;

  /**
   * a radix sort, used to build the array
   * @param from the pre-sort
   * @param to the post-sort
   * @param text from the original text
   * @param n the size of the text
   * @param radix the number of different elements in the text (useful because the algorithm is recursive)
   */
  void radixPass(const int *from, int *to, const int *text, const int n, const int radix);
  /**
   * build the suffix array in linear time
   * @param text the text (converted into integers)
   * @param sa the temporary suffix array
   * @param n the size of the text
   * @param radix the number of different elements in the text (useful because the algorithm is recursive)
   */
  void buildSuffixArray(const int *text, int *sa, const int n, const int radix);
  /**
   * compare two strings lexicographically
   * @param s1 the first string
   * @param s2 the second string
   * @return 0 if both strings are equal, a positive number if the first string is smaller than the other, the number is the number of common letters
   */
  int stringCompare (string s1, string s2);
  /**
   * compare two pieces of strings lexicographically
   * @param s1 the first string
   * @param s2 the second string
   * @param from where the comparison should begin
   * @param to where the comparison should stop
   * @return 0 if both strings are equal, a positive number if the first string is smaller than the other, the number is the number of common letters
   */
  int stringCompare (string s1, string s2, int from, int to);
  /**
   * set the lcps of the intervals (set @a lcp)
   */
  void setLcps();
  /**
   * set the children of the interval (set @a up, @a down and @a next)
   */
  void setChildren();
  /**
   * set the interval of the indexes of the suffixes (set @a minIndex and @a maxIndex)
   */
  void setMinMax();
  /**
   * set the information concerning the exploration (set @a patternSize, @a nbError and @a runs)
   */
  void setRuns();

  /**
   * get the line of the suffix array that has the lcp of the interval
   * @param i the lower bound of the interval
   * @param j the lower bound of the interval
   * @return the line
   */
  int getLcpSuffix (int i, int j);
  /**
   * get the lcp of an interval
   * @param i the lower bound of the interval
   * @param j the lower bound of the interval
   * @return the lcp
   */
  int getLcp (int i, int j);
  /**
   * set the interval of the indexes of the suffixes
   * @param i the lower bound of the interval
   * @param j the lower bound of the interval
   * @return the interval
   */
  pair <int, int> setMinMax (int i, int j);
  /**
   * whether the intervals have a common element
   * @param lb1 the lower bound of the first interval
   * @param ub1 the upper bound of the first interval
   * @param lb2 the lower bound of the second interval
   * @param ub2 the upper bound of the second interval
   * @return whether the intervals have a common element
   */
  bool emptyJoin (int lb1, int ub1, int lb2, int ub2);
  /**
   * whether the interval contains the value
   * @param i the value
   * @param lb the lower bound of the interval
   * @param ub the upper bound of the interval
   * @return whether the interval contains the value
   */
  bool isInside (int i, int lb, int ub);
  /**
   * get the interval of the indexes of the suffixes
   * @param i the lower bound of the interval
   * @param j the lower bound of the interval
   * @return the interval
   */
  pair <int, int> getIndexInterval (int i, int j);
  /**
   * give a list of the prefixes of @a s2 that match @a s1 with less that @a nbMaxErrors errors;
   * if @a bothSides is set to true, also give the prefixes of @a s1 that match @a s2;
   * a element of the list contains the numbers of letters matched and the number of mismatches
   * @param s1 the first string
   * @param s2 the first string
   * @param nbMaxErrors the maximum number of errors
   * @param bothSides if both sides of the dynamic programming matrix is given as solutions
   */
  void getCandidates (Sequence *s1, Sequence *s2, int nbMaxErrors, bool bothSides);


public:
  /**
   * constructor, build the suffix array from the given text
   * @param s the text
   * @param iv the interaction values
   */
  SuffixArray (Sequence *s, InteractionValues *iv);
  /**
   * find an @a maxCost - approximation of @a pattern in the sub-text that is between @a lbStart and @a ubStart
   * @param pattern the word searched for
   * @param lbStart the leftmost index of the first letter of the word
   * @param ubStart the rightmost index of the first letter of the word
   * @param lbStop the leftmost index of the last letter (plus one) of the word
   * @param ubStop the rightmost index of the last letter (plus one) of the word
   * @param maxCost the maximum number of differences between the word and the sub-text
   * @param support the support where to store the position of the sub-text
   * @return true if such a sub-text exists
   */
  bool lookFor (Sequence *pattern, int lbStart, int ubStart, int lbStop, int ubStop, int maxCost, Support *support);
  /**
   * find all the @a maxCost - approximations of @a pattern in the sub-text that is between @a lbStart and @a ubStart
   * @param pattern the word searched for
   * @param lbStart the leftmost index of the first letter of the word
   * @param ubStart the rightmost index of the first letter of the word
   * @param lbStop the leftmost index of the last letter (plus one) of the word
   * @param ubStop the rightmost index of the last letter (plus one) of the word
   * @param maxCost the maximum number of differences between the word and the sub-text
   * @param values two intervals that contain the place where the first letter of the sub-text could be, and where the last letter of the sub-text could be
   */
  void lookForAll (Sequence *pattern, int lbStart, int ubStart, int lbStop, int ubStop, int maxCost, int *values);
  //int tmp (string pattern, int lbStart, int ubStart, int lbStop, int ubStop, int maxCost);
  /**
   * find a @a maxCost - approximations of @a pattern in the sub-text that is between @a lbStart and @a ubStart
   * @param pattern the word searched for
   * @param lbStart the leftmost index of the first letter of the word
   * @param ubStart the rightmost index of the first letter of the word
   * @param lbStop the leftmost index of the last letter (plus one) of the word
   * @param ubStop the rightmost index of the last letter (plus one) of the word
   * @param maxCost the maximum number of differences between the word and the sub-text
   * @return the minimum distance between a sub-text and the word search for
   */
  int lookForApproximate (Sequence *pattern, int lbStart, int ubStart, int lbStop, int ubStop, int maxCost);
  /**
   * check whether the sub-text beginning at @a lb and finishing at @a ub is a @a maxCost approximation of @a pattern
   * @param pattern the word searched for
   * @param lb the index of the first letter of the word
   * @param ub the index of the last letter (plus one) of the word
   * @param maxCost the maximum number of differences between the word and the sub-text
   * @return the minimum distance between the sub-text and the word
   */
  int lookForApproximate (Sequence *pattern, int lb, int ub, int maxCost);
  /**
   * print a trace of the suffix array
   * @return a trace of this suffix array
   */
  string print ();
};

#endif
