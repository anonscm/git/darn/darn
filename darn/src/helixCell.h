// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef HCELL_H_INCLUDED
#define HCELL_H_INCLUDED

/**
 * Little class describing a cell of a matrix (that uses backtrace)
 */
struct HelixCell {
  /**
   * the value of the cell
   */
  int value;
  /**
   * the x-axis of the backtrace (first element of the trace)
   */
  int rootI;
  /**
   * the y-axis of the backtrace (first element of the trace)
   */
  int rootJ;
  /**
   * Constructor, only set the value (the backtrace are set to dummy values)
   * @a param v the value
   */
  HelixCell (const int v) : value(v), rootI(-1), rootJ(-1) {}
  /**
   * set all the values of the cell
   * @a param v the value
   * @a i the x-axis of the backtrace
   * @a j the y-axis of the backtrace
   */
  void setValues (const int v, const int i, const int j) {value = v; rootI = i; rootJ = j;}
  /**
   * set all the backtrace values of the cell
   * @a i the x-axis of the backtrace
   * @a j the y-axis of the backtrace
   */
  void setValues (const int i, const int j) {rootI = i; rootJ = j;}
};

#endif
