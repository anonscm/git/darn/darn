// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "hardPattern2.h"


HardPattern2::HardPattern2 (Variable *v1, Variable *v2, int i, string s, int mc, MatchingValues *mv) : HardConstraint(i, mc, 2, v1, v2) {
  sequence = v1->getSequence();
  pattern = new Sequence(s, false);
  reversePattern = pattern->getReverse();
  for (unsigned int i = 0; i < pattern->getLength(); i++) {
    if (!pattern->getNucleotide(i)->isAmbiguous()) {
      ++hardness;
    }
  }
  matchingValues = mv;
  directAutomaton = new Automaton(pattern, maxCost, matchingValues);
  reverseAutomaton = new Automaton(reversePattern, maxCost, matchingValues);
  automatons[DIRECT] = directAutomaton;
  automatons[REVERSE] = reverseAutomaton;
  name = "hard pattern";
}

bool HardPattern2::isConsistent (int i1, int i2) {
  Sequence *s = sequence->getSubSequence(i1, i2-i1+1);
  int score = getEditSubstitutionDistance(pattern, s, matchingValues);
  delete s;
  return (score < maxCost+1);
}

pair<int, int> HardPattern2::findFirstConsistent (int lb1, int ub1, int lb2, int ub2, int direction) {
  int start, stop;
  int score;
  int extraCost;
  // we scan the sequence from left to right
  if (direction == DIRECT) {
    start = max2(lb1, lb2 - (pattern->getLength()-1 + (maxCost+1)/matchingValues->getInsertionCost()));
    stop = min2(ub2-1, (ub1-1) + (pattern->getLength()-1 + (maxCost+1)/matchingValues->getInsertionCost()));

    // if the pattern is an acceptable approximation of the empty word
    if ((start >= lb2) && (start <= ub2-1) && (static_cast<int>(pattern->getLength()) * matchingValues->getInsertionCost() < maxCost+1)) {
      return pair<int, int>(start, start);
    }

    // we scan the text
    automatons[DIRECT]->reset();
    for (int i = start; i <= stop; i++) {

      // the word may not start outside the allowed bounds
      extraCost = ((i > ub1-1)? maxCost+1: 0);
      automatons[DIRECT]->setOffset(extraCost, i);
      
      automatons[DIRECT]->addLetter(sequence->getNucleotide(i));
      score = automatons[DIRECT]->getScore();

      // if we have found an acceptable approximation of the pattern
      if ((i >= lb2) && (i <= ub2-1) && (score < maxCost+1)) {
        return pair<int, int>(automatons[DIRECT]->getSupport(), i);
      }
    }
    return pair<int, int>(NOT_FOUND, NOT_FOUND);
  }
  // we scan the sequence from right to left
  else {
    start = min2(ub2-1, ub1-1 + (pattern->getLength()-1 + (maxCost+1)/matchingValues->getInsertionCost()));
    stop = max2(lb1, lb2 - (pattern->getLength()-1 + (maxCost+1)/matchingValues->getInsertionCost()));

    // if the pattern is an acceptable approximation of the empty word
    if ((lb1 <= start) && (start <= ub1-1) && (static_cast<int>(pattern->getLength()) * matchingValues->getInsertionCost() < maxCost+1)) {
      return pair<int, int>(start, start);
    }

    // we scan the text
    automatons[REVERSE]->reset();
    for (int i = start; i >= stop; i--) {

      // the word may not start outside the allowed bounds
      extraCost = ((i < lb2)? maxCost+1: 0);
      automatons[REVERSE]->setOffset(extraCost, i);
      
      automatons[REVERSE]->addLetter(sequence->getNucleotide(i));
      score = automatons[REVERSE]->getScore();

      // if we have found an acceptable approximation of the pattern
      if ((lb1 <= i) && (i <= ub1-1) && (score < maxCost+1)) {
        return pair<int, int>(i, automatons[REVERSE]->getSupport());
      }
    }
    return pair<int, int>(NOT_FOUND, NOT_FOUND);
  }
}

pair<int, int> HardPattern2::findFirstLastConsistent (int lb1, int ub1, int lb2, int ub2, int direction) {
  int start, stop;
  int firstLetterStop;
  int lastLetterStart, lastLetterStop;
  int firstConsistent = NOT_FOUND, lastConsistent = NOT_FOUND;
  int score;
  int extraCost;
  bool supportFound = false;
  // we scan the sequence from left to right
  if (direction == DIRECT) {
    start = max2(lb1, lb2 - (pattern->getLength()-1 + (maxCost+1)/matchingValues->getInsertionCost()));
    stop = min2(ub2-1, (ub1-1) + (pattern->getLength()-1 + maxCost / matchingValues->getInsertionCost()));
    firstLetterStop = ub1-1;
    lastLetterStart = lb2;
    lastLetterStop = ub2-1;

    // if the pattern is an accepted approximation of the empty word
    if ((lastLetterStart <= start) && (start <= lastLetterStop) && (static_cast<int>(pattern->getLength()) * matchingValues->getInsertionCost() < maxCost+1)) {
      firstConsistent = start;
      lastConsistent = firstConsistent;
      supportFound = true;
    }

    automatons[DIRECT]->reset();

    // start the scan
    for (int i = start; i <= stop; i++) {

      // the word may not start outside the allowed bounds
      extraCost = ((i > firstLetterStop)? maxCost+1: 0);
      automatons[DIRECT]->setOffset(extraCost, i);
      
      automatons[DIRECT]->addLetter(sequence->getNucleotide(i));
      score = automatons[DIRECT]->getScore();

      // if we have found an acceptable approximation of the pattern
      if ((i >= lastLetterStart) && (i <= lastLetterStop) && (score < maxCost+1)) {
        // we update the supports
        if (!supportFound) {
          firstConsistent = i;
          lastConsistent = firstConsistent;
          supportFound = true;
        }
        else {
          lastConsistent = i;
        }
      }
    }
    return pair<int, int>(firstConsistent, lastConsistent);
  }
  // we scan the sequence from right to left
  else {
    start = min2(ub2-1, ub1-1 + (pattern->getLength()-1 + (maxCost+1)/matchingValues->getInsertionCost()));
    stop = max2(lb1, lb2 - (pattern->getLength()-1 + maxCost / matchingValues->getInsertionCost()));
    firstLetterStop = lb2;
    lastLetterStart = ub1-1;
    lastLetterStop = lb1;

    // if the pattern is an accepted approximation of the empty word
    if ((lastLetterStop <= start) && (start <= lastLetterStart) && (static_cast<int>(pattern->getLength()) * matchingValues->getInsertionCost() < maxCost+1)) {
      firstConsistent = start;
      lastConsistent = firstConsistent;
      supportFound = true;
    }

    automatons[REVERSE]->reset();

    // start the scan
    for (int i = start; i >= stop; i--) {

      // the word may not start outside the allowed bounds
      extraCost = ((i < firstLetterStop)? maxCost+1: 0);
      automatons[REVERSE]->setOffset(extraCost, i);
      
      automatons[REVERSE]->addLetter(sequence->getNucleotide(i));
      score = automatons[REVERSE]->getScore();

      // if we have found an acceptable approximation of the pattern
      if ((lastLetterStop <= i) && (i <= lastLetterStart) && (score < maxCost+1)) {
        // we update the supports
        if (!supportFound) {
          firstConsistent = i;
          lastConsistent = firstConsistent;
          supportFound = true;
        }
        else {
          lastConsistent = i;
        }
      }
    }
    return pair<int, int>(lastConsistent, firstConsistent);
  }
}

void HardPattern2::revise () {
  revise(0);
  revise(1);
}

void HardPattern2::revise (int v) {
  reviseFromLb(v);
  reviseFromUb(v);
}

void HardPattern2::reviseFromAssignment (int v) {
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  int newLb, newUb;
  pair<int, int> result;

  if (checkLbSupport(v) || checkUbSupport(v)) {
    return;
  }

  lbSupports[0]->unset();
  ubSupports[0]->unset();
  lbSupports[1]->unset();
  ubSupports[1]->unset();

  // update the bounds of the other variable, using simple distance deductions
  if (v == 0) {
    // we know where the word starts, we shrink the end positions domain
    if (lb2 < lb1 + static_cast<int>(pattern->getLength()-1) - maxCost / matchingValues->getInsertionCost()) {
      lb2 = lb1 + (pattern->getLength()-1) - maxCost / matchingValues->getInsertionCost();
      unaryConstraints[1]->setLb(lb2);
    }
    if (ub2 > ub1 + static_cast<int>(pattern->getLength()-1) + maxCost / matchingValues->getInsertionCost()) {
      ub2 = ub1 + (pattern->getLength()-1) + maxCost / matchingValues->getInsertionCost();
      unaryConstraints[1]->setUb(ub2);
    }
  }
  else {
    // we know where the word ends, we shrink the start positions domain
    if (lb1 < lb2 - (static_cast<int>(pattern->getLength()-1) + maxCost / matchingValues->getInsertionCost())) {
      lb1 = lb2 - ((pattern->getLength()-1) + maxCost / matchingValues->getInsertionCost());
      unaryConstraints[0]->setLb(lb1);
    }
    if (ub1 > ub2 - (static_cast<int>(pattern->getLength()-1) - maxCost / matchingValues->getInsertionCost())) {
      ub1 = ub2 - ((pattern->getLength()-1) - maxCost / matchingValues->getInsertionCost());
      unaryConstraints[0]->setUb(ub1);
    }
  }

  // find the supports of the other variable
  result = findFirstLastConsistent(lb1, ub1, lb2, ub2, v);
  if (result.first == NOT_FOUND) {
    unaryConstraints[v]->setLb(unaryConstraints[v]->getUb());      
    return;
  }
  newLb = result.first;
  newUb = result.second;

  // shrink the bounds of the other variable
  unaryConstraints[1-v]->setLb(newLb);
  unaryConstraints[1-v]->setUb(newUb+1);

  // set the supports of the assigned variable
  lbSupports[v]->setSupport(v, unaryConstraints[v]->getLb());
  lbSupports[v]->setSupport(1-v, newLb);
  ubSupports[v]->setSupport(v, unaryConstraints[v]->getLb());
  ubSupports[v]->setSupport(1-v, newUb);
  
  // set the supports of the other variable
  lbSupports[1-v]->setSupport(1-v, newLb);
  lbSupports[1-v]->setSupport(v, unaryConstraints[v]->getLb());
  ubSupports[1-v]->setSupport(1-v, newUb);
  ubSupports[1-v]->setSupport(v, unaryConstraints[v]->getLb());

  if (unaryConstraints[1-v]->getUb() - unaryConstraints[1-v]->getLb() == 1) {
    if (!isConsistent(unaryConstraints[0]->getLb(), unaryConstraints[1]->getLb())) {
      cout << "sequence: ";
      for (int i = unaryConstraints[0]->getLb(); i <= unaryConstraints[1]->getLb(); i++) {
        cout << sequence->getNucleotide(i)->print();
      }
      cout << "  pattern: " << pattern->print() << endl;
      cout << "lb1: " << lb1 << " ub1: " << ub1 << " lb2: " << lb2 << " ub2: " << ub2 << endl;
    }
  }
}

void HardPattern2::reviseFromLb (int v) {
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  int newLbStart, newLbStop;
  pair<int, int> result; 

  if (checkLbSupport(v)) {
    return;
  }

  // we shrink the domain of the other variable
  if (v == 0) {
    if (lb2 < lb1 + static_cast<int>(pattern->getLength()-1) - maxCost / matchingValues->getInsertionCost()) {
      lb2 = lb1 + (pattern->getLength()-1) - maxCost / matchingValues->getInsertionCost();
      unaryConstraints[1]->setLb(lb2);
    }
  }
  else {
    if (lb1 < lb2 - (static_cast<int>(pattern->getLength()-1) + maxCost / matchingValues->getInsertionCost())) {
      lb1 = lb2 - ((pattern->getLength()-1) + maxCost / matchingValues->getInsertionCost());
      unaryConstraints[0]->setLb(lb1);
    }
  }

  lbSupports[0]->unset();
  lbSupports[1]->unset();

  // we now find the start and end positions of the leftmost word
  result = findFirstConsistent(lb1, ub1, lb2, ub2, DIRECT);
  newLbStart = result.first;
  newLbStop = result.second;
  if (newLbStart == NOT_FOUND) {
    unaryConstraints[v]->setLb(unaryConstraints[v]->getUb());
    return;
  }
  unaryConstraints[0]->setLb(newLbStart);
  unaryConstraints[1]->setLb(newLbStop);
  lbSupports[0]->setSupport(0, newLbStart);
  lbSupports[0]->setSupport(1, newLbStop);
  lbSupports[1]->setSupport(0, newLbStart);
  lbSupports[1]->setSupport(1, newLbStop);
}

void HardPattern2::reviseFromUb (int v) {
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  int newUbStart, newUbStop;
  pair<int, int> result; 
  if (checkLbSupport(v)) {
    return;
  }

  // we shrink the domain of the other variable
  if (v == 0) {
    if (ub2 > ub1 + static_cast<int>(pattern->getLength()-1) + maxCost / matchingValues->getInsertionCost()) {
      ub2 = ub1 + (pattern->getLength()-1) + maxCost / matchingValues->getInsertionCost();
      unaryConstraints[1]->setUb(ub2);
    }
  }
  else {
    if (ub1 > ub2 - (static_cast<int>(pattern->getLength()-1) - maxCost / matchingValues->getInsertionCost()) + 1) {
      ub1 = ub2 - ((pattern->getLength()-1) - maxCost / matchingValues->getInsertionCost());
      unaryConstraints[0]->setUb(ub1);
    }
  }

  lbSupports[0]->unset();
  lbSupports[1]->unset();

  // we now find the start and end positions of the leftmost word
  result = findFirstConsistent(lb1, ub1, lb2, ub2, REVERSE);
  newUbStart = result.first;
  newUbStop = result.second;
  if (newUbStart == NOT_FOUND) {
    unaryConstraints[v]->setLb(unaryConstraints[v]->getUb());
    return;
  }
  unaryConstraints[0]->setUb(newUbStart+1);
  unaryConstraints[1]->setUb(newUbStop+1);
  ubSupports[0]->setSupport(0, newUbStart);
  ubSupports[0]->setSupport(1, newUbStop);
  ubSupports[1]->setSupport(0, newUbStart);
  ubSupports[1]->setSupport(1, newUbStop);
}

bool HardPattern2::getConsistency (Support *s) {
  return (isConsistent(s->getSupport(0), s->getSupport(1)));
}

bool HardPattern2::printSpace (int var, int place) {
  return (var == place);
}
