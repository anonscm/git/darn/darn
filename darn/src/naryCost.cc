// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "naryCost.h"


NaryCost::NaryCost (int i, int nv) : Cost (i, nv) {}

void NaryCost::instanciate (int i, int val, int c) {
  if ((i < 0) || (i >= nbVariables)) {
    trace(1, "Error! Trying to instanciate a non-existing variable!");
    return;
  }
  if (discretized[i]) {
    return;
  }
  lb[i] = val;
  ub[i] = val+1;
  memProf[i][0] = c;
  discretized[i] = true;
}

void NaryCost::setDiscretized (int i, int l, int u) {
  if ((i < 0) || (i >= nbVariables)) {
    trace(1, "Error! Trying to discretize a non-existing variable!");
    return;
  }
  if (discretized[i]) {
    return;
  }
  lb[i] = l;
  ub[i] = u;
  for (int j = 1; j < ub[i]-lb[i]-1; j++) {
    memProf[i][j] = 0;
  }
  memProf[i][0] = lbMem[i];
  memProf[i][ub[i]-lb[i]-1] = ubMem[i];
  discretized[i] = true;
}
