// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef HDUPLEX_H_INCLUDED
#define HDUPLEX_H_INCLUDED

#include "fileReader.h"
#include "suffixArray.h"
#include "hardConstraint.h"


/**
 * This class stands for an duplex hard constraint.
 */
class HardDuplex : public HardConstraint {

protected:
  /**
   * the suffix array that is used to find quickly the duplex
   */
  SuffixArray *suffixArray;
  /**
   * interaction values
   */
  InteractionValues *interactionValues;
  
  /**
   * check whether the assignment of the variable @a index to @a val is consistent and possibly the solution to the support
   * @param index the index of the variable
   * @param val the value assigned to the variable
   * @param support where to store the support (if any)
   * @return if the assignment is consistent
   */
  virtual bool isConsistent (int index, int val, Support *support = NULL);
  /**
   * check whether there is a duplex in the given region
   * @param lb1 is the index where the first letter of the word is
   * @param lb2 is the index where the last letter (plus one) of the word is
   * @param lb3 is the leftmost index where the first letter of the sub-text can be
   * @param ub3 is the rightmost index where the first letter of the sub-text can be
   * @param lb4 is the leftmost index where the last letter of the sub-text can be
   * @param ub4 is the rightmost index where the last letter of the sub-text can be
   * @param support where to store the support (if any)
   * @return if there is a duplex
   */
  virtual bool isConsistent (int lb1, int lb2, int lb3, int ub3, int lb4, int ub4, Support *support = NULL);


public:
  /**
   * the constructor
   * @param v1 the first variable (the lower bound of the first sequence)
   * @param v2 the second variable (the upper bound of the first sequence)
   * @param v3 the third variable (the lower bound of the second sequence)
   * @param v4 the fourth variable (the upper bound of the second sequence)
   * @param i the index of this constraint
   * @param mc the cost to be paid if no duplex is found
   * @param s the sequence where the duplex is
   * @param iv the interaction values
   */
  HardDuplex (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int i, int mc, Sequence *s, InteractionValues *iv);
  /**
   * revision of the constraint, when no information is given on what led to the revision
   */
  virtual void revise ();
  /**
   * revision of the constraint, due to a given variable
   * @param v the index of the given variable
   */
  virtual void revise (int v);
  /**
   * revision of the constraint, when the lower bound of a variable has changed
   * @param v the index of the given variable
   */
  virtual void reviseFromLb (int v);
  /**
   * revision of the constraint, when the upper bound of a variable has changed
   * @param v the given variable
   */
  virtual void reviseFromUb (int v);
  /**
   * revision of the constraint, when the upper bound of a variable has been assigned
   * @param v the given variable
   */
  virtual void reviseFromAssignment (int v);
  /**
   * verfiy that the support is still valid
   * @param s the support
   */
  virtual bool getConsistency (Support *s);
  /**
   * whether one should print a space before or after the variable @a var
   *  (useful for printing the results)
   * @param var the index of the variable
   * @param place 0: before, 1: after
   * @return true, if a space should be printed
   */
  virtual bool printSpace (int var, int place);
};

#endif
