// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef SOFTCONS_H_INCLUDED
#define SOFTCONS_H_INCLUDED

#include "naryCost.h"
#include "abstractSoftConstraint.h"
#include "defaultUnarySoftConstraint.h"


/**
 * Provide advanced tools to model soft constraints
 */
class SoftConstraint : public AbstractSoftConstraint {

protected:

/**
 * whether this constraint is optional
 * (if not satisfied, the optional constraint yields a cost of @a maxcost,
 * whereas the mandatory constraint makes the network inconsistent)
 */
  bool optional;
/**
 * The unary constraints of the variables involved in this constraint.
 */
  DefaultUnarySoftConstraint **unaryConstraints;
  
public:
  /**
   * The constructor
   * @param i the index
   * @param e2c a mapping from errors to costs
   * @param nv the number of variables
   * @param o whether the constraint is optional
   * @param v the first variable
   */
  SoftConstraint (int i, ErrorsToCosts *e2c, bool o, int nv, Variable *v ...);
  /**
   * destructor
   */
  ~SoftConstraint ();

  /**
   * prepare the constraint for a new search
   */
  virtual void reset ();

  /**
   * Accessor to @a optional
   * @return true if this constraint is optional
   */
  virtual bool isOptional();
  /**
   * Accessor to an element of @a unaryConstraints
   * @param i the index of the unary constraint
   * @return a unary constraint
   */
  virtual DefaultUnarySoftConstraint* getUnaryConstraint(int i);
  /**
   * Get the maximum amount of cost projected on a unary constraint
   * @param uc of the unary constraint
   * @return the cost
   */
  //virtual int getMaxProjected (DefaultUnarySoftConstraint *uc);

  /**
   * @pre the concerned variable should not be discretized
   *
   * Project costs to the lower bound of a variable
   * @param uc the unary constraint of the variable
   */
  virtual void projectLb (DefaultUnarySoftConstraint *uc);
  /**
   * @pre the concerned variable should not be discretized
   *
   * Project costs to the upper bound of a variable
   * @param uc the unary constraint of the variable
   */
  virtual void projectUb (DefaultUnarySoftConstraint *uc);
  /**
   * @pre the concerned variable should be discretized
   *
   * Project costs to a value of a variable
   * @param uc the unary constraint of the variable
   * @param val the value of the variable
   */
  virtual void project (DefaultUnarySoftConstraint *uc, int val);
  /**
   * Handle a revision caused by the increase of a lower bound of a variable
   *   (perform several projections)
   * @param v the variable
   */
  virtual void reviseFromLb (Variable *v);
  /**
   * Handle a revision caused by the decrease of a upper bound of a variable
   *   (perform several projections)
   * @param v the variable
   */
  virtual void reviseFromUb (Variable *v);
  /**
   * Handle a revision caused by the deletion of a value of a variable
   *   (perform several projections)
   * @param v the variable
   * @param i the value
   */
  virtual void reviseFromHole (Variable *v, int i);
  /**
   * Handle a revision caused by the assignment of a variable
   *   (perform several projections)
   * @param v the variable
   */
  virtual void reviseFromAssignment (Variable *v);
  /**
   * Check the neighbor's values when the variable @a v is modified
   *   (perform several projections)
   * @param v the variable
   */
  virtual void reviseNeighbors (Variable *v);
};

#endif
