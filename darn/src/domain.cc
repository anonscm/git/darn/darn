// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "domain.h"


Domain::Domain (int i, int l, int u) : discretized(false), index(i), lb(l), ub(u) {
  nbStoredValues = 0;
  discreteDomain = new int[MAXSIZE+1];
  for (unsigned int i = 0; i <= MAXSIZE; i++) {
    discreteDomain[i] = -1;
  }
  memories = new DomainValues*[nbTotVariables];
  for (int i = 0; i < nbTotVariables; i++) {
    memories[i] = new DomainValues();
  }
}

Domain::~Domain () {
  /*
  delete[] discreteDomain;
  for (int i = 0; i < nbTotVariables; i++) {
    delete memories[i];
  }
  delete[] memories;
  */
}

void Domain::reset (int l, int u) {
  lb = l;
  ub = u;
  discretized = false;
  nbStoredValues = 0;
  for (unsigned int i = 0; i <= MAXSIZE; i++) {
    discreteDomain[i] = -1;
  }
}

bool Domain::isDiscretized () {
  return discretized;
}

bool Domain::isAssigned () {
  return ((discretized) && (ub - lb == 1));
}

int Domain::getLb () {
  return lb;
}

int Domain::getUb () {
  return ub;
}

int Domain::getSize () {
  return ub - lb;
}

void Domain::assign (int v) {
  assignmentHardRevision->addElement(index);
  assignmentComplicatedHardRevision->addElement(index);
  assignmentSoftRevision->addElement(index);
  assignmentComplicatedSoftRevision->addElement(index);
  setLb(v);
  setUb(v+1);
  setDiscretized();
}

void Domain::setDiscretized () {
  if (discretized) return;
  for (unsigned int i = 0; static_cast<int>(i) <= ub-lb; i++) {
    if (i > MAXSIZE) {
      trace(1, "Error! Trying to set a discretized domain's size bigger than MAXSIZE!");
      return;
    }
    discreteDomain[i] = i+lb;
  }
  for (unsigned int i = ub-lb+1; i <= MAXSIZE; i++) {
    discreteDomain[i] = -1;
  }
  discretized = true;
  firstElement = lb;
}

void Domain::setLb (int l) {
  if (l < lb) {
    trace(1, "Warning: trying to decrease the lower bound!");
  }
  if (l >= ub) {
    throw inconsistency(NULL);
  }
  if (l > lb) {
    lbHardRevision->addElement(index);
    lbComplicatedHardRevision->addElement(index);
    lbSoftRevision->addElement(index);
    lbComplicatedSoftRevision->addElement(index);
  }
  lb = l;
  if (!discretized) {
    return;
  }
  for (unsigned i = 0; i <= MAXSIZE; i++) {
    if ((discreteDomain[i] != -1) && (discreteDomain[i] >= lb)) {
      lb = discreteDomain[i];
      return;
    }
    discreteDomain[i] = -1;
  }
  lb = ub;
}

void Domain::setUb (int u) {
  int i;
  if (u > ub) {
    trace(1, "Warning: trying to increase the upper bound!");
  }
  if (u <= lb) {
    throw inconsistency(NULL);
  }
  if (u < ub) {
    ubHardRevision->addElement(index);
    ubComplicatedHardRevision->addElement(index);
    ubSoftRevision->addElement(index);
    ubComplicatedSoftRevision->addElement(index);
  }
  ub = u;
  if (!discretized) {
    return;
  }
  for (i = MAXSIZE; i >= 0; i--) {
    if (discreteDomain[i] != -1) {
      if (discreteDomain[i] == ub) {
        return;
      }
      if (discreteDomain[i] < ub) {
        ub = discreteDomain[i]+1;
        discreteDomain[i+1] = ub;
        return;
      }
    }
    discreteDomain[i] = -1;
  }
  ub = lb;
}

void Domain::incLb (int l) {
  setLb(lb+l);
}

void Domain::decUb (int u) {
  setUb(ub-u);
}

void Domain::delValue (int v) {
  if (!discretized) {
    trace(1, "Error: trying to remove a single value while not discretized!");
    return;
  }
  if (v == lb) {
    incLb(1);
    return;
  }
  if (v == ub-1) {
    decUb(1);
    return;
  }
  holeSoftRevision->addElement(index, v);
  holeComplicatedSoftRevision->addElement(index, v);
  if (discreteDomain[v-firstElement] != v) {
    trace(1, "Error! The discrete domain has a bad value!");
    return;
  }
  discreteDomain[v-firstElement] = -1;
}

bool Domain::isEmpty () {
  return (lb >= ub);
}

bool Domain::contains (int v) {
  int value;
  if (!discretized) {
    return ((v >= lb) && (v < ub));
  }
  if ((v < lb) || (v >= ub)) {
    return false;
  }
  value = discreteDomain[v-firstElement];
  if (value == -1) {
    return false;
  }
  if (value != v) {
    trace(1, "Error! The discrete domain has a bad value!");
    return false;
  }
  return true;
}

DomainIterator Domain::getDomainIterator () {
  if (discretized) {
    return DomainIterator(discreteDomain, lb, ub, firstElement);
  }
  return DomainIterator(lb, ub);
}

DomainIterator Domain::getReverseDomainIterator () {
  if (discretized) {
    return DomainIterator(discreteDomain, lb, ub, firstElement, true);
  }
  return DomainIterator(lb, ub, true);
}

string Domain::print () {
  ostringstream s;
  DomainIterator di = getDomainIterator();
  if (discretized) {
    for (; *di < ub; ++di) {
      s << " " << *di;
    }
    return s.str();
  }
  s << lb << "-" << ub;
  return s.str();
}

void Domain::save () {
  memories[nbStoredValues]->discretized = discretized;
  memories[nbStoredValues]->lb = lb;
  memories[nbStoredValues]->ub = ub;
  if (discretized) {
    for (unsigned int i = 0; i <= MAXSIZE; i++) {
      memories[nbStoredValues]->discreteDomain[i] = discreteDomain[i];
    }
    memories[nbStoredValues]->firstElement = firstElement;
  }
  nbStoredValues++;
}

void Domain::restore () {
  nbStoredValues--;
  discretized = memories[nbStoredValues]->discretized;
  lb = memories[nbStoredValues]->lb;
  ub = memories[nbStoredValues]->ub;
  if (discretized) {
    for (unsigned int i = 0; i <= MAXSIZE; i++) {
      discreteDomain[i] = memories[nbStoredValues]->discreteDomain[i];
    }
    firstElement = memories[nbStoredValues]->firstElement;
  }
}

