// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef HHELIXSIM_H_INCLUDED
#define HHELIXSIM_H_INCLUDED

#include "common.h"
#include "sequence.h"
#include "values.h"
#include "hardConstraint.h"

/**
 * This class stands for an simple helix hard constraint.
 */

class HardHelixSimple : public HardConstraint {
  
private:
  /**
   * the minimum length of a stem
   */
  int minStemLength;
  /**
   * the maximum length of a stem
   */
  int maxStemLength;
  /**
   * the minimum length of the loop
   */
  int minLoopLength;
  /**
   * the maximum length of the loop
   */
  int maxLoopLength;
  /**
   * whether bulges or insertions are allowed
   */
  bool insertion;
  /**
   * interaction values
   */
  InteractionValues *interactionValues;
  /**
   * a matrix, allocated once, used to perform the computations
   */
  int *matrix;
  /**
   * check whether there is an helix, knowing that a variable is assigned
   * @param var the index of the variable that has been assigned
   * @param value the value assigned to the variable
   * @param values where to store the support of this value (if any)
   * @return the values of the variables if there is an helix, a null pointer otherwise
   */
  int *isConsistent (int var, int value, int *values);
  /**
   * check whether there is an helix, between the specified bounds
   * @param lb1 the lower bound of the first variable
   * @param ub1 the upper bound of the first variable
   * @param lb2 the lower bound of the second variable
   * @param ub2 the upper bound of the second variable
   * @param lb3 the lower bound of the third variable
   * @param ub3 the upper bound of the third variable
   * @param lb4 the lower bound of the fourth variable
   * @param ub4 the upper bound of the fourth variable
   * @param values where to store the support of this value (if any)
   * @return the values of the variables if there is an helix, a null pointer otherwise
   */
  int *isConsistent (int lb1, int ub1, int lb2, int ub2, int lb3, int ub3, int lb4, int ub4, int *values);


public:
  /**
   * the constructor
   * @param v1 the first variable (the lower bound of the first stem)
   * @param v2 the second variable (the upper bound of the first stem)
   * @param v3 the third variable (the lower bound of the second stem)
   * @param v4 the fourth variable (the upper bound of the second stem)
   * @param i the index of this constraint
   * @param msl the minimum length of a stem
   * @param mmsl the maximum length of a stem
   * @param mll the minimum length of the loop
   * @param mmll the minimum length of the loop
   * @param mc the cost to be paid if no helix is found
   * @param iv interaction values
   * @param insert whether bulges are allowed
   */
  HardHelixSimple(Variable *v1, Variable *v2, Variable *v3, Variable *v4, int i, int msl, int mmsl, int mll, int mmll, int mc, InteractionValues *iv, bool insert = false);
  /**
   * revision of the constraint, when no information is given on what led to the revision
   */
  virtual void revise ();
  /**
   * revision of the constraint, due to a given variable
   * @param v the index of the given variable
   */
  virtual void revise (int v);
  /**
   * revision of the constraint, when the lower bound of a variable has changed
   * @param v the index of the given variable
   */
  virtual void reviseFromLb (int v);
  /**
   * revision of the constraint, when the upper bound of a variable has changed
   * @param v the given variable
   */
  virtual void reviseFromUb (int v);
  /**
   * revision of the constraint, when the upper bound of a variable has been assigned
   * @param v the given variable
   */
  virtual void reviseFromAssignment (int v);
  /**
   * verfiy that the support is still valid
   * @param s the support
   */
  virtual bool getConsistency (Support *s);
  /**
   * whether one should print a space before or after the variable @a var
   *  (useful for printing the results)
   * @param var the index of the variable
   * @param place 0: before, 1: after
   * @return true, if a space should be printed
   */
  virtual bool printSpace (int var, int place);
  /**
   * set the trace of this constraint to a trace matrix
   * @param start the start position of a candidate
   * @param values the set of positions taken by the variables
   * @param backtrace the trace matrix
   */
  virtual void getTrace (int start, int *values, bool **backtrace);
};

#endif
