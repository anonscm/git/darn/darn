// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "hardNonCanonicPair.h"


HardNonCanonicPair::HardNonCanonicPair (Variable *v1, Variable *v2, int i, Interaction it1, Interaction it2, Orientation ori, int f, int mc, IsostericityValues *iv) : HardConstraint(i, mc, 2, v1, v2) {
  simple = false;
  interaction1 = it1;
  interaction2 = it2;
  orientation = ori;
  family = f;
  hardness = 2;
  isostericityValues = iv;
  name = "hard non canonic pair";
}

bool HardNonCanonicPair::checkConsistency (int i1, int i2) {
  Nucleotide *n1 = unaryConstraints[0]->getVariable()->getSequence()->getNucleotide(i1);
  Nucleotide *n2 = unaryConstraints[1]->getVariable()->getSequence()->getNucleotide(i2);
  return (isostericityValues->getMatchCost(interaction1, interaction2, orientation, n1, n2, family));
}

void HardNonCanonicPair::revise () {
  revise(0);
  revise(1);
}

void HardNonCanonicPair::revise (int v) {
  reviseFromLb(v);
  reviseFromUb(v);
}

void HardNonCanonicPair::reviseFromAssignment (int v) {
  int i = unaryConstraints[v]->getLb();
  int lb = unaryConstraints[1-v]->getLb();
  int ub = unaryConstraints[1-v]->getUb();
  bool supportFound = false;
  if ((checkLbSupport(v)) || (checkUbSupport(v))) {
    return;
  }
  lbSupports[v]->unset();
  ubSupports[v]->unset();
  DomainIterator di1 = unaryConstraints[1-v]->getDomainIterator();
  for (; (*di1 < unaryConstraints[1-v]->getUb()) && (!supportFound); ++di1) {
    if (((v == 0)? checkConsistency(i, *di1): checkConsistency(*di1, i))) {
      if (*di1 != lb) {
        unaryConstraints[1-v]->setLb(*di1);
      }
      lbSupports[v]->setSupport(v, i);
      lbSupports[v]->setSupport(1-v, *di1);
      supportFound = true;
    }
  }
  if (!supportFound) {
    unaryConstraints[v]->setLb(unaryConstraints[v]->getUb());
  }
  DomainIterator di2 = unaryConstraints[1-v]->getReverseDomainIterator();
  for (; *di2 >= unaryConstraints[1-v]->getLb(); --di2) {
    if (((v == 0)? checkConsistency(i, *di2): checkConsistency(*di2, i))) {
      if (*di2 != ub-1) {
        unaryConstraints[1-v]->setUb(*di2+1);
      }
      ubSupports[v]->setSupport(v, i);
      ubSupports[v]->setSupport(1-v, *di2);
      return;
    }
  }
}

void HardNonCanonicPair::reviseFromLb (int v) {
  if (!unaryConstraints[1-v]->isAssigned()) {
    return;
  }
  int lb = unaryConstraints[v]->getLb();
  int i2 = unaryConstraints[1-v]->getLb();
  if (checkLbSupport(v)) {
    return;
  }
  lbSupports[v]->unset();
  DomainIterator di1 = unaryConstraints[v]->getDomainIterator();
  for (; *di1 < unaryConstraints[v]->getUb(); ++di1) {
    if (((v == 0)? checkConsistency(*di1, i2): checkConsistency(i2, *di1))) {
      if (*di1 != lb) {
        unaryConstraints[v]->setLb(*di1);
      }
      lbSupports[v]->setSupport(v, *di1);
      lbSupports[v]->setSupport(1-v, i2);
      return;
    }
  }
  unaryConstraints[v]->setLb(unaryConstraints[v]->getUb());
}

void HardNonCanonicPair::reviseFromUb (int v) {
  if (!unaryConstraints[1-v]->isAssigned()) {
    return;
  }
  int ub = unaryConstraints[v]->getUb();
  int i2 = unaryConstraints[1-v]->getLb();
  if (checkUbSupport(v)) {
    return;
  }
  ubSupports[v]->unset();
  DomainIterator di1 = unaryConstraints[v]->getReverseDomainIterator();
  for (; *di1 >= unaryConstraints[v]->getLb(); --di1) {
    if (((v == 0)? checkConsistency(*di1, i2): checkConsistency(i2, *di1))) {
      if (*di1 != ub-1) {
        unaryConstraints[v]->setUb(*di1+1);
      }
      ubSupports[v]->setSupport(v, *di1);
      ubSupports[v]->setSupport(1-v, i2);
      return;
    }
  }
  unaryConstraints[v]->setUb(unaryConstraints[v]->getLb());
}

bool HardNonCanonicPair::getConsistency (Support *s) {
  int i1 = s->getSupport(0);
  int i2 = s->getSupport(1);
  return (checkConsistency(i1, i2));
}

bool HardNonCanonicPair::printSpace (int var, int place) {
  return true;
}
