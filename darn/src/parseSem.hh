
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton interface for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     NUMBER = 258,
     SVAR = 259,
     TVAR = 260,
     WORD = 261,
     MAX_COST = 262,
     DECL_VARIABLE = 263,
     DECL_CONTENT = 264,
     DECL_PATTERN = 265,
     DECL_PATTERN2 = 266,
     DECL_SPACER = 267,
     DECL_COMPOSITION = 268,
     DECL_HELIX_SIMPLE = 269,
     DECL_HELIX = 270,
     DECL_REPETITION = 271,
     DECL_DUPLEX = 272,
     DECL_NONCANONICPAIR = 273,
     DECL_HAIRPIN = 274,
     DECL_SVAR = 275,
     DECL_TVAR = 276,
     PARAM_MODEL = 277,
     PARAM_WORD = 278,
     PARAM_NUCLEOTIDES = 279,
     PARAM_ERRORS = 280,
     PARAM_MINSTEM = 281,
     PARAM_MAXSTEM = 282,
     PARAM_STEM = 283,
     PARAM_MINLOOP = 284,
     PARAM_MAXLOOP = 285,
     PARAM_LOOP = 286,
     PARAM_MINLENGTH = 287,
     PARAM_MAXLENGTH = 288,
     PARAM_MINSOFTLENGTH = 289,
     PARAM_MAXSOFTLENGTH = 290,
     PARAM_LENGTH = 291,
     PARAM_MINCOSTS = 292,
     PARAM_MAXCOSTS = 293,
     PARAM_COSTS = 294,
     PARAM_INDELS = 295,
     PARAM_WOBBLE = 296,
     PARAM_INTERACTION = 297,
     PARAM_ORIENTATION = 298,
     PARAM_THRESHOLD = 299,
     PARAM_RELATION = 300,
     PARAM_FAMILY = 301,
     PARAM_SIZE = 302,
     PARAM_DISTANCE = 303,
     VALUE_TRUE = 304,
     VALUE_FALSE = 305,
     VALUE_ALL = 306,
     INTERACTION_WATSON_CRICK = 307,
     INTERACTION_HOOGSTEEN = 308,
     INTERACTION_SUGAR_EDGE = 309,
     ORIENTATION_CIS = 310,
     ORIENTATION_TRANS = 311,
     RELATION_NOT_LESS_THAN = 312,
     RELATION_NOT_GREATER_THAN = 313,
     MODEL_HARD = 314,
     MODEL_SOFT = 315,
     MODEL_OPTIONAL = 316,
     DOTS = 317,
     COMMA = 318,
     EQUAL = 319,
     LEFT_BRACKET = 320,
     RIGHT_BRACKET = 321,
     LEFT_PAR = 322,
     RIGHT_PAR = 323,
     LEFT_BRA = 324,
     RIGHT_BRA = 325,
     QUOTE = 326,
     PERCENT = 327,
     END_LINE = 328
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 1676 of yacc.c  */
#line 431 "parseSem.y"

    char *word;
    int value;



/* Line 1676 of yacc.c  */
#line 132 "parseSem.hh"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;

#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif

extern YYLTYPE yylloc;

