// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "hardDistance.h"


HardDistance::HardDistance (Variable *v1, Variable *v2, int i, int d1, int d2) : HardConstraint(i, d2, 2, v1, v2), minCost(d1) {
  hardness = 1;
  name = " hard distance";
}

void HardDistance::revise () {
  revise(0);
  revise(1);
}

void HardDistance::revise (int v) {
  reviseFromLb(v);
  reviseFromUb(v);
}

void HardDistance::reviseFromAssignment (int v) {
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  if ((checkLbSupport(v)) || (checkUbSupport(v))) {
    return;
  }
  lbSupports[v]->unset();
  ubSupports[v]->unset();
  if (v == 0) {
    if (lb2 < lb1+minCost) {
      unaryConstraints[1]->setLb(lb1+minCost);
    }
    if (ub2 > lb1+maxCost+1) {
      unaryConstraints[1]->setUb(lb1+maxCost+1);
    }
    lbSupports[0]->setSupport(0, lb1);
    lbSupports[0]->setSupport(1, lb1+minCost);
  }
  else if (v == 1) {
    if (lb1 < lb2-maxCost) {
      unaryConstraints[0]->setLb(lb2-maxCost);
    }
    if (ub1 > lb2-minCost+1) {
      unaryConstraints[0]->setUb(lb2-minCost+1);
    }
    lbSupports[1]->setSupport(0, lb2-maxCost);
    lbSupports[1]->setSupport(1, lb2);
  }
}

void HardDistance::reviseFromLb (int v) {
  int lb1 = unaryConstraints[0]->getLb();
  int lb2 = unaryConstraints[1]->getLb();
  if (checkLbSupport(v)) {
    return;
  }
  lbSupports[v]->unset();
  if (v == 0) {
    if (lb2 < lb1+minCost) {
      unaryConstraints[1]->setLb(lb1+minCost);
    }
    lbSupports[0]->setSupport(0, lb1);
    lbSupports[0]->setSupport(1, lb1+minCost);
  }
  else if (v == 1) {
    if (lb1 < lb2-maxCost) {
      unaryConstraints[0]->setLb(lb2-maxCost);
    }
    lbSupports[1]->setSupport(0, lb2-maxCost);
    lbSupports[1]->setSupport(1, lb2);
  }
}

void HardDistance::reviseFromUb (int v) {
  int ub1 = unaryConstraints[0]->getUb();
  int ub2 = unaryConstraints[1]->getUb();
  if (checkUbSupport(v)) {
    return;
  }
  ubSupports[v]->unset();
  if (v == 0) {
    if (ub2 > ub1+maxCost) {
      unaryConstraints[1]->setUb(ub1+maxCost);
    }
    ubSupports[0]->setSupport(0, ub1-1);
    ubSupports[0]->setSupport(1, ub1+maxCost-1);
  }
  if (v == 1) {
    if (ub1 > ub2-minCost) {
      unaryConstraints[0]->setUb(ub2-minCost);
    }
    ubSupports[1]->setSupport(0, ub2-minCost-1);
    ubSupports[1]->setSupport(1, ub2-1);
  }
}

bool HardDistance::getConsistency (Support *s) {
  int val1 = s->getSupport(0);
  int val2 = s->getSupport(1);
  return ((val1 + minCost <= val2) && (val1 + maxCost >= val2));
}
