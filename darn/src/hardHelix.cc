// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "hardHelix.h"


HardHelix::HardHelix (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int i, int hl, int mc, InteractionValues *iv) : HardConstraint (i, mc, 4, v1, v2, v3, v4), helixLength(hl) {
  simple = false;
  matrix = new HelixCell***[HELIXMAXLENGTH+1];
  for (unsigned int i = 0; i <= HELIXMAXLENGTH; i++) {
    matrix[i] = new HelixCell**[HELIXMAXLENGTH+1];
    for (unsigned int j = 0; j <= HELIXMAXLENGTH; j++) {
      matrix[i][j] = new HelixCell*[helixLength+1];
      for (int k = 0; k <= helixLength; k++) {
        matrix[i][j][k] = new HelixCell(maxCost+1);
      }
    }
  }
  hardness = helixLength;
  interactionValues = iv;
  name = "hard helix (with size penalty)";
}

bool HardHelix::isConsistent (int index, int val, Support *support) {
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  int lb3 = unaryConstraints[2]->getLb();
  int ub3 = unaryConstraints[2]->getUb();
  int lb4 = unaryConstraints[3]->getLb();
  int ub4 = unaryConstraints[3]->getUb();
  switch (index) {
    case 0:
      lb1 = val;
      ub1 = val+1;
      break;
    case 1:
      lb2 = val;
      ub2 = val+1;
      break;
    case 2:
      lb3 = val;
      ub3 = val+1;
      break;
    case 3:
      lb4 = val;
      ub4 = val+1;
      break;
  }
  return (isConsistent(lb1, ub1, lb2, ub2, lb3, ub3, lb4, ub4, support));
}

bool HardHelix::isConsistent (int lb1, int ub1, int lb2, int ub2, int lb3, int ub3, int lb4, int ub4, Support *support) {
  bool isASupport;
  int value = maxCost+1, match;
  int l1, l2;
  HelixCell *cell = NULL;
  if (ub2 > ub1 + (helixLength+1)) {
    ub2 = ub1 + (helixLength+1);
  }
  if (lb3 < lb4 - (helixLength+1)) {
    lb3 = lb4 - (helixLength+1);
  }
  if (lb1 < lb2 - (helixLength+1)) {
    lb1 = lb2 - (helixLength+1);
  }
  if (ub4 > ub3 + (helixLength+1)) {
    ub4 = ub3 + (helixLength+1);
  }
  l1 = ub2 - lb1;
  l2 = ub4 - lb3;
  if ((ub1 <= lb1) || (ub2 <= lb2) || (ub3 <= lb3) || (ub4 <= lb4) || (l1 <= 0) || (l2 <= 0)) {
    return false;
  }
  if ((l1 > static_cast<int>(HELIXMAXLENGTH)) || (l2 > static_cast<int>(HELIXMAXLENGTH))) {
    return true;
  }
  for (int i = 0; i <= l1; i++) {
    for (int j = 0; j <= l2; j++) {
      if ((i < ub1-lb1) && (j < ub4-lb4)) {
        matrix[i][j][0]->setValues(0, lb1+i, ub4-1-j);
      }
      else {
        matrix[i][j][0]->setValues(maxCost+1, -1, -1);
      }
    }
  }
  for (int i = 0; i <= l1; i++) {
    for (int k = 1; k <= helixLength; k++) {
      matrix[i][0][k]->setValues(maxCost+1, -1, -1);
    }
  }
  for (int j = 0; j <= l2; j++) {
    for (int k = 1; k <= helixLength; k++) {
      matrix[0][j][k]->setValues(maxCost+1, -1, -1);
    }
  }
  for (int k = 1; k <= helixLength; k++) {
    if (ub3 > ub4) {
      for (int i = 1; i < lb2-lb1+1; i++) {
        cell = matrix[i-1][0][k-1];
        value = cell->value+interactionValues->getInsertionCost();
        matrix[i][0][k]->setValues(value, cell->rootI, cell->rootJ);
      }
      for (int i = lb2-lb1+1; i <= l1; i++) {
        cell = matrix[i-1][0][k-1];
        value = cell->value+interactionValues->getInsertionCost();
        matrix[i][0][k]->setValues(value, cell->rootI, cell->rootJ);
        value += (helixLength - k)*interactionValues->getHelixLengthCost();
        if (value <= maxCost) {
          if (support != NULL) {
            support->setSupport(0, cell->rootI);
            support->setSupport(1, lb1+i-1);
            support->setSupport(2, ub4);
            support->setSupport(3, cell->rootJ);
          }
          return true;
        }
      }
    }
    else {
      for (int i = 1; i <= l1; i++) {
        cell = matrix[i-1][0][k-1];
        value = cell->value+interactionValues->getInsertionCost();
        matrix[i][0][k]->setValues(value, cell->rootI, cell->rootJ);
      }
    }
    if (lb2 < lb1) {
      for (int j = 1; j < ub4-ub3+1; j++) {
        cell = matrix[0][j-1][k-1];
        value = cell->value+interactionValues->getInsertionCost();
        matrix[0][j][k]->setValues(value, cell->rootI, cell->rootJ);
      }
      for (int j = ub4-ub3+1; j <= l2; j++) {
        cell = matrix[0][j-1][k-1];
        value = cell->value+interactionValues->getInsertionCost();
        matrix[0][j][k]->setValues(value, cell->rootI, cell->rootJ);
        value += (helixLength - k)*interactionValues->getHelixLengthCost();
        if (value <= maxCost) {
          if (support != NULL) {
            support->setSupport(0, cell->rootI);
            support->setSupport(1, lb1);
            support->setSupport(2, ub4-1-j);
            support->setSupport(3, cell->rootJ);
          }
         return true;
        }
      }
    }
    else {
      for (int j = 1; j <= l2; j++) {
        cell = matrix[0][j-1][k-1];
        value = cell->value+interactionValues->getInsertionCost();
        matrix[0][j][k]->setValues(value, cell->rootI, cell->rootJ);
      }
    }
  }
  for (int k = 1; k <= helixLength; k++) {
    isASupport = false;
    for (int i = 0; (i <= l1) && (!isASupport); i++) {
      if (matrix[i][0][k]->value <= maxCost) {
        isASupport = true;
      }
    }
    for (int j = 1; (j <= l2) && (!isASupport); j++) {
      if (matrix[0][j][k]->value <= maxCost) {
        isASupport = true;
      }
    }
    for (int i = 1; i <= l1; i++) {
      if ((lb2-lb1+1)-i > helixLength-k) {
        continue;
      }
      for (int j = 1; j <= l2; j++) {
        if ((ub4-ub3+1)-j > helixLength-k) {
          continue;
        }
        match = interactionValues->getMatchCost(sequence->getNucleotide(lb1+i-1), sequence->getNucleotide(ub4-j));
        switch (argMin3(matrix[i-1][j-1][k-1]->value+match, matrix[i][j-1][k-1]->value+interactionValues->getInsertionCost(), matrix[i-1][j][k-1]->value+interactionValues->getInsertionCost())) {
          case 0:
            cell = matrix[i-1][j-1][k-1];
            value = cell->value + match;
            break;
          case 1:
            cell = matrix[i][j-1][k-1];
            value = cell->value + interactionValues->getInsertionCost();
            break;
          case 2:
            cell = matrix[i-1][j][k-1];
            value = cell->value + interactionValues->getInsertionCost();
            break;
        }
        matrix[i][j][k]->setValues(value, cell->rootI, cell->rootJ);
        if ((i >= lb2-lb1+1) && (j >= ub4-ub3+1)) {
          value += (helixLength - k)*interactionValues->getHelixLengthCost();
          if (value <= maxCost) {
            if (support != NULL) {
              support->setSupport(0, cell->rootI);
              support->setSupport(1, lb1+i-1);
              support->setSupport(2, ub4-j);
              support->setSupport(3, cell->rootJ);
            }
            return true;
          }
        }
        else {
          if (value <= maxCost) {
            isASupport = true;
          }
        }
      }
    }
    if (!isASupport) {
      return false;
    }
  }
  return false;
}

void HardHelix::revise () {
  for (int i = 0; i < 4; i++) {
    revise(i);
  }
}

void HardHelix::revise (int v) {
  if ((!unaryConstraints[0]->isAssigned()) && (!unaryConstraints[1]->isAssigned()) && (!unaryConstraints[2]->isAssigned()) && ((!unaryConstraints[3]->isAssigned()))) {
    return;
  }
  reviseFromLb(v);
  reviseFromUb(v);
}

void HardHelix::reviseFromLb (int v) {
  int ub = unaryConstraints[v]->getUb();
  DomainIterator di = unaryConstraints[v]->getDomainIterator();
  if (checkLbSupport(v)) {
    return;
  }
  lbSupports[v]->unset();
  //if ((!unaryConstraints[0]->isAssigned()) && (!unaryConstraints[1]->isAssigned()) && (!unaryConstraints[2]->isAssigned()) && ((!unaryConstraints[3]->isAssigned()))) {
  //  return;
  //}
  for (; *di < ub; ++di) {
    if (isConsistent(v, *di, lbSupports[v])) {
      unaryConstraints[v]->setLb(*di);
      return;
    }
  }
  unaryConstraints[v]->setLb(ub);
}

void HardHelix::reviseFromUb (int v) {
  int lb = unaryConstraints[v]->getLb();
  DomainIterator di = unaryConstraints[v]->getReverseDomainIterator();
  if (checkUbSupport(v)) {
    return;
  }
  ubSupports[v]->unset();
  //if ((!unaryConstraints[0]->isAssigned()) && (!unaryConstraints[1]->isAssigned()) && (!unaryConstraints[2]->isAssigned()) && ((!unaryConstraints[3]->isAssigned()))) {
  //  return;
  //}
  for (; *di >= lb; --di) {
    if (isConsistent(v, *di, ubSupports[v])) {
      unaryConstraints[v]->setUb(*di+1);
      return;
    }
  }
  unaryConstraints[v]->setUb(lb);
}

void HardHelix::reviseFromAssignment (int v) {
  int lb = unaryConstraints[v]->getLb();
  if ((checkLbSupport(v)) || (checkUbSupport(v))) {
    return;
  }
  lbSupports[v]->unset();
  ubSupports[v]->unset();
  if (!isConsistent(v, lb, lbSupports[v])) {
    unaryConstraints[v]->setUb(lb);
  }
}

bool HardHelix::getConsistency (Support *s) {
  int val1 = s->getSupport(0);
  int val2 = s->getSupport(1);
  int val3 = s->getSupport(2);
  int val4 = s->getSupport(3);
  return isConsistent(val1, val1+1, val2, val2+1, val3, val3+1, val4, val4+1);
}

bool HardHelix::printSpace (int var, int place) {
  return (((var == 0) && (place == 0)) || ((var == 1) && (place == 1)) || ((var == 2) && (place == 0)) || ((var == 3) && (place == 1)));
}

void HardHelix::getTrace (int start, int *values, bool **backtrace) {
  int m[HELIXMAXLENGTH+1][HELIXMAXLENGTH+1];
  int l1, l2;
  int val1, val2, val3;
  Nucleotide *n1, *n2;
  l1 = values[1] - values[0] + 1;
  l2 = values[3] - values[2] + 1;

  if (l1 <= 0) {
    return;
  }
  if (l2 <= 0) {
    return;
  }

  // fill the dynamic programming matrix
  for (int i = 0; i <= l1; i++) {
    m[i][0] = i * interactionValues->getInsertionCost();
  }
  for (int j = 1; j <= l2; j++) {
    m[0][j] = j * interactionValues->getInsertionCost();
  }
  for (int i = 1; i <= l1; i++) {
    n1 = sequence->getNucleotide(values[0] + (i-1));
    for (int j = 1; j <= l2; j++) {
      n2 = sequence->getNucleotide(values[3] - (j-1));
      val1 = m[i-1][j-1] + interactionValues->getMatchCost(n1, n2);
      val2 = m[i][j-1] + interactionValues->getInsertionCost();
      val3 = m[i-1][j] + interactionValues->getInsertionCost();
      m[i][j] = min3(val1, val2, val3);
    }
  }

  // retrieve the trace
  int i = l1;
  int j = l2;
  while ((i != 0) || (j != 0)) {
    if ((i > 0)? (m[i][j] == m[i-1][j] + interactionValues->getInsertionCost()): false) {
      i--;
    }
    else if ((j > 0)? (m[i][j] == m[i][j-1] + interactionValues->getInsertionCost()): false) {
      j--;
    }
    else {
      if (m[i][j] == m[i-1][j-1]) {
        backtrace[values[0] + (i-1) - start][values[3] - (j-1) - start] = true;
      }
      i--;
      j--;
    }
  }
}
