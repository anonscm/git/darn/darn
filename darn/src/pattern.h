// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef PATT_H_INCLUDED
#define PATT_H_INCLUDED

#include "values.h"
#include "tools.h"
#include "softConstraint.h"


/**
 * The Pattern is a unary soft constraint. It gives the number of mismatch/insertion/deletion between the given @a pattern and a sequence.
 */
class Pattern : public SoftConstraint {

protected:
  /**
   * the sequence where the pattern should be found
   */
  Sequence* sequence;
  /**
   * the pattern to be compared with the sequence
   */
  Sequence *pattern;
  /**
   * a matrix, allocated once, used to perform the computations
   */
  int **matrix;
  /**
   * the algorithm to get the number the mismatch/insertion/deletion
   */
  int computeCost (int i);
  /**
   * matching values
   */
  MatchingValues *matchingValues;

public:

  /**
   * the constructor
   * @param v the variable constrained by this pattern
   * @param i the index of this constraint
   * @param o whether the pattern is optional
   * @param p the pattern
   * @param e2c the convertor from errors to costs
   * @param mv the matching values
   */
  Pattern (Variable *v, int i, bool o, string p, ErrorsToCosts *e2c, MatchingValues *mv);

  /**
   * @pre The variable should be discretized
   *
   * get the minimum of the function cost, having that the variable has been assigned to the given value
   * @param v the variable (used for reasons of compliance with the other constraints)
   * @param val the given element
   * @return the minimum
   */
  int getMinCost (Variable *v, int val);
  /**
   * @pre The variable should be not discretized
   *
   * compute the minimum cost of the constraint, having that the variable is assigned to the lower bound
   * @param v the variable (used for reasons of compliance with the other constraints)
   * @return the minimum
   */
  int getMinLbCost (Variable *v);
  /**
   * @pre The variable should be not discretized
   *
   * compute the minimum cost of the constraint, having that the variable is assigned to the upper bound
   * @param v the variable (used for reasons of compliance with the other constraints)
   * @return the minimum
   */
  int getMinUbCost (Variable *v);
  /**
   * get the minimum of the function cost (waits for the variable to be instanciated)
   * @return the minimum
   */
  virtual int getMinCost ();
  /**
   * compute the cost of the given support
   * @param s the given cost
   * @return the cost
   */
  virtual int getCost (Support *s);
  /**
   * get the contribution of this constraint to the global cost when all variables are assigned
   * @param values the values of the involved variables
   * @return the cost of this constraint
   */
  virtual int getFinalCost (int *values);
};

#endif
