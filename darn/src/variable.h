// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef  VAR_H_INCLUDED
#define  VAR_H_INCLUDED

#include "common.h"
#include "sequence.h"
#include "domain.h"

class Constraint;
class DefaultUnarySoftConstraint;
class AbstractSoftConstraint;
class AbstractHardConstraint;

/**
 * The Variable class is supposed to stand for the WCSP's variable. In our implementation, it is nothing but some pointers to other elements, especially UnaryConstraint.
 */
class Variable {

private:
  /**
   * a number attached to the variable, used for a name or an index
   */
  int index;
  /**
   * the name of the variable (for printing purpose only)
   */
  string name;
  /**
   * the domain of this variable, created in the constructor
   */
  Domain *domain;
  /**
   * all the nary constraints in which this constraints is involved
   */
  vector<AbstractSoftConstraint*> softconstraints;
  /**
   * the unary constraint of this variable
   */
  DefaultUnarySoftConstraint *unaryconstraint;
  /**
   * all the hard constraints in which this constraints is involved
   */
  vector<AbstractHardConstraint*> hardconstraints;
  
  /**
   * the sequence this variable referes to
   */
  Sequence *seq;

public:
  /**
   * constructor, which creates the domain
   * @param i the index
   * @param n the name
   * @param s the sequence
   */
  Variable (int i, string n, Sequence *s);
  /**
   * constructor
   */
  ~Variable ();
  /**
   * prepare the variable for a new search
   */
  void reset ();
  /**
   * accessor to the index
   * @return @a index
   */
  int getIndex ();
  /**
   * @return the name of the variable
   */
  string getName ();
  /**
   * @return the domain of the variable
   */
  Domain *getDomain ();
  /**
   * @return the sequence
   */
  Sequence *getSequence ();
  /**
   * set the unary constraint
   * @param c the unary constraint
   */
  void setUnaryConstraint (DefaultUnarySoftConstraint *c);
  /**
   * add a soft constraint
   * @param c the constraint
   */
  void addSoftConstraint (AbstractSoftConstraint *c);
  /**
   * add a hard constraint
   * @param c the constraint
   */
  void addHardConstraint (AbstractHardConstraint *c);
  /**
   * print the content of the variable (name and domain)
   */
  string print ();
  /**
   * @return the unary constraint
   */
  DefaultUnarySoftConstraint *getUnaryConstraint ();
  /**
   * @return the number of soft constraints
   */
  int getNbSoftConstraints ();
  /**
   * @param i the index of the wanted soft constraint
   * @return the ith soft constraint
   */
  AbstractSoftConstraint *getSoftConstraint (int i);
  /**
   * @return the number of hard constraints
   */
  int getNbHardConstraints ();
  /**
   * @param i the index of the wanted hard constraint
   * @return the ith hard constraint
   */
  AbstractHardConstraint *getHardConstraint (int i);
};


#endif
