// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.



#ifndef ABSCONS_H_INCLUDED
#define ABSCONS_H_INCLUDED

#include "variable.h"


/**
 * Provide minimal tools to model a constraint.
 * Contains accessors to variables.
 */
class AbstractConstraint {

protected:
  /**
   * the number of this constraint, used for a name or an index
   */
  int index;
  /**
   * the name of the helix
   */
  string name;
  /**
   * the arity of the constraint (0 by default)
   */
  int nbVariables;
  /**
   * the variables this object constraints
   */
  Variable **variables;
  /**
   * the upper bound beyond which a value is unconsistent
   */
  int maxCost;
  /**
   * whether the constraint is simple or complex (determines the order of the enforcement of a property)
   */
  bool simple;
  /**
   * quantify the hardness of the constraint (a high value indicates an very infrequently satisfied constraint)
   */
  int hardness;


public:
  
  /**
   * constructor
   * @param i the index
   * @param mc the threshold, or maximum cost allowed by the constraint
   * @param nv the number of variables
   */
  AbstractConstraint (int i, int mc, int nv = 0);

  /**
   * empty destructor
   */
  virtual ~AbstractConstraint ();

  /**
   * accessor to @a index
   */
	virtual int getIndex ();
  /**
   * accessor to @a simple
   */
  virtual bool isSimple ();
  /**
   * accessor to @a name
   */
  virtual string getShortName ();
  /**
   * set the hardness of this constraint
   */
  virtual void setHardness ();
  /**
   * accessor to @a hardness
   */
  virtual int getHardness ();
  /**
   * get the arity of this constraint
   * @return @a nbVariables
   */
  virtual int getNbVariables ();
  /**
   * get a specified variable this object constrains
   * @param i the index of the specified variable
   * @return the specified variable
   */
  virtual Variable *getVariable (int i);
  /**
   * get a specified domain this object constrains
   * @param i the index of the specified domain
   * @return the specified domain
   */
  virtual Domain *getDomain (int i);


  /**
   * knowing a variable @a v, it gives the index of the variable wrt to the @a this constraint
   * @a v the variable
   * @return the index
   */
  virtual int getVariableIndex (Variable *v);
  
  /**
   * return a string that stores the name of the constraint
   * @return the string
   */
  virtual string getName ();
  /**
   * whether one should print a space before or after the variable @a var
   *  (useful for printing the results)
   * @param var the index of the variable
   * @param place 0: before, 1: after
   * @return true, if a space should be printed
   */
  virtual bool printSpace (int var, int place);
  /**
   * return a string that stores the state of the constraint
   * @return the string
   */
  virtual string print ();
};

#endif
