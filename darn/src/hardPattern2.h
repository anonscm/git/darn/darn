// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef HPATT2_H_INCLUDED
#define HPATT2_H_INCLUDED

#include "tools.h"
#include "automaton.h"
#include "hardConstraint.h"


/**
 * This class stands for an pattern hard constraint, involving two variables:
 *   the first variable gives the start position, the other one, the end position.
 */
class HardPattern2 : public HardConstraint {

protected:
  
  /**
   * the sequence where the pattern should be found
   */
  Sequence* sequence;
  /**
   * the pattern to be compared with the sequence
   */
  Sequence* pattern;
  /**
   * the same pattern as @a pattern, read from right to left
   */
  Sequence* reversePattern;
  /**
   * an automaton, that scans the text from left to right
   */
  Automaton *directAutomaton;
  /**
   * an automaton, that scans the text from right to left
   */
  Automaton *reverseAutomaton;
  /**
   * both automatons, namely @a directAutomaton and @a reverseAutomaton
   */
  Automaton *automatons[2];
  /**
   * matching values
   */
  MatchingValues *matchingValues;
  /**
   * check if the the text beginning at @a i1 and finishing at @a i2
   *   is an approximation of the pattern
   * @param i1 the index of the first letter of the text
   * @param i2 the index of the last letter of the text
   * @return true if the pattern and the sub-text are alike
   */
  bool isConsistent (int i1, int i2);

  /**
   * find the first word beginning between @a lb1 and @a ub1, 
   *   finishing between @a lb2 and ub2, that is an approximation of @a pattern, 
   *   the text is read from left to right or right to left, depending on @a direction
   * @param lb1 the first position where the word may start
   * @param ub1 the last position where the word may start
   * @param lb2 the first position where the word may end
   * @param ub2 the last position where the word may end
   * @param direction the direction of the scan (from left to right or vice-versa)
   * @return the positions where the word starts and ends
   */
  pair<int, int> findFirstConsistent (int lb1, int ub1, int lb2, int ub2, int direction);

  /**
   * find the first and the last words beginning between @a lb1 and @a ub1, 
   *   finishing between @a lb2 and ub2, that are  approximations of @a pattern, 
   *   the text is read from left to right or right to left, depending on @a direction
   * @param lb1 the first position where the words may start
   * @param ub1 the last position where the words may start
   * @param lb2 the first position where the words may end
   * @param ub2 the last position where the words may end
   * @param direction the direction of the scan (from left to right or vice-versa)
   * @return the positions where the words end
   */
  pair<int, int> findFirstLastConsistent (int lb1, int ub1, int lb2, int ub2, int direction);


public:
  /**
   * the constructor
   * @param v1 the start position of the pattern
   * @param v2 the end position of the pattern
   * @param i the index of this constraint
   * @param s the pattern
   * @param mc the maximum number of mismatch/insertion/deletion
   * @param mv the matching values
   */
  HardPattern2 (Variable *v1, Variable *v2, int i, string s, int mc, MatchingValues *mv);
  /**
   * revision of the constraint, when no information is given on what led to the revision
   */
  virtual void revise ();
  /**
   * revision of the constraint, due to a given variable
   * @param v the index of the given variable
   */
  virtual void revise (int v);
  /**
   * revision of the constraint, when the lower bound of a variable has changed
   * @param v the index of the given variable
   */
  virtual void reviseFromLb (int v);
  /**
   * revision of the constraint, when the upper bound of a variable has changed
   * @param v the given variable
   */
  virtual void reviseFromUb (int v);
  /**
   * revision of the constraint, when the upper bound of a variable has been assigned
   * @param v the given variable
   */
  virtual void reviseFromAssignment (int v);
  /**
   * verfiy that the support is still valid
   * @param s the support
   */
  virtual bool getConsistency (Support *s);
  /**
   * whether one should print a space before or after the variable @a var
   *  (useful for printing the results)
   * @param var the index of the variable
   * @param place 0: before, 1: after
   * @return true, if a space should be printed
   */
  virtual bool printSpace (int var, int place);
};

#endif
