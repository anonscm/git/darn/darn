// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "pattern.h"


Pattern::Pattern (Variable *v, int i, bool o, string p, ErrorsToCosts *e2c, MatchingValues *mv) : SoftConstraint(i, e2c, o, 1, v) {
  sequence = v->getSequence();
  pattern = new Sequence(p, false, false);
  matrix = new int*[pattern->getLength()+1];
  matchingValues = mv;
  for (unsigned int i = 0; i <= pattern->getLength(); i++) {
    matrix[i] = new int[static_cast<int>(pattern->getLength())+errorsToCosts->getMaxErrors()*matchingValues->getMaxCost()+1];
  }
  name = "pattern";
}

int Pattern::computeCost (int i0) {
  unsigned int l1 = pattern->getLength();
  unsigned int l2 = ((i0+static_cast<int>(l1)+errorsToCosts->getMaxErrors()*matchingValues->getMaxCost() < static_cast<int>(sequence->getLength()))? l1+static_cast<unsigned int>(errorsToCosts->getMaxErrors()*matchingValues->getMaxCost()): static_cast<unsigned int>(sequence->getLength()-i0));
  int min, match;
  if (l2 <= 0) {
    return getMaxCost()+1;
  }
  for (unsigned int i = 0; i <= l1; i++) {
    matrix[i][0] = i*matchingValues->getInsertionCost();
  }
  for (unsigned int j = 1; j <= l2; j++) {
    matrix[0][j] = j*matchingValues->getInsertionCost();
  }
  for (unsigned int i = 1; i <= l1; i++) {
    for (unsigned int j = 1; j <= l2; j++) {
      match = matchingValues->getSubstitutionCost(pattern->getNucleotide(i-1), sequence->getNucleotide(j+i0-1));
      matrix[i][j] = min3(matrix[i-1][j-1]+match, matrix[i][j-1]+matchingValues->getInsertionCost(), matrix[i-1][j]+matchingValues->getInsertionCost());
    }
  }
  min = errorsToCosts->getCostFor(matrix[l1][1]);
  for (unsigned int i = 2; i <= l2; i++) {
    if (errorsToCosts->getCostFor(matrix[l1][i]) < min) {
      min = errorsToCosts->getCostFor(matrix[l1][i]);
    }
  }
  return min;
}

int Pattern::getMinCost () {
  int min = getMaxCost()+1;
  currentSupport->unset();
  if (!unaryConstraints[0]->isDiscretized()) {
    return 0;
  }
  DomainIterator di = unaryConstraints[0]->getDomainIterator();
  for (; *di < unaryConstraints[0]->getUb(); ++di) {
    if (getMemProfile(variables[0], *di) < min) {
      min = getMemProfile(variables[0], *di);
      currentSupport->setSupport(0, *di);
    }
  }
  return diffCost(min, getMem());
}

int Pattern::getMinCost (Variable *v, int val) {
  return diffCost(computeCost(val), sumCost(getMemProfile(variables[0], val), getMem()));
}

int Pattern::getMinLbCost (Variable *v) {
  return diffCost(computeCost(unaryConstraints[0]->getLb()), sumCost(getLbMem(variables[0]), getMem()));
}

int Pattern::getMinUbCost (Variable *v) {
  return diffCost(computeCost(unaryConstraints[0]->getUb()-1), sumCost(getUbMem(variables[0]), getMem()));
}

int Pattern::getCost (Support *s) {
  return diffCost(computeCost(s->getSupport(0)), getMem());
}

int Pattern::getFinalCost (int *values) {
  int val = values[0];
  unsigned int l1 = pattern->getLength();
  unsigned int l2 = ((val+static_cast<int>(l1)+errorsToCosts->getMaxErrors()*matchingValues->getMaxCost() < static_cast<int>(sequence->getLength()))? l1+static_cast<unsigned int>(errorsToCosts->getMaxErrors()*matchingValues->getMaxCost()): static_cast<unsigned int>(sequence->getLength()-val));
  int res;
  Sequence *s = sequence->getSubSequence(val, l2);
  res = min2(getMaxCost()+1, errorsToCosts->getCostFor(getEditDistanceFactor(pattern, s, matchingValues)));
  delete s;
  return res;
}
