// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef  TOP_H_INCLUDED
#define  TOP_H_INCLUDED

#include "common.h"

/**
 * The TopValues class contains contains stuff to save and restore Top while backtracking.
 */
struct TopValues {
  int value;
  TopValues () {}
  ~TopValues () {}
};


/**
 * This class stores and possibly modifies the top value of the network
 */
class Top {

private:
  /**
   * the initial value of Top
   */
  int initialValue;
  /**
   * the current value of Top
   */
  int currentValue;

  /**
   * the stack that contains the values that the class should save for backtracking safely
   */
  TopValues **memories;
  /**
   * the number of memories already stored
   */
  int nbStoredValues;

public:
  /**
   * the constructor
   * @param iv the initial value of Top
   */
  Top (const int iv) : initialValue(iv), currentValue(iv), nbStoredValues(0) {
    memories = new TopValues*[nbTotVariables];
    for (int i = 0; i < nbTotVariables; i++) {
      memories[i] = new TopValues();
    }
  }

  /**
   * destructor
   */
  ~Top () {
    /*
    for (int i = 0; i < nbTotVariables; i++) {
      delete memories[i];
    }
    delete[] memories;
    */
  }

  /**
   * get the value of Top
   * @return Top
   */
  int getValue () {
    return currentValue;
  }

  /**
   * get the initial value of Top
   * @return the initial Top
   */
  int getInitialValue () {
    return initialValue;
  }

  /**
   * change the value
   * @return true, if the value of Top has changed
   */
  bool changeValue (int score) {
    if (printDominatedSolutions) {
      return false;
    }
    int newScore = ((score == UNDEFINED)? initialValue: min2(score+1, initialValue));
    bool flag = (currentValue != newScore);
    currentValue = newScore;
    return flag;
  }

  /**
   * save the context
   */
  void save () {
    memories[nbStoredValues]->value = currentValue;
    nbStoredValues++;
  }

  /**
   * restore the context
   */
  void restore () {
    nbStoredValues--;
    currentValue = memories[nbStoredValues]->value;
  }

  /**
   * reset to the original cost
   */
  void reset () {
    currentValue = initialValue;
    nbStoredValues = 0;
  }
};


#endif
