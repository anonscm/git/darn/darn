// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "hardComposition.h"


HardComposition::HardComposition (Variable *v1, Variable *v2, int i, Nucleotide *n1, Nucleotide *n2, int t, Relation r) : HardConstraint(i, 0, 2, v1, v2) {
  nucleotide1 = n1;
  nucleotide2 = n2;
  relation = r;
  threshold = t;
  hardness = 0;
  name = "hard composition";
}

bool HardComposition::isCorrectNucleotide (Nucleotide *n) {
  return ((n == nucleotide1) || (n == nucleotide2));
}

bool HardComposition::checkRatio (int nbPositives, int nbTotal) {
  float nbPositivesFloat = nbPositives;
  float nbTotalFloat = nbTotal;
  if (nbTotal == 0) {
    return true;
  }
  if (relation == NOT_LESS_THAN) {
    return (nbPositivesFloat / nbTotalFloat) * 100 >= threshold;
  }
  if (relation == NOT_GREATER_THAN) {
    return (nbPositivesFloat / nbTotalFloat) * 100 <= threshold;
  }
  return false;
}

pair<int, int> HardComposition::isConsistent (int var, int bound) {
  int start, stop1, stop2;
  int nbPositives = 0;
  int nbTotal = 0;
    // the first value of the non assigned variable, such that the constraint is satisfied
  int firstSupport = NOT_FOUND;
    // the last value of the non assigned variable, such that the constraint is satisfied
  int lastSupport = NOT_FOUND;

    // the leftmost variable is assigned
  if (var == 0) {
    start = unaryConstraints[0]->getLb();
    stop1 = unaryConstraints[1]->getLb();
    stop2 = unaryConstraints[1]->getUb()-1;

      // count the (correct) nucleotides
    for (int i = start; i < stop1; i++) {
      nbTotal++;
      if (isCorrectNucleotide(sequence->getNucleotide(i))) {
        nbPositives++;
      }
    }

      // also count the (correct) nucleotides
    for (int i = max2(start, stop1); i <= stop2; i++) {
      nbTotal++;
      if (isCorrectNucleotide(sequence->getNucleotide(i))) {
        nbPositives++;
      }

        // if the ratio of nucleotides is correct
      if (checkRatio(nbPositives, nbTotal)) {
          // update the supports
        if (firstSupport == NOT_FOUND) {
          firstSupport = i;
            // we only had to find the lower bound of the rightmost variable
          if (bound == 0) {
            return pair<int, int>(firstSupport, firstSupport);
          }
        }
        lastSupport = i;
      }
    }
  }

    // the rightmost variable is assigned
  if (var == 1) {
    start = unaryConstraints[1]->getLb();
    stop1 = unaryConstraints[0]->getUb()-1;
    stop2 = unaryConstraints[0]->getLb();

      // count the (correct) nucleotides
    for (int i = start; i > stop1; i--) {
      nbTotal++;
      if (isCorrectNucleotide(sequence->getNucleotide(i))) {
        nbPositives++;
      }
    }

      // also count the (correct) nucleotides
    for (int i = min2(start, stop1); i >= stop2; i--) {
        nbTotal++;
        if (isCorrectNucleotide(sequence->getNucleotide(i))) {
          nbPositives++;
        }
        // if the ratio of nucleotides is correct
      if (checkRatio(nbPositives, nbTotal)) {
          // update the supports
        firstSupport = i;
        if (lastSupport == NOT_FOUND) {
          lastSupport = i;
            // we only had to find the upper bound of the leftmost variable
          if (bound == 1) {
            return pair<int, int>(lastSupport, lastSupport);
          }
        }
      }
    }
  }
  
  return pair<int, int>(firstSupport, lastSupport);
}

void HardComposition::revise () {
  revise(0);
  revise(1);
}

void HardComposition::revise (int v) {
  reviseFromLb(v);
  reviseFromUb(v);
}

void HardComposition::reviseFromLb (int v) {
  int lb;
  pair <int, int> p;
  if (checkLbSupport(v)) {
    return;
  }
  if (!unaryConstraints[1-v]->isAssigned()) {
    return;
  }
  lbSupports[v]->unset();
  lbSupports[1-v]->unset();
  ubSupports[1-v]->unset();
  p = isConsistent(1-v, 0);
  lb = p.first;
  if (lb == NOT_FOUND) {
      // the constraint cannot be satisfied
    unaryConstraints[v]->setLb(unaryConstraints[v]->getUb());
    return;
  }
  unaryConstraints[v]->setLb(lb);

    // set the supports of the unassigned variable
  lbSupports[v]->setSupport(v, lb);
  lbSupports[v]->setSupport(1-v, unaryConstraints[1-v]->getLb());

    // set the supports of the assigned variable
  lbSupports[1-v]->setSupport(1-v, unaryConstraints[1-v]->getLb());
  lbSupports[1-v]->setSupport(v, lb);
  ubSupports[1-v]->setSupport(1-v, unaryConstraints[1-v]->getLb());
  ubSupports[1-v]->setSupport(v, lb);
}

void HardComposition::reviseFromUb (int v) {
  int ub;
  pair <int, int> p;
  if (checkUbSupport(v)) {
    return;
  }
  if (!unaryConstraints[1-v]->isAssigned()) {
    return;
  }
  ubSupports[v]->unset();
  lbSupports[1-v]->unset();
  ubSupports[1-v]->unset();
  p = isConsistent(1-v, 1);
  ub = p.second;
  if (ub == NOT_FOUND) {
      // the constraint cannot be satisfied
    unaryConstraints[v]->setUb(unaryConstraints[v]->getLb());
    return;
  }
  unaryConstraints[v]->setUb(ub+1);

    // set the supports of the unassigned variable
  ubSupports[v]->setSupport(v, ub);
  ubSupports[v]->setSupport(1-v, unaryConstraints[1-v]->getLb());

    // set the supports of the assigned variable
  lbSupports[1-v]->setSupport(1-v, unaryConstraints[1-v]->getLb());
  lbSupports[1-v]->setSupport(v, ub);
  ubSupports[1-v]->setSupport(1-v, unaryConstraints[1-v]->getLb());
  ubSupports[1-v]->setSupport(v, ub);
}

void HardComposition::reviseFromAssignment (int v) {
  int lb, ub;
  pair <int, int> p;
  if ((checkLbSupport(v)) || (checkUbSupport(v))) {
    return;
  }
    // check the 2 bounds of the other variables
  p = isConsistent(v, 2);
  lb = p.first;
  ub = p.second;

    // if no support was found
  if (lb == NOT_FOUND) {
      // empty the domain of v
    unaryConstraints[v]->setLb(unaryConstraints[v]->getUb());
    return;
  }

    // set the new bounds for the other variable
  unaryConstraints[1-v]->setLb(lb);
  unaryConstraints[1-v]->setUb(ub+1);

    // set the supports of the recently assigned variable
  lbSupports[v]->setSupport(v, unaryConstraints[v]->getLb());
  lbSupports[v]->setSupport(1-v, lb);
  ubSupports[v]->setSupport(v, unaryConstraints[v]->getLb());
  ubSupports[v]->setSupport(1-v, ub);

    // set the supports of the unassigned variable
  lbSupports[1-v]->setSupport(1-v, lb);
  lbSupports[1-v]->setSupport(v, unaryConstraints[v]->getLb());
  ubSupports[1-v]->setSupport(1-v, ub);
  ubSupports[1-v]->setSupport(v, unaryConstraints[v]->getLb());
}

bool HardComposition::getConsistency (Support *s) {
  int start = s->getSupport(0);
  int stop = s->getSupport(1);
  int nbPositives = 0;
  int nbTotal = 0;
  for (int i = start; i <= stop; i++) {
    ++nbTotal;
    nbPositives += ((isCorrectNucleotide(sequence->getNucleotide(i)))? 1 : 0);
  }
  return (checkRatio(nbPositives, nbTotal));
}

