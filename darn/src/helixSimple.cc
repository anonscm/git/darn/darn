// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "helixSimple.h"


HelixSimple::HelixSimple (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int i, bool o, int msl, int mmsl, int mll, int mmll, ErrorsToCosts *e2c, InteractionValues *iv, bool insert) : SoftConstraint(i, e2c, o, 4, v1, v2, v3, v4), minStemLength(msl), maxStemLength(mmsl), minLoopLength(mll), maxLoopLength(mmll), insertion(insert) {
  simple = false;
  matrix = new int[maxStemLength+1];
  interactionValues = iv;
  name = "helix";
}

int HelixSimple::getMinCost () {
  return 0;
}

int HelixSimple::getMinCost (Variable *var, int val) {
  int i, lb1, ub1, lb2, ub2, lb3, ub3, lb4, ub4, result;
  currentSupport->unset();
  /*
  if (getMemory(var, val) >= getMaxCost()) {
    return 0;
  }
  */
  i = getVariableIndex(var);
  lb1 = getVariable(0)->getUnaryConstraint()->getLb();
  ub1 = getVariable(0)->getUnaryConstraint()->getUb();
  lb2 = getVariable(1)->getUnaryConstraint()->getLb();
  ub2 = getVariable(1)->getUnaryConstraint()->getUb();
  lb3 = getVariable(2)->getUnaryConstraint()->getLb();
  ub3 = getVariable(2)->getUnaryConstraint()->getUb();
  lb4 = getVariable(3)->getUnaryConstraint()->getLb();
  ub4 = getVariable(3)->getUnaryConstraint()->getUb();
  switch (i) {
    case 0:
      lb1 = val;
      ub1 = val+1;
      break;
    case 1:
      lb2 = val;
      ub2 = val+1;
      break;
    case 2:
      lb3 = val;
      ub3 = val+1;
      break;
    case 3:
      lb4 = val;
      ub4 = val+1;
      break;
  }
  result = getMinCost (lb1, ub1, lb2, ub2, lb3, ub3, lb4, ub4);
  /*
  if (result + getMemory(var, val) >= getMaxCost()) {
    return getMaxCost() - getMemory(var, val);
  }
  */
  return result;
}

int HelixSimple::getMinLbCost (Variable *var) {
  return getMinCost(var, var->getUnaryConstraint()->getLb());
}

int HelixSimple::getMinUbCost (Variable *var) {
  return getMinCost(var, var->getUnaryConstraint()->getUb()-1);
}

int HelixSimple::getMinCost (int lb1, int ub1, int lb2, int ub2, int lb3, int ub3, int lb4, int ub4) {
  if ((ub2-lb1 < static_cast<int>(HELIXMINLENGTH)) || (ub4-lb3 < static_cast<int>(HELIXMINLENGTH)) || (ub3-lb2 < static_cast<int>(LOOPMINLENGTH))) {
    return getMaxCost()+1;
  }
  if ((ub2-lb1 >= static_cast<int>(HELIXMAXLENGTH)) || (ub4-lb3 >= static_cast<int>(HELIXMAXLENGTH))) {
    return 0;
  }
  return computeResult(lb1, ub1, lb2, ub2, lb3, ub3, lb4, ub4);
 }

int HelixSimple::computeResult (int lb1, int ub1, int lb2, int ub2, int lb3, int ub3, int lb4, int ub4) {
  int min = getMaxCost()+1;
  int mem1, mem2, mem3, mem4, mem;
  bool isASupport = false;
  int startI, stopI, startJ, stopJ, startK, stopK, startL, stopL;
  int match, value, val1, val2, val3, previous, nbCurrentErrors, cost;

  mem = getMem();

  // find all possible values for the 1st variable
  startI = max4(lb1, lb2-(maxStemLength-1), lb3-(maxStemLength-1+maxLoopLength+1), lb4-(2*(maxStemLength-1)+maxLoopLength+1));
  stopI = min4(ub1-1, ub2-1-(minStemLength-1), ub3-1-(minStemLength-1+minLoopLength+1), ub4-1-(2*(minStemLength-1)+minLoopLength+1));

  // scan the possibles values for the first variable
  for (int i = startI; i <= stopI; i++) {
    mem1 = getMemory(unaryConstraints[0]->getVariable(), i);

    // find all possible values for the last variable
    startL = max4(i+2*(minStemLength-1)+(minLoopLength+1), lb2+(minStemLength-1+minLoopLength+1), lb3+(minStemLength-1), lb4);
    stopL = min4(i+(2*(maxStemLength-1)+maxLoopLength+1), ub2-1+(maxStemLength-1+maxLoopLength+1), ub3-1+(maxStemLength-1), ub4-1);

    // scan the possibles values for the last variable
    for (int l = startL; l <= stopL; l++) {
      mem4 = getMemory(unaryConstraints[3]->getVariable(), l);

      // find all possible values for the second variable
      startJ = max4(i+minStemLength-1, lb2, lb3-(maxLoopLength+1), l-(maxStemLength-1+maxLoopLength+1));
      stopJ = min4(i+maxStemLength-1, ub2-1, ub3-1-(minLoopLength+1), l-(minStemLength-1+minLoopLength+1));
      // find all possible values for the third variable
      startK = max4(i+minStemLength-1+minLoopLength+1, lb2+minLoopLength+1, lb3, l-(maxStemLength-1));
      stopK = min4(i+maxStemLength-1+maxLoopLength+1, ub2-1+maxLoopLength+1, ub3-1, l-(minStemLength-1));

      // if insertions are allowed
      if (insertion) {

        // set the base cases of the dynamic programming matrix
        for (int col = 0; col <= min2(l-startK+1, errorsToCosts->getMaxErrors() / interactionValues->getInsertionCost()); col++) {
          matrix[col] = col * interactionValues->getInsertionCost();
        }

        // fill in the matrix
        for (int lin = 1; lin <= stopJ-i+1; lin++) {
          if (lin - errorsToCosts->getMaxErrors() / interactionValues->getInsertionCost() > 1) {
            previous = matrix[lin - errorsToCosts->getMaxErrors() / interactionValues->getInsertionCost() - 1];
            matrix[lin - errorsToCosts->getMaxErrors() / interactionValues->getInsertionCost() - 1] = errorsToCosts->getMaxErrors() + 1;
          }
          else {
            previous = (lin-1) * interactionValues->getInsertionCost();
            matrix[0] = lin * interactionValues->getInsertionCost();
            isASupport = true;
          }

          for (int col = max2(1, lin-errorsToCosts->getMaxErrors()/interactionValues->getInsertionCost()); col <= min2(l-startK+1, lin+errorsToCosts->getMaxErrors()/interactionValues->getInsertionCost()); col++) {
            match = interactionValues->getMatchCost(sequence->getNucleotide(i+(lin-1)), sequence->getNucleotide(l-(col-1)));
            val1 = previous + match;
            if (col == lin+errorsToCosts->getMaxErrors()/interactionValues->getInsertionCost()) {
              val2 = errorsToCosts->getMaxErrors()+1;
            }
            else {
              val2 = matrix[col] + interactionValues->getInsertionCost();
            }
            val3 = matrix[col-1] + interactionValues->getInsertionCost();
            value = min3(val1, val2, val3);
            previous = matrix[col];
            matrix[col] = value;

            if (errorsToCosts->getCostFor(value) <= getMaxCost()) {
              // if a support has been found
              if ((i + lin-1 >= startJ) && (l-(col-1) <= stopK) && ((l-(col-1))-(i+(lin-1)) >= minLoopLength+1) && ((l-(col-1))-(i+(lin-1)) <= maxLoopLength+1)) {
                mem2 = getMemory(unaryConstraints[1]->getVariable(), i + lin-1);
                mem3 = getMemory(unaryConstraints[2]->getVariable(), l - (col-1));
                cost = diffCost(errorsToCosts->getCostFor(value), mem + mem1 + mem2 + mem3 + mem4);
                if (cost < min) {
                  min = cost;
                  currentSupport->setSupport(0, i);
                  currentSupport->setSupport(1, i + (lin-1));
                  currentSupport->setSupport(2, l - (col-1));
                  currentSupport->setSupport(3, l);
                  if (cost == 0) {
                    return 0;
                  }
                }
              }
              // a score in the line was less than maxCost
              isASupport = true;
            }
          }
          // if no score in the line was less than maxCost
          if (!isASupport) {
            // select another value for the second variable
            break;
          }
        }
      }

      // if no insertion is allowed
      else {
        // find all possible values for the second and third variables
        nbCurrentErrors = 0;

        // scan the possible values for the second and third variables
        for (int lin = 0, col = 0; ((i+lin <= stopJ) && (l-col >= startK)); lin++, col++) {
          match = interactionValues->getMatchCost(sequence->getNucleotide(i+lin), sequence->getNucleotide(l-col));
          nbCurrentErrors += match;
          if (errorsToCosts->getCostFor(nbCurrentErrors) > getMaxCost()) {
            break;
          }
          // if we have found a support
          if ((i+lin >= startJ) && (l-col <= stopK) && ((l-col)-(i+lin) >= minLoopLength+1) && ((l-col)-(i+lin) <= maxLoopLength+1)) {
            mem2 = getMemory(unaryConstraints[1]->getVariable(), i+lin);
            mem3 = getMemory(unaryConstraints[2]->getVariable(), l-col);
            cost = diffCost(errorsToCosts->getCostFor(nbCurrentErrors), mem + mem1 + mem2 + mem3 + mem4);
            if (cost < min) {
              min = cost;
              currentSupport->setSupport(0, i);
              currentSupport->setSupport(1, i + lin);
              currentSupport->setSupport(2, l - col);
              currentSupport->setSupport(3, l);
              if (cost == 0) {
                return 0;
              }
            }
          }
        }
      }
    }
  }
  return min;
}

int HelixSimple::getCost (Support *s) {
  int val1 = s->getSupport(0);
  int val2 = s->getSupport(1);
  int val3 = s->getSupport(2);
  int val4 = s->getSupport(3);
  return computeResult(val1, val1+1, val2, val2+1, val3, val3+1, val4, val4+1);
}

int HelixSimple::getFinalCost (int *values) {
  int val1 = values[0];
  int val2 = values[1];
  int val3 = values[2];
  int val4 = values[3];
  int l1 = val2-val1+1, l2 = val4-val3+1;
  Sequence *s1, *s2;
  s1 = sequence->getSubSequence(val1, l1);
  s2 = sequence->getSubSequence(val3, l2);
  return min2(getMaxCost()+1, errorsToCosts->getCostFor(getMatchCost(s1, s2, interactionValues, false)));
}
