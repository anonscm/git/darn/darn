// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "variable.h"

Variable::Variable (int i, string n, Sequence *s) : index(i), name(n), seq(s) {
  domain = new Domain(index, 0, seq->getLength());
}

Variable::~Variable () {
  /*
  delete domain;
  */
}

void Variable::reset () {
  domain->reset(0, seq->getLength());
}

int Variable::getIndex () {
  return index;
}

string Variable::getName () {
  return name;
}

Domain *Variable::getDomain () {
  return domain;
}

Sequence *Variable::getSequence () {
  return seq;
}

void Variable::setUnaryConstraint (DefaultUnarySoftConstraint *c) {
  unaryconstraint = c;
}

void Variable::addSoftConstraint (AbstractSoftConstraint *c) {
  softconstraints.push_back(c);
}

void Variable::addHardConstraint (AbstractHardConstraint *c) {
  hardconstraints.push_back(c);
}

string Variable::print () {
  return "[" + name + ":" + domain->print() + "]";
}

DefaultUnarySoftConstraint *Variable::getUnaryConstraint () {
  return unaryconstraint;
}

int Variable::getNbSoftConstraints () {
  return softconstraints.size();
}

AbstractSoftConstraint *Variable::getSoftConstraint (int i) {
  return softconstraints[i];
}

int Variable::getNbHardConstraints () {
  return hardconstraints.size();
}

AbstractHardConstraint *Variable::getHardConstraint (int i) {
  return hardconstraints[i];
}
