// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef  VI_H_INCLUDED
#define  VI_H_INCLUDED


#include "common.h"



/**
 * This class simply provides with tools to iterate on the set of unassigned variables.
 */
class VariableIterator {
  
private:
  /**
   * the total number of variables
   */
  int nbVariables;
  /**
   * the set of the unassigned variables
   * (a bit is true if the corresponding variable is unassigned)
   */
  bool *variables;
  /**
   * the cursor
   */
  int cursor;
  /**
   * get the next element (if exists)
   */
  void getNext () {
    for (cursor++; cursor < nbVariables; cursor++) {
      if (variables[cursor]) {
        return;
      }
    }
  }

public:
  /**
   * constructor, set the cursor to the first element (if exists)
   * @param nv the number of variables
   * @param v the set of variables
   */
  VariableIterator (int nv, bool *v) {
    nbVariables = nv;
    variables = v;
    cursor = -1;
    getNext();
  }

  /**
   * get the next element
   * @return itself
   */
  VariableIterator operator++ () {
    getNext();
    return *this;
  }

  /**
   * get the current element
   * @return the index of the current variable
   */
  int operator* () {
    if (cursor >= nbVariables) {
      trace(1, "Error! Trying to access an out-of-range variable in a VariableIterator!");
    }
    return cursor;
  }

  /**
   * check whether all unassigned variable have been scanned
   * @return true, if all unassigned variable have been scanned
   */
  bool isOver () {
    return (cursor >= nbVariables);
  }
};

#endif
