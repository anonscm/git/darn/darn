// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef  DOMIT_H_INCLUDED
#define  DOMIT_H_INCLUDED

#include "common.h"


/**
 * The DomainIterator class provides a convenient iterator for a domain (whichever the representation of the domain). The cursor can go forwards or backwards.
 */
class DomainIterator {

private:
  /**
   * whether the domain on which the class iterates is discretized
   */
  bool discretized;
  /**
   * the lower bound of the domain
   */
  int lb;
  /**
   * the upper bound of the domain
   */
  int ub;
  /**
   * if the domain is discretized, the authorized values
   */
  int *discreteDomain;
  /**
   * the first element of the domain when discretizing (used as a landmark)
   */
  int firstElement;
  /**
   * the current value of the iteration
   */
  int cursor;
  /**
   * whether the iterator should go backwards
   */
  bool reverse;

public:
  /**
   * constructor when the domain is not discretized
   * @param l the lower bound
   * @param u the upper bound
   * @param r whether the cursor goes backwards
   */
  DomainIterator (int l, int u, bool r = false);
  /**
   * constructor when the domain is discretized
   * @param dd the list of all authorized values
   * @param l the lower bound
   * @param u the upper bound
   * @param fe the first element of the values
   * @param r whether the cursor goes backwards
   */
  DomainIterator (int *dd, int l, int u, int fe, bool r = false);
  /**
   * @pre the iterator should go forwards
   * give the next element
   * @return the iterator itself
   */
  DomainIterator operator++ ();
  /**
   * @pre the iterator should go backwards
   * give the next element
   * @return the iterator itself
   */
  DomainIterator operator-- ();
  /**
   * deference the iterator
   * @return the value referenced by the iterator
   */
  int operator*();

  /**
   * move the cursor to the given value
   * @param v the given value
   */
  DomainIterator setTo (int v);
};


#endif
