// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "softConstraint.h"


SoftConstraint::SoftConstraint (int i, ErrorsToCosts *e2c, bool o, int nv, Variable *v ...) : AbstractSoftConstraint(i, e2c, nv), optional(o) {
  variables = new Variable*[nbVariables];
  unaryConstraints = new DefaultUnarySoftConstraint*[nbVariables];
  cost = new NaryCost(index, nbVariables);
  currentSupport = new Support(nbVariables);
  variables[0] = v;
  variables[0]->addSoftConstraint(this);
  unaryConstraints[0] = variables[0]->getUnaryConstraint();
  if (unaryConstraints[0]->isDiscretized()) {
    setDiscretized(variables[0]);
  }
  va_list var;
  va_start(var, v);
  for (int i = 1; i < nbVariables; i++) {
    variables[i] = va_arg(var, Variable*);
    variables[i]->addSoftConstraint(this);
    unaryConstraints[i] = variables[i]->getUnaryConstraint();
    if (unaryConstraints[i]->isDiscretized()) {
      setDiscretized(variables[i]);
    }
  }
  va_end(var);
}

SoftConstraint::~SoftConstraint () {
  /*
  delete[] variables;
  delete[] unaryConstraints;
  delete cost;
  delete currentSupport;
  */
}

void SoftConstraint::reset () {
  cost->reset();
  for (int i = 1; i < nbVariables; i++) {
    if (unaryConstraints[i]->isDiscretized()) {
      setDiscretized(variables[i]);
    }
  }
  AbstractSoftConstraint::reset();
}

bool SoftConstraint::isOptional() {
  return optional;
}

DefaultUnarySoftConstraint* SoftConstraint::getUnaryConstraint(int i) {
  return unaryConstraints[i];
}

/*
int SoftConstraint::getMaxProjected (DefaultUnarySoftConstraint *uc) {
  Variable *var = uc->getVariable();
  int max = 0;
  if (uc->isDiscretized()) {
    DomainIterator di = uc->getDomainIterator();
    for (;*di < uc->getUb(); ++di) {
      max = max2(max, getMemProfile(var, *di));
    }
    return max;
  }
  return max2(getLbMem(var), getUbMem(var));
}
*/

void SoftConstraint::projectLb (DefaultUnarySoftConstraint *uc) {
  int min;
  Variable *var = uc->getVariable();
  DefaultUnarySoftConstraint *currentUc;
  Variable *currentVariable;
  int maxProjected = 0;

  if (uc->isDiscretized()){
    project(uc, uc->getLb());
    return;
  }
  
    // if maxCost has already been projected, or the value is inconsistent, quit
  if ((getLbMem(var) >= getMaxCost()) || (zeroArityConstraint->getMinCost() + uc->getLbMem() - uc->getMem() >= top->getValue())) {
    return;
  }
    // if the support is valid, quit
  if (checkSupport(getLbSupport(var))) {
    return;
  }
  min = getMinLbCost(var);
  if (min < 0) {
    trace(1, "Error! Got a negative project cost for an lbMem!");
  }

    // never project more than maxCost to unary constraint (even in toto)
  for (int i = 0; i < getNbVariables(); i++) {
    currentUc = unaryConstraints[i];
    currentVariable = currentUc->getVariable();
    if (currentVariable != var) {
      maxProjected += getMaxProjected(currentVariable);
    }
  }
  maxProjected += getLbMem(var);
  if (min <= getMaxCost() - maxProjected) {
    getLbSupport(var)->copy(getCurrentSupport());
  }
  else {
    min = getMaxCost() - maxProjected;
  }

  incLbMem(var, min);
  uc->incLbMem(min);
}

void SoftConstraint::projectUb (DefaultUnarySoftConstraint *uc) {
  int min;
  Variable *var = uc->getVariable();
  DefaultUnarySoftConstraint *currentUc;
  Variable *currentVariable;
  int maxProjected = 0;

  if (uc->isDiscretized()){
    project(uc, uc->getUb()-1);
    return;
  }
  
    // if maxCost has already been projected, or the value is inconsistent, quit
  if ((getUbMem(var) >= getMaxCost()) || (zeroArityConstraint->getMinCost() + uc->getUbMem() - uc->getMem() >= top->getValue())) {
    return;
  }
    // if the support is valid, quit
  if (checkSupport(getUbSupport(var))) {
    return;
  }
  min = getMinUbCost(var);
  if (min < 0) {
    trace(1, "Error! Got a negative project cost for a ubMem!");
  }

    // never project more than maxCost to unary constraint (even in toto)
  for (int i = 0; i < getNbVariables(); i++) {
    currentUc = unaryConstraints[i];
    currentVariable = currentUc->getVariable();
    if (currentVariable != var) {
      maxProjected += getMaxProjected(currentVariable);
    }
  }
  maxProjected += getUbMem(var);
  if (min <= getMaxCost() - maxProjected) {
    getUbSupport(var)->copy(getCurrentSupport());
  }
  else {
    min = getMaxCost() - maxProjected;
  }

  incUbMem(var, min);
  uc->incUbMem(min);
}

void SoftConstraint::project (DefaultUnarySoftConstraint *uc, int val) {
  int min;
  Variable *var = uc->getVariable();
  DefaultUnarySoftConstraint *currentUc;
  Variable *currentVariable;
  int maxProjected = 0;

    // if maxCost has already been projected, or the value is inconsistent, quit
  if ((getMemProfile(var, val) >= getMaxCost()) || (zeroArityConstraint->getMinCost() + uc->getMemProfile(val) - uc->getMem() >= top->getValue())) {
    return;
  }
    // if the support is valid, quit
  if (checkSupport(getSupport(var, val))) {
    return;
  }
  min = getMinCost(var, val);
  if (min < 0) {
    trace(1, "Error! Got a negative project cost for a mem!");
  }
    // never project more than maxCost to unary constraint (even in toto)
  for (int i = 0; i < getNbVariables(); i++) {
    currentUc = unaryConstraints[i];
    currentVariable = currentUc->getVariable();
    if (currentVariable != var) {
      maxProjected += getMaxProjected(currentVariable);
    }
  }
  maxProjected += getMemProfile(var, val);
  if (min <= getMaxCost() - maxProjected) {
    getSupport(var, val)->copy(getCurrentSupport());
  }
  else {
    min = getMaxCost() - maxProjected;
  }

  incMemProfile(var, val, min);
  uc->incMemProfile(val, min);
}

void SoftConstraint::reviseFromLb (Variable *v) {
  DefaultUnarySoftConstraint *uc = v->getUnaryConstraint();
  if (!uc->isDiscretized()) {
    projectLb(uc);
    reviseNeighbors(v);
  }
  else {
    reviseFromHole(v, uc->getLb());
  }
}

void SoftConstraint::reviseFromUb (Variable *v) {
  DefaultUnarySoftConstraint *uc = v->getUnaryConstraint();
  if (!uc->isDiscretized()) {
    projectUb(uc);
    reviseNeighbors(v);
  }
  else {
    reviseFromHole(v, uc->getUb()-1);
  }
}

void SoftConstraint::reviseFromHole (Variable *v, int val) {
  if (!v->getUnaryConstraint()->isDiscretized()) {
    return;
  }
  reviseNeighbors(v);
}

void SoftConstraint::reviseFromAssignment (Variable *v) {
  DefaultUnarySoftConstraint *uc = v->getUnaryConstraint();
  int lb = uc->getLb();
  if (!uc->isDiscretized()) {
    projectLb(uc);
  }
  else {
    project(uc, lb);
  }
  reviseNeighbors(v);
}

void SoftConstraint::reviseNeighbors (Variable *v) {
  bool foundSupport;
  DefaultUnarySoftConstraint *uc = v->getUnaryConstraint();
  DefaultUnarySoftConstraint *currentUc;
  Variable *currentVariable;
  int currentValue;
  for (int i = 0; i < getNbVariables(); i++) {
    currentUc = unaryConstraints[i];
    currentVariable = currentUc->getVariable();
    if (currentVariable != v) {
      if (!currentUc->isDiscretized()) {
        do {
          foundSupport = true;
          projectLb(currentUc);
        }
        while ((!foundSupport) && (currentUc->getLb() < currentUc->getUb()));
        do {
          foundSupport = true;
          projectUb(currentUc);
        }
        while ((!foundSupport) && (currentUc->getLb() < currentUc->getUb()));
        currentUc->setANC();
      }
      else {
        DomainIterator di = currentUc->getDomainIterator();
        for (;*di < currentUc->getUb(); ++di) {
          currentValue = *di;
          project(currentUc, currentValue);
        }
        currentUc->setANC();
      }
      uc->setANC();
    }
  }
}
