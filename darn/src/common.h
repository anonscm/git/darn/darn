#ifndef COM_H_INCLUDED
#define COM_H_INCLUDED

#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <cstring>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <stack>
#include <list>
#include <ctime>
#include <csignal>
#include <unistd.h>
using namespace std;

class Sequence;
class Variable;
class AbstractConstraint;
class AbstractSoftConstraint;
class SoftConstraint;
class HardConstraint;
class Revision;
class FileReader;
class Top;
class Network;

// Global variables
extern ostream *outputStream;
extern int nbTotVariables;
extern vector<SoftConstraint *> softConstraints;
extern int nbSoftHelix;
extern vector<HardConstraint *> hardConstraints;
extern int nbHardHelix;
extern int traceLevel;
extern Variable **variables;
extern AbstractSoftConstraint *zeroArityConstraint;
extern Top *top;
extern Revision *lbHardRevision;
extern Revision *ubHardRevision;
extern Revision *lbComplicatedHardRevision;
extern Revision *ubComplicatedHardRevision;
extern Revision *assignmentHardRevision;
extern Revision *assignmentComplicatedHardRevision;
extern Revision *assignmentSoftRevision;
extern Revision *lbSoftRevision;
extern Revision *ubSoftRevision;
extern Revision *holeSoftRevision;
extern Revision *assignmentComplicatedSoftRevision;
extern Revision *lbComplicatedSoftRevision;
extern Revision *ubComplicatedSoftRevision;
extern Revision *holeComplicatedSoftRevision;
extern FileReader *sequenceFileReader;
extern Sequence *sequence;
extern Sequence **dupSequences;
extern int nbDupSequences;
extern char *descriptorFileName;
extern bool sequenceBoth;
extern bool sequenceReverse;
extern bool sequenceComplement;
extern bool duplexReverse;
extern bool duplexComplement;
extern bool printDominatedSolutions;
extern bool orderedByCost;
extern bool explanation;
extern bool gffFormat;
extern bool parserFormat;
extern bool fastaFormat;
extern bool displayStructure;
extern bool webDisplay;
extern char *drawDirectory;
extern char *webDrawDirectory;
extern Network *network;


/**
 * The class describes an exception raised by a constraint.
 */
class inconsistency {
  private:
    /**
     * the guilty constraint
     */
    AbstractConstraint *constraint;
  public:
    /**
     * the constructor
     * @param c the guilty constraint
     */
    inconsistency (AbstractConstraint *c) : constraint(c) {}
    /**
     * accessor
     * @return the guilty constraint
     */
    AbstractConstraint *getConstraint () {return constraint;}
};

/**
 * the number of different elements of the alphabet
 */
static const unsigned int NBNUCLEOTIDES = 6;
/**
 * the number of different elements of the alphabet (including ambiguous nucleotides)
 */
static const unsigned int NBEXTENDEDNUCLEOTIDES = 16;
/**
 * the maximum number of duplex sequences
 */
static const unsigned int NBMAXDUPLEXSEQUENCES = 1;
/**
 * the number of different possible nucleotides interaction (Watson-Crick, Hoogsteen, Sugar-Edge)
 */
static const unsigned int NBINTERACTIONS = 3;
/**
 * the number of different possible inucleotides nteraction (cis and trans)
 */
static const unsigned int NBORIENTATIONS = 2;
/**
 * the maximum number of helix scores stored
 */
static const unsigned int MAXHELIXSCORES = 100;
/**
 * the size of the domain of the variables where to discretize
 */
static const unsigned int MAXSIZE = 2;
/**
 * the maximum size of an helix
 */
static const unsigned int HELIXMAXLENGTH = 1000;
/**
 * the minimum size of an helix
 */
static const unsigned int HELIXMINLENGTH = 1;
/**
 * the maximum size of an hairpin size
 */
static const unsigned int HAIRPINMAXLENGTH = 2000;
/**
 * the minimum size of an hairpin size
 */
static const unsigned int HAIRPINMINLENGTH = 3;
/**
 * the maximum size of an hairpin loop
 */
static const unsigned int LOOPMAXLENGTH = 200;
/**
 * the minimum size of an hairpin loop
 */
static const unsigned int LOOPMINLENGTH = 3;
/**
 * the maximum size of the edge of the matrixes
 */
static const unsigned int MATRIXMAXLENGTH = 100;
/**
 * the maximum size of a duplex (plus number of errors)
 */
static const unsigned int DUPLEXMAXLENGTH = 250;
/**
 * the maximum number of errors that can be found in an element of structure
 */
static const unsigned int NBMAXERRORS = 1000;
/**
 * maximum number of isostericity families, given a non-canonic interaction
 */
static const unsigned int NBFAMILIES = 6;
/**
 * a constant that refers to all isostericity families, given a non-canonic interaction
 */
static const unsigned int ALLFAMILIES = 0;
/**
 * maximum size of a solution
 */
static const unsigned int MAXCANDIDATESIZE = 1000;
/**
 * the size of a line when outputing in FASTA format
 */
static const unsigned int MAXLINESIZE = 60;
/**
 * the maximum size of the hash table of the good recorder
 */
static const unsigned int HASHSIZE = 1001;
/**
 * useful constant, to be returned when a value could be found
 */
static const int NOT_FOUND = -1;
/**
 * useful constant, to be returned when a value is not defined
 */
static const int UNDEFINED = -2;
/**
 * where the costs are stored
 */
static const char costFile[] = "costs.dat";

/**
 * print a trace to the output stream
 * @param level the degree of verbosity of the message
 * @param message the message
 */
static inline void trace (int level, string message) {
  if (level < traceLevel) (*outputStream) << message << endl;
}

/**
 * useful function to get the minimum value out of two values
 * @param i1 the first value
 * @param i2 the second value
 * @return the minimum of @a i1 and @a i2
 */
static inline int min2 (const int i1, const int i2) {
  return ((i1 < i2)? i1: i2);
}

/**
 * useful function to get the minimum value out of three values
 * @param i1 the first value
 * @param i2 the second value
 * @param i3 the third value
 * @return the minimum of @a i1, @a i2 and @a i3
 */
static inline int min3 (const int i1, const int i2, const int i3) {
  return min2(min2(i1, i2), i3);
}

/**
 * useful function to get the minimum value out of four values
 * @param i1 the first value
 * @param i2 the second value
 * @param i3 the third value
 * @param i4 the fourth value
 * @return the minimum of @a i1, @a i2, @a i3 and @a i4
 */
static inline int min4 (const int i1, const int i2, const int i3, const int i4) {
  return min2(min2(i1, i2), min2(i3, i4));
}

/**
 * useful function to get the maximum value out of two values
 * @param i1 the first value
 * @param i2 the second value
 * @return the maximum of @a i1 and @a i2
 */
static inline int max2 (const int i1, const int i2) {
    return ((i1 > i2)? i1: i2);
}

/**
 * useful function to get the maximum value out of three values
 * @param i1 the first value
 * @param i2 the second value
 * @param i3 the third value
 * @return the maximum of @a i1, @a i2 and @a i3
 */
static inline int max3 (const int i1, const int i2, const int i3) {
    return max2(max2(i1, i2), i3);
}

/**
 * useful function to get the maximum value out of four values
 * @param i1 the first value
 * @param i2 the second value
 * @param i3 the third value
 * @param i4 the fourth value
 * @return the maximum of @a i1, @a i2, @a i3 and @a i4
 */
static inline int max4 (const int i1, const int i2, const int i3, const int i4) {
    return max2(max2(i1, i2), max2(i3, i4));
}

/**
 * useful function to get the minimum element of two values
 * @param i1 the first value
 * @param i2 the second value
 * @return 0, if @a i1 <= @a i2, 1 otherwise
 */
static inline int argMin2 (const int i1, const int i2) {
  return ((i2 < i1)? 1: 0);
}

/**
 * useful function to get the minimum element of three values
 * @param i1 the first value
 * @param i2 the second value
 * @param i3 the third value
 * @return 0, if @a i1 <= @a i2 and @a i2 <= @a i3, and so on
 */
static inline int argMin3 (const int i1, const int i2, const int i3) {
  if (i2 < i1) {
    return ((i3 < i2)? 2: 1);
  }
  else {
    return ((i3 < i1)? 2: 0);
  }
}

/**
 * useful function to get the minimum element of four values
 * @param i1 the first value
 * @param i2 the second value
 * @param i3 the third value
 * @param i4 the fourth value
 * @return 0, if @a i1 <= @a i2 and @a i2 <= @a i3 and @a i3 <= @a i4, and so on
 */
static inline int argMin4 (const int i1, const int i2, const int i3, const int i4) {
  if (i2 < i1) {
    if (i3 < i2) {
      return ((i4 < i3)? 3: 2);
    }
    else {
      return ((i4 < i2)? 3: 1);
    }
  }
  else {
    if (i3 < i1) {
      return ((i4 < i3)? 3: 2);
    }
    else {
      return ((i4 < i1)? 3: 0);
    }
  }
}

/**
 * lexicographic order for pairs
 * @param a1 the first element of the first pair
 * @param a2 the second element of the first pair
 * @param b1 the first element of the second pair
 * @param b2 the second element of the second pair
 * @return true, if the first pair in not greater than the second one
 */
static inline bool leq (const int a1, const int a2, const int b1, const int b2) {
  return ((a1 < b1) || ((a1 == b1) && (a2 <= b2))); 
}

/**
 * lexicographic order for triples
 * @param a1 the first element of the first triple
 * @param a2 the second element of the first triple
 * @param a3 the third element of the first triple
 * @param b1 the first element of the second triple
 * @param b2 the second element of the second triple
 * @param b3 the third element of the second triple
 * @return true, if the first triple in not greater than the second one
 */
static inline bool leq (const int a1, const int a2, const int a3, const int b1, const int b2, const int b3) {
  return ((a1 < b1) || ((a1 == b1) && leq(a2,a3, b2,b3))); 
}


/**
 * give the possible relations with respect to a threshold
 */
typedef enum {NOT_LESS_THAN, NOT_GREATER_THAN} Relation;


/**
 * The class Pair provides some tools to manage with a pair of integers
 */
struct Pair {
  /**
   * the first element of the quadruplet
   */
  int first;
  /**
   * the second element of the quadruplet
   */
  int second;
  /**
   * constructor
   * @param i the first element
   * @param j the second element
   */
  Pair (int i = 0, int j = 0) : first(i), second(j) {}
  /**
   * the test of equality
   * @param p1 the first pair
   * @param p2 the second pair
   * @return true if both pairs are equal (i.e. all elements are equal)
   */
  friend int operator==(const Pair &p1, const Pair &p2) {
    return (((p1.first == p2.first) && (p1.second == p2.second))? 1: 0);
  }
  /**
   * the test of inequality
   * @param p1 the first pair
   * @param p2 the second pair
   * @return true if the first pair is less than the second (with the lexicographic order)
   */
  friend bool operator<(const Pair &p1, const Pair &p2) {
    if (p1.first != p2.first) {
      return (p2.second - p1.second < 0);
    }
    return (p2.second - p1.second < 0);
  }
  /**
   * the key comparator fonction for the map container
   * @param p1 the first pair
   * @param p2 the second pair
   * @return true if the first pair is less than the second
   */
  bool operator()(const Pair p1, const Pair p2) const {
    return (p1 < p2);
  } 
  /**
   * the accessor the a random element
   * @param i the index of a element
   * @return the value of the element
   */
  int getElement (int i) {
    switch (i) {
      case 0:
        return first;
      case 1:
        return second;
      default:
        trace (1, "Error! Trying to get a non-existing value in a pair!");
        return 0;
    }
  }
};

#endif
