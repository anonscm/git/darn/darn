// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "abstractSoftConstraint.h"

AbstractSoftConstraint::AbstractSoftConstraint (int i, ErrorsToCosts *e2c, int nv) : AbstractConstraint(i, e2c->getMaxCost(), nv) {
  simple = true;
  cost = new Cost(index, 0);
  currentSupport = new Support(0);
  errorsToCosts = e2c;
}

AbstractSoftConstraint::~AbstractSoftConstraint () {
  /*
  delete cost;
  delete currentSupport;
  */
}

void AbstractSoftConstraint::reset () {
  cost->reset();
  currentSupport->reset();
}

void AbstractSoftConstraint::assign (Variable *v, int val) {
  int c;
  if (v->getDomain()->isDiscretized()) {
    return;
  }
  if (val == v->getDomain()->getLb()) {
    c = getLbMem(v);
  }
  else if (val == v->getDomain()->getUb()-1) {
    c = getUbMem(v);
  }
  else {
    c = 0;
  }
  cost->assign(getVariableIndex(v), val, c);
}

void AbstractSoftConstraint::setDiscretized (Variable *v) {
  cost->setDiscretized(getVariableIndex(v), v->getDomain()->getLb(), v->getDomain()->getUb());
}

int AbstractSoftConstraint::getMem () {
  return cost->getMem();
}

void AbstractSoftConstraint::setMem (int m) {
  cost->setMem(m);
}

void AbstractSoftConstraint::incMem (int m) {
  setMem(getMem() + m);
}

void AbstractSoftConstraint::decMem (int m) {
  setMem(diffCost(getMem(), m));
}

int AbstractSoftConstraint::getMemProfile (Variable *v, int val) {
  return cost->getMemProfile(getVariableIndex(v), val);
}

void AbstractSoftConstraint::setMemProfile (Variable *v, int value, int m) {
  cost->setMemProfile(getVariableIndex(v), value, m);
}

void AbstractSoftConstraint::incMemProfile (Variable *v, int value, int m) {
  setMemProfile(v, value, getMemProfile(v, value) + m);
}

void AbstractSoftConstraint::decMemProfile (Variable *v, int value, int m) {
  setMemProfile(v, value, diffCost(getMemProfile(v, value), m));
}

int AbstractSoftConstraint::getLbMem(Variable *v) {
  return cost->getLbMem(getVariableIndex(v));
}

int AbstractSoftConstraint::getUbMem(Variable *v) {
  return cost->getUbMem(getVariableIndex(v));
}

void AbstractSoftConstraint::setLbMem (Variable *v, int m) {
  cost->setLbMem(getVariableIndex(v), m);
  if (v->getDomain()->getLb()+1 == v->getDomain()->getUb()) {
    cost->setUbMem(getVariableIndex(v), m);
  }
}

void AbstractSoftConstraint::setUbMem (Variable *v, int m) {
  cost->setUbMem(getVariableIndex(v), m);
  if (v->getDomain()->getLb()+1 == v->getDomain()->getUb()) {
    cost->setLbMem(getVariableIndex(v), m);
  }
}

void AbstractSoftConstraint::incLbMem (Variable *v, int m) {
  setLbMem(v, getLbMem(v) + m);
}

void AbstractSoftConstraint::incUbMem (Variable *v, int m) {
  setUbMem(v, getUbMem(v) + m);
}

void AbstractSoftConstraint::decLbMem (Variable *v, int m) {
  setLbMem(v, diffCost(getLbMem(v), m));
}

void AbstractSoftConstraint::decUbMem (Variable *v, int m) {
  setUbMem(v, diffCost(getUbMem(v), m));
}

void AbstractSoftConstraint::resetLbMem (Variable *v) {
  if (v->getDomain()->isDiscretized()) {
    return;
  }
  if (v->getDomain()->getLb() == v->getDomain()->getUb()-1) {
    cost->setLbMem(getVariableIndex(v), getUbMem(v));
  }
  else {
    cost->setLbMem(getVariableIndex(v), 0);
  }
}

void AbstractSoftConstraint::resetUbMem (Variable *v) {
  if (v->getDomain()->isDiscretized()) {
    return;
  }
  if (v->getDomain()->getLb() == v->getDomain()->getUb()-1) {
    cost->setUbMem(getVariableIndex(v), getLbMem(v));
  }
  else {
    cost->setUbMem(getVariableIndex(v), 0);
  }
}

int AbstractSoftConstraint::getMinCost () {
  return cost->getMem();
}

int AbstractSoftConstraint::getMinCost (Variable *v, int val) {
  return 0;
}

int AbstractSoftConstraint::getMinLbCost (Variable *v) {
  return 0;
}

int AbstractSoftConstraint::getMinUbCost (Variable *v) {
  return 0;
}

int AbstractSoftConstraint::getMaxCost () const {
  return errorsToCosts->getMaxCost();
}

bool AbstractSoftConstraint::isUnconsistent () {
  return ((cost->getMem() >= getMaxCost()) || (cost->getMem() >= top->getValue()));
}

int AbstractSoftConstraint::getMemory(Variable *v, int val) {
  if (v->getDomain()->isDiscretized()) {
    return getMemProfile(v, val);
  }
  if (val == v->getDomain()->getLb()) {
    return getLbMem(v);
  }
  if (val == v->getDomain()->getUb()-1) {
    return getUbMem(v);
  }
  return 0;
}

Support *AbstractSoftConstraint::getCurrentSupport () {
  return currentSupport;
}

Support *AbstractSoftConstraint::getZeroSupport () {
  return cost->getZeroSupport();
}

Support *AbstractSoftConstraint::getLbSupport (Variable *v) {
  return cost->getLbSupport(getVariableIndex(v));
}

Support *AbstractSoftConstraint::getUbSupport (Variable *v) {
  return cost->getUbSupport(getVariableIndex(v));
}

Support *AbstractSoftConstraint::getSupport (Variable *v, int i) {
  return cost->getSupport(getVariableIndex(v), i);
}

int AbstractSoftConstraint::getCost (Support *s) {
  return getMaxCost();
}

bool AbstractSoftConstraint::checkSupport (Support *s) {
  int c;
  if (!s->isSet()) {
    return false;
  }
  for (int i = 0; i < nbVariables; i++) {
    if (!variables[i]->getDomain()->contains(s->getSupport(i))) {
      s->unset();
      return false;
    }
  }
  c = getCost(s);
  if (traceLevel > 1) {
    if (c != 0) {
      trace(1, "Error! A support has increased its cost!");
    }
  }
  return (c == 0);
}

int AbstractSoftConstraint::getMaxProjected (Variable *var) {
  int max = 0;
  if (var->getDomain()->isDiscretized()) {
    DomainIterator di = var->getDomain()->getDomainIterator();
    for (;*di < var->getDomain()->getUb(); ++di) {
      max = max2(max, getMemProfile(var, *di));
    }
    return max;
  }
  return max2(getLbMem(var), getUbMem(var));
}

int AbstractSoftConstraint::getFinalCost (int *values) {
  return 0;
}

string AbstractSoftConstraint::print () {
  ostringstream s;
  s << "{" << variables[0]->getName();
  for (int i = 1; i < nbVariables; i++) {
    s << "-" << variables[i]->getName();
  }
  s << "}: " << getMem();
  return s.str();
}

void AbstractSoftConstraint::save () {
  cost->save();
  currentSupport->save();
}

void AbstractSoftConstraint::restore () {
  cost->restore();
  currentSupport->restore();
}

