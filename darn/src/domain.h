// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef  DOM_H_INCLUDED
#define  DOM_H_INCLUDED

#include "common.h"
#include "revision.h"
#include "domainIterator.h"


/**
 * The DomainValues class contains all Domain needs to save and restore while backtracking.
 */
struct DomainValues {
  bool discretized;
  int lb;
  int ub;
  int *discreteDomain;
  int firstElement;
  DomainValues () {
    discreteDomain = new int[MAXSIZE+1];
  }
  ~DomainValues () {
    /*
    delete[] discreteDomain;
    */
  }
};


/**
 * The Domain class stands for the WCSP's variable domains. It may have the regular representations (with holes inside the domain, called discretized domain) or an interval representation (with no hole inside).
 */
class Domain {

private:
  /**
   * whether the variable has a domain or interval representatiop,
   */
  bool discretized;
  /**
   * a number attached to the domain (the same as the corresponding variable), used for a name
   */
  int index;
  /**
   * the lower bound of the domain
   */
  int lb;
  /**
   * the upper bound of the domain
   */
  int ub;
  /**
   * when discretized, the list of all the allowed values of the variable
   */
  int *discreteDomain;
  /**
   * the first element of the domain when discretizing (used as a landmark)
   */
  int firstElement;
  /**
   * the stack that contains the values that the class should save for backtracking safely
   */
  DomainValues **memories;
  /**
   * the number of memories already stored
   */
  int nbStoredValues;
  
public:
  /**
   * the constructor, not discretized by default
   * @param i the index
   * @param l the lower bound
   * @param u the upper bound
   */
  Domain (int i, int l, int u);
  /**
   * destructor
   */
  ~Domain ();
  /**
   * prepare the domain for a new search
   * @param l the lower bound
   * @param u the upper bound
   */
  void reset (int l, int u);
  /**
   * @return whether the class has a discretized representation
   */
  bool isDiscretized ();
  /**
   * @return whether the class is assigned (i.e. has a discretized representation and a domain size equal to one)
   */
  bool isAssigned ();
  /**
   * @return the lower bound
   */
  int getLb ();
  /**
   * @return the upper bound
   */
  int getUb ();
  /**
   * @return the size of the domain
   */
  int getSize ();
  /**
   * shrink the domain's size to one (and possibly discretizes the domain)
   * @param v the value taken by the variable
   */
  void assign (int v);
  /**
   * @pre The domain is not discretized
   *
   * discretize the domain and fill the list with 0
   */
  void setDiscretized ();
  /**
   * set the lower bound
   * @param l the new lower bound
   */
  void setLb (int l);
  /**
   * set the upper bound
   * @param u the new upper bound
   */
  void setUb (int u);
  /**
   * increase the lower bound
   * @param l the increase of the lower bound
   */
  void incLb (int l);
  /**
   * decrease the upper bound
   * @param u the decrease of the upper bound
   */
  void decUb (int u);
  /**
   * @pre The domain is discretized
   *
   * delete a value of the domain
   * @param v the deleted value
   */
  void delValue (int v);
  /**
   * @return whether the domain is empty (ub == lb+1)
   */
  bool isEmpty ();
  /**
   * check whether the given value is in the domain
   * @param v the checked value
   * @return whether the value is in the domain
   */
  bool contains (int v);
  /**
   * @return a new DomainIterator
   */
  DomainIterator getDomainIterator ();
  /**
   * @return a new DomainIterator that moves backwards
   */
  DomainIterator getReverseDomainIterator ();
  /**
   * @return a string that represents the domain
   */
  string print ();
  /**
   * save the current values in a stack
   */
  void save ();
  /**
   * restore the current values in a stack
   */
  void restore ();
};

#endif
