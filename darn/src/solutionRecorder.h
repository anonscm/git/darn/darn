// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef SRECORDER_H_INCLUDED
#define SRECORDER_H_INCLUDED

#include "common.h"
#include "sequence.h"
#include "defaultUnarySoftConstraint.h"
#include "softConstraint.h"
#include "hardConstraint.h"

extern "C" {
#include "../lib/ViennaLib/fold.h"
#include "../lib/ViennaLib/PS_dot.h"
}


/**
 * This structure stores the configuration of a solution.
 */
struct SolutionRecord {
  /**
   * the number of values (variables) stored
   */
  int nbValues;
  /**
   * the values of the variables
   */
  int *values;
  /**
   * the cost of the solution
   */
  int cost;
  /**
   * the smallest value of the variables in the main sequence (the leftmost position in the sequence)
   */
  int min;
  /**
   * the greatest value of the variables in the main sequence (the rightmost position in the sequence)
   */
  int max;
  /**
   * the left- and rightmost values of the variables in the duplex sequences
   */
  Pair *dupMinMax;
  /**
   * whether the solution is dominated
   */
  bool dominated;
  /**
   * the structure of the solution found
   */
  char *structure;
  /**
   * the energy of the structure
   */
  float energy;

  /**
   * the constructor
   * @param nv the number of variables in the problem
   */
  SolutionRecord (int nv) : nbValues(nv) {
    values = new int[nbValues];
    if (nbDupSequences > 0) {
      dupMinMax = new Pair[nbDupSequences];
    }
    dominated = false;
  }

  /**
   * copy constructor
   * @param sr another instance
   */
  SolutionRecord (SolutionRecord &sr) : nbValues(sr.nbValues), cost(sr.cost), min(sr.min), max(sr.max), dominated(sr.dominated) {
    values = new int[nbValues];
    for (int i = 0; i < nbValues; i++) {
      values[i] = sr.values[i];
    }
    if (nbDupSequences > 0) {
      dupMinMax = new Pair[nbDupSequences];
      for (int i = 0; i < nbDupSequences; i++) {
        dupMinMax[i] = sr.dupMinMax[i];
      }
    }
    structure = new char[max - min + 2];
    strcpy(structure, sr.structure);
  }

  /**
   * whether the stored solution is dominated by a given one
   * If two solutions overlap, with the same cost, the most stable is said dominated.
   * If both solutions are equally stable, the leftmost solution dominates.
   * @param sr a given solution
   * @return true, if this element is dominated
   */
  bool isDominated (SolutionRecord *sr) {
    if (max < sr->min) {
      return false;
    }
    if (min > sr->max) {
      return false;
    }
    if (cost < sr->cost) {
      return false;
    }
    if (cost > sr->cost) {
      return true;
    }
    if (energy < sr->energy) {
      return false;
    }
    if (energy > sr->energy) {
      return true;
    }
    if (min < sr->min) {
      return false;
    }
    if (min > sr->min) {
      return true;
    }
    return (max > sr->max);
  }
  /**
   * whether the stored solution dominates a given one
   * If two solutions overlap, with the same cost, the most stable is said dominated.
   * If both solutions are equally stable, the leftmost solution dominates.
   * @param sr a given solution
   * @return true, if this element dominates the given one
   */
  bool dominate (SolutionRecord *sr) {
    if (max < sr->min) {
      return false;
    }
    if (min > sr->max) {
      return false;
    }
    if (cost < sr->cost) {
      return true;
    }
    if (cost > sr->cost) {
      return false;
    }
    if (energy < sr->energy) {
      return true;
    }
    if (energy > sr->energy) {
      return false;
    }
    if (min < sr->min) {
      return true;
    }
    if (min > sr->min) {
      return false;
    }
    return (max <= sr->max);
  }
  
  /**
   * print the content of this element
   * @param mes the string to write into
   */
  void print (char *mes) {
    sprintf(mes, "%i-%i (%i, %f)%c", min, max, cost, energy, (dominated)? 'X': 'V');
  }
};

/**
 * This stores the rightmost position of a solution, as well as some other useful information.
 */
class MaxValue {

private:
  /**
   * the caracteristics of a solution
   */
  SolutionRecord *sRecord;

public:
  /**
   * constructor
   * @param m the position
   * @param c the cost of a solution
   * @param sr the caracteristics of a solution
   */
  MaxValue (SolutionRecord *sr) {
    sRecord = sr;
  }

  /**
   * destructor
   */
  ~MaxValue () {}
  /**
   * get the cost
   * @return the cost
   */
  int getCost () {
    return sRecord->cost;
  }
  
  /**
   * get the position
   * @return the position
   */
  int getValue () {
    return sRecord->max;
  }

  /**
   * get the energy
   * @return the energy
   */
  float getEnergy () {
    return sRecord->energy;
  }

  /**
   * set the current solution dominated
   */
  void setDominated () {
    sRecord->dominated = true;
  }

  /**
   * Whether this solution is dominated by another one
   * @param sr a new solution
   * @return true, if this solution is dominated
   */
  bool isDominated (SolutionRecord *sr) {
    return sRecord->isDominated(sr);
  }

  /**
   * Whether the stored solutions dominate a given one
   * @param sr a new solution
   * @return true, if the given solution is dominated
   */
  bool dominate (SolutionRecord *sr) {
    return sRecord->dominate(sr);
  }

  /**
   * compares to instances
   * @param m the other instance
   * @return true if the other instance is smaller
   */
  bool operator< (MaxValue &m) {
    if (getValue() < m.getValue()) return true;
    if (getValue() > m.getValue()) return false;
    if (getCost() < m.getCost()) return true;
    if (getCost() > m.getCost()) return false;
    return (getEnergy() < m.getEnergy());
  }

  /**
   * print the current instance
   * @param mes the string to write into
   */
  void print (char *mes) {
    sprintf(mes, "%i-%i (%i, %f)%c", sRecord->min, sRecord->max, sRecord->cost, sRecord->energy, (sRecord->dominated)? 'X': 'V');
  }
};


/**
 * This class stores the leftmost position of a solution, the rightmost positions, as well as other useful information.
 */
class MinValue {

private:
  /**
   * the minimum value (the index of the row of the sparse matrix)
   */
  int min;
  /**
   * the maximum values (the row of the sparse matrix)
   */
  list < MaxValue *> maxValues;
  /**
   * the maximum value of this row found so far
   */
  int max;

public:
  /**
   * constructor
   * @param sr the caracteristics
   */
  MinValue (SolutionRecord *sr) {
    min = sr->min;
    max = sr->max;
    maxValues.push_back(new MaxValue(sr));
  }

  /**
   * destructor
   */
  ~MinValue () {
    /*
    for (list<MaxValue*>::iterator it = maxValues.begin(); it != maxValues.end(); ++it) {
      delete (*it);
    }
    */
  }

  /**
   * get the position
   * @return the position
   */
  int getValue () {
    return min;
  }

  /**
   * add a new solution, of which the leftmost position is the one stored in this instance
   * @param m the rightmost position
   * @param c the cost of the solution
   * @param sr the caracteristics of the solution
   */
  void addMaxValue (SolutionRecord *sr) {
    MaxValue *maxValue = new MaxValue(sr);
    max = max2(sr->max, max);

    for (list<MaxValue*>::iterator it = maxValues.begin(); it != maxValues.end(); ++it) {
      if (! (*(*it) < *maxValue)) {
        maxValues.insert(it, maxValue);
        return;
      }
    }
    maxValues.push_back(maxValue);
  }

  /**
   * whether these solutions overlap with given one
   * @param m1 the leftmost position of another solution
   * @param m2 the rightmost position of another solution
   * @return true, if one of the solutions overlaps with given one
   */
  bool contains (int m1, int m2) {
    if ((m1 > max) || (m2 < min)) {
      return false;
    }
    return true;
  }

  /**
   * Whether the stored solutions dominate a given one
   * @param sr a new solution
   * @return true, if one of the solutions dominate a given one
   */
  bool dominate (SolutionRecord *sr) {
    for (list<MaxValue*>::iterator it = maxValues.begin(); it != maxValues.end(); ++it) {
      if ((*it)->dominate(sr)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Possibly set to 'dominated' the solutions, given another one.
   * @param sr a new solution
   * @return true, if the solution is dominated
   */
  void setDominates (SolutionRecord *sr) {
    for (list<MaxValue*>::iterator it = maxValues.begin(); it != maxValues.end(); ++it) {
      if ((*it)->isDominated(sr)) {
        (*it)->setDominated();
      }
    }
  }

  /**
   * get the score of the optimal solution that overlaps the given interval
   * @a m1 the lower bound of the interval
   * @a m2 the upper bound of the interval
   * @return a score, or UNDEFINED if no such score exists
   */
  int getCurrentOptimalSolution (int m1, int m2) {
    int minCost = UNDEFINED, cost;
    if (m1 <= min) {
      for (list<MaxValue*>::iterator it = maxValues.begin(); it != maxValues.end(); ++it) {
        cost = (*it)->getCost();
        if (minCost == UNDEFINED) {
          minCost = cost;
        }
        else {
          minCost = min2(minCost, cost);
        }
        if (minCost == 0) {
          return 0;
        }
      }
    }
    else {
      for (list<MaxValue*>::iterator it = maxValues.begin(); it != maxValues.end(); ++it) {
        if ((*it)->getValue() >= m1) {
          cost = (*it)->getCost();
          if (minCost == UNDEFINED) {
            minCost = cost;
          }
          else {
            minCost = min2(minCost, cost);
          }
          if (minCost == 0) {
            return 0;
          }
        }
      }
    }
    return minCost;
  }

  /**
   * compare two instances
   * @param m another instance
   * @return true, if this instance is less than the other one
   */
  bool operator< (MinValue &m) {
    return (min < m.min);
  }

  /**
   * compare this instance with a value
   * @param m another leftmost position
   * @return true, if this instance is less than the other one
   */
  bool operator< (int m) {
    return (min < m);
  }

  /**
   * compare this instance with a value
   * @param m another leftmost position
   * @return true, if this instance is equal to the other one
   */
  bool operator== (int m) {
    return (min == m);
  }

  /**
   * print this instance
   * @param mes the string to write into
   */
  void print (char *mes) {
    sprintf(mes, "%i-%i  -> ", min, max);
    for (list<MaxValue*>::iterator it = maxValues.begin(); it != maxValues.end(); ++it) {
      (*it)->print(mes + strlen(mes));
      strcat(mes, "  ");
    }
  }
};


/**
 * Represent a kind of sparse matrix, used to store the solutions.
 * Actually, it is a list of lists. The main list stores the leftmost positions of the solutions. The other lists, the rightmost positions.
 * The list of leftmost positions is stored in decreasing order.
 */
class MinMaxValues {

private:
  /**
   * the list of lists
   */
  list < MinValue *> minValues;
  /**
   * the maximum size of the candidates found so far
   */
  int maxSize;

public:
  /**
   * constructor
   */
  MinMaxValues () : maxSize(1) {}

  /**
   * destructor
   */
  ~MinMaxValues () {
    /*
    for (list<MinValue*>::iterator it = minValues.begin(); it != minValues.end(); ++it) {
      delete (*it);
    }
    */
  }

  /**
   * remove all values
   */
  void reset () {
    for (list<MinValue*>::iterator it = minValues.begin(); it != minValues.end(); ++it) {
      delete (*it);
    }
    minValues.clear();
    maxSize = 0;
  }

  /**
   * whether a new element is dominated by the already stored solutions
   * @param sr a new solution
   * @return true, if the new solution is dominated
   */
  bool dominate (SolutionRecord *sr) {
    // loop over all the stored solutions
    for (list<MinValue*>::iterator it = minValues.begin(); it != minValues.end(); ++it) {

      // if the current solution is too far away, abort
      if ((*it)->getValue() + maxSize < sr->min) {
        return false;
      }

      if ((*it)->contains(sr->min, sr->max)) {
        if ((*it)->dominate(sr)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * update the dominance of the already stored solution wrt to a new solution
   * @param sr a new solution
   */
  void setDominates (SolutionRecord *sr) {
    // loop over all the stored solutions
    for (list<MinValue*>::iterator it = minValues.begin(); it != minValues.end(); ++it) {

      // if the current solution is too far away, abort
      if ((*it)->getValue() + maxSize < sr->min) {
        return;
      }

      if ((*it)->contains(sr->min, sr->max)) {
        (*it)->setDominates(sr);
      }
    }
  }

  /**
   * add a new solution
   * @param sr all the information about this solution
   */
  void addMinMaxValue (SolutionRecord *sr) {
    maxSize = max2(maxSize, (sr->max - sr->min + 1));

    for (list<MinValue*>::iterator it = minValues.begin(); it != minValues.end(); ++it) {
      if ((*it)->getValue() == sr->min) {
        (*it)->addMaxValue(sr);
        return;
      }
      if (((*it)->getValue() < sr->min)) {
        minValues.insert(it, new MinValue(sr));
        return;
      }
    }
    minValues.push_back(new MinValue(sr));
  }

  /**
   * get the score of the optimal solution that overlaps the given interval
   * @a m1 the lower bound of the interval
   * @a m2 the upper bound of the interval
   * @return a score, or UNDEFINED if no such score exists
   */
  int getCurrentOptimalSolution (int m1, int m2) {
    int minCost = UNDEFINED, cost;
    for (list<MinValue*>::iterator it = minValues.begin(); it != minValues.end(); ++it) {

      // if the current solution is too far away, abort
      if ((*it)->getValue() + maxSize < m1) {
        return minCost;
      }

      if ((*it)->contains(m1, m2)) {
        cost = (*it)->getCurrentOptimalSolution(m1, m2);
        if (minCost == UNDEFINED) {
          minCost = cost;
        }
        else {
          minCost = min2(minCost, cost);
        }
        if (minCost == 0) {
          return 0;
        }
      }
    }
    return minCost;
  }

  /**
   * print the left- and rightmost positions of the already stored solutions
   * @param mes the string to write into
   */
  void print (char *mes) {
    mes[0] = '\0';
    for (list<MinValue*>::iterator it = minValues.begin(); it != minValues.end(); ++it) {
      (*it)->print(mes+strlen(mes));
      strcat(mes, "\n");
    }
  }
};


/**
 * This class stores the solutions found by the WCSP solutions, and classify them between dominiated/undominated solutions.
 */
class SolutionRecorder {

private:

  /**
   * the structure that stores the left- and rightmost positions of the solutions found so far
   */
  MinMaxValues *mmv;

  /**
   * the trace matrix
   */
  bool **backtrace;

  /**
   * the variables of the network
   */
  DefaultUnarySoftConstraint **unaryConstraints;

  /**
   * all the informations about all the solutions
   */
  vector <SolutionRecord *> solutionRecords;

  /**
   * the maximum cost of the solutions
   *  (useful if the ordered by cost format is used)
   */
  int maxCost;

  /**
   * the informations about all the solutions ordered by cost
   *  (useful if the ordered by cost format is used)
   */
  list <SolutionRecord *> *solutionsRecordsByCosts;

  /**
   * the number of solutions found so far
   */
  int nbSolutions;

  /**
   * the number of solutions found during the process of @a print
   */
  int nbUndominatedSolutions;

  /**
   * the number of displayed solutions
   */
  int nbDisplayedSolutions;
  /**
   * the stored solutions of the plus strand,
   *  to be merged with the solutions of the minus strand
   */
  vector <SolutionRecord *> storedSolutionsPlus;
  /**
   * the stored solutions of the minus strand,
   *  to be merged with the solutions of the plus strand
   */
  vector <SolutionRecord *> storedSolutionsMinus;

  /**
   * print all the (possibly undominated) solutions using the "normal" format
   * @param softConstraints the soft constraints used
   */
  void printNormal (vector <SoftConstraint*> &softConstraints, bool firstWay);
  /**
   * print all the (possibly undominated) solutions using the "normal" format,
   *   and solutions with lower score displayed first
   * @param softConstraints the soft constraints used
   */
  void printByCost (vector <SoftConstraint*> &softConstraints, bool firstWay);
  /**
   * print all the (possibly undominated) solutions using the fasta format
   * @param store whether solutions should be stored instead of being printed
   */
  void printFasta (bool store);
  /**
   * print all the (possibly undominated) solutions using the GFF format
   * @param store whether solutions should be stored instead of being printed
   */
  void printGff (bool store);

  /**
   * print one solution using the "normal" format
   * @param sr a valid assignment
   * @param softConstraints the soft constraints used
   */
  void printSolutionNormal (SolutionRecord *sr, vector <SoftConstraint*> &softConstraints, bool firstWay);
  /**
   * print one solution using the GFF format
   * @param sr a valid assignment
   * @param reverse the direction actually is from right to left
   */
  void printSolutionGff (SolutionRecord *sr, bool reverse);
  /**
   * print one solution using the fasta format
   * @param sr a valid assignment
   * @param reverse the direction actually is from right to left
   */
  void printSolutionFasta (SolutionRecord *sr, bool reverse);

public:

  /**
   * the constructor
   * @param uc the set of variables of the network
   * @param mc the maximum cost of the solutions
   */
  SolutionRecorder (DefaultUnarySoftConstraint **uc, int mc);

  /**
   * destructor
   */
  ~SolutionRecorder ();

  /**
   * forget all the stored solutions
   */
  void reset ();

  /**
   * add a solution
   */
  void addSolution ();

  /**
   * get the score of the optimal solution that overlap the current partial assignment
   * @return a score, or UNDEFINED if no such score exists
   */
  int getCurrentOptimalSolution ();

  /**
   * print the (possible undominated) solutions
   * @param softConstraints the soft constraints used
   * @param store whether solutions should be stored instead of being printed
   */
  void print (vector <SoftConstraint*> &softConstraints, bool store);
  
  /**
   * get the number of solutions (dominated and undominated)
   * @return the number of solutions
   */
  int getNbSolutions ();

  /**
   * @warning should be called after @a print
   *
   * get the number of undominated solutions
   * @return the number of undominated solutions
   */
  int getNbUndominatedSolutions ();
};

#endif
