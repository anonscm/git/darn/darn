// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "parseSem.hh"
#include "revision.h"
#include "sequence.h"
#include "values.h"
#include "parametersReader.h"
#include "network.h"
#include "main.h"

/**
 * Initialize all the static elements and start the network exploration.
 */
int main (int argc, char *argv[]) {

  time_t startTime = time(NULL);

  nbTotVariables = 0;
  nbSoftHelix = 0;
  nbHardHelix = 0;

  nucleotideA = new Nucleotide('A');
  nucleotideC = new Nucleotide('C');
  nucleotideG = new Nucleotide('G');
  nucleotideT = new Nucleotide('T');
  nucleotideN = new Nucleotide('N');
  nucleotideX = new Nucleotide('X');
  nucleotideR = new Nucleotide('R');
  nucleotideY = new Nucleotide('Y');
  nucleotideW = new Nucleotide('W');
  nucleotideS = new Nucleotide('S');
  nucleotideM = new Nucleotide('M');
  nucleotideK = new Nucleotide('K');
  nucleotideB = new Nucleotide('B');
  nucleotideD = new Nucleotide('D');
  nucleotideH = new Nucleotide('H');
  nucleotideV = new Nucleotide('V');
  nucleotides = new Nucleotide*[NBEXTENDEDNUCLEOTIDES];

  nucleotides[0] = nucleotideA;
  nucleotides[1] = nucleotideC;
  nucleotides[2] = nucleotideG;
  nucleotides[3] = nucleotideT;
  nucleotides[4] = nucleotideN;
  nucleotides[5] = nucleotideX;
  nucleotides[6] = nucleotideR;
  nucleotides[7] = nucleotideY;
  nucleotides[8] = nucleotideW;
  nucleotides[9] = nucleotideS;
  nucleotides[10] = nucleotideM;
  nucleotides[11] = nucleotideK;
  nucleotides[12] = nucleotideB;
  nucleotides[13] = nucleotideD;
  nucleotides[14] = nucleotideH;
  nucleotides[15] = nucleotideV;

  dupSequences = new Sequence*[NBMAXDUPLEXSEQUENCES];

  if (readParameters(argc, argv)) {
    return 0;
  }
  ValuesReader::read(costFile);

  yyin = fopen(descriptorFileName, "r");
  if (yyin == NULL) {
    cerr << "Error: Cannot read the descriptor file!"<<endl;
    exit(1);
  }
  trace(10, "        Descriptor file red.");
  yyparse();
  trace(10, "        Descriptor file parsed.");

  // catching Crtl+C and timer interruption
  signal(SIGINT, catchInterruption);
  signal(SIGALRM, catchAlarm);
  
  network->explore();
  if (sequenceBoth) {
    network->reverseSequence();
    network->explore();
  }
  for (int i = 1; i < sequenceFileReader->getNbSequences(); i++) {
    sequenceFileReader->setSequence(i, sequence, sequenceBoth, sequenceReverse, sequenceComplement);
    network->reset();
    network->explore();
    if (sequenceBoth) {
      network->reverseSequence();
      network->explore();
    }
  }

  time_t stopTime = time(NULL);

  printTime(difftime(stopTime, startTime));
}
