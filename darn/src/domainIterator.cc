// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "domainIterator.h"

DomainIterator::DomainIterator (int l, int u, bool r) : discretized(false), lb(l), ub(u), reverse(r) {
  cursor = ((reverse)? max2(0, ub-1): max2(0, lb));
}

DomainIterator::DomainIterator (int *dd, int l, int u, int fe, bool r) : discretized(true), lb(l), ub(u), discreteDomain(dd), firstElement(fe), reverse(r) {
  if (!reverse) {
    setTo(lb);
    /*
    for (cursor = 0; cursor <= MAXSIZE; cursor++) {
      if (discreteDomain[cursor] != -1) {
        return;
      }
    }
    */
  }
  else {
    setTo(ub-1);
    /*
    for (cursor = MAXSIZE; cursor >= 0; cursor--) {
      if (discreteDomain[cursor] != -1) {
        cursor--;
        return;
      }
    }
    */
  }
}

DomainIterator DomainIterator::operator++ () {
  if (reverse) {
    trace(1, "Error! Trying to move forwards a reverse iterator!");
    return *this;
  }
  if (discretized) {
    do {
      cursor++;
      if (cursor+firstElement > ub) {
        return *this;
      }
    }
    while (discreteDomain[cursor] == -1);
    //while ((cursor <= MAXSIZE) && (discreteDomain[cursor] == -1));
  }
  else {
    cursor++;
  }
  return *this;
}

DomainIterator DomainIterator::operator-- () {
  if (!reverse) {
    trace(1, "Error! Trying to move backwards a non reverse iterator!");
    return *this;
  }
  if (discretized) {
    do {
      cursor--;
      if (cursor+firstElement < lb) {
        return *this;
      }
    }
    while ((cursor >= 0) && (discreteDomain[cursor] == -1));
  }
  else {
    cursor--;
  }
  return *this;
}

int DomainIterator::operator* () {
  if (discretized) {
    if (cursor+firstElement > ub) {
      return ub;
    }
    if (cursor+firstElement < lb) {
      return lb-1;
    }
    return discreteDomain[cursor];
  }
  return cursor;
}

DomainIterator DomainIterator::setTo (int v) {
  if (!discretized) {
    cursor = v;
  }
  else {
    if (!reverse) {
      cursor = v-firstElement;
      while (discreteDomain[cursor] == -1) {
        cursor++;
        if (cursor+firstElement > ub) {
          return *this;
        }
      }
    }
    else {
      cursor = v-firstElement;
      while (discreteDomain[cursor] == -1) {
        cursor--;
        if (cursor+firstElement < lb) {
          return *this;
        }
      }
    }
  }
  return *this;
}
