// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "solutionRecorder.h"



SolutionRecorder::SolutionRecorder (DefaultUnarySoftConstraint **uc, int mc) : maxCost(mc), nbSolutions(0), nbUndominatedSolutions(0), nbDisplayedSolutions(0) {
  unaryConstraints = uc;
  mmv = new MinMaxValues();
  if (orderedByCost) {
    solutionsRecordsByCosts = new list <SolutionRecord *>[maxCost];
  }
  backtrace = new bool*[MAXCANDIDATESIZE];
  for (unsigned int i = 0; i < MAXCANDIDATESIZE; i++) {
    backtrace[i] = new bool[MAXCANDIDATESIZE];
  }
}

SolutionRecorder::~SolutionRecorder () {
  /*
  for (int i = 0; i < static_cast<int>(solutionRecords.size()); i++) {
    delete solutionRecords[i];
  }
  delete mmv;
  delete[] solutionsRecordsByCosts;
  for (int i = 0; i < static_cast<int>(storedSolutionsPlus.size()); i++) {
    delete storedSolutionsPlus[i];
  }
  for (int i = 0; i < static_cast<int>(storedSolutionsMinus.size()); i++) {
    delete storedSolutionsMinus[i];
  }
  for (int i = 0; i < MAXCANDIDATESIZE; i++) {
    delete[] backtrace[i];
  }
  delete[] backtrace;
  */
}

void SolutionRecorder::reset () {
  nbSolutions = 0;
  nbUndominatedSolutions = 0;
  nbDisplayedSolutions = 0;
  mmv->reset();
  for (int i = 0; i < static_cast<int>(solutionRecords.size()); i++) {
    delete solutionRecords[i];
  }
  solutionRecords.clear();
  if (orderedByCost) {
    for (int i = 0; i < maxCost; i++) {
      solutionsRecordsByCosts[i].clear();
    }
  }
}

void SolutionRecorder::addSolution () {
  int min = sequence->getLength();
  int max = 0;
  int dupMin = sequence->getLength();
  int dupMax = 0;
  int cost;
  int val;
  char *structure;
  float energy;
  int values[nbTotVariables];
  HardConstraint *hc;
  nbSolutions++;

  // create a new solution
  SolutionRecord *sr = new SolutionRecord(nbTotVariables);
  if (nbDupSequences > 0) {
    dupMin = dupSequences[0]->getLength();
    dupMax = 0;
  }

  // retrieve the cost
  cost = zeroArityConstraint->getMinCost();

  // get the left- and rightmost positions
  for (int i = 0; i < nbTotVariables; i++) {
    val = unaryConstraints[i]->getLb();
    sr->values[i] = val;
    if (unaryConstraints[i]->getVariable()->getSequence() == sequence) {
      if (val < min) min = val;
      if (val > max) max = val;
    }
    else {
      if (val < dupMin) dupMin = val;
      if (val > dupMax) dupMax = val;
    }
  }
  sr->cost = cost;
  sr->min = min;
  sr->max = max;
  if (nbDupSequences > 0) {
    sr->dupMinMax[0].first = dupMin;
    sr->dupMinMax[0].second = dupMax;
  }

  // get the structure
  structure = new char[max - min + 2];
  structure[max-min+1] = '\0';
  for (int i = 0; i <= max-min; i++) {
    structure[i] = '.';
    for (int j = 0; j <= max-min; j++) {
      backtrace[i][j] = false;
    }
  }
  for (unsigned int i = 0; i < hardConstraints.size(); i++) {
    hc = hardConstraints[i];
    for (int j = 0; j < hc->getNbVariables(); j++) {
      values[j] = hc->getVariable(j)->getUnaryConstraint()->getLb();
    }
    hc->getTrace(min, values, backtrace);
  }
  for (int i = 0; i < max - min + 1; i++) {
    for (int j = i+1; j < max - min + 1; j++) {
      if (backtrace[i][j]) {
        structure[i] = '(';
        structure[j] = ')';
        break;
      }
    }
  }
  sr->structure = structure;

  // compute the energy
  char seq[max-min+2];
  for (int i = min; i <= max; i++) {
    seq[i-min] = sequence->getNucleotide(i)->print();
  }
  seq[max-min+1] = '\0';
  energy = energy_of_struct(seq, structure);
  sr->energy = energy;

  // test dominance of this solution
  sr->dominated = mmv->dominate(sr);

  // test dominance of the other solutions
  mmv->setDominates(sr);

  if (traceLevel > 0) {
    if (! sr->dominated) {
      if (mmv->dominate(sr)) {
        trace(1, "Error! Adding a locally optimal solution which is dominated!");
        char mes[3046];
        strcpy(mes, "\tnew element is: ");
        sr->print(mes + strlen(mes));
        trace(1, mes);
        strcpy(mes, "\tsolution profile is:");
        mmv->print(mes + strlen(mes));
        trace(1, mes);
      }
    }
  }

  // put into arrays
  solutionRecords.push_back(sr);
  mmv->addMinMaxValue(sr);

  // possibly sort the solutions
  if (orderedByCost) {
    if (!sr->dominated) {
      solutionsRecordsByCosts[sr->cost].push_back(sr);
    }
  }
}

int SolutionRecorder::getCurrentOptimalSolution () {
  int min = sequence->getLength()+1, max = UNDEFINED;
  for (int i = 0; i < nbTotVariables; i++) {
    if (unaryConstraints[i]->getVariable()->getSequence() == sequence) {
      if (unaryConstraints[i]->getUb() == unaryConstraints[i]->getLb() + 1) {
        min = min2(min, unaryConstraints[i]->getLb());
        max = max2(max, unaryConstraints[i]->getLb());
      }
    }
  }
  if (max == UNDEFINED) {
    return UNDEFINED;
  }
  return mmv->getCurrentOptimalSolution(min, max);
}

void SolutionRecorder::print (vector <SoftConstraint*> &softConstraints, bool firstWay) {
  if (gffFormat) {
    printGff(firstWay);
  }
  else if (fastaFormat) {
    printFasta(firstWay);
  }
  else {
    if (orderedByCost) {
      printByCost(softConstraints, firstWay);
    }
    else {
      printNormal(softConstraints, firstWay);
    }
  }
}

void SolutionRecorder::printNormal (vector <SoftConstraint*> &softConstraints, bool firstWay) {
  SolutionRecord *sr;

  for (unsigned int i = 0; i < solutionRecords.size(); i++) {
    sr = solutionRecords[i];

    if (!sr->dominated) {
      nbUndominatedSolutions++;
    }
      
    if ((printDominatedSolutions) || (!sr->dominated)) {
      printSolutionNormal(sr, softConstraints, firstWay);
    }
  }
}

void SolutionRecorder::printByCost (vector <SoftConstraint*> &softConstraints, bool firstWay) {
  SolutionRecord *sr;

  for (int i = 0; i < maxCost; i++) {
    for (list<SolutionRecord*>::iterator it = solutionsRecordsByCosts[i].begin(); it != solutionsRecordsByCosts[i].end(); ++it) {
      sr = *it;
      if (!sr->dominated) {
        nbUndominatedSolutions++;
      }
        
      if ((printDominatedSolutions) || (!sr->dominated)) {
        printSolutionNormal(sr, softConstraints, firstWay);
      }
    }
  }
}

void SolutionRecorder::printSolutionNormal (SolutionRecord *sr, vector <SoftConstraint*> &softConstraints, bool firstWay) {
  ostringstream oss;
  string s, s1, sStructure;
  int start, stop;
  int indexStart, indexStop;
  bool varFound;
  bool blankBefore, blankAfter, alreadyBlank;
  Variable *variable = NULL;
  AbstractConstraint *constraint;
  int offset;
  int score;
  int values[nbTotVariables];
  char structureFileName[255];
  char outputStructureFileName[255];
  bool direction = sequenceReverse;
  if (firstWay) {
    direction = true;
  }


  nbDisplayedSolutions++;

  // draw the structure
  if (drawDirectory != NULL) {
    char seq[sr->max - sr->min + 2];
    for (int i = sr->min; i <= sr->max; i++) {
      seq[i-sr->min] = sequence->getNucleotide(i)->print();
    }
    seq[sr->max - sr->min + 1] = '\0';
    sprintf(structureFileName, "%s/%04i.ps", drawDirectory, nbDisplayedSolutions);
    sprintf(outputStructureFileName, "%s/%04i.ps", (((webDisplay) && (webDrawDirectory != NULL))? webDrawDirectory: drawDirectory), nbDisplayedSolutions);
    PS_rna_plot(seq, sr->structure, structureFileName);
  }
  
  if (webDisplay) {
    oss << "<p>" << endl;
  }

  if (printDominatedSolutions) {
    if (sr->dominated) {
      oss << "D  ";
    }
    else {
      oss << "   ";
    }
  }
          // print the bounds
  indexStart = ((direction)?sr->min:sequence->getLength()-sr->min-1) + 1;
  indexStop = ((direction)?sr->max:sequence->getLength()-sr->max-1) + 1;
  oss << "Solution #" << nbDisplayedSolutions;
  if (webDisplay) {
    oss << "&nbsp;&nbsp;&nbsp;<a href=\"" << outputStructureFileName << "\">Show structure</a>\n</p>\n<p>";
  }
  oss << "\nStart: " << indexStart << "   Stop: " << indexStop;

  if (nbDupSequences > 0) {
    oss << "   [target: " << sr->dupMinMax[0].first << "-" << sr->dupMinMax[0].second << "]";
  }
  oss << "    (score: " << sr->cost << ", energy: " << sr->energy << ")" << endl;

  if (webDisplay) {
    oss << "</p>\n<pre>" << endl;
  }

      // print the main stem
  start = max2(0, sr->min-5);
  stop = min2(sequence->getLength()-1, sr->max+5);
  offset = 0;
  alreadyBlank = false;
  for (int j = start; j <= stop; j++) {
    blankBefore = false;
    blankAfter = false;
    
      // find whether there is a variable pointing at this position
    varFound = false;
    for (int k = 0; k < nbTotVariables; k++) {

      if (unaryConstraints[k]->getVariable()->getSequence() == sequence) {
        if (sr->values[k] == j) {
          variable = unaryConstraints[k]->getVariable();

            // check whether spaces should be added
          for (int l = 0; l < variable->getNbHardConstraints(); l++) {
            constraint = (AbstractConstraint*) variable->getHardConstraint(l);
            if (constraint->printSpace(constraint->getVariableIndex(variable), 0)) {
              blankBefore = true;
            }
            if (constraint->printSpace(constraint->getVariableIndex(variable), 1)) {
              blankAfter = true;
            }
          }
          for (int l = 0; l < variable->getNbSoftConstraints(); l++) {
            constraint = (AbstractConstraint*) variable->getSoftConstraint(l);
            if (constraint->printSpace(constraint->getVariableIndex(variable), 0)) {
              blankBefore = true;
            }
            if (constraint->printSpace(constraint->getVariableIndex(variable), 1)) {
              blankAfter = true;
            }
          }
            
          varFound = true;
          break;
        }
      }
    }

      // add a space before the variable
    if ((blankBefore) && (!alreadyBlank)) {
      if (offset == 0) {
        s += " ";
      }
      else {
        offset--;
      }
      s1 += " ";
      sStructure += " ";
    }

    if (offset == 0) {
      if (varFound) {
          // print the name of the variable
        s += variable->getName();
        offset += variable->getName().length() - 1;
      }
      else {
        s += " ";
      }
    }
    else {
      offset--;
    }

      // print a nucleotide
    s1 += sequence->getNucleotide(j)->print();
    sStructure += (((j < sr->min) || (j > sr->max))? ' ': sr->structure[j - sr->min]);

      // add a space after the variable
    if (blankAfter) {
      if (offset == 0) {
        s += " ";
      }
      else {
        offset--;
      }
      s1 += " ";
      sStructure += " ";
    }
    alreadyBlank = blankAfter;
  }
  oss << s << "\n" << s1 << "\n";
  if (displayStructure) {
    oss << sStructure << "\n";
  }
  
      // print the target stem
  if (nbDupSequences > 0) {
    offset = 0;
    s = "";
    s1 = "";
    start = max2(0, sr->dupMinMax[0].first-5);
    stop = min2(dupSequences[0]->getLength()-1, sr->dupMinMax[0].second+5);
    alreadyBlank = false;
    for (int j = start; j <= stop; j++) {
      blankBefore = false;
      blankAfter = false;
      
        // find whether there is a variable pointing at this position
      varFound = false;
      for (int k = 0; k < nbTotVariables; k++) {

        if (unaryConstraints[k]->getVariable()->getSequence() == dupSequences[0]) {
          if (sr->values[k] == j) {
            variable = unaryConstraints[k]->getVariable();

              // check whether spaces should be added
            for (int l = 0; l < variable->getNbHardConstraints(); l++) {
              constraint = (AbstractConstraint*) variable->getHardConstraint(l);
              if (constraint->printSpace(constraint->getVariableIndex(variable), 0)) {
                blankBefore = true;
              }
              if (constraint->printSpace(constraint->getVariableIndex(variable), 1)) {
                blankAfter = true;
              }
            }
            for (int l = 0; l < variable->getNbSoftConstraints(); l++) {
              constraint = (AbstractConstraint*) variable->getSoftConstraint(l);
              if (constraint->printSpace(constraint->getVariableIndex(variable), 0)) {
                blankBefore = true;
              }
              if (constraint->printSpace(constraint->getVariableIndex(variable), 1)) {
                blankAfter = true;
              }
            }
              
            varFound = true;
            break;
          }
        }
      }

        // add a space before the variable
      if ((blankBefore) && (!alreadyBlank)) {
        if (offset == 0) {
          s += " ";
        }
        else {
          offset--;
        }
        s1 += " ";
      }

      if (offset == 0) {
        if (varFound) {
            // print the name of the variable
          s += variable->getName();
          offset += variable->getName().length() - 1;
        }
        else {
          s += " ";
        }
      }
      else {
        offset--;
      }

        // print a nucleotide
      s1 += dupSequences[0]->getNucleotide(j)->print();

        // add a space after the variable
      if (blankAfter) {
        if (offset == 0) {
          s += " ";
        }
        else {
          offset--;
        }
        s1 += " ";
      }
      alreadyBlank = blankAfter;
    }
    oss << s << "\n" << s1 << "\n";
  }
  if (explanation) {
    if (sr->cost > 0) {
      oss << "\tExplanation:" << endl;
      for (int j = 0; j < static_cast<int>(softConstraints.size()); j++) {
        for (int k = 0; k < softConstraints[j]->getNbVariables(); k++) {
          values[k] = sr->values[softConstraints[j]->getVariable(k)->getIndex()];
        }
        score = softConstraints[j]->getFinalCost(values);
        if (score > 0) {
          oss << "\t\t" << softConstraints[j]->getName() << " gave a cost of " << score << endl;
        }
      }
    }
  }

  if (webDisplay) {
    oss << "</pre>" << endl;
  }

  oss << endl;

  trace(0, oss.str());
}

void SolutionRecorder::printGff (bool firstWay) {
  SolutionRecord *sr;
  int itPlus, itMinus;

  for (int i = 0; i < static_cast<int>(solutionRecords.size()); i++) {
    sr = solutionRecords[((sequenceReverse)?solutionRecords.size()-1-i:i)];

    if (!sr->dominated) {
      nbUndominatedSolutions++;
    }
    
    if ((printDominatedSolutions) || (!sr->dominated)) {
      if (sequenceBoth) {
        if (firstWay) {
          storedSolutionsPlus.push_back(new SolutionRecord(*sr));
        }
        else {
          storedSolutionsMinus.push_back(new SolutionRecord(*sr));
        }
      }
      else {
        printSolutionGff(sr, false);
      }
    }
  }

  if ((sequenceBoth) && (!firstWay)) {
    itPlus = 0;
    itMinus = storedSolutionsMinus.size()-1;

      // merge and print solutions from plus and minus strands
    while ((itPlus < static_cast<int>(storedSolutionsPlus.size())) && (itMinus >= 0)) {
      if (storedSolutionsPlus[itPlus]->min <= static_cast<int>(sequence->getLength())-storedSolutionsMinus[itMinus]->min-1) {
        printSolutionGff(storedSolutionsPlus[itPlus], false);
        itPlus++;
      }
      else {
        printSolutionGff(storedSolutionsMinus[itMinus], true);
        itMinus--;
      }
    }

      // print remaining solutions from both strands
    if (itPlus >= static_cast<int>(storedSolutionsPlus.size())) {
      for (int i = itMinus; i >= 0; i--) {
        printSolutionGff(storedSolutionsMinus[i], true);
      }
    }
    if (itMinus < 0) {
      for (int i = itPlus; i < static_cast<int>(storedSolutionsPlus.size()); i++) {
        printSolutionGff(storedSolutionsPlus[i], false);
      }
    }

    storedSolutionsPlus.clear();
    storedSolutionsMinus.clear();
  }
}

void SolutionRecorder::printFasta (bool firstWay) {
  SolutionRecord *sr;

  for (unsigned int i = 0; i < solutionRecords.size(); i++) {
    sr = solutionRecords[i];

    if (!sr->dominated) {
      nbUndominatedSolutions++;
    }
      
    if ((printDominatedSolutions) || (!sr->dominated)) {
      printSolutionFasta(sr, firstWay);
    }
  }
}

void SolutionRecorder::printSolutionGff (SolutionRecord *sr, bool reverse) {
  int indexStart, indexStop;
  char mesg[511];
  bool direction = sequenceReverse;
  if (reverse) {
    direction = true;
  }

  if (webDisplay) {
    trace(0, "<pre>");
  }

  nbDisplayedSolutions++;
  indexStart = ((direction)? sequence->getLength()-sr->max-1: sr->min) + 1;
  indexStop = ((direction)? sequence->getLength()-sr->min-1: sr->max) + 1;
  sprintf(mesg, "Darn_Pred_%i\tDARN\t.\t%i\t%i\t%i\t%s\t.", nbDisplayedSolutions, indexStart, indexStop, sr->cost, ((direction)? "-": "+"));
  if (nbDupSequences > 0) {
    sprintf(mesg, "%s\tTarget %i %i", mesg, sr->dupMinMax[0].first + 1, sr->dupMinMax[0].second + 1);
  }
  trace(0, mesg);

  if (parserFormat) {
    char seq[sr->max - sr->min + 2];
    if ((!sequenceBoth) || (reverse)) {
      for (int i = sr->min; i <= sr->max; i++) {
        seq[i-sr->min] = sequence->getNucleotide(i)->print();
      }
    }
    else {
      for (unsigned int i = sequence->getLength() - sr->min - 1; i >= sequence->getLength() - sr->max - 1; i--) {
        seq[sequence->getLength()-sr->min-1-i] = sequence->getNucleotide(i)->getComplement()->print();
      }
    }
    seq[sr->max - sr->min + 1] = '\0';
    trace(0, seq);
    trace(0, sr->structure);
  }

  if (webDisplay) {
    trace(0, "</pre>");
  }
}

void SolutionRecorder::printSolutionFasta (SolutionRecord *sr, bool reverse) {
  bool direction = sequenceReverse;
  int indexStart, indexStop;
  ostringstream oss;

  if (reverse) {
    direction = true;
  }

  nbDisplayedSolutions++;

  if (webDisplay) {
    oss << "<pre>" << endl;
  }

    // get the bounds
  indexStart = ((direction)?sr->min:sequence->getLength()-sr->min-1) + 1;
  indexStop = ((direction)?sr->max:sequence->getLength()-sr->max-1) + 1;
  oss << ">Darn solution #" << nbDisplayedSolutions << " " << indexStart << "-" << indexStop;

    // print the main stem
  for (int j = sr->min; j <= sr->max; j++) {
    if ((j - sr->min) % MAXLINESIZE == 0) {
      oss << endl;
    }
      // print a nucleotide
    oss << sequence->getNucleotide(j)->print();
  }

  if (webDisplay) {
    oss << "</pre>" << endl;
  }

  trace(0, oss.str());
}

int SolutionRecorder::getNbSolutions () {
  return nbSolutions;
}

int SolutionRecorder::getNbUndominatedSolutions () {
  return nbUndominatedSolutions;
}
