// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef FILEREADER_H_INCLUDED
#define FILEREADER_H_INCLUDED

#include "common.h"
#include "sequence.h"

#define MAX_LINE_LEN 255

/**
 * Read a multi-fasta file
 */
class FileReader {

private:
  /**
   * the number of sequences contained in the file
   */
  int nbSequences;
  /**
   * the name of the file
   */
  string fileName;


public:
  /**
   * the constructor, reads the file
   * @param fn the file name
   */
  FileReader (string fn) : nbSequences(0), fileName(fn) {
    ifstream fileStream(fileName.c_str());
    char c;
    int newLine = 1;

    if (!fileStream.is_open() || !fileStream.good()) {
      cerr << "Error: Cannot open file \"" + string(fileName) + "\" for input!"<<endl;
      exit(1);
    }
    c = fileStream.get();
    while (fileStream.good()) {
      if ((c == '\n') || (c == '\r')) {
        newLine = 1;
      }
      else {
        if ((newLine) && (c == '>')) {
          nbSequences++;
        }
        newLine = 0;
      }
      c = fileStream.get();
    }
    trace(10, "\tFile \"" + fileName + "\" red.");
  }

  /**
   * @return the number of sequences
   */
  int getNbSequences () {
    return nbSequences;
  }

  /**
   * copy the given sequence into the Sequence object
   * @param part the index of the sequence
   * @param seq the Sequence object
   * @param both if one should consider both strands
   * @param reverse if the considered sequence is the reverse one
   * @param complement if the considered sequence is the complement one
   */
  void setSequence (int part, Sequence *&seq, bool both = false, bool reverse = false, bool complement = false) {
    char *sequenceName = new char[0];
    char *se, m[255];
    int cpt = -1;
    int startPosition;
    ifstream fileStream(fileName.c_str());
    unsigned int length = 0;
    char c;
    int newLine = 1;

    if (part >= nbSequences) {
      cerr << "Error: Trying to access the " << part << " part of the file " << fileName.c_str() <<", which does not exist!" << endl;
      exit(1);
    }
    if (!fileStream.is_open() || !fileStream.good()) {
      cerr << "Error: Cannot open file \"" + string(fileName) + "\" for input!"<<endl;
      exit(1);
    }

    // skip the previous parts
    c = fileStream.get();
    while (fileStream.good()) {
      if ((c == '\n') || (c == '\r')) {
        newLine = 1;
      }
      else {
        if ((newLine) && (c == '>')) {
          cpt++;
        }
        newLine = 0;
        // found sequence name
        if (cpt == part) {
          startPosition = fileStream.tellg();
          length        = 0;
          // find sequence name size
          while (fileStream.good()) {
            c = fileStream.get();
            if ((c == '\n') || (c == '\r')) {
              break;
            }
            length++;
          }
          // copy sequence name
          sequenceName = new char[length + 1];
          fileStream.clear();
          fileStream.seekg(startPosition);
          for (unsigned int i = 0; i < length; i++) {
            sequenceName[i] = (char) fileStream.get();
          }
          sequenceName[length] = 0;
          break;
        }
      }
      c = fileStream.get();
    }
    while (fileStream.good()) {
      if (! fileStream.good()) {
        break;
      }
      if ((c != '\n') && (c != '\r')) {
        fileStream.putback(c);
        break;
      }
      c = (char) fileStream.get();
    }
    // set a tag to the beginning of our part and counts the number of letters
    newLine       = 1;
    startPosition = fileStream.tellg();
    length        = 0;
    c             = fileStream.get();
    while (fileStream.good()) {
      if ((c == '\n') || (c == '\r')) {
        newLine = 1;
      }
      else {
        if ((newLine) && (c == '>')) {
          break;
        }
        length++;
        newLine = 0;
      }
      c = fileStream.get();
    }

    if (length == 0) {
      cerr << "Error: Genomic sequence (main or duplex stem) empty !" <<endl;
      exit(1);
    }

    // allocate the string
    se = new char[length+1];
    // back to the tag
    fileStream.clear();
    fileStream.seekg(startPosition);
    
    // fill the string
    length = 0;
    c      = fileStream.get();
    while (fileStream.good()) {
      if ((c == '\n') || (c == '\r')) {
        newLine = 1;
      }
      else {
        if ((newLine) && (c == '>')) {
          break;
        }
        se[length++] = c;
        newLine = 0;
      }
      c = fileStream.get();
    }
    se[length] = 0;

    if (seq == NULL) {
      seq = new Sequence(se, length, sequenceName, true, both, reverse, complement);
    }
    else {
      seq->reset(se, length, sequenceName, both, reverse, complement);
    }
    delete[] se;
    delete[] sequenceName;
    sprintf(m, "\t\t%ith part red.", part);
    trace(10, string(m));
  }
};

#endif
