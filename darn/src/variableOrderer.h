// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef  VO_H_INCLUDED
#define  VO_H_INCLUDED


#include "common.h"
#include "variableContainer.h"
#include "defaultUnarySoftConstraint.h"


/**
 * An pure virtual (interface) variable oderer.
 * The aim is to choose the next variable, knowing the previous assigned variables, and some basic considerations about the not assigned ones.
 */

class VariableOrderer {

protected:
  /**
   * the array of the soft unary constraints of the variables (of the whole problem).
   */
  DefaultUnarySoftConstraint **unaryConstraints;
  /**
   * the set of the indices of the unassigned variables
   */
  VariableContainer *variableContainer;

public:
  /**
   * The constructor
   * @param uc the soft unary constraints of the variables, to be copied
   * @param vc the set of assigned variables
   */
  VariableOrderer (DefaultUnarySoftConstraint **uc, VariableContainer *vc) {
    unaryConstraints = uc;
    variableContainer = vc;
  }

  /**
   * empty destructor
   */
  virtual ~VariableOrderer () {}

  /**
   * Get the next variable
   */
  virtual int getNextVariable () = 0;
};


/**
 * This class implements the most simple variable ordering: one after the other, in a static fashion
 */
class SimpleVariableOrderer : public VariableOrderer {

public:
  /**
   * The constructor
   * @param uc the soft unary constraints of the variables, to be copied
   * @param vc the set of assigned variables
   */
  SimpleVariableOrderer (DefaultUnarySoftConstraint **uc, VariableContainer *vc) : VariableOrderer(uc, vc) {}
  
  /**
   * Get the next variable
   */
  virtual int getNextVariable () {
    int nextVariable;
    VariableIterator *vi = variableContainer->getIterator();
    nextVariable = *(*vi);
    delete vi;
    return nextVariable;
  }
};

/**
 * This class implements a max(deg) variable ordering
 */
class DegVariableOrderer : public VariableOrderer {

public:
  /**
   * The constructor
   * @param uc the soft unary constraints of the variables, to be copied
   * @param vc the set of assigned variables
   */
  DegVariableOrderer (DefaultUnarySoftConstraint **uc, VariableContainer *vc) : VariableOrderer(uc, vc) {}
  
  /**
   * Get the next variable
   */
  virtual int getNextVariable () {
    int val, max, var, nextVariable;
    DefaultUnarySoftConstraint *uc;
    VariableIterator *vi = variableContainer->getIterator();
    nextVariable = *(*vi);
    uc = unaryConstraints[nextVariable];
    max = uc->getVariable()->getNbSoftConstraints() + uc->getVariable()->getNbHardConstraints();
    for (++(*vi); !(vi->isOver()); ++(*vi)) {
      var = *(*vi);
      uc = unaryConstraints[var];
      val = uc->getDomain()->getSize() / (uc->getVariable()->getNbSoftConstraints() + uc->getVariable()->getNbHardConstraints());
      if (uc->getVariable()->getSequence() != sequence) {
        val -= sequence->getLength();
      }
      if (val > max) {
        max = val;
        nextVariable = var;
      }
    }
    delete vi;
    return nextVariable;
  }
};


/**
 * This class implements a min(size/deg) variable ordering
 */
class SizeDegVariableOrderer : public VariableOrderer {

public:
  /**
   * The constructor
   * @param uc the soft unary constraints of the variables, to be copied
   * @param vc the set of assigned variables
   */
  SizeDegVariableOrderer (DefaultUnarySoftConstraint **uc, VariableContainer *vc) : VariableOrderer(uc, vc) {}
  
  /**
   * Get the next variable
   */
  virtual int getNextVariable () {
    int val, min, var, nextVariable;
    DefaultUnarySoftConstraint *uc;
    VariableIterator *vi = variableContainer->getIterator();
    nextVariable = *(*vi);
    uc = unaryConstraints[nextVariable];
    min = uc->getDomain()->getSize() / (uc->getVariable()->getNbSoftConstraints()+uc->getVariable()->getNbHardConstraints());
    for (++(*vi); !(vi->isOver()); ++(*vi)) {
      var = *(*vi);
      uc = unaryConstraints[var];
      val = uc->getDomain()->getSize() / (uc->getVariable()->getNbSoftConstraints()+uc->getVariable()->getNbHardConstraints());
      if (uc->getVariable()->getSequence() != sequence) {
        val += sequence->getLength();
      }
      if (val < min) {
        min = val;
        nextVariable = var;
      }
    }
    delete vi;
    return nextVariable;
  }
};


/**
 * This class implements variable ordering that depend on the hardness of the variables
 */
class HardnessVariableOrderer : public VariableOrderer {

public:
  /**
   * The constructor
   * @param uc the soft unary constraints of the variables, to be copied
   * @param vc the set of assigned variables
   */
  HardnessVariableOrderer (DefaultUnarySoftConstraint **uc, VariableContainer *vc) : VariableOrderer(uc, vc) {}
  
  /**
   * Get the next variable
   */
  virtual int getNextVariable () {
    int val, max, varIndex, nextVariable;
    Variable *var;
    VariableIterator *vi = variableContainer->getIterator();
    nextVariable = *(*vi);
    var = unaryConstraints[nextVariable]->getVariable();
    max = 0;
    for (int i = 0; i < var->getNbHardConstraints(); i++) {
      max += var->getHardConstraint(i)->getHardness();
    }
    for (int i = 0; i < var->getNbSoftConstraints(); i++) {
      max += var->getSoftConstraint(i)->getHardness();
    }
    for (++(*vi); !(vi->isOver()); ++(*vi)) {
      varIndex = *(*vi);
      var = unaryConstraints[varIndex]->getVariable();
      val = 0;
      for (int i = 0; i < var->getNbHardConstraints(); i++) {
        val += var->getHardConstraint(i)->getHardness();
      }
      for (int i = 0; i < var->getNbSoftConstraints(); i++) {
        val += var->getSoftConstraint(i)->getHardness();
      }
      if (var->getSequence() != sequence) {
        val -= sequence->getLength();
      }
      if (val > max) {
        max = val;
        nextVariable = varIndex;
      }
    }
    delete vi;
    return nextVariable;
  }
};

#endif
