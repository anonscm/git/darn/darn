// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "abstractConstraint.h"


AbstractConstraint::AbstractConstraint (int i, int mc, int nv) : index(i), nbVariables(nv), maxCost(mc), simple(true), hardness(0) {
  setHardness();
}

AbstractConstraint::~AbstractConstraint () {}

int AbstractConstraint::getIndex () {
  return index;
}

bool AbstractConstraint::isSimple () {
  return simple;
}

string AbstractConstraint::getShortName () {
  return name;
}

void AbstractConstraint::setHardness () { 
  hardness = 0;
}

int AbstractConstraint::getHardness () {
  return hardness;
}

int AbstractConstraint::getNbVariables () {
  return nbVariables;
}

Variable *AbstractConstraint::getVariable (int i) {
  if ((i < 0) || (i > nbVariables)) {
    trace(1, "Error! Trying to access a non existing variable!");
    return NULL;
  }
  return variables[i];
}

Domain *AbstractConstraint::getDomain (int i) {
  Variable *variable = getVariable(i);
  if (variable == NULL) {
    trace(1, "Error! Trying to access a non existing domain!");
    return NULL;
  }
  return variable->getDomain();
}

int AbstractConstraint::getVariableIndex (Variable *v) {
  for (int i = 0; i < nbVariables; i++) {
    if (v == variables[i]) {
      return i;
    }
  }
  trace(1, "Error! Trying to access the index of a non existing value!");
  return -1;
}

string AbstractConstraint::getName () {
  ostringstream s;
  if (nbVariables == 0) {
    return "c0";
  }
  s << name << " (" << variables[0]->getName();
  for (int i = 1; i < nbVariables; i++) {
    s << ", " << variables[i]->getName();
  }
  s << ")";
  return s.str();
}

bool AbstractConstraint::printSpace (int var, int place) {
  return false;
}

string AbstractConstraint::print () {
  ostringstream s;
  if (nbVariables == 0) {
    return "{} ";
  }
  s << "{" << variables[0]->getName();
  for (int i = 1; i < nbVariables; i++) {
    s << "-" << variables[i]->getName();
  }
  s << "} ";
  return s.str();
}
