// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "revision.h"

Revision::Revision (int s) : size(s), currentElement(0), nbElements(0) {
  isPresent = new bool[size];
  values = new int[size];
  for (int i = 0; i < size; i++) {
    values[i] = -1;
    isPresent[i] = false;
  }
}

void Revision::addElement (int i, int v) {
  if (!isPresent[i]) {
    isPresent[i] = true;
    nbElements++;
  }
  values[i] = v;
}

int Revision::getCurrentElement () {
  if (isEmpty()) {
    return -1;
  }
  if (!isPresent[currentElement]) {
    getNext();
  }
  return currentElement;
}

int Revision::getCurrentValue () {
  if (getCurrentElement() != -1) {
    return values[currentElement];
  }
  return -1;
}

void Revision::getNext () {
  if (isEmpty()) {
    return;
  }
  while (!isPresent[currentElement]) {
    currentElement = (currentElement+1) % size;
  }
}

void Revision::removeCurrentElement () {
  if (!isPresent[currentElement]) {
    return;
  }
  isPresent[currentElement] = false;
  nbElements--;
  getNext();
}

bool Revision::isEmpty () {
  return (nbElements == 0);
}

void Revision::setAll () {
  for (int i = 0; i < size; i++) {
    isPresent[i] = true;
  }
  nbElements = size;
}

void Revision::clear () {
  for (int i = 0; i < size; i++) {
    isPresent[i] = false;
  }
  nbElements = 0;
}
