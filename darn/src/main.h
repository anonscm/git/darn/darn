// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

/**
 * a pointer to the nucleotide 'A'
 */
Nucleotide *nucleotideA;
/**
 * a pointer to the nucleotide 'C'
 */
Nucleotide *nucleotideC;
/**
 * a pointer to the nucleotide 'G'
 */
Nucleotide *nucleotideG;
/**
 * a pointer to the nucleotide 'T' (or 'U')
 */
Nucleotide *nucleotideT;
/**
 * a pointer to the nucleotide 'N' (any nucleotide)
 */
Nucleotide *nucleotideN;
/**
 * a pointer to the nucleotide 'X' (the 'unexisting' nucleotide)
 */
Nucleotide *nucleotideX;
/**
 * a pointer to the nucleotide 'R' ('A' or 'G')
 */
Nucleotide *nucleotideR;
/**
 * a pointer to the nucleotide 'Y' ('C' or 'U')
 */
Nucleotide *nucleotideY;
/**
 * a pointer to the nucleotide 'W' ('A' or 'U')
 */
Nucleotide *nucleotideW;
/**
 * a pointer to the nucleotide 'S' ('C' or 'G')
 */
Nucleotide *nucleotideS;
/**
 * a pointer to the nucleotide 'M' ('A' or 'C')
 */
Nucleotide *nucleotideM;
/**
 * a pointer to the nucleotide 'K' ('G' or 'U')
 */
Nucleotide *nucleotideK;
/**
 * a pointer to the nucleotide 'B' (everything but 'A')
 */
Nucleotide *nucleotideB;
/**
 * a pointer to the nucleotide 'D' (everything but 'C')
 */
Nucleotide *nucleotideD;
/**
 * a pointer to the nucleotide 'H' (everything but 'G')
 */
Nucleotide *nucleotideH;
/**
 * a pointer to the nucleotide 'V' (everything but 'T')
 */
Nucleotide *nucleotideV;
/**
 * a pointer to all the nucleotides
 */
Nucleotide **nucleotides;


/**
 * the output stream of the program (can be a file or the standard output)
 */
ostream *outputStream;
/**
 * the total number of variables
 */
int nbTotVariables;
/**
 * the value of top
 */
Top *top;
/**
 * the soft constraints
 */
vector<SoftConstraint *> softConstraints;
/**
 * the total number of soft helices
 */
int nbSoftHelix;
/**
 * the hard constraints
 */
vector<HardConstraint *> hardConstraints;
/**
 * the total number of hard helices
 */
int nbHardHelix;
/**
 * the debug level (highest is most verbose)
 *  (default: 1)
 */
int traceLevel;
/**
 * all the variables of the constraint network
 */
Variable **variables;
/**
 * the c0 constraints
 */
AbstractSoftConstraint *zeroArityConstraint;
/**
 * the stack that stores all variables such that their lower bound has increased
 *   (used to process hard easy constraints)
 */
Revision *lbHardRevision;
/**
 * the stack that stores all variables such that their upper bound has increased
 *   (used to process hard easy constraints)
 */
Revision *ubHardRevision;
/**
 * the stack that stores all variables such that their lower bound has increased
 *   (used to process hard complicated constraints)
 */
Revision *lbComplicatedHardRevision;
/**
 * the stack that stores all variables such that their upper bound has increased
 *   (used to process hard complicated constraints)
 */
Revision *ubComplicatedHardRevision;
/**
 * the stack that stores all variables that have been assigned
 *   (used to process hard easy constraints)
 */
Revision *assignmentHardRevision;
/**
 * the stack that stores all variables that have been assigned
 *   (used to process hard complicated constraints)
 */
Revision *assignmentComplicatedHardRevision;
/**
 * the stack that stores all variables that have been assigned
 *   (used to process soft easy constraints)
 */
Revision *assignmentSoftRevision;
/**
 * the stack that stores all variables such that their lower bound has increased
 *   (used to process soft easy constraints)
 */
Revision *lbSoftRevision;
/**
 * the stack that stores all variables such that their upper bound has increased
 *   (used to process soft easy constraints)
 */
Revision *ubSoftRevision;
/**
 * the stack that stores all variables such that a value has been removed
 *   (used to process soft easy constraints)
 */
Revision *holeSoftRevision;
/**
 * the stack that stores all variables that have been assigned
 *   (used to process soft complicated constraints)
 */
Revision *assignmentComplicatedSoftRevision;
/**
 * the stack that stores all variables such that their lower bound has increased
 *   (used to process soft complicated constraints)
 */
Revision *lbComplicatedSoftRevision;
/**
 * the stack that stores all variables such that their upper bound has increased
 *   (used to process soft complicated constraints)
 */
Revision *ubComplicatedSoftRevision;
/**
 * the stack that stores all variables such that a value has been removed
 *   (used to process soft complicated constraints)
 */
Revision *holeComplicatedSoftRevision;

/**
 * the file that stores the main sequence
 */
FileReader *sequenceFileReader;
/**
 * the main sequence
 */
Sequence *sequence;
/**
 * the duplex sequences
 */
Sequence **dupSequences;
/**
 * the number of duplex sequences
 *  (default: 0 or 1)
 */
int nbDupSequences;
/**
 * the name of the file that contains the descriptor
 */
char *descriptorFileName;
/**
 * whether the sequence should be read from left to right and from right to left 
 *  (default: false)
 */
bool sequenceBoth;
/**
 * whether the sequence should be read from left to right
 *  (default: false)
 */
bool sequenceReverse;
/**
 * whether the nucleotides on the sequence should be complemented
 *  (default: false)
 */
bool sequenceComplement;
/**
 * whether the duplex sequences should be read from left to right
 *  (default: false)
 */
bool duplexReverse;
/**
 * whether the nucleotides on the duplex sequences should be complemented
 *  (default: false)
 */
bool duplexComplement;
/**
 * whether dominated solutions should be printed
 *  (default: false)
 */
bool printDominatedSolutions;
/**
 * whether solutions should be printed with GFF format
 *  (default: false)
 */
bool gffFormat;
/**
 * whether solutions should be printed with parser format
 *  (default: false)
 */
bool parserFormat;
/**
 * whether solutions should be printed with fasta format
 *  (default: false)
 */
bool fastaFormat;
/**
 * whether solutions should be ordered by increasing cost
 *  (default: false)
 */
bool orderedByCost;
/**
 * display the costs given by each constraint
 *  (default: false)
 */
bool explanation;
/**
 * display the structure of the solutions found
 *  (default: false)
 */
bool displayStructure;
/**
 * output the results using HTML markups
 *  (default: false)
 */
bool webDisplay;
/**
 * the directory where to draw solutions
 *  (default: NULL)
 */
char *drawDirectory;
/**
 * the WWW address to the directory that contains the drawings
 *  (default: NULL)
 */
char *webDrawDirectory;
/**
 * the constraint network
 */
Network *network;


/**
 * declare static 'ValuesReader::matchingValues' variable
 */
MatchingValues *ValuesReader::matchingValues;
/**
 * declare static 'ValuesReader::interactionValues' variable
 */
InteractionValues *ValuesReader::interactionValues;
/**
 * declare static 'ValuesReader::interactionValuesWithWobble' variable
 */
InteractionValues *ValuesReader::interactionValuesWithWobble;
/**
 * declare static 'ValuesReader::isostericityValues' variable
 */
IsostericityValues *ValuesReader::isostericityValues;

/**
 * declare input file for descriptor parser
 */
extern FILE *yyin;
/**
 * declare main function for descriptor parser
 */
extern void yyparse();


/**
 * catch a Ctrl+C interruption
 * @param sigNum the id number of the caught signal
 */
void catchInterruption (int sig_num) {
  alarm(0);
  trace(1, "\n\n\tInterruption! Displaying solutions...\n");
  network->printSolutions();
  exit(1);
}

/**
 * catch a time out interruption
 * @param sigNum the id number of the caught signal
 */
void catchAlarm (int sig_num) {
  alarm(0);
  trace(1, "\n\n\tTime out! Displaying solutions...\n");
  network->printSolutions();
  exit(1);
}

/**
 * print the time spent to solve the problem
 * @param time the time spent
 */
void printTime (double time) {
  if ((! gffFormat) && (! fastaFormat)) {
    char mesg[511];
    sprintf(mesg, "\n%s\t\t\t(total number of solutions: %i)%s", ((webDisplay)? "<p>\n": ""), network->getNbUndominatedSolutions(), ((webDisplay)? "\n</p>": ""));
    trace(0, mesg);
    sprintf(mesg, "\n%s\t\t\t(time spent: %i second(s))%s", ((webDisplay)? "<p>\n": ""), static_cast<int>(time), ((webDisplay)? "\n</p>": ""));
    trace(0, mesg);
  }
}

#endif
