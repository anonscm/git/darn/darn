// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef COMP_H_INCLUDED
#define COMP_H_INCLUDED

#include "values.h"
#include "softConstraint.h"

/**
 * This class stands for an composition soft constraint, involving two variables:
 *   the first variable gives the start position, the other one, the end position of
 *   an interval that should containt a given proportion of dinucleotides
 */
class Composition : public SoftConstraint {

protected:
  /**
   * the first nucleotide considered
   */
  Nucleotide *nucleotide1;
  /**
   * the second nucleotide considered
   */
  Nucleotide *nucleotide2;
  /**
   * the minimum threshold of the proportion of dinucleotides
   */
  int threshold1;
  /**
   * the maximum threshold of the proportion of dinucleotides
   */
  int threshold2;
  /**
   * the minimum cost given by the constraint
   */
  int minCost;
  /**
   * whether the proportion should be not less or not greater that the thresholds
   */
  Relation relation;

  /**
   * check whether the given nucleotide is one of the dinucleotides
   * @param n the given nucleotide
   * @return true, if the given nucleotide is one of the dinucleotides
   */
  inline bool isCorrectNucleotide (Nucleotide *n);
  /**
   * check if the ration @a nbPositives over @a nbTotal satisfies the constraint
   * @param nbPositives is the number of nucleotides similar to @a nucleotide1 or @a nucleotide2
   * @param nbTotal is the total number of nucleotides 
   * @return true if the ration satisfies the constraint
   */
  inline int getCost (int nbPositives, int nbTotal);
  /**
   * get the minimum cost of the constraint, when the first variable is @a i1, and the second variable is @a i2
   * @param i1 the first position where the word may start
   * @param i2 the first position where the word may end
   * @return the minimum cost of the constraint
   */
  int computeCost (int i1, int i2);

public:

  /**
   * the constructor
   * @param v1 the start position of the considered interval
   * @param v2 the end position of the considered interval
   * @param i the index of this constraint
   * @param o whether the pattern is optional
   * @param n1 the first nucleotide considered
   * @param n2 the first nucleotide considered
   * @param t1 the first threshold
   * @param t2 the second threshold
   * @param r the relation with respect to the thresholds
   * @param mc the minimum cost given by the constraint
   * @param e2c the convertor from errors to costs
   */
  Composition (Variable *v1, Variable *v2, int i, bool o, Nucleotide *n1, Nucleotide *n2, int t1, int t2, Relation r, int mc, ErrorsToCosts *e2c);

  /**
   * get the minimum of the function cost
   * @return the minimum
   */
  virtual int getMinCost ();
  /**
   * @pre The variable @a v should be discretized
   *
   * get the minimum of the function cost, having that the given variable has been assigned to the given value
   * @param v the given variable
   * @param val the given element
   * @return the minimum
   */
  virtual int getMinCost (Variable *v, int val);
  /**
   * @pre The variable @a v should be not discretized
   *
   * compute the minimum cost of the constraint, having that the specified variable is assigned to the lower bound
   * @param v the specified variable
   * @return the minimum
   */
  virtual int getMinLbCost (Variable *v);
  /**
   * @pre The variable @a v should be not discretized
   *
   * compute the minimum cost of the constraint, having that the specified variable is assigned to the upper bound
   * @param v the specified variable
   * @return the minimum
   */
  virtual int getMinUbCost (Variable *v);

  /**
   * compute the cost of the given support
   * @param s the support
   * @return the cost
   */
  virtual int getCost (Support *s);
  /**
   * get the contribution of this constraint to the global cost when all variables are assigned
   * @param values the values of the involved variables
   * @return the cost of this constraint
   */
  virtual int getFinalCost (int *values);
};

#endif
