 /* Darn: RNA Motif Localization
    Copyright (C) 2010  INRA

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>. */


%{
#include "common.h"
#include "parseSem.hh"
%}

/* Prevent the need for linking with -lfl */
%option noyywrap


BLANK           [ \t]+
NUMBER          -?[0-9]+
WORD            [A-Za-z]+
SVAR            [XS][0-9]+
TVAR            [YT][0-9]+
SHELLCOMMENTS   "#"[^\n]*"\n"
CCOMMENTS       "//"[^\n]*"\n"


%%

MAXCOST           {
                    trace(100,"MAXCOST");
                    return MAX_COST;
                  }
MAX_COST          {
                    trace(100,"MAX_COST");
                    return MAX_COST;
                  }
TOP_VALUE         {
                    trace(100,"TOP_VALUE");
                    return MAX_COST;
                  }
POSITIONS         {
                    trace(100,"POSITIONS");
                    return DECL_VARIABLE;
                  }
VARIABLES         {
                    trace(100,"VARIABLES");
                    return DECL_VARIABLE;
                  }
S_VAR             {
                    trace(100,"S_VAR");
                    return DECL_SVAR;
                  }
T_VAR             {
                    trace(100,"T_VAR");
                    return DECL_TVAR;
                  }
X_VAR             {
                    trace(100,"X_VAR");
                    return DECL_SVAR;
                  }
Y_VAR             {
                    trace(100,"Y_VAR");
                    return DECL_TVAR;
                  }
CONTENT           {
                    trace(100,"CONTENT");
                    return DECL_CONTENT;
                  }
PATTERN           {
                    trace(100,"PATTERN");
                    return DECL_PATTERN;
                  }
PATTERN2          {
                    trace(100,"PATTERN2");
                    return DECL_PATTERN2;
                  }
SPACER            {
                    trace(100,"SPACER");
                    return DECL_SPACER;
                  }
DISTANCE          {
                    trace(100,"DISTANCE");
                    return DECL_SPACER;
                  }
COMPOSITION       {
                    trace(100,"COMPOSITION");
                    return DECL_COMPOSITION;
                  }
PAIR              {
                    trace(100,"PAIR");
                    return DECL_NONCANONICPAIR;
                  }
NON_CANONIC_PAIR  {
                    trace(100,"NON_CANONIC_PAIR");
                    return DECL_NONCANONICPAIR;
                  }
HELIX             {
                    trace(100,"HELIX");
                    return DECL_HELIX_SIMPLE;
                  }
REPETITION        {
                    trace(100,"REPETITION");
                    return DECL_REPETITION;
                  }
HELIX2            {
                    trace(100,"HELIX2");
                    return DECL_HELIX;
                  }
DUPLEX            {
                    trace(100,"DUPLEX");
                    return DECL_DUPLEX;
                  }
HAIRPIN           {
                    trace(100,"HAIRPIN");
                    return DECL_HAIRPIN;
                  }
FOLDING           {
                    trace(100,"FOLDING");
                    return DECL_HAIRPIN;
                  }

word              {
                    trace(100,"word");
                    return PARAM_WORD;
                  }
nucleotides       {
                    trace(100,"nucleotides");
                    return PARAM_NUCLEOTIDES;
                  }
nucleotide        {
                    trace(100,"nucleotide");
                    return PARAM_NUCLEOTIDES;
                  }
errors            {
                    trace(100,"errors");
                    return PARAM_ERRORS;
                  }
err               {
                    trace(100,"err");
                    return PARAM_ERRORS;
                  }
err_sub           {
                    trace(100, "err_sub");
                    return PARAM_ERRORS;
                  }
err_ins           {
                    trace(100, "err_ins");
                    return PARAM_ERRORS;
                  }
err_del           {
                    trace(100, "err_del");
                    return PARAM_ERRORS;
                  }
stem              {
                    trace(100, "stem");
                    return PARAM_STEM;
                  }
stemin            {
                    trace(100, "stemin");
                    return PARAM_MINSTEM;
                  }
stemax            {
                    trace(100, "stemax");
                    return PARAM_MAXSTEM;
                  }
loop              {
                    trace(100, "loop");
                    return PARAM_LOOP;
                  }
loopmin           {
                    trace(100, "loopmin");
                    return PARAM_MINLOOP;
                  }
loopmax           {
                    trace(100, "loopmax");
                    return PARAM_MAXLOOP;
                  }
length            {
                    trace(100, "length");
                    return PARAM_LENGTH;
                  }
lenmin            {
                    trace(100, "lenmin");
                    return PARAM_MINLENGTH;
                  }
lenmax            {
                    trace(100, "lenmax");
                    return PARAM_MAXLENGTH;
                  }
cost              {
                    trace(100, "cost");
                    return PARAM_COSTS;
                  }
costs             {
                    trace(100, "costs");
                    return PARAM_COSTS;
                  }
costmin           {
                    trace(100, "costmin");
                    return PARAM_MINCOSTS;
                  }
costmax           {
                    trace(100, "costmax");
                    return PARAM_MAXCOSTS;
                  }
interaction       {
                    trace(100, "interaction");
                    return PARAM_INTERACTION;
                  }
orientation       {
                    trace(100, "orientation");
                    return PARAM_ORIENTATION;
                  }
threshold         {
                    trace(100, "threshold");
                    return PARAM_THRESHOLD;
                  }
family            {
                    trace(100, "family");
                    return PARAM_FAMILY;
                  }
indels            {
                    trace(100, "indels");
                    return PARAM_INDELS;
                  }
wobble            {
                    trace(100, "wobble");
                    return PARAM_WOBBLE;
                  }
distance          {
                    trace(100, "distance");
                    return PARAM_DISTANCE;
                  }
size              {
                    trace(100, "size");
                    return PARAM_SIZE;
                  }
model             {
                    trace(100, "model");
                    return PARAM_MODEL;
                  }


WATSON            {
                    trace(100,"WATSON");
                    return INTERACTION_WATSON_CRICK;
                  }
WATSON_CRICK      {
                    trace(100,"WATSON_CRICK");
                    return INTERACTION_WATSON_CRICK;
                  }
WATSON-CRICK      {
                    trace(100,"WATSON-CRICK");
                    return INTERACTION_WATSON_CRICK;
                  }
HOOGSTEEN         {
                    trace(100,"HOOGSTEEN");
                    return INTERACTION_HOOGSTEEN;
                  }
SUGAR_EDGE        {
                    trace(100,"SUGAR_EDGE");
                    return INTERACTION_SUGAR_EDGE;
                  }
SUGAR             {
                    trace(100,"SUGAR-EDGE");
                    return INTERACTION_SUGAR_EDGE;
                  }
CIS               {
                    trace(100,"CIS");
                    return ORIENTATION_CIS;
                  }
TRANS             {
                    trace(100,"TRANS");
                    return ORIENTATION_TRANS;
                  }
NOT_LESS_THAN     {
                    trace(100,"NOT_LESS_THAN");
                    return RELATION_NOT_LESS_THAN;
                  }
GREATER_THAN      {
                    trace(100,"GREATER_THAN");
                    return RELATION_NOT_LESS_THAN;
                  }
">="              {
                    trace(100,">=");
                    return RELATION_NOT_LESS_THAN;
                  }
">"               {
                    trace(100,">");
                    return RELATION_NOT_LESS_THAN;
                  }
NOT_GREATER_THAN  {
                    trace(100,"NOT_GREATER_THAN");
                    return RELATION_NOT_GREATER_THAN;
                  }
LESS_THAN         {
                    trace(100,"LESS_THAN");
                    return RELATION_NOT_GREATER_THAN;
                  }
"<="              {
                    trace(100,"<=");
                    return RELATION_NOT_GREATER_THAN;
                  }
"<"               {
                    trace(100,"<");
                    return RELATION_NOT_GREATER_THAN;
                  }
hard              {
                    trace(100,"hard");
                    return MODEL_HARD;
                  }
soft              {
                    trace(100,"soft");
                    return MODEL_SOFT;
                  }
optional         {
                    trace(100,"optional");
                    return MODEL_OPTIONAL;
                  }

true              {
                    trace(100,"true");
                    return VALUE_TRUE;
                  }
yes               {
                    trace(100,"yes");
                    return VALUE_TRUE;
                  }
false             {
                    trace(100,"false");
                    return VALUE_FALSE;
                  }
no                {
                    trace(100,"no");
                    return VALUE_FALSE;
                  }
all               {
                    trace(100,"all");
                    return VALUE_ALL;
                  }


"..."             {
                    trace(100,"...");
                    return DOTS;
                  }
".."              {
                    trace(100,"..");
                    return DOTS;
                  }
"["               {
                    trace(100,"[");
                    return LEFT_BRACKET;
                  }
"]"               {
                    trace(100,"]");
                    return RIGHT_BRACKET;
                  }
"("               {
                    trace(100,"(");
                    return LEFT_PAR;
                  }
")"               {
                    trace(100,")");
                    return RIGHT_PAR;
                  }
"{"               {
                    trace(100,"{");
                    return LEFT_BRA;
                  }
"}"               {
                    trace(100,"}");
                    return RIGHT_BRA;
                  }
"="               {
                    trace(100,"=");
                    return EQUAL;
                  }
","               {
                    trace(100,",");
                    return COMMA;
                  }
"%"               {

                    trace(100,"%");
                    return PERCENT;
                  }
"\""              {
                    trace(100," \" ");
                    return QUOTE;
                  }
\n                {
                    trace(100,"END_LINE");
                    return END_LINE;
                  }

{BLANK}           { /* empty */ }

{NUMBER}          {
                    trace(100,"NUMBER");
                    yylval.value = atoi(yytext);
                    return NUMBER;
                  }

{SVAR}            {
                    trace(100,"SVAR");
                    yylval.value = atoi(yytext+1);
                    return SVAR;
                  }
{TVAR}            {
                    trace(100,"TVAR");
                    yylval.value = atoi(yytext+1);
                    return TVAR;
                  }

{SHELLCOMMENTS}   { /* empty */
                    trace(100, "SHELL COMMENTS");
                  }
{CCOMMENTS}       { /* empty */
                    trace(100, "C COMMENTS");
                  }

{WORD}            {
                    trace(100,"WORD");
                    yylval.word = (char *) malloc(sizeof(char) * (1+yyleng));
                    strcpy(yylval.word, yytext);
                    return WORD;
                  }


%%

