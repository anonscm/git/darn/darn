// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef HDIST_H_INCLUDED
#define HDIST_H_INCLUDED

#include "hardConstraint.h"


/**
 * This class stands for a distance hard constraint.
 */
class HardDistance : public HardConstraint {

protected:

  /**
   * the minimum distance between the variables
   * (the maximum distance is stored in maxCost)
   */
  int minCost;
  

public:
  /**
   * the constructor
   * @param v1 the first variable (the lower bound of the first sequence)
   * @param v2 the second variable (the upper bound of the first sequence)
   * @param i the index of this constraint
   * @param d1 the minimum distance between the variables
   * @param d2 the maximum distance between the variables
   */
  HardDistance (Variable *v1, Variable *v2, int i, int d1, int d2);
  /**
   * revision of the constraint, when no information is given on what led to the revision
   */
  virtual void revise ();
  /**
   * revision of the constraint, due to a given variable
   * @param v the index of the given variable
   */
  virtual void revise (int v);
  /**
   * revision of the constraint, when the lower bound of a variable has changed
   * @param v the index of the given variable
   */
  virtual void reviseFromLb (int v);
  /**
   * revision of the constraint, when the upper bound of a variable has changed
   * @param v the given variable
   */
  virtual void reviseFromUb (int v);
  /**
   * revision of the constraint, when the upper bound of a variable has been assigned
   * @param v the given variable
   */
  virtual void reviseFromAssignment (int v);
  /**
   * verfiy that the support is still valid
   * @param s the support
   */
  virtual bool getConsistency (Support *s);
};

#endif
