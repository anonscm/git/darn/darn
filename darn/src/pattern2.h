// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef PATT2_H_INCLUDED
#define PATT2_H_INCLUDED

#include "values.h"
#include "tools.h"
#include "automaton.h"
#include "softConstraint.h"

/**
 * This class stands for an pattern soft constraint, involving two variables:
 *   the first variable gives the start position, the other one, the end position.
 */
class Pattern2 : public SoftConstraint {

protected:
  /**
   * the sequence where the pattern should be found
   */
  Sequence* sequence;
  /**
   * the pattern to be compared with the sequence
   */
  Sequence* pattern;
  /**
   * the same pattern as @a pattern, read from right to left
   */
  Sequence* reversePattern;
  /**
   * an automaton, that scans the text from left to right
   */
  Automaton *directAutomaton;
  /**
   * an automaton, that scans the text from right to left
   */
  Automaton *reverseAutomaton;
  /**
   * both automatons, namely @a directAutomaton and @a reverseAutomaton
   */
  Automaton *automatons[2];
  /**
   * matching values
   */
  MatchingValues *matchingValues;

  /**
   * it checks whether the candidate found by @a computeCost is valid
   * @param lb1 the first position where the candidate may start
   * @param ub1 the last position where the candidate may start
   * @param lb2 the first position where the word may end
   * @param ub2 the last position where the word may end
   * @return the edit cost of the candidate
   */
  int computeErrors (int lb1, int ub1, int lb2, int ub2);
  /**
   * the algorithm to get the minimum number the mismatch/insertion/deletion between the specified bound
   * @param lb1 the first position where the word may start
   * @param ub1 the last position where the word may start
   * @param lb2 the first position where the word may end
   * @param ub2 the last position where the word may end
   * @return the edit cost
   */
  int computeCost (int lb1, int ub1, int lb2, int ub2);

public:

  /**
   * the constructor
   * @param v1 the start position of the pattern
   * @param v2 the end position of the pattern
   * @param i the index of this constraint
   * @param o whether the pattern is optional
   * @param s the pattern
   * @param e2c the convertor from errors to costs
   * @param mv the matching values
   */
  Pattern2 (Variable *v1, Variable *v2, int i, bool o, string s, ErrorsToCosts *e2c, MatchingValues *mv);

  /**
   * get the minimum of the function cost
   * @return the minimum
   */
  virtual int getMinCost ();
  /**
   * @pre The variable @a v should be discretized
   *
   * get the minimum of the function cost, having that the given variable has been assigned to the given value
   * @param v the given variable
   * @param val the given element
   * @return the minimum
   */
  virtual int getMinCost (Variable *v, int val);
  /**
   * @pre The variable @a v should be not discretized
   *
   * compute the minimum cost of the constraint, having that the specified variable is assigned to the lower bound
   * @param v the specified variable
   * @return the minimum
   */
  virtual int getMinLbCost (Variable *v);
  /**
   * @pre The variable @a v should be not discretized
   *
   * compute the minimum cost of the constraint, having that the specified variable is assigned to the upper bound
   * @param v the specified variable
   * @return the minimum
   */
  virtual int getMinUbCost (Variable *v);

  /**
   * compute the cost of the given support
   * @param s the support
   * @return the cost
   */
  virtual int getCost (Support *s);
  /**
   * get the contribution of this constraint to the global cost when all variables are assigned
   * @param values the values of the involved variables
   * @return the cost of this constraint
   */
  virtual int getFinalCost (int *values);
};

#endif
