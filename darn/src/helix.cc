// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "helix.h"


Helix::Helix (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int i, bool o, int hl, ErrorsToCosts *e2c, InteractionValues *iv) : SoftConstraint(i, e2c, o, 4, v1, v2, v3, v4), helixLength(hl) {
  simple = false;
  matrix = new HelixCell***[HELIXMAXLENGTH+1];
  for (unsigned int i = 0; i <= HELIXMAXLENGTH; i++) {
    matrix[i] = new HelixCell**[HELIXMAXLENGTH+1];
    for (unsigned int j = 0; j <= HELIXMAXLENGTH; j++) {
      matrix[i][j] = new HelixCell*[helixLength+1];
      for (unsigned int k = 0; k <= helixLength; k++) {
        matrix[i][j][k] = new HelixCell(errorsToCosts->getMaxErrors()+1);
      }
    }
  }
  interactionValues = iv;
  name = "helix (with size penalty)";
}

int Helix::getMinCost () {
  int result;
  currentSupport->unset();
  if ((!unaryConstraints[0]->isAssigned()) && (!unaryConstraints[1]->isAssigned()) && (!unaryConstraints[2]->isAssigned()) && (!unaryConstraints[3]->isAssigned())) {
    return 0;
  }
  if (getMem() >= getMaxCost()) {
    return 0;
  }
  result = getMinCost(unaryConstraints[0]->getLb(), unaryConstraints[0]->getUb(), unaryConstraints[1]->getLb(), unaryConstraints[1]->getUb(), unaryConstraints[2]->getLb(), unaryConstraints[2]->getUb(), unaryConstraints[3]->getLb(), unaryConstraints[3]->getUb());
  if (result + getMem() >= getMaxCost()) {
    return getMaxCost() - getMem();
  }
  return result;
}

int Helix::getMinCost (Variable *var, int val) {
  int i, lb1, ub1, lb2, ub2, lb3, ub3, lb4, ub4, result;
  currentSupport->unset();
  if ((!getVariable(0)->getUnaryConstraint()->isAssigned()) && (!getVariable(1)->getUnaryConstraint()->isAssigned()) && (!getVariable(2)->getUnaryConstraint()->isAssigned()) && (!getVariable(3)->getUnaryConstraint()->isAssigned())) {
    return 0;
  }
  if (getMemory(var, val) >= getMaxCost()) {
    return 0;
  }
  i = getVariableIndex(var);
  lb1 = getVariable(0)->getUnaryConstraint()->getLb();
  ub1 = getVariable(0)->getUnaryConstraint()->getUb();
  lb2 = getVariable(1)->getUnaryConstraint()->getLb();
  ub2 = getVariable(1)->getUnaryConstraint()->getUb();
  lb3 = getVariable(2)->getUnaryConstraint()->getLb();
  ub3 = getVariable(2)->getUnaryConstraint()->getUb();
  lb4 = getVariable(3)->getUnaryConstraint()->getLb();
  ub4 = getVariable(3)->getUnaryConstraint()->getUb();
  switch (i) {
    case 0:
      lb1 = val;
      ub1 = val+1;
      break;
    case 1:
      lb2 = val;
      ub2 = val+1;
      break;
    case 2:
      lb3 = val;
      ub3 = val+1;
      break;
    case 3:
      lb4 = val;
      ub4 = val+1;
      break;
  }
  result = getMinCost (lb1, ub1, lb2, ub2, lb3, ub3, lb4, ub4);
  if (result + getMemory(var, val) >= getMaxCost()) {
    return getMaxCost() - getMemory(var, val);
  }
  return result;
}

int Helix::getMinLbCost (Variable *var) {
  return getMinCost(var, var->getUnaryConstraint()->getLb());
}

int Helix::getMinUbCost (Variable *var) {
  return getMinCost(var, var->getUnaryConstraint()->getUb()-1);
}

int Helix::getMinCost (int lb1, int ub1, int lb2, int ub2, int lb3, int ub3, int lb4, int ub4) {
  if ((ub2-lb1 < static_cast<int>(HELIXMINLENGTH)) || (ub4-lb3 < static_cast<int>(HELIXMINLENGTH)) || (ub3-lb2 < static_cast<int>(LOOPMINLENGTH))) {
    return getMaxCost()+1;
  }
  if ((ub2-lb1 >= static_cast<int>(HELIXMAXLENGTH)) || (ub4-lb3 >= static_cast<int>(HELIXMAXLENGTH))) {
    return 0;
  }
  return computeResult(lb1, ub1, lb2, ub2, lb3, ub3, lb4, ub4);
 }

int Helix::computeResult (int lb1, int ub1, int lb2, int ub2, int lb3, int ub3, int lb4, int ub4) {
  int min = getMaxCost()+1, value = getMaxCost()+1, match;
  HelixCell *cell = NULL;
  int mem1, mem2, mem3, mem4, mem;
  int l1 = ub2-lb1;
  int l2 = ub4-lb3;
  if ((l1 > static_cast<int>(HELIXMAXLENGTH)) || (l2 > static_cast<int>(HELIXMAXLENGTH))) {
    return 0;
  }
  mem = getMem();
  for (int i = 0; i <= l1; i++) {
    for (int j = 0; j <= l2; j++) {
      for (unsigned int k = 0; k <= helixLength; k++) {
        matrix[i][j][k]->setValues(getMaxCost()+1, -1, -1);
      }
      if ((i < ub1-lb1) && (j < ub4-lb4)) {
        mem1 = getMemory(variables[0], i+lb1);
        mem4 = getMemory(variables[3], ub4-1-j);
        if (mem1 + mem4 + mem <= getMaxCost()) {
          matrix[i][j][0]->setValues(- (mem1 + mem4), i+lb1, ub4-1-j);
        }
      }
    }
  }
  for (unsigned int k = 1; k <= helixLength; k++) {
    if (ub3 > ub4) {
      mem3 = getMemory(variables[2], ub4);
      for (int i = 1; i < lb2-lb1+1; i++) {
        cell = matrix[i-1][0][k-1];
        matrix[i][0][k]->setValues(cell->value+interactionValues->getInsertionCost(), cell->rootI, cell->rootJ);
      }
      for (int i = lb2-lb1+1; i <= l1; i++) {
        cell = matrix[i-1][0][k-1];
        value = cell->value+interactionValues->getInsertionCost();
        matrix[i][0][k]->setValues(value, cell->rootI, cell->rootJ);
        mem2 = getMemory(variables[1], i-1+lb1);
        value += (helixLength - k)*interactionValues->getHelixLengthCost() - (mem2 + mem3);
        if (errorsToCosts->getCostFor(value) < min) {
          if (mem2 + mem3 + mem <= getMaxCost()) {
            min = errorsToCosts->getCostFor(value);
            currentSupport->setSupport(0, cell->rootI);
            currentSupport->setSupport(1, i-1+lb1);
            currentSupport->setSupport(2, ub4);
            currentSupport->setSupport(3, cell->rootJ);
          }
        }
      }
    }
    else {
      for (int i = 1; i <= l1; i++) {
        cell = matrix[i-1][0][k-1];
        matrix[i][0][k]->setValues(cell->value+interactionValues->getInsertionCost(), cell->rootI, cell->rootJ);
      }
    }
    if (lb2 < lb1) {
      mem2 = getMemory(variables[1], lb1-1);
      for (int j = 1; j < ub4-ub3+1; j++) {
        cell = matrix[0][j-1][k-1];
        matrix[0][j][k]->setValues(cell->value+interactionValues->getInsertionCost(), cell->rootI, cell->rootJ);
      }
      for (int j = ub4-ub3+1; j <= l2; j++) {
        cell = matrix[0][j-1][k-1];
        value = cell->value+interactionValues->getInsertionCost();
        matrix[0][j][k]->setValues(value, cell->rootI, cell->rootJ);
        mem3 = getMemory(variables[2], ub4-j);
        value += (helixLength - k)*interactionValues->getHelixLengthCost() - (mem2 + mem3);
        if (errorsToCosts->getCostFor(value) < min) {
          if (mem2 + mem3 + mem <= getMaxCost()) {
            min = errorsToCosts->getCostFor(value);
            currentSupport->setSupport(0, cell->rootI);
            currentSupport->setSupport(1, lb1-1);
            currentSupport->setSupport(2, ub4-j);
            currentSupport->setSupport(3, cell->rootJ);
          }
        }
      }
    }
    else {
      for (int j = 1; j <= l2; j++) {
        cell = matrix[0][j-1][k-1];
        matrix[0][j][k]->setValues(cell->value+interactionValues->getInsertionCost(), cell->rootI, cell->rootJ);
      }
    }
  }
  for (int i = 1; i <= l1; i++) {
    for (int j = 1; j <= l2; j++) {
      match = interactionValues->getMatchCost(sequence->getNucleotide(i+lb1-1), sequence->getNucleotide(ub4-j));
      if ((i >= lb2-lb1+1) && (j >= ub4-ub3+1)) {
        mem2 = getMemory(variables[1], i-1+lb1);
        mem3 = getMemory(variables[2], ub4-j);
        for (unsigned int k = 1; k <= helixLength; k++) {
          switch(argMin3(matrix[i-1][j-1][k-1]->value+match, matrix[i][j-1][k-1]->value+interactionValues->getInsertionCost(), matrix[i-1][j][k-1]->value+interactionValues->getInsertionCost())) {
            case 0:
              cell = matrix[i-1][j-1][k-1];
              value = cell->value + match;
              break;
            case 1:
              cell = matrix[i][j-1][k-1];
              value = cell->value + interactionValues->getInsertionCost();
              break;
            case 2:
              cell = matrix[i-1][j][k-1];
              value = cell->value + interactionValues->getInsertionCost();
              break;
          }
          matrix[i][j][k]->setValues(value, cell->rootI, cell->rootJ);
          value += (helixLength - k)*interactionValues->getHelixLengthCost() - (mem2 + mem3);
          if (errorsToCosts->getCostFor(value) < min) {
            if (mem2 + mem3 + mem <= getMaxCost()) {
              min = errorsToCosts->getCostFor(value);
              currentSupport->setSupport(0, cell->rootI);
              currentSupport->setSupport(1, i-1+lb1);
              currentSupport->setSupport(2, ub4-j);
              currentSupport->setSupport(3, cell->rootJ);
            }
          }
        }
      }
      else {
        for (unsigned int k = 1; k <= helixLength; k++) {
          switch(argMin3(matrix[i-1][j-1][k-1]->value+match, matrix[i][j-1][k-1]->value+interactionValues->getInsertionCost(), matrix[i-1][j][k-1]->value+interactionValues->getInsertionCost())) {
            case 0:
              cell = matrix[i-1][j-1][k-1];
              value = cell->value + match;
              break;
            case 1:
              cell = matrix[i][j-1][k-1];
              value = cell->value + interactionValues->getInsertionCost();
              break;
            case 2:
              cell = matrix[i-1][j][k-1];
              value = cell->value + interactionValues->getInsertionCost();
              break;
          }
          matrix[i][j][k]->setValues(value, cell->rootI, cell->rootJ);
        }
      }
    }
  }
  return min - mem;
}

int Helix::getCost (Support *s) {
  int val1 = s->getSupport(0);
  int val2 = s->getSupport(1);
  int val3 = s->getSupport(2);
  int val4 = s->getSupport(3);
  return computeResult(val1, val1+1, val2, val2+1, val3, val3+1, val4, val4+1);
}

int Helix::getFinalCost (int *values) {
  int val1, val2, val3, val4;
  val1 = values[0];
  val2 = values[1];
  val3 = values[2];
  val4 = values[3];
  return min2(getMaxCost()+1, errorsToCosts->getCostFor(computeResult(val1, val1+1, val2, val2+1, val3, val3+1, val4, val4+1)));
}
