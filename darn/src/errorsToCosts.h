// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef E2C_H_INCLUDED
#define E2C_H_INCLUDED

#include "common.h"

#define IMPOSSIBLE (-1)

/**
 * This class makes it possible to transform a number of errors, and vice-versa, for soft constraints
 */
class ErrorsToCosts {

protected:
  /**
   * conversion array
   */
  int *errors2costs;
  /**
   * maximum number of errors allowed
   */
  int maxErrors;
  /**
   * maximum cost given
   */
  int maxCost;


public:

  /**
   * constructor
   * @param me maximum number of errors allowed
   * @param c conversion array (default is identity)
   */
  ErrorsToCosts (const int me, const int *c = NULL) : maxErrors(me) {
    errors2costs = new int[maxErrors+1];
    if (c == NULL) {
      for (int i = 0; i < maxErrors+1; i++) {
        errors2costs[i] = i;
      }
      maxCost = maxErrors;
      return;
    }

    for (int i = 0; i < maxErrors+1; i++) {
      errors2costs[i] = c[i];
    }
    maxCost = errors2costs[maxErrors];
  }

  /**
   * accessor to @a maxErrors
   * @return the maximum number of errors allowed
   */
  int getMaxErrors () const {
    return maxErrors;
  }

  /**
   * accessor to @a maxCost
   * @return the maximum cost given
   */
  int getMaxCost () const {
    return maxCost;
  }

  /**
   * convert an number of errors to a cost
   * @param e a number of errors
   * @return a cost
   */
  int getCostFor (const int e) {
    if (e > maxErrors) {
      return errors2costs[maxErrors]+1;
    }
    return errors2costs[e];
  }

  /**
   * get the maximum number of errors such that its cost is not greater than a given cost
   * @param c a cost
   * @return a number of errors
   */
  int getErrorsNotGreaterThan (const int c) {
    int result = IMPOSSIBLE;
    for (int error = 0; error < maxErrors+1; error++) {
      if (errors2costs[error] > c) {
        return result;
      }
      result = errors2costs[error];
    }
    return maxErrors;
  }

  /**
   * get the maximum number of errors such that its cost is less than a given cost
   * @param c a cost
   * @return a number of errors
   */
  int getErrorsLessThan (const int c) {
    int result = IMPOSSIBLE;
    for (int error = 0; error < maxErrors+1; error++) {
      if (errors2costs[error] >= c) {
        return result;
      }
      result = errors2costs[error];
    }
    return maxErrors;
  }
};

#endif
