// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "hardDuplex.h"


HardDuplex::HardDuplex (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int i, int mc, Sequence *s, InteractionValues *iv) : HardConstraint (i, mc, 4, v1, v2, v3, v4) {
  simple = false;
  hardness = 0;
  interactionValues = iv;
  suffixArray = new SuffixArray(s, interactionValues);
  name = "hard duplex";
}

bool HardDuplex::isConsistent (int index, int val, Support *support) {
  int lb1 = unaryConstraints[0]->getLb();
  int lb2 = unaryConstraints[1]->getLb();
  int lb3 = unaryConstraints[2]->getLb();
  int ub3 = unaryConstraints[2]->getUb();
  int lb4 = unaryConstraints[3]->getLb();
  int ub4 = unaryConstraints[3]->getUb();
  if ((!unaryConstraints[0]->isAssigned()) || (!unaryConstraints[1]->isAssigned())) {
    return true;
  }
  switch (index) {
    case 2:
      lb3 = val;
      ub3 = val+1;
      break;
    case 3:
      lb4 = val;
      ub4 = val+1;
      break;
  }
  return (isConsistent(lb1, lb2, lb3, ub3, lb4, ub4, support));
}

bool HardDuplex::isConsistent (int lb1, int lb2, int lb3, int ub3, int lb4, int ub4, Support *support) {
  int min = maxCost;
  if ((!unaryConstraints[0]->isAssigned()) || (!unaryConstraints[1]->isAssigned())) {
    return true;
  }
  
  if (lb1 > lb2) {
    support->unset();
    return false;
  }
  
  support->setSupport(0, lb1);
  support->setSupport(2, lb1);
  Sequence s (*sequence, lb1, lb2-lb1+1);
  if (suffixArray->lookFor(&s, lb3, ub3, lb4, ub4, min, support)) {
    return true;
  }
  return false;
}

void HardDuplex::revise () {
  int lb1 = unaryConstraints[0]->getLb();
  int lb2 = unaryConstraints[1]->getLb();
  int lb3 = unaryConstraints[2]->getLb();
  int ub3 = unaryConstraints[2]->getUb();
  int lb4 = unaryConstraints[3]->getLb();
  int ub4 = unaryConstraints[3]->getUb();
  int values[8];
  if ((!unaryConstraints[0]->isAssigned()) || (!unaryConstraints[1]->isAssigned())) {
    return ;
  }

  if (lb1 > lb2) {
    unaryConstraints[0]->setLb(unaryConstraints[0]->getLb()+1);
  }

  Sequence s (*sequence, lb1, lb2-lb1+1);
  suffixArray->lookForAll(&s, lb3, ub3, lb4, ub4, maxCost, values);
  if (values[0] > lb3) {
    unaryConstraints[2]->setLb(values[0]);
  }
  if (values[2] < ub3) {
    unaryConstraints[2]->setUb(values[2]);
  }
  if (values[4] > lb4) {
    unaryConstraints[3]->setLb(values[4]);
  }
  if (values[6] < ub4) {
    unaryConstraints[3]->setUb(values[6]);
  }
  lbSupports[0]->setSupport(0, lb1);
  lbSupports[0]->setSupport(1, lb2);
  lbSupports[0]->setSupport(2, values[0]);
  lbSupports[0]->setSupport(3, values[1]);
  ubSupports[0]->copy(lbSupports[0]);
  lbSupports[1]->copy(lbSupports[0]);
  ubSupports[1]->copy(lbSupports[0]);
  lbSupports[2]->copy(lbSupports[0]);
  ubSupports[2]->setSupport(0, lb1);
  ubSupports[2]->setSupport(1, lb2);
  ubSupports[2]->setSupport(2, values[2]-1);
  ubSupports[2]->setSupport(3, values[3]);
  lbSupports[3]->setSupport(0, lb1);
  lbSupports[3]->setSupport(1, lb2);
  lbSupports[3]->setSupport(2, values[4]);
  lbSupports[3]->setSupport(3, values[5]);
  ubSupports[3]->setSupport(0, lb1);
  ubSupports[3]->setSupport(1, lb2);
  ubSupports[3]->setSupport(2, values[6]-1);
  ubSupports[3]->setSupport(3, values[7]);
}

void HardDuplex::revise (int v) {
  revise();
}

void HardDuplex::reviseFromLb (int v) {
  if (v < 2) {
    return;
  }
  if (checkLbSupport(v)) {
    return;
  }
  revise();
}

void HardDuplex::reviseFromUb (int v) {
  if (v < 2) {
    return;
  }
  if (checkUbSupport(v)) {
    return;
  }
  revise();
}

void HardDuplex::reviseFromAssignment (int v) {
  if (v < 2) {
    return;
  }
  if ((checkLbSupport(v)) || (checkUbSupport(v))) {
    return;
  }
  revise();
}

bool HardDuplex::getConsistency (Support *s) {
  int val1 = s->getSupport(0);
  int val2 = s->getSupport(1);
  int val3 = s->getSupport(2);
  int val4 = s->getSupport(3);
  Sequence seq (*sequence, val1, val2-val1+1);

  if (val1 > val2) {
    return false;
  }

  return (suffixArray->lookForApproximate(&seq, val3, val4+1, maxCost) < maxCost+1);
}

bool HardDuplex::printSpace (int var, int place) {
  return (((var == 0) && (place == 0)) || ((var == 1) && (place == 1)) || ((var == 2) && (place == 0)) || ((var == 3) && (place == 1)));
}
