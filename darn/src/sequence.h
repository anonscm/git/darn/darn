// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef SEQUENCE_H_INCLUDED
#define SEQUENCE_H_INCLUDED

#include "common.h"


class Nucleotide;

extern Nucleotide *nucleotideA;
extern Nucleotide *nucleotideC;
extern Nucleotide *nucleotideG;
extern Nucleotide *nucleotideT;
extern Nucleotide *nucleotideN;
extern Nucleotide *nucleotideX;

extern Nucleotide *nucleotideR;
extern Nucleotide *nucleotideY;
extern Nucleotide *nucleotideW;
extern Nucleotide *nucleotideS;
extern Nucleotide *nucleotideM;
extern Nucleotide *nucleotideK;
extern Nucleotide *nucleotideB;
extern Nucleotide *nucleotideD;
extern Nucleotide *nucleotideH;
extern Nucleotide *nucleotideV;

extern Nucleotide **nucleotides;



/**
 * The class Nucleotide provides some member functions to handle the nucleotides
 */
class Nucleotide {

protected:
  /**
   * the structure that defines all the possible nucleotides
   */
  enum Letter {A, C, G, T, N, X, R, Y, W, S, M, K, B, D, H, V};
  /**
   * the current nucleotide
   */
  Letter letter;
  /**
   * constructor with a given letter (protected because the structure Letter is protected)
   * @param l the letter
   */
  Nucleotide (Letter l) : letter(l) {}

public:
  /**
   * constructor with a character (which sould be one of the sixteen letters)
   * @param c the character
   */
  Nucleotide (char c = 'X') {
    setLetter(c);
  }

  /**
   * empty destructor
   */
  ~Nucleotide () {}

  /**
   * reset the content of this nucleotide
   * @param c the character
   */
  void setLetter (char c) {
    switch (c) {
      case 'A':
      case 'a':
        letter = A;
        break;
      case 'C':
      case 'c':
        letter = C;
        break;
      case 'G':
      case 'g':
        letter = G;
        break;
      case 'T':
      case 't':
        letter = T;
        break;
      case 'N':
      case 'n':
        letter = N;
        break;
      case 'X':
      case 'x':
        letter = X;
        break;
      case 'R':
      case 'r':
        letter = R;
        break;
      case 'Y':
      case 'y':
        letter = Y;
        break;
      case 'W':
      case 'w':
        letter = W;
        break;
      case 'S':
      case 's':
        letter = S;
        break;
      case 'M':
      case 'm':
        letter = M;
        break;
      case 'K':
      case 'k':
        letter = K;
        break;
      case 'B':
      case 'b':
        letter = B;
        break;
      case 'D':
      case 'd':
        letter = D;
        break;
      case 'H':
      case 'h':
        letter = H;
        break;
      case 'V':
      case 'v':
        letter = V;
        break;
      default:
        cerr << "Error: A nucleotide '%c' was found in genomic sequence which is not in UIPAC code !" <<endl;
        exit(1);
    }
  }

  /**
   * the copy constructor
   * @param n the nucleotide to be copied
   * @return a copied nucleotide
   */
  Nucleotide operator= (Nucleotide &n) {
    letter = n.letter;
    return *this;
  }

  /**
   * whether the current nucleotide is the same as the given one
   * @param n the given nucleotide
   * @return true if @a n is the same nucleotide
   */
  bool isEqual (Nucleotide *n) {
    return (letter == n->letter);
  }

  /**
   * print a trace of the nucleotide
   * @return a character
   */
  char print () {
    switch (letter) {
      case A:
        return 'A';
      case C:
        return 'C';
      case G:
        return 'G';
      case T:
        return 'T';
      case N:
        return 'N';
      case X:
        return 'X';
      case R:
        return 'R';
      case Y:
        return 'Y';
      case W:
        return 'W';
      case S:
        return 'S';
      case M:
        return 'M';
      case K:
        return 'K';
      case B:
        return 'B';
      case D:
        return 'D';
      case H:
        return 'H';
      case V:
        return 'V';
      default:
        return 'N';
    }
  }

  /**
   * get the corresponding (const) nucleotide
   * @param c a character
   * @return a nucleotide
   */
  static Nucleotide *getNucleotide (char c) {
    switch (c) {
      case 'A':
      case 'a':
        return nucleotideA;
      case 'C':
      case 'c':
        return nucleotideC;
      case 'G':
      case 'g':
        return nucleotideG;
      case 'T':
      case 't':
      case 'U':
      case 'u':
        return nucleotideT;
      case 'N':
      case 'n':
        return nucleotideN;
      case 'X':
      case 'x':
        return nucleotideX;
      case 'R':
      case 'r':
        return nucleotideR;
      case 'Y':
      case 'y':
        return nucleotideY;
      case 'W':
      case 'w':
        return nucleotideW;
      case 'S':
      case 's':
        return nucleotideS;
      case 'M':
      case 'm':
        return nucleotideM;
      case 'K':
      case 'k':
        return nucleotideK;
      case 'B':
      case 'b':
        return nucleotideB;
      case 'D':
      case 'd':
        return nucleotideD;
      case 'H':
      case 'h':
        return nucleotideH;
      case 'V':
      case 'v':
        return nucleotideV;
      default:
        cerr << "Error: Trying to have the correspondance of a nucleotide which is not in UIPAC code !" <<endl;
        exit(1);
    }
  }

  /**
   * get the complement nucleotide
   * @return the complement nucleotide
   */
  Nucleotide *getComplement () {
    switch (letter) {
      case A:
        return nucleotideT;
      case C:
        return nucleotideG;
      case G:
        return nucleotideC;
      case T:
        return nucleotideA;
      case N:
        return nucleotideN;
      case X:
        return nucleotideX;
      case R:
        return nucleotideY;
      case Y:
        return nucleotideR;
      case W:
        return nucleotideW;
      case S:
        return nucleotideS;
      case M:
        return nucleotideK;
      case K:
        return nucleotideM;
      case B:
        return nucleotideV;
      case D:
        return nucleotideH;
      case H:
        return nucleotideD;
      case V:
        return nucleotideB;
      default:
        cerr << "Error: Trying to get the complement of a nucleotide which is not in UIPAC code !" <<endl;
        exit(1);
    }
  }

  /**
   * get a small integer for a nucleotide
   * @return a code
   */
  char getCode () {
    switch (letter) {
      case A:
        return 0;
      case C:
        return 1;
      case G:
        return 2;
      case T:
        return 3;
      case N:
        return 4;
      case X:
        return 5;
      case R:
        return 6;
      case Y:
        return 7;
      case W:
        return 8;
      case S:
        return 9;
      case M:
        return 10;
      case K:
        return 11;
      case B:
        return 12;
      case D:
        return 13;
      case H:
        return 14;
      case V:
        return 15;
      default:
        cerr << "Error: Trying to get the code of a nucleotide which is not in UIPAC code !" <<endl;
        exit(1);
    }
  }

  /**
   * whether the current nucleotide is ambiguous
   * @return true, if we have an ambiguous nucleotide
   */
  bool isAmbiguous () {
    return ((int) letter >= (int) N);
  }
};


/**
 * The class Sequence stores the long DNA sequence and provides some member function to interact with it.
 */
class Sequence {

protected:
  /**
   * the sequence length
   */
  unsigned int length;
  /**
   * the nucleotide vector corresponding to the sequence
   */
  char *letters;
  /**
   * the nucleotide vector corresponding to the reverse sequence
   */
  char *reverseLetters;
  /**
   * the name of the sequence
   */
  string name;

  /**
   * Alternative constructor
   * copy part of the nucleotides array to create a subsequence
   * @param n the nucleotides array
   * @param l the length of the new subsequence
   */
  Sequence (char *n, unsigned int l) : length(l) {
    letters = n;
  }

  /**
   * Actually build the sequence
   * Set the nucleotide vector with the nucleotides corresponding to the sequence.
   * If the given sequence is a main sequence, the 'N' nucleotides are replaced by 'X's.
   * @param s the string used to build the (nucleotides) sequence
   * @param l the length of the string
   * @param sequenceName the name of the sequence
   * @param mainSequence whether the given sequence is a main sequence
   * @param both whether both strands should be scanned
   * @param reverse whether the sequence is the reverse of the given string
   * @param complement whether the sequence is the complement of the given string
   */
  void setSequence (const char *s, unsigned int l, const char *sequenceName, bool mainSequence, bool both = false, bool reverse = false, bool complement = false) {
    name = string(sequenceName);
    length = l;
    letters = new char[length];
    if (both) {
      reverseLetters = new char[length];
    }
    else {
      reverseLetters = NULL;
    }
    if (mainSequence) {
      if (reverse) {
        if (complement) {
          // main sequence, reverse, complement
          for (unsigned int i = 0; i < length; i++) {
            letters[i] = Nucleotide::getNucleotide(s[length-i-1])->getComplement()->getCode();
            if (letters[i] == nucleotideN->getCode()) {
              letters[i] = nucleotideX->getCode();
            }
            else if (letters[i] >= (int) NBNUCLEOTIDES) {
              letters[i] = nucleotideN->getCode();
            }
          }
        }
        else {
          // main sequence, reverse, not complement
          for (unsigned int i = 0; i < length; i++) {
            letters[i] = Nucleotide::getNucleotide(s[length-i-1])->getCode();
            if (letters[i] == nucleotideN->getCode()) {
              letters[i] = nucleotideX->getCode();
            }
            else if (letters[i] >= (int) NBNUCLEOTIDES) {
              letters[i] = nucleotideN->getCode();
            }
          }
        }
      }
      else {
        if (complement) {
          // main sequence, not reverse, complement
          for (unsigned int i = 0; i < length; i++) {
            letters[i] = Nucleotide::getNucleotide(s[i])->getComplement()->getCode();
            if (letters[i] == nucleotideN->getCode()) {
              letters[i] = nucleotideX->getCode();
            }
            else if (letters[i] >= (int) NBNUCLEOTIDES) {
              letters[i] = nucleotideN->getCode();
            }
          }
        }
        else {
          // main sequence, not reverse, not complement and other strand
          if (both) {
            for (unsigned int i = 0; i < length; i++) {
              letters[i] = Nucleotide::getNucleotide(s[i])->getCode();
              reverseLetters[length-1 - i] = Nucleotide::getNucleotide(s[i])->getComplement()->getCode();
              if (letters[i] == nucleotideN->getCode()) {
                letters[i] = nucleotideX->getCode();
                reverseLetters[length-1 - i] = nucleotideX->getCode();
              }
              else if (letters[i] >= (int) NBNUCLEOTIDES) {
                letters[i] = nucleotideN->getCode();
                reverseLetters[length-1 - i] = nucleotideN->getCode();
              }
            }

          }
          else {
            // main sequence, not reverse, not complement, one strand
            for (unsigned int i = 0; i < length; i++) {
              letters[i] = Nucleotide::getNucleotide(s[i])->getCode();
              if (letters[i] == nucleotideN->getCode()) {
                letters[i] = nucleotideX->getCode();
              }
              else if (letters[i] >= (int) NBNUCLEOTIDES) {
                letters[i] = nucleotideN->getCode();
              }
            }
          }
        }
      }
    }
    else {
      if (reverse) {
        if (complement) {
          // not main sequence, reverse, complement
          for (unsigned int i = 0; i < length; i++) {
            letters[i] = Nucleotide::getNucleotide(s[length-i-1])->getComplement()->getCode();
          }
        }
        else {
          // not main sequence, reverse, not complement
          for (unsigned int i = 0; i < length; i++) {
            letters[i] = Nucleotide::getNucleotide(s[length-i-1])->getCode();
          }
        }
      }
      else {
        if (complement) {
          // not main sequence, not reverse, complement
          for (unsigned int i = 0; i < length; i++) {
            letters[i] = Nucleotide::getNucleotide(s[i])->getComplement()->getCode();
          }
        }
        else {
          // not main sequence, not reverse, not complement
          for (unsigned int i = 0; i < length; i++) {
            letters[i] = Nucleotide::getNucleotide(s[i])->getCode();
          }
        }
      }
    }
  }

public:
  /**
   * Constructor
   * Set the nucleotide vector with the nucleotides corresponding to the sequence.
   * If the given sequence is a main sequence, the 'N' nucleotides are replaced by 'X's.
   * @param s the string used to build the (nucleotides) sequence
   * @param mainSequence whether the given sequence is a main sequence
   * @param both whether both strands should be scanned
   * @param reverse whether the sequence is the reverse of the given string
   * @param complement whether the sequence is the complement of the given string
   */
  Sequence (const string s, bool mainSequence, bool both = false, bool reverse = false, bool complement = false) {
    setSequence(s.c_str(), s.length(), "", mainSequence, both, reverse, complement);
  }

  /**
   * Constructor
   * Set the nucleotide vector with the nucleotides corresponding to the sequence.
   * If the given sequence is a main sequence, the 'N' nucleotides are replaced by 'X's.
   * @param s the string used to build the (nucleotides) sequence
   * @param l the length of the string
   * @param sequenceName the name of the sequence
   * @param mainSequence whether the given sequence is a main sequence
   * @param both whether both strands should be scanned
   * @param reverse whether the sequence is the reverse of the given string
   * @param complement whether the sequence is the complement of the given string
   */
  Sequence (const char *s, unsigned int l, char *sequenceName, bool mainSequence, bool both = false, bool reverse = false, bool complement = false) {
    setSequence(s, l, sequenceName, mainSequence, both, reverse, complement);
  }

  /**
   * Constructor
   * Copy part of the given sequence
   * @warning Does not really copy the sequence, just gets the pointer !
   * @param s the sequence to be copied
   * @param lb the start position to be copied
   * @param ub the number of nucleotides to be copied
   */
  Sequence (Sequence &s, int lb, int ub) {
    letters = s.letters + lb;
    length = ub;
    name = s.getName();
  }

  /**
   * Constructor
   * Copy a suffix of the given sequence
   * @warning Does not really copy the sequence, just gets the pointer !
   * @param s the sequence to be copied
   * @param lb the start position to be copied
   */
  Sequence (Sequence &s, int lb) {
    letters = s.letters + lb;
    length = s.length - lb;
    name = s.getName();
  }

  /**
   * destructor
   */
  ~Sequence () {
    /*
    delete[] letters;
    if (reverseLetters != NULL) {
      delete[] reverseLetters;
    }
    */
  }

  /**
   * reset the sequence for a new search
   * @param s the new sequence
   * @param l the length of the new sequence
   * @param sequenceName the name of the sequence
   * @param both whether both strands should be scanned
   * @param reverse whether the strand should be read from left to right
   * @param complement whether bases should be replaced by their complementary one
   */
  void reset (const char *s, unsigned int l, char *sequenceName, bool both = false, bool reverse = false, bool complement = false) {
    delete[] letters;
    setSequence(s, l, sequenceName, true, both, reverse, complement);
  }

  /**
   * get the name of the sequence
   * @return the name
   */
  string getName () {
    return name;
  }

  /**
   * get the size of the sequence
   * @return the length of the sequence
   */
  unsigned int getLength () {
    return length;
  }

  /**
   * get a subsequence
   * @warning should be freed by delete
   * @param lb the beginning of the subsequence
   * @param ub the end of the subsequence
   * @return a subsequence as an array of nucleotides
   */
  Sequence *getSubSequence (int lb, int ub) {
    return (new Sequence(letters + lb, ub));
  }

  /**
   * get a suffix
   * @warning should be freed by delete
   * @param lb the end of the subsequence
   * @return a subsequence as an array of nucleotides
   */
  Sequence *getSubSequence (int lb) {
    return (new Sequence(letters + lb, length-lb));
  }

  /**
   * get the reverse of this sequence
   * @warning should be freed by delete
   * @return a sequence that contains the same nucleotides, in reverse order
   */
  Sequence *getReverse () {
    char *rev = new char[length];
    Sequence *s;
    for (unsigned int i = 0; i < getLength(); i++) {
      rev[i] = getNucleotide(getLength()-1-i)->getCode();
    }
    s = new Sequence(rev, length);
    return s;
  }

  /**
   * switch to the reverse complement sequence
   * (can be done only once)
   */
  void reverse () {
    if (reverseLetters == NULL) {
      trace(1, "Error! Trying to reverse a sequence!");
    }
    letters = reverseLetters;
  }

  /**
   * give the nucleotide at the given position
   * @param i a position
   * @return the nucleotide at the given position
   */
  Nucleotide *getNucleotide (unsigned int i) {
    if ((i < 0) || (i >= length)) {
      trace(1, "Error! Trying to access a nucleotide which is out of the bounds of the sequence!");
    }
    return nucleotides[(int) letters[i]];
  }

  /**
   * whether the nucleotides at the given positions are the same
   * @param i the first position
   * @param j the second position
   * @return true if the nucleotides are the same
   */
  bool isEqual (int i, int j) {
    return getNucleotide(i)->isEqual(getNucleotide(j));
  }

  /**
   * print the current sequence
   * @return the string
   */
  string print () {
    string s;
    for (unsigned int i = 0; i < length; i++) {
      s += getNucleotide(i)->print();
    }
    return s;
  }
};

#endif
