// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "distance.h"


Distance::Distance (Variable *v1, Variable *v2, int i, bool o, int d1, int d2, int d3, int d4, int c1, ErrorsToCosts *e2c) : SoftConstraint(i, e2c, o, 2, v1, v2), dist1(d1), dist2(d2), dist3(d3), dist4(d4), minCost(c1) {
  name = "distance";
}


int Distance::computeCost (int i1, int i2) {
  if (i2 < i1 + dist1) {
    return getMaxCost();
  }
  if (i2 < i1 + dist2) {
    return static_cast<int>((static_cast<float>(minCost-getMaxCost())/(dist2-dist1))*i2 + (static_cast<float>(getMaxCost()*(i1+dist2)-minCost*(i1+dist1)))/(dist2-dist1));
  }
  if (i2 <= i1 + dist3) {
    return minCost;
  }
  if (i2 < i1 + dist4) {
    return static_cast<int>((static_cast<float>(getMaxCost()-minCost)/(dist4-dist3))*i2 + (static_cast<float>(minCost*(i1+dist4)-getMaxCost()*(i1+dist3)))/(dist4-dist3));
  }
  return getMaxCost();
}

int Distance::computeCostUndiscretized (int min1, int max1, int min2, int max2) {
  if (max2 < min1 + dist1) {
    return getMaxCost();
  }
  if (max2 < min1 + dist2) {
    return static_cast<int>((static_cast<float>(minCost-getMaxCost())/(dist2-dist1))*max2 + (static_cast<float>(getMaxCost()*(min1+dist2)-minCost*(min1+dist1)))/(dist2-dist1));
  }
  if (min2 <= max1 + dist3) {
    return minCost;
  }
  if (min2 < max1 + dist4) {
    return static_cast<int>((static_cast<float>(getMaxCost()-minCost)/(dist4-dist3))*min2 + (static_cast<float>(minCost*(max1+dist4)-getMaxCost()*(max1+dist3)))/(dist4-dist3));
  }
  return getMaxCost();
}

int Distance::computeCost (int lb1, int ub1, int lb2, int ub2) {
  DomainIterator di1 = unaryConstraints[0]->getDomainIterator();
  DomainIterator di2 = unaryConstraints[1]->getDomainIterator();
  int min = getMaxCost()+1, mem = getMem(), mem1, mem2;
  int value;
  currentSupport->unset();

//  di1.setTo(lb1);
  while (*di1 < lb1) {
    ++di1;
  }
  for (; *di1 < ub1; ++di1) {
    mem1 = getMemory(variables[0], *di1);
    di2 = unaryConstraints[1]->getDomainIterator();
    //di2.setTo(lb2);
    while (*di2 < lb2) {
      ++di2;
    }
    for (; *di2 < ub2; ++di2) {
      mem2 = getMemory(variables[1], *di2);
      value = computeCost(*di1, *di2) - mem1 - mem2 - mem;
      if (value < min) {
        currentSupport->setSupport(0, *di1);
        currentSupport->setSupport(1, *di2);
        min = value;
      }
    }
  }
  return min;
}

int Distance::getMinCost (Variable *v, int val) {
  int ind = getVariableIndex(v);
  int value, valueLb, valueUb;
  int min = getMaxCost()+1, mem1, mem2, mem = getMem();
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  currentSupport->unset();
  if (ind == 0) {
    mem1 = cost->getMemProfile(0, val);
    if (!unaryConstraints[1]->isDiscretized()) {
      value = computeCostUndiscretized(val, val, lb2+1, ub2-2);
      valueLb = computeCost(val, lb2) - cost->getLbMem(1);
      valueUb = computeCost(val, ub2-1) - cost->getUbMem(1);
      return min3(valueLb, value, valueUb) - (mem1 + mem);
    }
    else {
      DomainIterator di2 = unaryConstraints[1]->getDomainIterator();
      for (; *di2 < ub2; ++di2) {
        value = computeCost(val, *di2) - (mem1 + cost->getMemProfile(1, *di2) + mem);
        if (value < min) {
          min = value;
          currentSupport->setSupport(0, val);
          currentSupport->setSupport(1, *di2);
          if (min == 0) {
            return 0;
          }
        }
      }
      return min;
    }
  }
  else {
    mem2 = cost->getMemProfile(1, val);
    if (!unaryConstraints[0]->isDiscretized()) {
      value = computeCostUndiscretized(lb1+1, ub1-2, val, val);
      valueLb = computeCost(lb1, val) - cost->getLbMem(0);
      valueUb = computeCost(ub1-1, val) - cost->getUbMem(0);
      return min3(valueLb, value, valueUb) - (mem2 + mem);
    }
    else {
      DomainIterator di1 = unaryConstraints[0]->getDomainIterator();
      for (; *di1 < ub1; ++di1) {
        value = computeCost(*di1, val) - (cost->getMemProfile(0, *di1) + mem2 + mem);
        if (value < min) {
          min = value;
          currentSupport->setSupport(0, *di1);
          currentSupport->setSupport(1, val);
          if (min == 0) {
            return 0;
          }
        }
      }
      return min;
    }
  }
}

int Distance::getMinLbCost (Variable *v) {
  int ind = getVariableIndex(v);
  int value, valueLb, valueUb;
  int min = getMaxCost()+1, mem1, mem2, mem = getMem();
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  currentSupport->unset();
  if (ind == 0) {
    if (unaryConstraints[0]->isDiscretized()) {
      mem1 = cost->getMemProfile(0, lb1);
    }
    else {
      mem1 = cost->getLbMem(0);
    }
    if (!unaryConstraints[1]->isDiscretized()) {
      value = computeCostUndiscretized(lb1, lb1, lb2+1, ub2-2);
      valueLb = computeCost(lb1, lb2) - cost->getLbMem(1);
      valueUb = computeCost(lb1, ub2-1) - cost->getUbMem(1);
      return min3(valueLb, value, valueUb) - (mem1 + mem);
    }
    else {
      DomainIterator di2 = unaryConstraints[1]->getDomainIterator();
      for (; *di2 < ub2; ++di2) {
        value = computeCost(lb1, *di2) - (mem1 + cost->getMemProfile(1, *di2) + mem);
        if (value < min) {
          min = value;
          currentSupport->setSupport(0, lb1);
          currentSupport->setSupport(1, *di2);
          if (min == 0) {
            return 0;
          }
        }
      }
      return min;
    }
  }
  else {
    if (unaryConstraints[1]->isDiscretized()) {
      mem2 = cost->getMemProfile(1, lb2);
    }
    else {
      mem2 = cost->getLbMem(1);
    }
    if (!unaryConstraints[0]->isDiscretized()) {
      value = computeCostUndiscretized(lb1+1, ub1-2, lb2, lb2);
      valueLb = computeCost(lb1, lb2) - cost->getLbMem(0);
      valueUb = computeCost(ub1-1, lb2) - cost->getUbMem(0);
      return min3(valueLb, value, valueUb) - (mem2 + mem);
    }
    else {
      DomainIterator di1 = unaryConstraints[0]->getDomainIterator();
      for (; *di1 < ub1; ++di1) {
        value = computeCost(*di1, lb2) - (cost->getMemProfile(0, *di1) + mem2 + mem);
        if (value < min) {
          min = value;
          currentSupport->setSupport(0, *di1);
          currentSupport->setSupport(1, lb2);
          if (min == 0) {
            return 0;
          }
        }
      }
      return min;
    }
  }
}

int Distance::getMinUbCost (Variable *v) {
  int ind = getVariableIndex(v);
  int value, valueLb, valueUb;
  int min = getMaxCost()+1, mem1, mem2, mem = getMem();
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  currentSupport->unset();
  if (ind == 0) {
    if (unaryConstraints[0]->isDiscretized()) {
      mem1 = cost->getMemProfile(0, ub1-1);
    }
    else {
      mem1 = cost->getUbMem(0);
    }
    if (!unaryConstraints[1]->isDiscretized()) {
      value = computeCostUndiscretized(ub1-1, ub1-1, lb2+1, ub2-2);
      valueLb = computeCost(ub1-1, lb2) - cost->getLbMem(1);
      valueUb = computeCost(ub1-1, ub2-1) - cost->getUbMem(1);
      return min3(valueLb, value, valueUb) - (mem1 + mem);
    }
    else {
      DomainIterator di2 = unaryConstraints[1]->getDomainIterator();
      for (; *di2 < ub2; ++di2) {
        value = computeCost(ub1-1, *di2) - (mem1 + cost->getMemProfile(1, *di2) + mem);
        if (value < min) {
          min = value;
          currentSupport->setSupport(0, ub1-1);
          currentSupport->setSupport(1, *di2);
          if (min == 0) {
            return 0;
          }
        }
      }
      return min;
    }
  }
  else {
    if (unaryConstraints[1]->isDiscretized()) {
      mem2 = cost->getMemProfile(1, ub2-1);
    }
    else {
      mem2 = cost->getUbMem(1);
    }
    if (!unaryConstraints[0]->isDiscretized()) {
      value = computeCostUndiscretized(lb1+1, ub1-2, ub2-1, ub2-1);
      valueLb = computeCost(lb1, ub2-1) - cost->getLbMem(0);
      valueUb = computeCost(ub1-1, ub2-1) - cost->getUbMem(0);
      return min3(valueLb, value, valueUb) - (mem2 + mem);
    }
    else {
      DomainIterator di1 = unaryConstraints[0]->getDomainIterator();
      for (; *di1 < ub1; ++di1) {
        value = computeCost(*di1, ub2-1) - (cost->getMemProfile(0, *di1) + mem2 + mem);
        if (value < min) {
          min = value;
          currentSupport->setSupport(0, *di1);
          currentSupport->setSupport(1, ub2-1);
          if (min == 0) {
            return 0;
          }
        }
      }
      return min;
    }
  }
}

int Distance::getMinCost () {
  int val, min;
  int value, valueLb1, valueUb1, valueLb2, valueUb2, valueLbLb, valueLbUb, valueUbLb, valueUbUb;
  int lbMem1, ubMem1, lbMem2, ubMem2, currentMem, mem;
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  currentSupport->unset();

  if ((!unaryConstraints[0]->isDiscretized()) && (!unaryConstraints[1]->isDiscretized())) {
    if ((ub1 <= lb1) || (ub2 <= lb2)) {
      return getMaxCost()+1;
    }
    lbMem1 = cost->getLbMem(0);
    ubMem1 = cost->getUbMem(0);
    lbMem2 = cost->getLbMem(1);
    ubMem2 = cost->getUbMem(1);
    mem = getMem();
    value = getMaxCost()+1;

    value = computeCostUndiscretized(lb1+1, ub1-2, lb2+1, ub2-2) - mem;

    valueLb1 = computeCostUndiscretized(lb1, lb1, lb2+1, ub2-2) - lbMem1 - mem;
    value = min2(valueLb1, value);
    valueUb1 = computeCostUndiscretized(ub1-1, ub1-1, lb2+1, ub2-2) - ubMem1 - mem;
    value = min2(valueUb1, value);
    valueLb2 = computeCostUndiscretized(lb1+1, ub1-2, lb2, lb2) - lbMem2 - mem;
    value = min2(valueLb2, value);
    valueUb2 = computeCostUndiscretized(lb1+1, ub1-2, ub2-1, ub2-1) - ubMem2 - mem;
    value = min2(valueUb2, value);

    valueLbLb = computeCost(lb1, lb2) - lbMem1 - lbMem2 - mem;
    if (valueLbLb < value) {
      value = valueLbLb;
    }

    valueLbUb = computeCost(lb1, ub2-1) - lbMem1 - ubMem2 - mem;
    if (valueLbUb < value) {
      value = valueLbUb;
    }

    valueUbLb = computeCost(ub1-1, lb2) - ubMem1 - lbMem2 - mem;
    if (valueUbLb < value) {
      value = valueUbLb;
    }

    valueUbUb = computeCost(ub1-1, ub2-1) - ubMem1 - ubMem2 - mem;
    if (valueUbUb < value) {
      value = valueUbUb;
    }
    return value;
  }
  if ((unaryConstraints[0]->isDiscretized()) && (!unaryConstraints[1]->isDiscretized())) {
    lbMem2 = cost->getLbMem(1);
    ubMem2 = cost->getUbMem(1);
    mem = getMem();
    min = getMaxCost()+1;
    DomainIterator di1 = unaryConstraints[0]->getDomainIterator();
    for (; *di1 < ub1; ++di1) {
      val = *di1;
      currentMem = cost->getMemProfile(0, val);
      valueLb2 = computeCost(val, lb2) - (currentMem + lbMem2 + mem);
      valueUb2 = computeCost(val, ub2-1) - (currentMem + ubMem2 + mem);
      value = computeCostUndiscretized(val, val, lb2+1, ub2-2) - (currentMem + mem);
      min = min4(min, valueLb2, value, valueUb2);
      if (min == 0) {
        return 0;
      }
    }
    return min;
  }
  if ((!unaryConstraints[0]->isDiscretized()) && (unaryConstraints[1]->isDiscretized())) {
    lbMem1 = cost->getLbMem(0);
    ubMem1 = cost->getUbMem(0);
    mem = getMem();
    min = getMaxCost()+1;
    DomainIterator di2 = unaryConstraints[1]->getDomainIterator();
    for (; *di2 < ub2; ++di2) {
      val = *di2;
      currentMem = cost->getMemProfile(1, val);
      valueLb1 = computeCost(lb1, val) - (lbMem1 + currentMem + mem);
      valueUb1 = computeCost(ub1-1, val) - (ubMem1 + currentMem + mem);
      value = computeCostUndiscretized(lb1+1, ub1-2, val, val) - (currentMem + mem);
      min = min4(min, valueLb1, value, valueUb1);
      if (min == 0) {
        return 0;
      }
    }
    return min;
  }
  return computeCost(unaryConstraints[0]->getLb(), unaryConstraints[0]->getUb(), unaryConstraints[1]->getLb(), unaryConstraints[1]->getUb());
}

/*
void Distance::reviseFromLb (Variable *v) {
  int ind = getVariableIndex(v);
  int lb1 = unaryConstraints[0]->getLb();
  int lb2 = unaryConstraints[1]->getLb();
  if (ind == 0) {
    if (lb2 < lb1+dist1) {
      unaryConstraints[1]->setLb(lb1+dist1);
    }
  }
  else {
    if (lb1 < lb2-dist4) {
      unaryConstraints[0]->setLb(lb2-dist4);
    }
  }
  SoftConstraint::reviseFromLb(v);
}

void Distance::reviseFromUb (Variable *v) {
  int ind = getVariableIndex(v);
  int ub1 = unaryConstraints[0]->getUb();
  int ub2 = unaryConstraints[1]->getUb();
  if (ind == 0) {
    if (ub2 > ub1+dist4+1) {
      unaryConstraints[1]->setUb(ub1+dist4+1);
    }
  }
  else {
    if (ub1 > ub2-dist1+1) {
      unaryConstraints[0]->setUb(ub2-dist1+1);
    }
  }
  SoftConstraint::reviseFromUb(v);
}

void Distance::reviseFromAssignment (Variable *v) {
  int ind = getVariableIndex(v);
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  if (ind == 0) {
    if (lb2 < lb1+dist1) {
      unaryConstraints[1]->setLb(lb1+dist1);
    }
    if (ub2 > lb1+dist4+1) {
      unaryConstraints[1]->setUb(lb1+dist4+1);
    }
  }
  else {
    if (lb1 < lb2-dist4) {
      unaryConstraints[0]->setLb(lb2-dist4);
    }
    if (ub1 > lb2-dist1+1) {
      unaryConstraints[0]->setUb(lb2-dist1+1);
    }
  }
  SoftConstraint::reviseFromAssignment(v);
}
*/

int Distance::getCost (Support *s) {
  int val1 = s->getSupport(0), val2 = s->getSupport(1);
  int result = computeCost(val1, val2) - getMemory(variables[0], val1) - getMemory(variables[1], val2) - getMem();
  return result;
}

int Distance::getFinalCost (int *values) {
  int val1 = values[0];
  int val2 = values[1];
  return min2(getMaxCost()+1, computeCostUndiscretized(val1, val1, val2, val2));
}
