// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "defaultUnarySoftConstraint.h"


DefaultUnarySoftConstraint::DefaultUnarySoftConstraint (Variable *v, int i, ErrorsToCosts *e2c) : AbstractSoftConstraint(i, e2c, 1) {
  variables = new Variable*[1];
  variables[0] = v;
  variable = variables[0];
  domain = variable->getDomain();
  variable->setUnaryConstraint(this);
  cost = new UnaryCost(index);
  currentSupport = new Support(1);
  if (getUb() - getLb() < static_cast<int>(MAXSIZE)) {
    setDiscretized();
  }
}

DefaultUnarySoftConstraint::~DefaultUnarySoftConstraint () {
  /*
  delete variable;
  delete currentSupport;
  delete cost;
  */
}

void DefaultUnarySoftConstraint::reset () {
  cost->reset();
  currentSupport->reset();
  if (getUb() - getLb() < static_cast<int>(MAXSIZE)) {
    setDiscretized();
  }
  AbstractSoftConstraint::reset();
}

Variable *DefaultUnarySoftConstraint::getVariable () {
  return variable;
}

Domain *DefaultUnarySoftConstraint::getDomain () {
  return domain;
}

UnaryCost *DefaultUnarySoftConstraint::getUnaryCost () {
  return (UnaryCost *) cost;
}

bool DefaultUnarySoftConstraint::isDiscretized () {
  return domain->isDiscretized();
}

void DefaultUnarySoftConstraint::setDiscretized () {
  domain->setDiscretized();
  ((UnaryCost *) cost)->setDiscretized(domain->getLb(), domain->getUb());
  for (int i = 0; i < variable->getNbSoftConstraints(); i++) {
    variable->getSoftConstraint(i)->setDiscretized(variable);
  }
}

void DefaultUnarySoftConstraint::assign (int val) {
  int c = 0;
  if (!isDiscretized()) {
    if (val == getLb()) {
      c = getMem() + getLbMem();
    }
    else if (val == getUb()-1) {
      c = getMem() + getUbMem();
    }
    else {
      c = getMem();
    }
  }
  else {
    c = getMem() + getMemProfile(val);
  }
  ((UnaryCost *) cost)->assign(val, c);
  for (int i = 0; i < variable->getNbSoftConstraints(); i++) {
    variable->getSoftConstraint(i)->assign(variable, val);
  }
  // remove bound at the end, because NaryConstraint::assign needs Domain::getLb and Domain::getUb
  domain->assign(val);
}

bool DefaultUnarySoftConstraint::isAssigned () {
  return domain->isAssigned();
}

int DefaultUnarySoftConstraint::getLb () {
  return domain->getLb();
}

int DefaultUnarySoftConstraint::getUb () {
  return domain->getUb();
}

void DefaultUnarySoftConstraint::delValue (int i) {
  if (!isDiscretized()) {
    trace(1, "Error: trying to delete a non-existing value!");
    return;
  }
  domain->delValue(i);
}

void DefaultUnarySoftConstraint::setLb (int l) {
  char mes[255];
  if (l < getLb()) {
    sprintf(mes, "Warning: trying to decrease the lower bound of a domain (%s: %i -> %i)!", getVariable()->print().c_str(), getLb(), l);
    trace(1, mes);
    return;
  }
  if (l == getLb()) {
    return;
  }
  domain->setLb(l);
  for (int i = 0; i < variable->getNbSoftConstraints(); i++) {
    variable->getSoftConstraint(i)->resetLbMem(variable);
  }
  resetLbMem();
}

void DefaultUnarySoftConstraint::setUb (int u) {
  char mes[255];
  if (u > getUb()) {
    sprintf(mes, "Warning: trying to increase the upper bound of a domain (%s: %i -> %i)!", getVariable()->print().c_str(), getUb(), u);
    trace(1, mes);
    return;
  }
  if (u == getUb()) {
    return;
  }
  domain->setUb(u);
  for (int i = 0; i < variable->getNbSoftConstraints(); i++) {
    variable->getSoftConstraint(i)->resetUbMem(variable);
  }
  resetUbMem();
}

void DefaultUnarySoftConstraint::incLb (int l) {
  setLb(getLb() + l);
}

void DefaultUnarySoftConstraint::decUb (int u) {
  setUb(getUb() - u);
}

bool DefaultUnarySoftConstraint::isEmpty () {
  return domain->isEmpty();
}

bool DefaultUnarySoftConstraint::contains (int v) {
  return domain->contains(v);
}

DomainIterator DefaultUnarySoftConstraint::getDomainIterator () {
  return domain->getDomainIterator();
}

DomainIterator DefaultUnarySoftConstraint::getReverseDomainIterator () {
  return domain->getReverseDomainIterator();
}

int DefaultUnarySoftConstraint::getLbMem () {
  return ((UnaryCost *) cost)->getLbMem();
}

int DefaultUnarySoftConstraint::getUbMem () {
  return ((UnaryCost *) cost)->getUbMem();
}

void DefaultUnarySoftConstraint::setLbMem (int i) {
  ((UnaryCost *) cost)->setLbMem(i);
  if (getLb()+1 == getUb()) {
    ((UnaryCost *) cost)->setUbMem(i);
  }
}

void DefaultUnarySoftConstraint::setUbMem (int i) {
  ((UnaryCost *) cost)->setUbMem(i);
  if (getLb()+1 == getUb()) {
    ((UnaryCost *) cost)->setLbMem(i);
  }
}

void DefaultUnarySoftConstraint::incLbMem (int i) {
  if (i < 0) {
    trace(1, "Error! Trying to increase a lb memory with a negative cost!");
  }
  setLbMem(getLbMem() + i);
}

void DefaultUnarySoftConstraint::incUbMem (int i) {
  if (i < 0) {
    trace(1, "Error! Trying to increase a ub memory with a negative cost!");
  }
  setUbMem(getUbMem() + i);
}

void DefaultUnarySoftConstraint::decLbMem (int i) {
  if (i < 0) {
    trace(1, "Error! Trying to decrease a lb memory with a negative cost!");
  }
  setLbMem(diffCost(getLbMem(), i));
}

void DefaultUnarySoftConstraint::decUbMem (int i) {
  if (i < 0) {
    trace(1, "Error! Trying to decrease a ub memory with a negative cost!");
  }
  setUbMem(diffCost(getUbMem(), i));
}

void DefaultUnarySoftConstraint::resetLbMem () {
  if (isDiscretized()) {
    return;
  }
  if (getLb() == getUb()-1) {
    ((UnaryCost*) cost)->setLbMem(getMem() + getUbMem());
  }
  else {
    ((UnaryCost*) cost)->setLbMem(getMem());
  }
}

void DefaultUnarySoftConstraint::resetUbMem () {
  if (isDiscretized()) {
    return;
  }
  if (getLb() == getUb()-1) {
    ((UnaryCost*) cost)->setUbMem(getMem() + getLbMem());
  }
  else {
    ((UnaryCost*) cost)->setUbMem(getMem());
  }
}

int DefaultUnarySoftConstraint::getMemProfile (int i) {
  return ((UnaryCost *) cost)->getMemProfile(i);
}

void DefaultUnarySoftConstraint::setMemProfile (int i, int v) {
  ((UnaryCost *) cost)->setMemProfile(i, v);
}

void DefaultUnarySoftConstraint::incMemProfile (int i, int v) {
  if (v < 0) {
    trace(1, "Error! Trying to increase a memory profile with a negative cost!");
  }
  setMemProfile(i, getMemProfile(i) + v);
}

void DefaultUnarySoftConstraint::decMemProfile (int i, int v) {
  if (v < 0) {
    trace(1, "Error! Trying to decrease a memory profile with a negative cost!");
  }
  setMemProfile(i, diffCost(getMemProfile(i), v));
}

int DefaultUnarySoftConstraint::getMem () {
  return ((UnaryCost *) cost)->getMem();
}

void DefaultUnarySoftConstraint::setMem (int m) {
  ((UnaryCost *) cost)->setMem(m);
}

void DefaultUnarySoftConstraint::incMem (int m) {
  if (m < 0) {
    trace(1, "Error! Trying to increase a memory with a negative cost!");
  }
  setMem(getMem() + m);
}

void DefaultUnarySoftConstraint::decMem (int m) {
  if (m < 0) {
    trace(1, "Error! Trying to decrease a memory with a negative cost!");
  }
  setMem(diffCost(getMem(), m));
}

int DefaultUnarySoftConstraint::getMinCost () {
  int min = getMaxCost()+1;
  int value;
  currentSupport->unset();
  if (!isDiscretized()) {
    return 0;
  }
  DomainIterator di = getDomainIterator();
  for (; *di < getUb(); ++di) {
    value = getMemProfile(*di);
    if (value < min) {
      currentSupport->setSupport(0, *di);
      min = value;
    }
  }
  return diffCost(min, getMem());
}

int DefaultUnarySoftConstraint::getCost (Support *s) {
  int value = s->getSupport(0);
  if (!isDiscretized()) {
    if (value == getLb()) {
      return diffCost(getLbMem(), getMem());
    }
    if (value == getUb()-1) {
      return diffCost(getUbMem(), getMem());
    }
    return 0;
  }
  return diffCost(getMemProfile(value), getMem());
}

void DefaultUnarySoftConstraint::zeroInverse () {
  int min;
  min = getMinCost();
  if (min < 0) {
    trace(1, "Error! Got a negative project cost for a Mem!");
  }
  if (getNbVariables() != 1) {
    getZeroSupport()->copy(getCurrentSupport());
  }
  incMem(min);
  zeroArityConstraint->incMem(min);
  if (zeroArityConstraint->isUnconsistent()) {
    throw inconsistency(this);
  }
}

bool DefaultUnarySoftConstraint::setANC () {
  bool changed = false;
  zeroInverse();
  if (!isDiscretized()) {
    while (sumCost(zeroArityConstraint->getMinCost(), diffCost(getLbMem(), getMem())) >= top->getValue()) {
      incLb(1);
      changed = true;
      if (isEmpty()) {
        throw inconsistency(this);
      }
    }
    while (sumCost(zeroArityConstraint->getMinCost(), diffCost(getUbMem(), getMem())) >= top->getValue()) {
      decUb(1);
      changed = true;
      if (isEmpty()) {
        throw inconsistency(this);
      }
    }
    if (getUb() - getLb() < static_cast<int>(MAXSIZE)) {
      setDiscretized();
      zeroInverse();
    }
  }
  if (isDiscretized()) {
    DomainIterator di = getDomainIterator();
    for (; *di < getUb(); ++di) {
      if (sumCost(zeroArityConstraint->getMinCost(), diffCost(getMemProfile(*di), getMem())) >= top->getValue()) {
        changed = true;
        delValue(*di);
      }
    }
  }
  return changed;
}

string DefaultUnarySoftConstraint::print () {
  ostringstream s;
  s << "{" << variable->getName() << "}: " << getMem();
  return s.str();
}

void DefaultUnarySoftConstraint::save () {
  domain->save();
  ((UnaryCost *) cost)->save();
}

void DefaultUnarySoftConstraint::restore () {
  domain->restore();
  ((UnaryCost *) cost)->restore();
}
