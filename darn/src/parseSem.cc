
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.4.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 1



/* Copy the first part of user declarations.  */

/* Line 189 of yacc.c  */
#line 17 "parseSem.y"

#include "common.h"
#include "values.h"
#include "network.h"

int yylex(void);
int yyerror(char *msg);
int messageErrorMissingParameter(const char constraint[], const char arg[]);
int messageErrorMissingVariable(const char constraint[], int arg);

/**
 * number of possible constraint parameters
 */
static const int MAX_ARGS = 27;
/**
 * maximum number of constraint variables
 */
static const int MAX_VAR_ARGS = 4;

/**
 * set of possible types of constraint parameters
 */
typedef enum {STRING, INTEGER, BOOLEAN, INTERACTION, ORIENTATION, RELATION, MODEL} ConstraintParameterType;


/**
 * set of possible models for a function
 */
typedef enum {hardModel, softModel, optionalModel} Model;
/**
 * the name of the previous models
 */
const char* modelNames[] = {"hard", "soft", "optional"};

/**
 * structure recording the value of a parameter
 */
union ConstraintParameter {
  /**
   * a string
   */
  char *stringValue;
  /**
   * an integer
   */
  int integerValue;
  /**
   * a boolean
   */
  bool booleanValue;
  /**
   * an interaction (for a non-canonic interaction constraint)
   */
  Interaction interactionValue;
  /**
   * an orientation (for a non-canonic interaction constraint)
   */
  Orientation orientationValue;
  /**
   * a greater than / less than relation (for the threshold of a composition constraint)
   */
  Relation relationValue;
  /**
   * the model of a function
   */
  Model modelValue;
};

/**
 * name of the constraint parameters
 */
typedef enum {MODEL_A, WORD_A, ERRORS_A, MINSTEM_A, MAXSTEM_A, MINLOOP_A, MAXLOOP_A, MINLENGTH_A, MAXLENGTH_A, MINSOFTLENGTH_A, MAXSOFTLENGTH_A, MINCOSTS_A, MAXCOSTS_A, INDELS_A, WOBBLE_A, INTERACTION1_A, INTERACTION2_A, ORIENTATION_A, NUCLEOTIDES_A, THRESHOLD1_A, THRESHOLD2_A, RELATION_A, FAMILY_A, MINSIZE_A, MAXSIZE_A, MINDISTANCE_A, MAXDISTANCE_A} ConstraintParameterName;

/**
 * types of the constraint parameters (related to @a ConstraintParameterName)
 */
ConstraintParameterType types[MAX_ARGS] = {MODEL, STRING, INTEGER, INTEGER, INTEGER, INTEGER, INTEGER, INTEGER, INTEGER, INTEGER, INTEGER, INTEGER, INTEGER, BOOLEAN, BOOLEAN, INTERACTION, INTERACTION, ORIENTATION, STRING, INTEGER, INTEGER, RELATION, INTEGER, INTEGER, INTEGER, INTEGER, INTEGER};

/**
 * structure recording all the parameters of a constraint
 */
struct ConstraintParameters {
  /**
   * the values of the possible parameters
   */
  ConstraintParameter parameters[MAX_ARGS];
  /**
   * whether a parameter have been given by the user
   */
  bool marks[MAX_ARGS];

  /**
   * Constructor
   */
  ConstraintParameters () {
    for (int i = 0; i < MAX_ARGS; i++) {
      marks[i] = false;
    }
  }

  /**
   * set the value of a parameter
   * @param cp the value of the parameter
   * @param cpn the name of the parameter
   */
  void addElement (ConstraintParameter cp, ConstraintParameterName cpn) {
    switch (types[cpn]) {
      case STRING:
        parameters[cpn].stringValue = cp.stringValue;
        break;
      case BOOLEAN:
        parameters[cpn].booleanValue = cp.booleanValue;
        break;
      case INTEGER:
        parameters[cpn].integerValue = cp.integerValue;
        break;
      case INTERACTION:
        parameters[cpn].interactionValue = cp.interactionValue;
        break;
      case ORIENTATION:
        parameters[cpn].orientationValue = cp.orientationValue;
        break;
      case RELATION:
        parameters[cpn].relationValue = cp.relationValue;
        break;
      case MODEL:
        parameters[cpn].modelValue = cp.modelValue;
        break;
    }
    marks[cpn] = true;
  }

  /**
   * get the value of a parameter
   * @param cpn the name of the parameter
   * @return the value of the parameter
   */
  ConstraintParameter getElement (ConstraintParameterName cpn) {
    return parameters[cpn];
  }

  /**
   * check whether the value of a parameter has been given
   * @param cpn the name of the parameter
   * @return true, if the value has been given
   */
  bool isSet (ConstraintParameterName cpn) {
    return (marks[cpn]);
  }

  /**
   * erase all the values
   */
  void clear () {
    for (int i = 0; i < MAX_ARGS; i++) {
      marks[i] = false;
    }
  }
};

/**
 * structure recording all the variables of a constraint
 */
struct ConstraintVariables {
  /**
   * the set of variables
   */
  Variable **constraintVariables;
  /**
   * the number of variable given by the user
   */
  int nbConstraintVariables;

  /**
   * Constructor
   */
  ConstraintVariables () {
    constraintVariables = new Variable*[MAX_VAR_ARGS];
    nbConstraintVariables = 0;
  }

  /**
   * set a variable to the constraint
   * @param v the variable
   */
  void addElement (Variable *v) {
    constraintVariables[nbConstraintVariables] = v;
    nbConstraintVariables++;
  }

  /**
   * get a variable
   * @param i the index of the variable
   */
  Variable *getElement (int i) {
    return constraintVariables[i];
  }

  /**
   * check whether a variable has been given
   * @param i the index of the parameter
   * @return true, if the variable has been given
   */
  bool isSet (int i) {
    return (nbConstraintVariables > i);
  }

  /**
   * erase all the variables
   */
  void clear () {
    nbConstraintVariables = 0;
  }
};

/**
 * structure recording the costs of a soft constraint
 */
struct SoftConstraintCosts {
  /**
   * the set of costs
   */
  int *costs;
  /**
   * the number of costs given by the user
   */
  int nbCosts;
  /**
   * the maximum cost
   */
  int maxCost;
  /**
   * a string containing the content
   */
  char content[255];

  /**
   * Constructor
   */
  SoftConstraintCosts (int mc) : maxCost(mc) {
    costs = new int[maxCost+1];
    nbCosts = 0;
    content[0] = '\0';
  }

  /**
   * set a cost to the constraint
   * @param c the variable
   */
  void addElement (int c) {
    char tmp[255];
    if (nbCosts > maxCost+1) {
      cerr << "Error: Trying to access an out of range cost!" <<endl;
      exit(1);
    }
    costs[nbCosts] = c;
    nbCosts++;
    if (nbCosts == 1) {
      sprintf(content, " {");
    }
    else {
      content[strlen(content)-1] = '\0';
      strcat(content, ", ");
    }
    strcpy(tmp, content);
    sprintf(content, "%s%i", tmp, c);
    strcat(content, "}");
  }

  /**
   * get the costs
   * @return the costs
   */
  int *getCosts () {
    if (isSet()) {
      return costs;
    }
    return NULL;
  }

  /**
   * get the number of costs given by the user
   * @return the number of costs
   */ 
  int getNbCosts () {
    return nbCosts;
  }

  /**
   * check whether costs have been given
   * @return true, if the variable has been given
   */
  bool isSet () {
    return (nbCosts > 0);
  }

  /**
   * erase all the costs
   */
  void clear () {
    nbCosts = 0;
    content[0] = '\0';
  }

  /**
   * print the current content
   * @return a string
   */
  char *print () {
    return content;
  }
};

/**
 * an explicit name for the parameters
 */
const char *constraintParameterComments[] = {"model", "word", "errors", "minimum stem length", "maximum stem length", "minimum loop length", "maximum loop length", "minimum length", "maximum length", "minimum soft length", "maximum soft length", "minimum cost", "maximum cost", "insertions/deletions accepted", "wobble accepted", "first interaction", "second interaction", "orientation", "nucleotides", "first threshold", "second threshold", "relation","isostericity family"};

/**
 * the set of possible variable for a constraint
 */
ConstraintVariables constraintVariables;
/**
 * the set of possible parameters for a constraint
 */
ConstraintParameters constraintParameters;
/**
 * a value of parameter (global variable because heavily used)
 */
ConstraintParameter constraintParameter;
/**
 * the set of costs for a soft constraint
 */
SoftConstraintCosts *constraintCosts;


/**
 * check whether the variables have been provided by the user
 * @param constraintName an explicit name for the constraint
 * @param nbConstraintVariables the number of variables used by the constraint
 */
void checkParameters (const char *constraintName, int nbConstraintVariables) {
  for (int i = 0; i < nbConstraintVariables; i++) {
    if (!constraintVariables.isSet(i)) {
      messageErrorMissingVariable(constraintName, i+1);
      break;
    }
  }
}

/**
 * check whether the variables and the parameters have been provided by the user
 * @param constraintName an explicit name for the constraint
 * @param nbConstraintVariables the number of variables used by the constraint
 * @param nbConstraintParameters the number of parameters used by the constraint
 * @param cpn0 the first parameter
 */
void checkParameters (const char *constraintName, int nbConstraintVariables, int nbConstraintParameters, ConstraintParameterName cpn0 ...) {
  ConstraintParameterName cpn;
  va_list l;
  va_start(l, cpn0);

  for (int i = 0; i < nbConstraintVariables; i++) {
    if (!constraintVariables.isSet(i)) {
      messageErrorMissingVariable(constraintName, i+1);
      break;
    }
  }
  for (int i = 1; i < nbConstraintParameters; i++) {
    cpn = (ConstraintParameterName) va_arg(l, int);
    if (!constraintParameters.isSet(cpn)) {
      messageErrorMissingParameter(constraintName, constraintParameterComments[cpn]);
    }
  }
  va_end(l);
}

/**
 * number of variables of the network
 */
int nbVars;
/**
 * maximum cost
 */
int maxCost = 1;
/**
 * index of the first S variable (on the main stem)
 */
int firstSVarIndex;
/**
 * index of the last S variable (on the main stem)
 */
int lastSVarIndex;
/**
 * number of S variables (on the main stem)
 */
int nbSVars;
/**
 * index of the first T variable (on the target stem)
 */
int firstTVarIndex;
/**
 * index of the last T variable (on the target stem)
 */
int lastTVarIndex;
/**
 * number of T variables (on the target stem)
 */
int nbTVars;



/* Line 189 of yacc.c  */
#line 486 "parseSem.cc"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     NUMBER = 258,
     SVAR = 259,
     TVAR = 260,
     WORD = 261,
     MAX_COST = 262,
     DECL_VARIABLE = 263,
     DECL_CONTENT = 264,
     DECL_PATTERN = 265,
     DECL_PATTERN2 = 266,
     DECL_SPACER = 267,
     DECL_COMPOSITION = 268,
     DECL_HELIX_SIMPLE = 269,
     DECL_HELIX = 270,
     DECL_REPETITION = 271,
     DECL_DUPLEX = 272,
     DECL_NONCANONICPAIR = 273,
     DECL_HAIRPIN = 274,
     DECL_SVAR = 275,
     DECL_TVAR = 276,
     PARAM_MODEL = 277,
     PARAM_WORD = 278,
     PARAM_NUCLEOTIDES = 279,
     PARAM_ERRORS = 280,
     PARAM_MINSTEM = 281,
     PARAM_MAXSTEM = 282,
     PARAM_STEM = 283,
     PARAM_MINLOOP = 284,
     PARAM_MAXLOOP = 285,
     PARAM_LOOP = 286,
     PARAM_MINLENGTH = 287,
     PARAM_MAXLENGTH = 288,
     PARAM_MINSOFTLENGTH = 289,
     PARAM_MAXSOFTLENGTH = 290,
     PARAM_LENGTH = 291,
     PARAM_MINCOSTS = 292,
     PARAM_MAXCOSTS = 293,
     PARAM_COSTS = 294,
     PARAM_INDELS = 295,
     PARAM_WOBBLE = 296,
     PARAM_INTERACTION = 297,
     PARAM_ORIENTATION = 298,
     PARAM_THRESHOLD = 299,
     PARAM_RELATION = 300,
     PARAM_FAMILY = 301,
     PARAM_SIZE = 302,
     PARAM_DISTANCE = 303,
     VALUE_TRUE = 304,
     VALUE_FALSE = 305,
     VALUE_ALL = 306,
     INTERACTION_WATSON_CRICK = 307,
     INTERACTION_HOOGSTEEN = 308,
     INTERACTION_SUGAR_EDGE = 309,
     ORIENTATION_CIS = 310,
     ORIENTATION_TRANS = 311,
     RELATION_NOT_LESS_THAN = 312,
     RELATION_NOT_GREATER_THAN = 313,
     MODEL_HARD = 314,
     MODEL_SOFT = 315,
     MODEL_OPTIONAL = 316,
     DOTS = 317,
     COMMA = 318,
     EQUAL = 319,
     LEFT_BRACKET = 320,
     RIGHT_BRACKET = 321,
     LEFT_PAR = 322,
     RIGHT_PAR = 323,
     LEFT_BRA = 324,
     RIGHT_BRA = 325,
     QUOTE = 326,
     PERCENT = 327,
     END_LINE = 328
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 214 of yacc.c  */
#line 431 "parseSem.y"

    char *word;
    int value;



/* Line 214 of yacc.c  */
#line 602 "parseSem.cc"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


/* Copy the second part of user declarations.  */


/* Line 264 of yacc.c  */
#line 627 "parseSem.cc"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
	     && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE) + sizeof (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  5
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   235

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  74
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  50
/* YYNRULES -- Number of rules.  */
#define YYNRULES  119
/* YYNRULES -- Number of states.  */
#define YYNSTATES  240

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   328

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     8,    13,    21,    35,    41,    52,    53,
      56,    59,    61,    63,    65,    67,    69,    71,    73,    75,
      77,    81,    84,    87,    90,    93,    97,   101,   105,   109,
     113,   120,   122,   126,   128,   130,   132,   134,   136,   138,
     140,   142,   144,   146,   148,   150,   152,   154,   156,   158,
     160,   162,   164,   166,   168,   170,   172,   174,   176,   178,
     180,   182,   184,   188,   192,   196,   200,   206,   210,   214,
     218,   224,   228,   232,   236,   242,   246,   250,   254,   260,
     270,   274,   278,   282,   288,   292,   296,   300,   304,   308,
     312,   316,   320,   324,   328,   332,   336,   340,   346,   350,
     355,   359,   364,   370,   377,   383,   390,   396,   402,   404,
     408,   414,   424,   428,   429,   433,   435,   439,   441,   442
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      75,     0,    -1,   123,    76,    77,    78,    -1,     7,    64,
       3,   123,    -1,     8,    20,    64,     3,    62,     3,   123,
      -1,     8,    20,    64,     3,    62,     3,    63,    21,    64,
       3,    62,     3,   123,    -1,    20,     3,    62,     3,   123,
      -1,    20,     3,    62,     3,   123,    21,     3,    62,     3,
     123,    -1,    -1,    79,    78,    -1,    80,   123,    -1,    81,
      -1,    82,    -1,    83,    -1,    84,    -1,    85,    -1,    88,
      -1,    86,    -1,    87,    -1,    89,    -1,    10,    90,   120,
      -1,    12,    90,    -1,    13,    90,    -1,    18,    90,    -1,
      19,    90,    -1,    14,    90,   120,    -1,    16,    90,   120,
      -1,    15,    90,   120,    -1,    17,    90,   120,    -1,    65,
      91,    66,    -1,    65,    91,    66,    67,    91,    68,    -1,
      92,    -1,    92,    63,    91,    -1,    93,    -1,    95,    -1,
      96,    -1,    97,    -1,    98,    -1,    99,    -1,   100,    -1,
     101,    -1,   102,    -1,   103,    -1,   104,    -1,   105,    -1,
     106,    -1,   107,    -1,   108,    -1,   109,    -1,   110,    -1,
     111,    -1,   112,    -1,   113,    -1,   114,    -1,   115,    -1,
     116,    -1,   117,    -1,   118,    -1,   119,    -1,    94,    -1,
       4,    -1,     5,    -1,    22,    64,    59,    -1,    22,    64,
      60,    -1,    22,    64,    61,    -1,    23,    64,     6,    -1,
      23,    64,    71,     6,    71,    -1,    25,    64,     3,    -1,
      26,    64,     3,    -1,    27,    64,     3,    -1,    28,    64,
       3,    62,     3,    -1,    28,    64,     3,    -1,    29,    64,
       3,    -1,    30,    64,     3,    -1,    31,    64,     3,    62,
       3,    -1,    31,    64,     3,    -1,    32,    64,     3,    -1,
      33,    64,     3,    -1,    36,    64,     3,    62,     3,    -1,
      36,    64,     3,    62,     3,    62,     3,    62,     3,    -1,
      36,    64,     3,    -1,    37,    64,     3,    -1,    38,    64,
       3,    -1,    39,    64,     3,    62,     3,    -1,    39,    64,
       3,    -1,    40,    64,    49,    -1,    40,    64,    50,    -1,
      41,    64,    49,    -1,    41,    64,    50,    -1,    42,    64,
      52,    -1,    42,    64,    53,    -1,    42,    64,    54,    -1,
      43,    64,    55,    -1,    43,    64,    56,    -1,    46,    64,
       3,    -1,    46,    64,    51,    -1,    24,    64,     6,    -1,
      24,    64,    71,     6,    71,    -1,    44,    57,     3,    -1,
      44,    57,     3,    72,    -1,    44,    58,     3,    -1,    44,
      58,     3,    72,    -1,    44,    57,     3,    62,     3,    -1,
      44,    57,     3,    62,     3,    72,    -1,    44,    58,     3,
      62,     3,    -1,    44,    58,     3,    62,     3,    72,    -1,
      47,    64,     3,    62,     3,    -1,    48,    64,     3,    62,
       3,    -1,     6,    -1,     6,    64,     3,    -1,     6,    64,
       3,    62,     3,    -1,     6,    64,     3,    62,     3,    62,
       3,    62,     3,    -1,     6,    64,     6,    -1,    -1,    69,
     121,    70,    -1,   122,    -1,   121,    63,   122,    -1,     3,
      -1,    -1,    73,   123,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   464,   464,   468,   475,   491,   517,   533,   561,   563,
     567,   576,   577,   578,   579,   580,   581,   582,   583,   584,
     588,   619,   663,   724,   764,   799,   847,   890,   926,   960,
     961,   965,   966,   970,   971,   972,   973,   974,   975,   976,
     977,   978,   979,   980,   981,   982,   983,   984,   985,   986,
     987,   988,   989,   990,   991,   992,   993,   994,   995,   999,
    1005,  1009,  1016,  1021,  1026,  1034,  1039,  1047,  1060,  1068,
    1076,  1083,  1092,  1100,  1108,  1115,  1125,  1133,  1141,  1148,
    1159,  1170,  1178,  1186,  1193,  1202,  1207,  1215,  1220,  1228,
    1238,  1248,  1261,  1266,  1274,  1279,  1287,  1292,  1300,  1307,
    1314,  1321,  1328,  1337,  1346,  1355,  1367,  1377,  1387,  1393,
    1399,  1405,  1411,  1419,  1421,  1425,  1426,  1430,  1436,  1438
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "NUMBER", "SVAR", "TVAR", "WORD",
  "MAX_COST", "DECL_VARIABLE", "DECL_CONTENT", "DECL_PATTERN",
  "DECL_PATTERN2", "DECL_SPACER", "DECL_COMPOSITION", "DECL_HELIX_SIMPLE",
  "DECL_HELIX", "DECL_REPETITION", "DECL_DUPLEX", "DECL_NONCANONICPAIR",
  "DECL_HAIRPIN", "DECL_SVAR", "DECL_TVAR", "PARAM_MODEL", "PARAM_WORD",
  "PARAM_NUCLEOTIDES", "PARAM_ERRORS", "PARAM_MINSTEM", "PARAM_MAXSTEM",
  "PARAM_STEM", "PARAM_MINLOOP", "PARAM_MAXLOOP", "PARAM_LOOP",
  "PARAM_MINLENGTH", "PARAM_MAXLENGTH", "PARAM_MINSOFTLENGTH",
  "PARAM_MAXSOFTLENGTH", "PARAM_LENGTH", "PARAM_MINCOSTS",
  "PARAM_MAXCOSTS", "PARAM_COSTS", "PARAM_INDELS", "PARAM_WOBBLE",
  "PARAM_INTERACTION", "PARAM_ORIENTATION", "PARAM_THRESHOLD",
  "PARAM_RELATION", "PARAM_FAMILY", "PARAM_SIZE", "PARAM_DISTANCE",
  "VALUE_TRUE", "VALUE_FALSE", "VALUE_ALL", "INTERACTION_WATSON_CRICK",
  "INTERACTION_HOOGSTEEN", "INTERACTION_SUGAR_EDGE", "ORIENTATION_CIS",
  "ORIENTATION_TRANS", "RELATION_NOT_LESS_THAN",
  "RELATION_NOT_GREATER_THAN", "MODEL_HARD", "MODEL_SOFT",
  "MODEL_OPTIONAL", "DOTS", "COMMA", "EQUAL", "LEFT_BRACKET",
  "RIGHT_BRACKET", "LEFT_PAR", "RIGHT_PAR", "LEFT_BRA", "RIGHT_BRA",
  "QUOTE", "PERCENT", "END_LINE", "$accept", "program",
  "overall_cost_declaration", "variable_declaration",
  "function_declarations", "function_declaration", "function", "pattern",
  "spacer", "composition", "noncanonicpair", "hairpin", "helix_simple",
  "repetition", "helix", "duplex", "parameters", "parameters_no_bracket",
  "parameter", "variable_parameter", "variable", "model_parameter",
  "word_parameter", "errors_parameter", "minstem_parameter",
  "maxstem_parameter", "minmaxstem_parameter", "minloop_parameter",
  "maxloop_parameter", "minmaxloop_parameter", "minlength_parameter",
  "maxlength_parameter", "minmaxlength_parameter", "mincosts_parameter",
  "maxcosts_parameter", "minmaxcosts_parameter", "indels_parameter",
  "wobble_parameter", "interaction_parameter", "orientation_parameter",
  "family_parameter", "nucleotides_parameter", "threshold_parameter",
  "sizes_parameter", "distances_parameter", "default_parameter", "costs",
  "cost_declarations", "cost_declaration", "end_line", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    74,    75,    76,    77,    77,    77,    77,    78,    78,
      79,    80,    80,    80,    80,    80,    80,    80,    80,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      90,    91,    91,    92,    92,    92,    92,    92,    92,    92,
      92,    92,    92,    92,    92,    92,    92,    92,    92,    92,
      92,    92,    92,    92,    92,    92,    92,    92,    92,    93,
      94,    94,    95,    95,    95,    96,    96,    97,    98,    99,
     100,   100,   101,   102,   103,   103,   104,   105,   106,   106,
     106,   107,   108,   109,   109,   110,   110,   111,   111,   112,
     112,   112,   113,   113,   114,   114,   115,   115,   116,   116,
     116,   116,   116,   116,   116,   116,   117,   118,   119,   119,
     119,   119,   119,   120,   120,   121,   121,   122,   123,   123
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     4,     4,     7,    13,     5,    10,     0,     2,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       3,     2,     2,     2,     2,     3,     3,     3,     3,     3,
       6,     1,     3,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     3,     3,     3,     3,     5,     3,     3,     3,
       5,     3,     3,     3,     5,     3,     3,     3,     5,     9,
       3,     3,     3,     5,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     5,     3,     4,
       3,     4,     5,     6,     5,     6,     5,     5,     1,     3,
       5,     9,     3,     0,     3,     1,     3,     1,     0,     2
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
     118,   118,     0,     0,   119,     1,     0,     0,     0,     0,
       0,     8,   118,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     2,     8,   118,    11,    12,    13,
      14,    15,    17,    18,    16,    19,     3,     0,     0,     0,
     113,    21,    22,   113,   113,   113,   113,    23,    24,     9,
      10,     0,   118,    60,    61,   108,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    31,    33,    59,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,     0,
      20,    25,    27,    26,    28,     0,     6,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    29,     0,   117,     0,   115,   118,     0,
     109,   112,    62,    63,    64,    65,     0,    96,     0,    67,
      68,    69,    71,    72,    73,    75,    76,    77,    80,    81,
      82,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    98,   100,    94,    95,     0,     0,     0,    32,     0,
     114,     0,     4,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    99,     0,   101,     0,     0,     0,   116,     0,
       0,   110,    66,    97,    70,    74,    78,    83,   102,   104,
     106,   107,    30,     0,   118,     0,     0,   103,   105,     0,
       7,     0,     0,     0,     0,     0,   118,   111,    79,     5
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     7,    11,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    40,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,    96,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   110,   146,   147,     3
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -126
static const yytype_int16 yypact[] =
{
     -64,   -64,    12,    31,  -126,  -126,   -44,    -3,    87,    71,
      89,    14,   -64,    29,    32,    30,    30,    30,    30,    30,
      30,    30,    30,    30,  -126,    14,   -64,  -126,  -126,  -126,
    -126,  -126,  -126,  -126,  -126,  -126,  -126,    93,    94,    17,
      34,  -126,  -126,    34,    34,    34,    34,  -126,  -126,  -126,
    -126,    36,   -64,  -126,  -126,    38,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    53,    54,
      55,    56,    57,    58,    59,    60,   -43,    61,    62,    63,
      64,    65,  -126,  -126,  -126,  -126,  -126,  -126,  -126,  -126,
    -126,  -126,  -126,  -126,  -126,  -126,  -126,  -126,  -126,  -126,
    -126,  -126,  -126,  -126,  -126,  -126,  -126,  -126,  -126,    96,
    -126,  -126,  -126,  -126,  -126,    97,    80,     4,    18,    -5,
      -4,   113,   126,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,    35,    37,    28,    33,   139,   140,
       1,   141,   142,    79,    17,  -126,    13,  -126,   -60,   145,
      88,  -126,  -126,  -126,  -126,  -126,   143,  -126,   146,  -126,
    -126,  -126,    91,  -126,  -126,    92,  -126,  -126,    95,  -126,
    -126,    98,  -126,  -126,  -126,  -126,  -126,  -126,  -126,  -126,
    -126,   -56,   -54,  -126,  -126,    99,   100,    17,  -126,    96,
    -126,   144,  -126,   101,   148,    84,    85,   155,   156,   161,
     163,   164,  -126,   165,  -126,   166,   167,   103,  -126,   108,
     170,   112,  -126,  -126,  -126,  -126,   114,  -126,   105,   106,
    -126,  -126,  -126,   172,   -64,   176,   177,  -126,  -126,   119,
    -126,   120,   121,   181,   182,   183,   -64,  -126,  -126,  -126
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -126,  -126,  -126,  -126,   162,  -126,  -126,  -126,  -126,  -126,
    -126,  -126,  -126,  -126,  -126,  -126,    52,  -125,  -126,  -126,
    -126,  -126,  -126,  -126,  -126,  -126,  -126,  -126,  -126,  -126,
    -126,  -126,  -126,  -126,  -126,  -126,  -126,  -126,  -126,  -126,
    -126,  -126,  -126,  -126,  -126,  -126,    -9,  -126,     0,    -1
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint8 yytable[] =
{
       4,   155,   157,   191,   183,     9,   201,   150,   203,     1,
     151,    36,     5,     1,   138,   139,   202,    10,   204,   188,
       8,    53,    54,    55,    15,    50,    16,    17,    18,    19,
      20,    21,    22,    23,   111,   112,   113,   114,     6,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,   116,   184,    68,    69,    70,    71,    72,    73,    74,
      75,    76,   207,    77,    78,    79,   156,   158,    41,    42,
      43,    44,    45,    46,    47,    48,   189,   152,   153,   154,
     176,   177,   178,   190,   172,   173,   174,   175,   179,   180,
      12,    13,    14,    37,    38,    39,    51,    52,   115,   145,
     148,   149,   117,   109,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   159,   130,   131,   132,
     133,   134,   135,   136,   137,   140,   141,   142,   144,   160,
     143,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   181,   182,   185,   186,   187,   192,   193,   195,
     194,   211,   196,   197,   198,   212,   213,   199,   214,   215,
     200,   205,   206,   210,   216,   209,   217,   218,   219,   220,
     221,   222,   223,   224,   225,   229,   226,   227,   228,   231,
     232,   233,   234,   235,   236,   237,   238,    49,     0,   208,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   230,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   239
};

static const yytype_int16 yycheck[] =
{
       1,     6,     6,    63,     3,     8,    62,     3,    62,    73,
       6,    12,     0,    73,    57,    58,    72,    20,    72,   144,
      64,     4,     5,     6,    10,    26,    12,    13,    14,    15,
      16,    17,    18,    19,    43,    44,    45,    46,     7,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    52,    51,    36,    37,    38,    39,    40,    41,    42,
      43,    44,   187,    46,    47,    48,    71,    71,    16,    17,
      18,    19,    20,    21,    22,    23,    63,    59,    60,    61,
      52,    53,    54,    70,    49,    50,    49,    50,    55,    56,
       3,    20,     3,    64,    62,    65,     3,     3,    62,     3,
       3,    21,    64,    69,    64,    64,    64,    64,    64,    64,
      64,    64,    64,    64,    64,    64,     3,    64,    64,    64,
      64,    64,    64,    64,    64,    64,    64,    64,    63,     3,
      66,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,    67,   148,     3,     6,
      62,     3,     6,    62,    62,    71,    71,    62,     3,     3,
      62,    62,    62,    62,     3,    21,     3,     3,     3,     3,
       3,    68,    64,     3,    62,     3,    62,    72,    72,     3,
       3,    62,    62,    62,     3,     3,     3,    25,    -1,   189,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   224,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   236
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    73,    75,   123,   123,     0,     7,    76,    64,     8,
      20,    77,     3,    20,     3,    10,    12,    13,    14,    15,
      16,    17,    18,    19,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,   123,    64,    62,    65,
      90,    90,    90,    90,    90,    90,    90,    90,    90,    78,
     123,     3,     3,     4,     5,     6,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    46,    47,    48,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,    69,
     120,   120,   120,   120,   120,    62,   123,    64,    64,    64,
      64,    64,    64,    64,    64,    64,    64,    64,    64,    64,
      64,    64,    64,    64,    64,    64,    64,    64,    57,    58,
      64,    64,    64,    66,    63,     3,   121,   122,     3,    21,
       3,     6,    59,    60,    61,     6,    71,     6,    71,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,    49,    50,    49,    50,    52,    53,    54,    55,
      56,     3,     3,     3,    51,     3,     3,    67,    91,    63,
      70,    63,   123,     3,    62,     6,     6,    62,    62,    62,
      62,    62,    72,    62,    72,    62,    62,    91,   122,    21,
      62,     3,    71,    71,     3,     3,     3,     3,     3,     3,
       3,     3,    68,    64,     3,    62,    62,    72,    72,     3,
     123,     3,     3,    62,    62,    62,     3,     3,     3,   123
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value, Location); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    YYLTYPE const * const yylocationp;
#endif
{
  if (!yyvaluep)
    return;
  YYUSE (yylocationp);
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep, yylocationp)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    YYLTYPE const * const yylocationp;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, YYLTYPE *yylsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yylsp, yyrule)
    YYSTYPE *yyvsp;
    YYLTYPE *yylsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       , &(yylsp[(yyi + 1) - (yynrhs)])		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, yylsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
#else
static void
yydestruct (yymsg, yytype, yyvaluep, yylocationp)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
    YYLTYPE *yylocationp;
#endif
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}

/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Location data for the lookahead symbol.  */
YYLTYPE yylloc;

/* Number of syntax errors so far.  */
int yynerrs;



/*-------------------------.
| yyparse or yypush_parse.  |
`-------------------------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{


    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.
       `yyls': related to locations.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

    /* The locations where the error started and ended.  */
    YYLTYPE yyerror_range[2];

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yyls = yylsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;
  yylsp = yyls;

#if YYLTYPE_IS_TRIVIAL
  /* Initialize the default location before parsing starts.  */
  yylloc.first_line   = yylloc.last_line   = 1;
  yylloc.first_column = yylloc.last_column = 1;
#endif

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;
	YYLTYPE *yyls1 = yyls;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yyls1, yysize * sizeof (*yylsp),
		    &yystacksize);

	yyls = yyls1;
	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
	YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;
  *++yylsp = yylloc;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location.  */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 3:

/* Line 1455 of yacc.c  */
#line 469 "parseSem.y"
    {
            maxCost = (yyvsp[(3) - (4)].value);
            constraintCosts = new SoftConstraintCosts(NBMAXERRORS);
          ;}
    break;

  case 4:

/* Line 1455 of yacc.c  */
#line 476 "parseSem.y"
    {
            char str[10];
            firstSVarIndex = (yyvsp[(4) - (7)].value);
            lastSVarIndex = (yyvsp[(6) - (7)].value);
            firstTVarIndex = 0;
            lastTVarIndex = 0;
            nbSVars = lastSVarIndex - firstSVarIndex + 1;
            nbTVars = 0;
            nbVars = nbSVars;
            network = new Network(nbVars, maxCost);
            for (int i = firstSVarIndex; i <= lastSVarIndex; i++) {
              sprintf(str, "X%i", i);
              network->makeVariable(string(str), sequence);
            }
          ;}
    break;

  case 5:

/* Line 1455 of yacc.c  */
#line 492 "parseSem.y"
    {
            char str[10];
            firstSVarIndex = (yyvsp[(4) - (13)].value);
            lastSVarIndex = (yyvsp[(6) - (13)].value);
            firstTVarIndex = (yyvsp[(10) - (13)].value);
            lastTVarIndex = (yyvsp[(12) - (13)].value);
            nbSVars = lastSVarIndex - firstSVarIndex + 1;
            nbTVars = lastTVarIndex - firstTVarIndex + 1;
            nbVars = nbSVars + nbTVars;

            if (nbDupSequences == 0) {
              trace(0, "Error! The duplex file is not set!");
              exit(1);
            }

            network = new Network(nbVars, maxCost);
            for (int i = firstSVarIndex; i <= lastSVarIndex; i++) {
              sprintf(str, "X%i", i);
              network->makeVariable(string(str), sequence);
            }
            for (int i = firstTVarIndex; i <= lastTVarIndex; i++) {
              sprintf(str, "Y%i", i);
              network->makeVariable(string(str), dupSequences[0]);
            }
          ;}
    break;

  case 6:

/* Line 1455 of yacc.c  */
#line 518 "parseSem.y"
    {
            char str[10];
            firstSVarIndex = (yyvsp[(2) - (5)].value);
            lastSVarIndex = (yyvsp[(4) - (5)].value);
            firstTVarIndex = 0;
            lastTVarIndex = 0;
            nbSVars = lastSVarIndex - firstSVarIndex + 1;
            nbTVars = 0;
            nbVars = nbSVars;
            network = new Network(nbVars, maxCost);
            for (int i = firstSVarIndex; i <= lastSVarIndex; i++) {
              sprintf(str, "X%i", i);
              network->makeVariable(string(str), sequence);
            }
          ;}
    break;

  case 7:

/* Line 1455 of yacc.c  */
#line 534 "parseSem.y"
    {
            char str[10];
            firstSVarIndex = (yyvsp[(2) - (10)].value);
            lastSVarIndex = (yyvsp[(4) - (10)].value);
            firstTVarIndex = (yyvsp[(7) - (10)].value);
            lastTVarIndex = (yyvsp[(9) - (10)].value);
            nbSVars = lastSVarIndex - firstSVarIndex + 1;
            nbTVars = lastTVarIndex - firstTVarIndex + 1;
            nbVars = nbSVars + nbTVars;

            if (nbDupSequences == 0) {
              trace(0, "Error! The duplex file is not set!");
              exit(1);
            }

            network = new Network(nbVars, maxCost);
            for (int i = firstSVarIndex; i <= lastSVarIndex; i++) {
              sprintf(str, "X%i", i);
              network->makeVariable(string(str), sequence);
            }
            for (int i = firstTVarIndex; i <= lastTVarIndex; i++) {
              sprintf(str, "Y%i", i);
              network->makeVariable(string(str), dupSequences[0]);
            }
          ;}
    break;

  case 10:

/* Line 1455 of yacc.c  */
#line 568 "parseSem.y"
    {
            constraintVariables.clear();
            constraintParameters.clear();
            constraintCosts->clear();
          ;}
    break;

  case 20:

/* Line 1455 of yacc.c  */
#line 589 "parseSem.y"
    {
            Variable *v1, *v2;
            char *word;
            int errors = 0;
            Model model = hardModel;
            const char *constraintName = "pattern constraint (2)";
            checkParameters(constraintName, 2, 1, WORD_A);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            word = constraintParameters.getElement(WORD_A).stringValue;
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(ERRORS_A)) {
              errors = constraintParameters.getElement(ERRORS_A).integerValue;
            }
            if (model != optionalModel) {
              network->makeHardDistance(v1, v2, strlen(word)-1-errors, strlen(word)-1+errors);
              network->makeHardPattern2(v1, v2, word, errors);
            }
            if (model != hardModel) {
              network->makePattern2(v1, v2, word, new ErrorsToCosts(errors, constraintCosts->getCosts()));
            }
            char mes[255];
            sprintf(mes, "\tCreating pattern (2) [%s, %i, %s] (%s, %s)%s", word, errors, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          ;}
    break;

  case 21:

/* Line 1455 of yacc.c  */
#line 620 "parseSem.y"
    {
            Variable *v1, *v2;
            int minLength, maxLength;
            int minSoftLength, maxSoftLength;
            int minCosts = 0;
            int maxCosts = maxCost;
            Model model = hardModel;
            const char *constraintName = "spacer";
            checkParameters(constraintName, 2, 2, MINLENGTH_A, MAXLENGTH_A);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            minLength = constraintParameters.getElement(MINLENGTH_A).integerValue;
            maxLength = constraintParameters.getElement(MAXLENGTH_A).integerValue;
            minSoftLength = minLength;
            maxSoftLength = maxLength;
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(MINCOSTS_A)) {
              minCosts = constraintParameters.getElement(MINCOSTS_A).integerValue;
            }
            if (constraintParameters.isSet(MAXCOSTS_A)) {
              maxCosts = constraintParameters.getElement(MAXCOSTS_A).integerValue;
            }
            if (constraintParameters.isSet(MINSOFTLENGTH_A)) {
              minSoftLength = constraintParameters.getElement(MINSOFTLENGTH_A).integerValue;
            }
            if (constraintParameters.isSet(MAXSOFTLENGTH_A)) {
              maxSoftLength = constraintParameters.getElement(MAXSOFTLENGTH_A).integerValue;
            }
            if (model != optionalModel) {
              network->makeHardDistance(v1, v2, minLength, maxLength);
            }
            if (model != hardModel) {
              network->makeDistance(v1, v2, minLength, minSoftLength, maxSoftLength, maxLength, minCosts, new ErrorsToCosts(maxCosts, constraintCosts->getCosts()));
            }
            char mes[255];
            sprintf(mes, "\tCreating spacer [%i, %i, %i, %i, %i, %i, %s] (%s, %s)%s", minLength, minSoftLength, maxSoftLength, maxLength, minCosts, maxCosts, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          ;}
    break;

  case 22:

/* Line 1455 of yacc.c  */
#line 664 "parseSem.y"
    {
            Variable *v1, *v2;
            int t1 = 0;
            int t2 = 100;
            int c1 = 0;
            int c2 = maxCost;
            char *nuc;
            Nucleotide *n1 = nucleotideC;
            Nucleotide *n2 = nucleotideG;
            Relation r = NOT_LESS_THAN;
            Model model = hardModel;
            const char *constraintName = "composition";
            checkParameters(constraintName, 2, 1, THRESHOLD1_A);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(RELATION_A)) {
              r = constraintParameters.getElement(RELATION_A).relationValue;
            }
            if (constraintParameters.isSet(NUCLEOTIDES_A)) {
              nuc = constraintParameters.getElement(NUCLEOTIDES_A).stringValue;
              n1 = Nucleotide::getNucleotide(nuc[0]);
              n2 = Nucleotide::getNucleotide(nuc[1]);
            }
            if (constraintParameters.isSet(THRESHOLD1_A)) {
              t1 = constraintParameters.getElement(THRESHOLD1_A).integerValue;
            }
            if (constraintParameters.isSet(THRESHOLD2_A)) {
              t2 = constraintParameters.getElement(THRESHOLD2_A).integerValue;
            }
            else {
              t2 = t1;
            }
            if (constraintParameters.isSet(MINCOSTS_A)) {
              c1 = constraintParameters.getElement(MINCOSTS_A).integerValue;
            }
            if (constraintParameters.isSet(MAXCOSTS_A)) {
              c2 = constraintParameters.getElement(MAXCOSTS_A).integerValue;
            }
            if (model != optionalModel) {
              if (r == NOT_LESS_THAN) {
                network->makeHardComposition(v1, v2, n1, n2, t1, r);
              }
              else {
                network->makeHardComposition(v1, v2, n1, n2, t2, r);
              }
            }
            if (model != hardModel) {
              network->makeComposition(v1, v2, n1, n2, t1, t2, r, c1, new ErrorsToCosts(c2, constraintCosts->getCosts()));

            }
            char mes[255];
            sprintf(mes, "\tCreating composition [%c, %c, %s, %i, %i, %i, %i, %s] (%s, %s)%s", n1->print(), n2->print(), ((r == NOT_LESS_THAN)? " >= ": " <= "), t1, t2, c1, c2, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          ;}
    break;

  case 23:

/* Line 1455 of yacc.c  */
#line 725 "parseSem.y"
    {
            Variable *v1, *v2;
            Interaction i1, i2;
            Orientation o;
            int mc = maxCost;
            int family = 0;
            Model model = hardModel;
            const char *constraintName = "non canonic pairing";
            checkParameters(constraintName, 2, 3, INTERACTION1_A, INTERACTION2_A, ORIENTATION_A);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            i1 = constraintParameters.getElement(INTERACTION1_A).interactionValue;
            i1 = constraintParameters.getElement(INTERACTION2_A).interactionValue;
            o = constraintParameters.getElement(ORIENTATION_A).orientationValue;
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(MAXCOSTS_A)) {
              mc = constraintParameters.getElement(MAXCOSTS_A).integerValue;
            }
            i1 = constraintParameters.getElement(INTERACTION1_A).interactionValue;
            i2 = constraintParameters.getElement(INTERACTION2_A).interactionValue;
            o = constraintParameters.getElement(ORIENTATION_A).orientationValue;
            if (constraintParameters.isSet(FAMILY_A)) {
              family = constraintParameters.getElement(FAMILY_A).integerValue;
            }
            if (model != optionalModel) {
              network->makeHardNonCanonicPair(v1, v2, i1, i2, o, family, mc);
            }
            if (model != hardModel) {
              network->makeNonCanonicPair(v1, v2, i1, i2, o, family, new ErrorsToCosts(mc, constraintCosts->getCosts()));
            }
            char mes[255];
            sprintf(mes, "\tCreating non canonic pair [%i, %i, %i, %i, %i, %s] (%s, %s)%s", i1, i2, o, family, mc, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          ;}
    break;

  case 24:

/* Line 1455 of yacc.c  */
#line 765 "parseSem.y"
    {
            Variable *v1, *v2;
            int minLength, maxLength;
            int errors = 0;
            Model model = hardModel;
            bool wobble = false;
            const char *constraintName = "hairpin";
            checkParameters(constraintName, 2, 2, MINLENGTH_A, MAXLENGTH_A);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(ERRORS_A)) {
              errors = constraintParameters.getElement(ERRORS_A).integerValue;
            }
            if (constraintParameters.isSet(WOBBLE_A)) {
              wobble = constraintParameters.getElement(WOBBLE_A).booleanValue;
            }
            minLength = constraintParameters.getElement(MINLENGTH_A).integerValue;
            maxLength = constraintParameters.getElement(MAXLENGTH_A).integerValue;
            if (model != optionalModel) {
              network->makeHardHairpin(v1, v2, minLength, maxLength, errors, wobble);
            }
            if (model != hardModel) {
              network->makeHairpin(v1, v2, minLength, maxLength, new ErrorsToCosts(errors, constraintCosts->getCosts()), wobble);
            }
            char mes[255];
            sprintf(mes, "\tCreating folding [%i, %i, %i, %i, %s] (%s, %s)%s", minLength, maxLength, wobble, errors, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          ;}
    break;

  case 25:

/* Line 1455 of yacc.c  */
#line 800 "parseSem.y"
    {
            Variable *v1, *v2, *v3, *v4;
            int minStem, maxStem;
            int minLoop, maxLoop;
            int errors = 0;
            Model model = hardModel;
            bool indels = false;
            bool wobble = false;
            const char *constraintName = "simple helix";
            checkParameters(constraintName, 4, 4, MINSTEM_A, MAXSTEM_A, MINLOOP_A, MAXLOOP_A);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            v3 = constraintVariables.getElement(2);
            v4 = constraintVariables.getElement(3);
            minStem = constraintParameters.getElement(MINSTEM_A).integerValue;
            maxStem = constraintParameters.getElement(MAXSTEM_A).integerValue;
            minLoop = constraintParameters.getElement(MINLOOP_A).integerValue;
            maxLoop = constraintParameters.getElement(MAXLOOP_A).integerValue;
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(ERRORS_A)) {
              errors = constraintParameters.getElement(ERRORS_A).integerValue;
            }
            if (constraintParameters.isSet(INDELS_A)) {
              indels = constraintParameters.getElement(INDELS_A).booleanValue;
            }
            if (constraintParameters.isSet(WOBBLE_A)) {
              wobble = constraintParameters.getElement(WOBBLE_A).booleanValue;
            }
            if (model != optionalModel) {
              network->makeHardDistance(v1, v2, minStem-1, maxStem-1);
              network->makeHardDistance(v2, v3, minLoop+1, maxLoop+1);
              network->makeHardDistance(v3, v4, minStem-1, maxStem-1);
              network->makeHardHelixSimple(v1, v2, v3, v4, minStem, maxStem, minLoop, maxLoop, errors, wobble, indels);
            }
            if (model != hardModel) {
              network->makeHelixSimple(v1, v2, v3, v4, minStem, maxStem, minLoop, maxLoop, new ErrorsToCosts(errors, constraintCosts->getCosts()), wobble, indels);
              
            }
            char mes[255];
            sprintf(mes, "\tCreating simple helix [%i, %i, %i, %i, %i, %i, %i, %s] (%s, %s, %s, %s)%s", minStem, maxStem, minLoop, maxLoop, errors, wobble, indels, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), v3->getName().c_str(), v4->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          ;}
    break;

  case 26:

/* Line 1455 of yacc.c  */
#line 848 "parseSem.y"
    {
            Variable *v1, *v2, *v3, *v4;
            int errors = 0;
            Model model = hardModel;
            bool indels = false;
            const char *constraintName = "repetition";
            int minSize, maxSize;
            int minDistance, maxDistance;
            checkParameters(constraintName, 4, 4, MINSIZE_A, MAXSIZE_A, MINDISTANCE_A, MAXDISTANCE_A);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            v3 = constraintVariables.getElement(2);
            v4 = constraintVariables.getElement(3);
            minSize = constraintParameters.getElement(MINSIZE_A).integerValue;
            maxSize = constraintParameters.getElement(MAXSIZE_A).integerValue;
            minDistance = constraintParameters.getElement(MINDISTANCE_A).integerValue;
            maxDistance = constraintParameters.getElement(MAXDISTANCE_A).integerValue;
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(ERRORS_A)) {
              errors = constraintParameters.getElement(ERRORS_A).integerValue;
            }
            if (constraintParameters.isSet(INDELS_A)) {
              indels = constraintParameters.getElement(INDELS_A).booleanValue;
            }
            if (model != optionalModel) {
              network->makeHardDistance(v1, v2, minSize-1, maxSize-1);
              network->makeHardDistance(v2, v3, minDistance+1, maxDistance+1);
              network->makeHardDistance(v3, v4, minSize-1, maxSize-1);
              network->makeHardRepetition(v1, v2, v3, v4, minSize, maxSize, minDistance, maxDistance, errors, indels);
            }
            if (model != hardModel) {
              network->makeRepetition(v1, v2, v3, v4, minSize, maxSize, minDistance, maxDistance, new ErrorsToCosts(errors, constraintCosts->getCosts()), indels);
            }
            char mes[255];
            sprintf(mes, "\tCreating repetition [%i, %i, %i, %i, %i, %i, %s] (%s, %s, %s, %s)%s", minSize, maxSize, minDistance, maxDistance, errors, indels, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), v3->getName().c_str(), v4->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          ;}
    break;

  case 27:

/* Line 1455 of yacc.c  */
#line 891 "parseSem.y"
    {
            Variable *v1, *v2, *v3, *v4;
            int errors = 0;
            Model model = hardModel;
            bool wobble = false;
            const char *constraintName = "helix";
            int maxStem;
            checkParameters(constraintName, 4, 1, MAXSTEM_A);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            v3 = constraintVariables.getElement(2);
            v4 = constraintVariables.getElement(3);
            maxStem = constraintParameters.getElement(MAXSTEM_A).integerValue;
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(ERRORS_A)) {
              errors = constraintParameters.getElement(ERRORS_A).integerValue;
            }
            if (constraintParameters.isSet(WOBBLE_A)) {
              wobble = constraintParameters.getElement(WOBBLE_A).booleanValue;
            }
            if (model != optionalModel) {
              network->makeHardHelix(v1, v2, v3, v4, maxStem, errors, wobble);
            }
            if (model != hardModel) {
              network->makeHelix(v1, v2, v3, v4, maxStem, new ErrorsToCosts(errors, constraintCosts->getCosts()), wobble);
            }
            char mes[255];
            sprintf(mes, "\tCreating helix [%i, %i, %i, %s] (%s, %s, %s, %s)%s", maxStem, wobble, errors, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), v3->getName().c_str(), v4->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          ;}
    break;

  case 28:

/* Line 1455 of yacc.c  */
#line 927 "parseSem.y"
    {
            Variable *v1, *v2, *v3, *v4;
            int errors = 0;
            Model model = hardModel;
            bool wobble = false;
            const char *constraintName = "duplex";
            checkParameters(constraintName, 4);
            v1 = constraintVariables.getElement(0);
            v2 = constraintVariables.getElement(1);
            v3 = constraintVariables.getElement(2);
            v4 = constraintVariables.getElement(3);
            if (constraintParameters.isSet(MODEL_A)) {
              model = constraintParameters.getElement(MODEL_A).modelValue;
            }
            if (constraintParameters.isSet(ERRORS_A)) {
              errors = constraintParameters.getElement(ERRORS_A).modelValue;
            }
            if (constraintParameters.isSet(WOBBLE_A)) {
              wobble = constraintParameters.getElement(WOBBLE_A).booleanValue;
            }
            if (model != optionalModel) {
              network->makeHardDuplex(v1, v2, v3, v4, errors, dupSequences[0], wobble);
            }
            if (model != hardModel) {
              network->makeDuplex(v1, v2, v3, v4, new ErrorsToCosts(errors, constraintCosts->getCosts()), dupSequences[0], wobble);
            }
            char mes[255];
            sprintf(mes, "\tCreating duplex [%i, %i, %s] (%s, %s, %s, %s)%s", wobble, errors, modelNames[(int) model], v1->getName().c_str(), v2->getName().c_str(), v3->getName().c_str(), v4->getName().c_str(), constraintCosts->print());
            trace(11, mes);
          ;}
    break;

  case 59:

/* Line 1455 of yacc.c  */
#line 1000 "parseSem.y"
    {
            constraintVariables.addElement(variables[(yyvsp[(1) - (1)].value)]);
          ;}
    break;

  case 60:

/* Line 1455 of yacc.c  */
#line 1006 "parseSem.y"
    {
            (yyval.value) = (yyvsp[(1) - (1)].value) - firstSVarIndex;
          ;}
    break;

  case 61:

/* Line 1455 of yacc.c  */
#line 1010 "parseSem.y"
    {
            (yyval.value) = (yyvsp[(1) - (1)].value) + nbSVars - firstTVarIndex;
          ;}
    break;

  case 62:

/* Line 1455 of yacc.c  */
#line 1017 "parseSem.y"
    {
            constraintParameter.modelValue = hardModel;
            constraintParameters.addElement(constraintParameter, MODEL_A);
          ;}
    break;

  case 63:

/* Line 1455 of yacc.c  */
#line 1022 "parseSem.y"
    {
            constraintParameter.modelValue = softModel;
            constraintParameters.addElement(constraintParameter, MODEL_A);
          ;}
    break;

  case 64:

/* Line 1455 of yacc.c  */
#line 1027 "parseSem.y"
    {
            constraintParameter.modelValue = optionalModel;
            constraintParameters.addElement(constraintParameter, MODEL_A);
          ;}
    break;

  case 65:

/* Line 1455 of yacc.c  */
#line 1035 "parseSem.y"
    {
            constraintParameter.stringValue = (yyvsp[(3) - (3)].word);
            constraintParameters.addElement(constraintParameter, WORD_A);
          ;}
    break;

  case 66:

/* Line 1455 of yacc.c  */
#line 1040 "parseSem.y"
    {
            constraintParameter.stringValue = (yyvsp[(4) - (5)].word);
            constraintParameters.addElement(constraintParameter, WORD_A);
          ;}
    break;

  case 67:

/* Line 1455 of yacc.c  */
#line 1048 "parseSem.y"
    {
            if (!constraintParameters.isSet(ERRORS_A)) {
              constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            }
            else {
              constraintParameter.integerValue = (yyvsp[(3) - (3)].value) + constraintParameters.getElement(ERRORS_A).integerValue;
            }
            constraintParameters.addElement(constraintParameter, ERRORS_A);
          ;}
    break;

  case 68:

/* Line 1455 of yacc.c  */
#line 1061 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            constraintParameters.addElement(constraintParameter, MINSTEM_A);
          ;}
    break;

  case 69:

/* Line 1455 of yacc.c  */
#line 1069 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            constraintParameters.addElement(constraintParameter, MAXSTEM_A);
          ;}
    break;

  case 70:

/* Line 1455 of yacc.c  */
#line 1077 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (5)].value);
            constraintParameters.addElement(constraintParameter, MINSTEM_A);
            constraintParameter.integerValue = (yyvsp[(5) - (5)].value);
            constraintParameters.addElement(constraintParameter, MAXSTEM_A);
          ;}
    break;

  case 71:

/* Line 1455 of yacc.c  */
#line 1084 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            constraintParameters.addElement(constraintParameter, MINSTEM_A);
            constraintParameters.addElement(constraintParameter, MAXSTEM_A);
          ;}
    break;

  case 72:

/* Line 1455 of yacc.c  */
#line 1093 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            constraintParameters.addElement(constraintParameter, MINLOOP_A);
          ;}
    break;

  case 73:

/* Line 1455 of yacc.c  */
#line 1101 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            constraintParameters.addElement(constraintParameter, MAXLOOP_A);
          ;}
    break;

  case 74:

/* Line 1455 of yacc.c  */
#line 1109 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (5)].value);
            constraintParameters.addElement(constraintParameter, MINLOOP_A);
            constraintParameter.integerValue = (yyvsp[(5) - (5)].value);
            constraintParameters.addElement(constraintParameter, MAXLOOP_A);
          ;}
    break;

  case 75:

/* Line 1455 of yacc.c  */
#line 1116 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            constraintParameters.addElement(constraintParameter, MINLOOP_A);
            constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            constraintParameters.addElement(constraintParameter, MAXLOOP_A);
          ;}
    break;

  case 76:

/* Line 1455 of yacc.c  */
#line 1126 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            constraintParameters.addElement(constraintParameter, MINLENGTH_A);
          ;}
    break;

  case 77:

/* Line 1455 of yacc.c  */
#line 1134 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            constraintParameters.addElement(constraintParameter, MAXLENGTH_A);
          ;}
    break;

  case 78:

/* Line 1455 of yacc.c  */
#line 1142 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (5)].value);
            constraintParameters.addElement(constraintParameter, MINLENGTH_A);
            constraintParameter.integerValue = (yyvsp[(5) - (5)].value);
            constraintParameters.addElement(constraintParameter, MAXLENGTH_A);
          ;}
    break;

  case 79:

/* Line 1455 of yacc.c  */
#line 1149 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (9)].value);
            constraintParameters.addElement(constraintParameter, MINLENGTH_A);
            constraintParameter.integerValue = (yyvsp[(5) - (9)].value);
            constraintParameters.addElement(constraintParameter, MINSOFTLENGTH_A);
            constraintParameter.integerValue = (yyvsp[(7) - (9)].value);
            constraintParameters.addElement(constraintParameter, MAXSOFTLENGTH_A);
            constraintParameter.integerValue = (yyvsp[(9) - (9)].value);
            constraintParameters.addElement(constraintParameter, MAXLENGTH_A);
          ;}
    break;

  case 80:

/* Line 1455 of yacc.c  */
#line 1160 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            constraintParameters.addElement(constraintParameter, MINLENGTH_A);
            constraintParameters.addElement(constraintParameter, MINSOFTLENGTH_A);
            constraintParameters.addElement(constraintParameter, MAXSOFTLENGTH_A);
            constraintParameters.addElement(constraintParameter, MAXLENGTH_A);
          ;}
    break;

  case 81:

/* Line 1455 of yacc.c  */
#line 1171 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            constraintParameters.addElement(constraintParameter, MINCOSTS_A);
          ;}
    break;

  case 82:

/* Line 1455 of yacc.c  */
#line 1179 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            constraintParameters.addElement(constraintParameter, MAXCOSTS_A);
          ;}
    break;

  case 83:

/* Line 1455 of yacc.c  */
#line 1187 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (5)].value);
            constraintParameters.addElement(constraintParameter, MINCOSTS_A);
            constraintParameter.integerValue = (yyvsp[(5) - (5)].value);
            constraintParameters.addElement(constraintParameter, MAXCOSTS_A);
          ;}
    break;

  case 84:

/* Line 1455 of yacc.c  */
#line 1194 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            constraintParameters.addElement(constraintParameter, MINCOSTS_A);
            constraintParameters.addElement(constraintParameter, MAXCOSTS_A);
          ;}
    break;

  case 85:

/* Line 1455 of yacc.c  */
#line 1203 "parseSem.y"
    {
            constraintParameter.booleanValue = true;
            constraintParameters.addElement(constraintParameter, INDELS_A);
          ;}
    break;

  case 86:

/* Line 1455 of yacc.c  */
#line 1208 "parseSem.y"
    {
            constraintParameter.booleanValue = false;
            constraintParameters.addElement(constraintParameter, INDELS_A);
          ;}
    break;

  case 87:

/* Line 1455 of yacc.c  */
#line 1216 "parseSem.y"
    {
            constraintParameter.integerValue = true;
            constraintParameters.addElement(constraintParameter, WOBBLE_A);
          ;}
    break;

  case 88:

/* Line 1455 of yacc.c  */
#line 1221 "parseSem.y"
    {
            constraintParameter.integerValue = false;
            constraintParameters.addElement(constraintParameter, WOBBLE_A);
          ;}
    break;

  case 89:

/* Line 1455 of yacc.c  */
#line 1229 "parseSem.y"
    {
            constraintParameter.interactionValue = WATSON_CRICK;
            if (!constraintParameters.isSet(INTERACTION1_A)) {
              constraintParameters.addElement(constraintParameter, INTERACTION1_A);
            }
            else {
              constraintParameters.addElement(constraintParameter, INTERACTION2_A);
            }
          ;}
    break;

  case 90:

/* Line 1455 of yacc.c  */
#line 1239 "parseSem.y"
    {
            constraintParameter.interactionValue = HOOGSTEEN;
            if (!constraintParameters.isSet(INTERACTION1_A)) {
              constraintParameters.addElement(constraintParameter, INTERACTION1_A);
            }
            else {
              constraintParameters.addElement(constraintParameter, INTERACTION2_A);
            }
          ;}
    break;

  case 91:

/* Line 1455 of yacc.c  */
#line 1249 "parseSem.y"
    {
            constraintParameter.interactionValue = SUGAR_EDGE;
            if (!constraintParameters.isSet(INTERACTION1_A)) {
              constraintParameters.addElement(constraintParameter, INTERACTION1_A);
            }
            else {
              constraintParameters.addElement(constraintParameter, INTERACTION2_A);
            }
          ;}
    break;

  case 92:

/* Line 1455 of yacc.c  */
#line 1262 "parseSem.y"
    {
            constraintParameter.orientationValue = CIS;
            constraintParameters.addElement(constraintParameter, ORIENTATION_A);
          ;}
    break;

  case 93:

/* Line 1455 of yacc.c  */
#line 1267 "parseSem.y"
    {
            constraintParameter.orientationValue = TRANS;
            constraintParameters.addElement(constraintParameter, ORIENTATION_A);
          ;}
    break;

  case 94:

/* Line 1455 of yacc.c  */
#line 1275 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            constraintParameters.addElement(constraintParameter, FAMILY_A);
          ;}
    break;

  case 95:

/* Line 1455 of yacc.c  */
#line 1280 "parseSem.y"
    {
            constraintParameter.integerValue = 0;
            constraintParameters.addElement(constraintParameter, FAMILY_A);
          ;}
    break;

  case 96:

/* Line 1455 of yacc.c  */
#line 1288 "parseSem.y"
    {
            constraintParameter.stringValue = (yyvsp[(3) - (3)].word);
            constraintParameters.addElement(constraintParameter, NUCLEOTIDES_A);
          ;}
    break;

  case 97:

/* Line 1455 of yacc.c  */
#line 1293 "parseSem.y"
    {
            constraintParameter.stringValue = (yyvsp[(4) - (5)].word);
            constraintParameters.addElement(constraintParameter, NUCLEOTIDES_A);
          ;}
    break;

  case 98:

/* Line 1455 of yacc.c  */
#line 1301 "parseSem.y"
    {
            constraintParameter.relationValue = NOT_LESS_THAN;
            constraintParameters.addElement(constraintParameter, RELATION_A);
            constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            constraintParameters.addElement(constraintParameter, THRESHOLD1_A);
          ;}
    break;

  case 99:

/* Line 1455 of yacc.c  */
#line 1308 "parseSem.y"
    {
            constraintParameter.relationValue = NOT_LESS_THAN;
            constraintParameters.addElement(constraintParameter, RELATION_A);
            constraintParameter.integerValue = (yyvsp[(3) - (4)].value);
            constraintParameters.addElement(constraintParameter, THRESHOLD1_A);
          ;}
    break;

  case 100:

/* Line 1455 of yacc.c  */
#line 1315 "parseSem.y"
    {
            constraintParameter.relationValue = NOT_GREATER_THAN;
            constraintParameters.addElement(constraintParameter, RELATION_A);
            constraintParameter.integerValue = (yyvsp[(3) - (3)].value);
            constraintParameters.addElement(constraintParameter, THRESHOLD1_A);
          ;}
    break;

  case 101:

/* Line 1455 of yacc.c  */
#line 1322 "parseSem.y"
    {
            constraintParameter.relationValue = NOT_GREATER_THAN;
            constraintParameters.addElement(constraintParameter, RELATION_A);
            constraintParameter.integerValue = (yyvsp[(3) - (4)].value);
            constraintParameters.addElement(constraintParameter, THRESHOLD1_A);
          ;}
    break;

  case 102:

/* Line 1455 of yacc.c  */
#line 1329 "parseSem.y"
    {
            constraintParameter.relationValue = NOT_LESS_THAN;
            constraintParameters.addElement(constraintParameter, RELATION_A);
            constraintParameter.integerValue = (yyvsp[(3) - (5)].value);
            constraintParameters.addElement(constraintParameter, THRESHOLD1_A);
            constraintParameter.integerValue = (yyvsp[(5) - (5)].value);
            constraintParameters.addElement(constraintParameter, THRESHOLD2_A);
          ;}
    break;

  case 103:

/* Line 1455 of yacc.c  */
#line 1338 "parseSem.y"
    {
            constraintParameter.relationValue = NOT_LESS_THAN;
            constraintParameters.addElement(constraintParameter, RELATION_A);
            constraintParameter.integerValue = (yyvsp[(3) - (6)].value);
            constraintParameters.addElement(constraintParameter, THRESHOLD1_A);
            constraintParameter.integerValue = (yyvsp[(5) - (6)].value);
            constraintParameters.addElement(constraintParameter, THRESHOLD2_A);
          ;}
    break;

  case 104:

/* Line 1455 of yacc.c  */
#line 1347 "parseSem.y"
    {
            constraintParameter.relationValue = NOT_GREATER_THAN;
            constraintParameters.addElement(constraintParameter, RELATION_A);
            constraintParameter.integerValue = (yyvsp[(3) - (5)].value);
            constraintParameters.addElement(constraintParameter, THRESHOLD1_A);
            constraintParameter.integerValue = (yyvsp[(5) - (5)].value);
            constraintParameters.addElement(constraintParameter, THRESHOLD2_A);
          ;}
    break;

  case 105:

/* Line 1455 of yacc.c  */
#line 1356 "parseSem.y"
    {
            constraintParameter.relationValue = NOT_GREATER_THAN;
            constraintParameters.addElement(constraintParameter, RELATION_A);
            constraintParameter.integerValue = (yyvsp[(3) - (6)].value);
            constraintParameters.addElement(constraintParameter, THRESHOLD1_A);
            constraintParameter.integerValue = (yyvsp[(5) - (6)].value);
            constraintParameters.addElement(constraintParameter, THRESHOLD2_A);
          ;}
    break;

  case 106:

/* Line 1455 of yacc.c  */
#line 1368 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (5)].value);
            constraintParameters.addElement(constraintParameter, MINSIZE_A);
            constraintParameter.integerValue = (yyvsp[(5) - (5)].value);
            constraintParameters.addElement(constraintParameter, MAXSIZE_A);
          ;}
    break;

  case 107:

/* Line 1455 of yacc.c  */
#line 1378 "parseSem.y"
    {
            constraintParameter.integerValue = (yyvsp[(3) - (5)].value);
            constraintParameters.addElement(constraintParameter, MINDISTANCE_A);
            constraintParameter.integerValue = (yyvsp[(5) - (5)].value);
            constraintParameters.addElement(constraintParameter, MAXDISTANCE_A);
          ;}
    break;

  case 108:

/* Line 1455 of yacc.c  */
#line 1388 "parseSem.y"
    {
            char str[521];
            sprintf(str, "Error! Do not understand '%s'!", (yyvsp[(1) - (1)].word));
            yyerror((char*) str);
          ;}
    break;

  case 109:

/* Line 1455 of yacc.c  */
#line 1394 "parseSem.y"
    {
            char str[521];
            sprintf(str, "Error! Do not understand '%s'!", (yyvsp[(1) - (3)].word));
            yyerror((char*) str);
          ;}
    break;

  case 110:

/* Line 1455 of yacc.c  */
#line 1400 "parseSem.y"
    {
            char str[521];
            sprintf(str, "Error! Do not understand '%s'!", (yyvsp[(1) - (5)].word));
            yyerror((char*) str);
          ;}
    break;

  case 111:

/* Line 1455 of yacc.c  */
#line 1406 "parseSem.y"
    {
            char str[521];
            sprintf(str, "Error! Do not understand '%s'!", (yyvsp[(1) - (9)].word));
            yyerror((char*) str);
          ;}
    break;

  case 112:

/* Line 1455 of yacc.c  */
#line 1412 "parseSem.y"
    {
            char str[521];
            sprintf(str, "Error! Do not understand '%s'!", (yyvsp[(1) - (3)].word));
            yyerror((char*) str);
          ;}
    break;

  case 117:

/* Line 1455 of yacc.c  */
#line 1431 "parseSem.y"
    {
            constraintCosts->addElement((yyvsp[(1) - (1)].value));
          ;}
    break;



/* Line 1455 of yacc.c  */
#line 3232 "parseSem.cc"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }

  yyerror_range[0] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval, &yylloc);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  yyerror_range[0] = yylsp[1-yylen];
  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      yyerror_range[0] = *yylsp;
      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp, yylsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;

  yyerror_range[1] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the lookahead.  YYLOC is available though.  */
  YYLLOC_DEFAULT (yyloc, (yyerror_range - 1), 2);
  *++yylsp = yyloc;

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval, &yylloc);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp, yylsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 1675 of yacc.c  */
#line 1442 "parseSem.y"


int messageErrorMissingParameter(const char constraint[], const char arg[]) {
  char str[512];
  sprintf(str, "Error! Missing '%s' argument in %s", arg, constraint);
  return yyerror((char*) str);
}

int messageErrorMissingVariable(const char constraint[], int arg) {
  char str[512];
  sprintf(str, "Error! Missing %ith variable in %s", arg, constraint);
  return yyerror((char*) str);
}

int	yyerror (char msg[]) {
  char str[512];
  sprintf(str, "Parse error in the descriptor: %s.\n", msg);
	trace(0, str);
	exit(1);
	return false;
}

