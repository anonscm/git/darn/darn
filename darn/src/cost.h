// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef COST_H_INCLUDED
#define COST_H_INCLUDED

#include "common.h"
#include "support.h"


/**
 * The CostValues contains all Cost needs to save and restore while backtracking.
 */
struct CostValues {
  int nbVariables;
  int mem, **memProf;
  int *lbMem, *ubMem;
  int *lb, *ub;
  bool *discretized;
  CostValues (int nv) : nbVariables(nv) {
    discretized = new bool[nbVariables];
    lbMem = new int[nbVariables];
    ubMem = new int[nbVariables];
    lb = new int[nbVariables];
    ub = new int[nbVariables];
    memProf = new int*[nbVariables];
    for (int i = 0; i < nbVariables; i++) {
      memProf[i] = new int[MAXSIZE];
    }
  }
  ~CostValues () {
    /*
    for (int i = 0; i < nbVariables; i++) {
      delete[] memProf[i];
    }
    delete[] discretized;
    delete[] lbMem;
    delete[] ubMem;
    delete[] lb;
    delete[] ub;
    delete[] memProf;
    */
  }
};


/**
 * The Cost class implements all the mecanism for storing the modifications of the costs for the lowest common denominator of the constraints.
 */
class Cost {

protected:
  /**
   * a number that represent the name or the index of this constraint
   */
  int index;
  /**
   * the number of variables involved
   */
  int nbVariables;
  /**
   * whether the variables are discretized
   */
  bool *discretized;
  /**
   * the memory of the cost -- what has been projected to the zero arity constraint
   */
  int mem;
  /**
   * when a variable is discretized, its memory profile -- what has been projected from the constraint to an element of this variable
   */
  int **memProf;
  /**
   * when a variable is discretized, the beginning of its memory profile (not less than its lower bound)
   */
  int *lb;
  /**
   * when a variable is discretized, the end of its memory profile (not greater than its upper bound)
   */
  int *ub;
  /**
   * when a variable is not discretized, the memory of its lower bound -- what has been projected from the constraint to this lower bound
   */
  int *lbMem;
  /**
   * when a variable is not discretized, the memory of its upper bound -- what has been projected from the constraint to this upper bound
   */
  int *ubMem;
  /**
   * the support of the constraint (for zero-inversibility)
   */
  Support *zeroSupport;
  /**
   * the supports of the lower bounds of the not-discretized domains
   */
  Support **lbSupports;
  /**
   * the supports of the upper bounds of the not-discretized domains
   */
  Support **ubSupports;
  /**
   * the support of the discretized variables
   */
  Support ***supports;

  /**
   * the number of stored values
   */
  int nbStoredValues;
  /**
   * the vector that contains the values (@a mem) that the class should save for backtracking safely
   */
  CostValues **memories;
  
public:
  /**
   * the constructor
   * @param i the index of the constrain (related to @a index)
   * @param nv the number of variables
   */
  Cost (int i, int nv);
  /**
   * destructor
   */
  virtual ~Cost ();
  /**
   * prepare cost for a new search
   */
  virtual void reset ();
  /**
   * whether the memory profile of a variable is discretized
   * @param i the index of the variable
   * @return true if the given memory profile is discretized
   */
  virtual bool isDiscretized (int i);
  /**
   * creates modify the bounds and eventually the memory profile of a variable
   * @param i the index of the variable
   * @param val the value the variable is assigned to
   * @param c the cost the value takes
   */
  virtual void assign (int i, int val, int c);
  /**
   * @pre the specified variable is not discretized
   * 
   * creates the memory profile of a variable
   * @param i the index of the variable
   * @param lb its lower bound
   * @param ub its upper bound
   */
  virtual void setDiscretized (int i, int lb, int ub);
  /**
   * get the memory of the cost -- what has been projected to the zero arity constraint
   * @return the memory
   */
  virtual int getMem ();
  /**
   * set the memory of the cost -- what has been projected to the zero arity constraint
   * @param m the new memory
   */
  virtual void setMem (int m);
  /**
   * get the memory of the cost -- what has been projected directly to the zero arity constraint
   * @return the cost
   */
  virtual int getMemProfile (int i, int value);
  /**
   * @pre the specified variable is discretized
   * 
   * set a memory of the cost -- what has been projected from the constraint to an element of a variable
   * @param i the index of the variable
   * @param value the element of the variable
   * @param m the new cost
   */
  virtual void setMemProfile (int i, int value, int m);
  /**
   * @pre the specified variable is not discretized
   * 
   * get a memory of the cost -- what has been projected from the constraint to the lower bound of a variable
   * @param i the index of the variable
   * @return the cost
   */
  virtual int getLbMem (int i);
  /**
   * @pre the specified variable is not discretized
   * 
   * get a memory of the cost -- what has been projected from the constraint to the upper bound of a variable
   * @param i the index of the variable
   * @return the cost
   */
  virtual int getUbMem (int i);
  /**
   * @pre the specified variable is not discretized
   * 
   * set a memory of the cost -- what has been projected from the constraint to the lower bound of a variable
   * @param i the index of the variable
   * @param m the new cost
   */
  virtual void setLbMem (int i, int m);
  /**
   * @pre the specified variable is not discretized
   * 
   * increase a memory of the cost -- what has been projected from the constraint to the lower bound of a variable
   * @param i the index of the variable
   * @param m the increase
   */
  virtual void setUbMem (int i, int m);
  /**
   * get the support of the zero-inversibility of the constraint
   * @return the support
   */
  virtual Support *getZeroSupport ();
  /**
   * get the support of the lower bound of a given variable
   * @param v the variable
   * @return the support
   */
  virtual Support *getLbSupport (int v);
  /**
   * get the support of the upper bound of a given variable
   * @param v the variable
   * @return the support
   */
  virtual Support *getUbSupport (int v);
  /**
   * get the support of a value of a given variable
   * @param v the variable
   * @param i the value
   * @return the support
   */
  virtual Support *getSupport (int v, int i);

  /**
   * save the current values in a stack
   */
  virtual void save ();
  /**
   * restore the current values in a stack
   */
  virtual void restore ();
};

#endif
