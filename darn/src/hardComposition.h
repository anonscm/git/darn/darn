// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef HCOMP_H_INCLUDED
#define HCOMP_H_INCLUDED

#include "sequence.h"
#include "hardConstraint.h"


/**
 * This class ensures the proportion of dinucleotides (usually G+C or A+T) is not less
 *   or not greater than a given threshold.
 */
class HardComposition : public HardConstraint {

protected:
  
  /**
   * the first nucleotide considered
   */
  Nucleotide *nucleotide1;
  /**
   * the second nucleotide considered
   */
  Nucleotide *nucleotide2;
  /**
   * the threshold of the proportion of dinucleotides
   */
  int threshold;
  /**
   * whether the proportion should be not less or not greater that the threshold
   */
  Relation relation;

  /**
   * check whether the given nucleotide is one of the dinucleotides
   * @param n the given nucleotide
   * @return true, if the given nucleotide is one of the dinucleotides
   */
  inline bool isCorrectNucleotide (Nucleotide *n);
  /**
   * check if the ration @a nbPositives over @a nbTotal satisfies the constraint
   * @param nbPositives is the number of nucleotides similar to @a nucleotide1 or @a nucleotide2
   * @param nbTotal is the total number of nucleotides 
   * @return true if the ration satisfies the constraint
   */
  inline bool checkRatio (int nbPositives, int nbTotal);
  /**
   * @pre at least one variable is assigned
   * check if there is a valid assignment
   * @param var indicates which variable has been assigned
   * @param bound indicates which bound(s) of the domain of the unassigned variable has moved (0 = lower bound, 1 = upper bound, 2 = both bounds)
   * @return the new bounds
   */
  pair<int, int> isConsistent (int var, int bound);


public:
  /**
   * the constructor
   * @param v1 the start position of the interval
   * @param v2 the end position of the interval
   * @param i the index of this constraint
   * @param n1 the first nucleotide
   * @param n2 the second nucleotide
   * @param t the threshold
   * @param r the relation
   */
  HardComposition (Variable *v1, Variable *v2, int i, Nucleotide *n1, Nucleotide *n2, int t, Relation r);
  /**
   * revision of the constraint, when no information is given on what led to the revision
   */
  virtual void revise ();
  /**
   * revision of the constraint, due to a given variable
   * @param v the index of the given variable
   */
  virtual void revise (int v);
  /**
   * revision of the constraint, when the lower bound of a variable has changed
   * @param v the index of the given variable
   */
  virtual void reviseFromLb (int v);
  /**
   * revision of the constraint, when the upper bound of a variable has changed
   * @param v the given variable
   */
  virtual void reviseFromUb (int v);
  /**
   * revision of the constraint, when the upper bound of a variable has been assigned
   * @param v the given variable
   */
  virtual void reviseFromAssignment (int v);
  /**
   * verfiy that the support is still valid
   * @param s the support
   */
  virtual bool getConsistency (Support *s);
};

#endif

