// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "hardPattern.h"


HardPattern::HardPattern (Variable *v1, int i, string s, int mc, MatchingValues *mv) : HardConstraint(i, mc, 1, v1) {
  sequence = v1->getSequence();
  pattern = new Sequence(s, false, false);
  for (unsigned int i = 0; i < pattern->getLength(); i++) {
    if (pattern->getNucleotide(i) != nucleotideN) {
      ++hardness;
    }
  }
  matrix = new int[pattern->getLength()+1];
  matchingValues = mv;
  name = "hard pattern";
}

bool HardPattern::isConsistent (int i1) {
  int l1 = pattern->getLength();
  int l2 = ((i1+l1+(maxCost+1)/matchingValues->getMaxCost() < static_cast<int>(sequence->getLength()))? l1+(maxCost+1)/matchingValues->getMaxCost(): static_cast<int>(sequence->getLength())-i1);
  int value, match, min;
  int prec;
  int start, stop;
  if (l2 <= 0) {
    return false;
  }
  // if no error is allowed
  if (maxCost == 0) {
    if (static_cast<unsigned int>(i1 + l1) > sequence->getLength()) {
      return false;
    }
    for (int i = 0; i < l1; i++) {
      if (matchingValues->getSubstitutionCost(pattern->getNucleotide(i), sequence->getNucleotide(i1+i)) != 0) {
        return false;
      }
    }
    return true;
  }
  // if errors are allowed
  start = 0;
  stop = min2((maxCost+1)/matchingValues->getMaxCost(), l1);
  for (int i = 0; i <= l1; i++) {
    matrix[i] = i*matchingValues->getInsertionCost();
  }
  for (int j = 1; j <= l2; j++) {
    start = max2(1, j - (maxCost+1)/matchingValues->getMaxCost());
    stop = min2(l1, j + (maxCost+1)/matchingValues->getMaxCost());
    if (start == 1) {
      prec = (j-1)*matchingValues->getInsertionCost();
    }
    else {
      prec = matrix[start-1];
    }
    min = j*matchingValues->getInsertionCost();
    for (int i = start; i <= stop; i++) {
      match = matchingValues->getSubstitutionCost(pattern->getNucleotide(i-1), sequence->getNucleotide(j+i1-1));
      value = min3(prec+match, matrix[i]+matchingValues->getInsertionCost(), matrix[i-1]+matchingValues->getInsertionCost());
      prec =  matrix[i];
      matrix[i] = value;
      if (value < min) {
        min = value;
      }
    }
    if ((start <= l1) && (stop >= l1)) {
      if (matrix[l1] <= maxCost) {
        return true;
      }
    }
    if (min > maxCost) {
      return false;
    }
  }
  return false;
}

void HardPattern::revise () {
  revise(0);
}

void HardPattern::revise (int v) {
  reviseFromLb(v);
  reviseFromUb(v);
}

void HardPattern::reviseFromAssignment (int v) {
  int lb = unaryConstraints[0]->getLb();
  int ub = unaryConstraints[0]->getUb();
  if (checkLbSupport(v) || checkUbSupport(v)) {
    return;
  }
  lbSupports[0]->unset();
  ubSupports[0]->unset();
  if (isConsistent(lb)) {
    lbSupports[0]->setSupport(0, lb);
    ubSupports[0]->setSupport(0, lb);
    return;
  }
  unaryConstraints[0]->setLb(ub);
}

void HardPattern::reviseFromLb (int v) {
  DomainIterator di = unaryConstraints[0]->getDomainIterator();
  int ub = unaryConstraints[0]->getUb();
  if (checkLbSupport(v)) {
    return;
  }
  lbSupports[0]->unset();
  for (; *di < ub; ++di) {
    if (isConsistent(*di)) {
      lbSupports[0]->setSupport(0, *di);
      unaryConstraints[0]->setLb(*di);
      return;
    }
  }
  unaryConstraints[0]->setLb(ub);
}

void HardPattern::reviseFromUb (int v) {
  DomainIterator di = unaryConstraints[0]->getReverseDomainIterator();
  int lb = unaryConstraints[0]->getLb();
  if (checkUbSupport(v)) {
    return;
  }
  ubSupports[0]->unset();
  for (; *di >= lb; --di) {
    if (isConsistent(*di)) {
      ubSupports[0]->setSupport(0, *di);
      unaryConstraints[0]->setUb((*di)+1);
      return;
    }
  }
  unaryConstraints[0]->setUb(lb);
}

bool HardPattern::getConsistency (Support *s) {
  return (isConsistent(s->getSupport(0)));
}

bool HardPattern::printSpace (int var, int place) {
  return (place == 0);
}
