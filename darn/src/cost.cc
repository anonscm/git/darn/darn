// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "cost.h"


Cost::Cost (int i, int nv) : index(i), nbVariables(nv) {
  mem = 0;
  nbStoredValues = 0;
  discretized = new bool[nbVariables];
  memProf = new int*[nbVariables];
  lbMem = new int[nbVariables];
  ubMem = new int[nbVariables];
  lb = new int[nbVariables];
  ub = new int[nbVariables];
  zeroSupport = new Support(nbVariables);
  supports = new Support**[nbVariables];
  lbSupports = new Support*[nbVariables];
  ubSupports = new Support*[nbVariables];
  for (int i = 0; i < nbVariables; i++) {
    discretized[i] = false;
    lbMem[i] = 0;
    ubMem[i] = 0;
    memProf[i] = new int[MAXSIZE];
    lbSupports[i] = new Support(nbVariables);
    ubSupports[i] = new Support(nbVariables);
    supports[i] = new Support*[MAXSIZE];
    for (unsigned int j = 0; j < MAXSIZE; j++) {
      supports[i][j] = new Support(nbVariables);
    }
  }
  memories = new CostValues*[nbTotVariables];
  for (int i = 0; i < nbTotVariables; i++) {
    memories[i] = new CostValues(nbVariables);
  }
}

Cost::~Cost () {
  /*
  for (int i = 0; i < nbVariables; i++) {
    for (unsigned int j = 0; j < MAXSIZE; j++) {
      delete supports[i][j];
    }
    delete[] memProf[i];
    delete lbSupports[i];
    delete ubSupports[i];
    delete[] supports[i];
  }
  for (int i = 0; i < nbTotVariables; i++) {
    delete memories[i];
  }
  delete[] discretized;
  delete[] memProf;
  delete[] lbMem;
  delete[] ubMem;
  delete[] lb;
  delete[] ub;
  delete zeroSupport;
  delete[] supports;
  delete[] lbSupports;
  delete[] ubSupports;
  delete[] memories;
  */
}

void Cost::reset () {
  mem = 0;
  nbStoredValues = 0;
  zeroSupport->reset();
  for (int i = 0; i < nbVariables; i++) {
    discretized[i] = false;
    lbMem[i] = 0;
    ubMem[i] = 0;
    lbSupports[i]->reset();
    ubSupports[i]->reset();
    for (unsigned int j = 0; j < MAXSIZE; j++) {
      supports[i][j]->reset();
    }
  }
}

bool Cost::isDiscretized (int i) {
  if ((i < 0) || (i > nbVariables)) {
    trace(1, "Error! Trying to discretize a non-existing variable!");
    return false;
  }
  return discretized[i];
}

void Cost::assign (int i, int val, int c) {
  if ((i < 0) || (i >= nbVariables)) {
    trace(1, "Error! Trying to assign a non-existing variable!");
    return;
  }
  if (discretized[i]) {
    return;
  }
  lb[i] = val;
  ub[i] = val+1;
  memProf[i][0] = c;
  discretized[i] = true;
}

void Cost::setDiscretized (int i, int l, int u) {
  if ((i < 0) || (i >= nbVariables)) {
    trace(1, "Error! Trying to discretize a non-existing variable!");
    return;
  }
  if (discretized[i]) {
    return;
  }
  lb[i] = l;
  ub[i] = u;
  for (int j = 1; j < ub[i]-lb[i]-1; j++) {
    memProf[i][j] = 0;
  }
  memProf[i][0] = lbMem[i];
  memProf[i][ub[i]-lb[i]-1] = ubMem[i];
  discretized[i] = true;
}

int Cost::getMem () {
  return mem;
}

void Cost::setMem (int m) {
  mem = m;
}

int Cost::getMemProfile (int i, int value) {
  if ((i < 0) || (i > nbVariables)) {
    trace(1, "Error! Trying to read a non-existing variable!");
    return -1;
  }
  if (!discretized[i]) {
    trace(1, "Error: trying to read a non-existing memory!");
    return -1;
  }
  if ((value < lb[i]) || (value >= ub[i])) {
    trace(1, "Error: trying to read a non-existing memory!");
    return -1;
  }
  return memProf[i][value-lb[i]];
}

void Cost::setMemProfile (int i, int value, int m) {
  if ((i < 0) || (i > nbVariables)) {
    trace(1, "Error! Trying to modify a non-existing variable!");
    return;
  }
  if (!discretized[i]) {
    trace(1, "Error: trying to modify a non-existing memory!");
    return;
  }
  if ((value < lb[i]) || (value >= ub[i])) {
    trace(1, "Error: trying to modify a non-existing memory!");
    return;
  }
  memProf[i][value-lb[i]] = m;
  return;
}

int Cost::getLbMem (int i) {
  if ((i < 0) || (i > nbVariables)) {
    trace(1, "Error! Trying to read a non-existing variable!");
    return -1;
  }
  if (discretized[i]) {
    trace(1, "Error: trying to read a non-existing lb memory!");
    return -1;
  }
  return lbMem[i];
}

int Cost::getUbMem (int i) {
  if ((i < 0) || (i > nbVariables)) {
    trace(1, "Error! Trying to read a non-existing variable!");
    return -1;
  }
  if (discretized[i]) {
    trace(1, "Error: trying to read a non-existing ub memory!");
    return -1;
  }
  return ubMem[i];
}

void Cost::setLbMem (int i, int m) {
  if ((i < 0) || (i > nbVariables)) {
    trace(1, "Error! Trying to modify a non-existing variable!");
    return;
  }
  if (discretized[i]) {
    trace(1, "Error: trying to modify a non-existing lb memory!");
    return;
  }
  lbMem[i] = m;
}

void Cost::setUbMem (int i, int m) {
  if ((i < 0) || (i > nbVariables)) {
    trace(1, "Error! Trying to modify a non-existing variable!");
    return;
  }
  if (discretized[i]) {
    trace(1, "Error: trying to modify a non-existing ub memory!");
    return;
  }
  ubMem[i] = m;
}

Support *Cost::getZeroSupport () {
  return zeroSupport;
}

Support *Cost::getLbSupport (int v) {
  if ((v < 0) || (v > nbVariables)) {
    trace(1, "Error! Trying to get a non existing support!");
    return NULL;
  }
  if (discretized[v]) {
    trace(1, "Error! Trying to get the lb support of a discretized variable!");
    return NULL;
  }
  return lbSupports[v];
}

Support *Cost::getUbSupport (int v) {
  if ((v < 0) || (v > nbVariables)) {
    trace(1, "Error! Trying to get a non existing support!");
    return NULL;
  }
  if (discretized[v]) {
    trace(1, "Error! Trying to get the lb support of a discretized variable!");
    return NULL;
  }
  return ubSupports[v];
}

Support *Cost::getSupport (int v, int i) {
  if ((v < 0) || (v > nbVariables)) {
    trace(1, "Error! Trying to get a non existing support!");
    return NULL;
  }
  if (!discretized[v]) {
    trace(1, "Error! Trying to get the support of a non discretized variable!");
    return NULL;
  }
  if ((i < lb[v]) || (i >= ub[v])) {
    trace(1, "Error! Trying to get an out of range support!");
    return NULL;
  }
  return supports[v][i-lb[v]];
}

void Cost::save () {
  zeroSupport->save();
  for (int i = 0; i < nbVariables; i++) {
    lbSupports[i]->save();
    ubSupports[i]->save();
    for (unsigned int j = 0; j < MAXSIZE; j++) {
      supports[i][j]->save();
    }
  }
  memories[nbStoredValues]->mem = mem;
  for (int i = 0; i < nbVariables; i++) {
    memories[nbStoredValues]->discretized[i] = discretized[i];
    if (!discretized[i]) {
      memories[nbStoredValues]->lbMem[i] = lbMem[i];
      memories[nbStoredValues]->ubMem[i] = ubMem[i];
    }
    else {
      memories[nbStoredValues]->lb[i] = lb[i];
      memories[nbStoredValues]->ub[i] = ub[i];
      for (int j = 0; j < ub[i]-lb[i]; j++) {
        memories[nbStoredValues]->memProf[i][j] = memProf[i][j];
      }
    }
  }
  nbStoredValues++;
}

void Cost::restore () {
  nbStoredValues--;
  zeroSupport->restore();
  for (int i = 0; i < nbVariables; i++) {
    lbSupports[i]->restore();
    ubSupports[i]->restore();
    for (unsigned int j = 0; j < MAXSIZE; j++) {
      supports[i][j]->restore();
    }
  }
  mem = memories[nbStoredValues]->mem;
  for (int i = 0; i < nbVariables; i++) {
    discretized[i] = memories[nbStoredValues]->discretized[i];
    if (!discretized[i]) {
      lbMem[i] = memories[nbStoredValues]->lbMem[i];
      ubMem[i] = memories[nbStoredValues]->ubMem[i];
    }
    else {
      lb[i] = memories[nbStoredValues]->lb[i];
      ub[i] = memories[nbStoredValues]->ub[i];
      for (int j = 0; j < ub[i]-lb[i]; j++) {
        memProf[i][j] = memories[nbStoredValues]->memProf[i][j];
      }
    }
  }
}
