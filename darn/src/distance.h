// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef DIST_H_INCLUDED
#define DIST_H_INCLUDED

#include "softConstraint.h"

/**
 * The Distance is a Nary (Binary) Constraint. It gives @a maxCost if the variables are too close (less than @a dist1) or too far away (greater than @a dist4) from each other, @a minCost if the variable have the good distance to each other (between @a dist2 and @a dist3). Between @a dist1 and @a dist2 on the one hand, and between @a dist3 and @a dist4 on the other hand, the cost profile is affine.
 */
class Distance : public SoftConstraint {

protected:
  /**
   * the first given distance
   */
  int dist1;
  /**
   * the second given distance, supposed to be greater than @a dist1
   */
  int dist2;
  /**
   * the third given distance, supposed to be greater than @a dist2
   */
  int dist3;
  /**
   * the fourth given distance, supposed to be greater than @a dist3
   */
  int dist4;
  /**
   * the minimum cost given by this constraint
   */
  int minCost;

  /**
   * computes the minimum cost of the function (excluding the costs of the projections), given the bounds of the variables
   * take constant time
   * @param min1 the lower bound of the first variable
   * @param max1 the upper bound of the first variable
   * @param min2 the lower bound of the second variable
   * @param max2 the upper bound of the second variable
   * @return the minimum cost
   */
  int computeCostUndiscretized (int min1, int max1, int min2, int max2);
  /**
   * computes the minimum cost of the function (including the costs of the projections), given the bounds of the variables
   * take linear time
   * @param min1 the lower bound of the first variable
   * @param max1 the upper bound of the first variable
   * @param min2 the lower bound of the second variable
   * @param max2 the upper bound of the second variable
   * @return the minimum cost
   */
  int computeCost (int min1, int max1, int min2, int max2);
  /**
   * computes the cost of the function (including the costs of the projections), given the values of the variables
   * @param i1 the value of the first variable
   * @param i2 the value of the second variable
   * @return the cost
   */
  int computeCost (int i1, int i2);


public:
  /**
   * the constructor
   * @param v1 the first variable
   * @param v2 the second variable
   * @param i the index of this constraint
   * @param o whether the distance is optional
   * @param d1 the first distance
   * @param d2 the second distance
   * @param d3 the third distance
   * @param d4 the fourth distance
   * @param c1 the minimum cost
   * @param e2c the convertor from errors to costs
   */
  Distance (Variable *v1, Variable *v2, int i, bool o, int d1, int d2, int d3, int d4, int c1, ErrorsToCosts *e2c);

  /**
   * get the minimum of the function cost
   * @return the minimum
   */
  virtual int getMinCost ();
  /**
   * @pre The variable @a v should be discretized
   *
   * get the minimum of the function cost, having that the given variable has been assigned to the given value
   * @param v the given variable
   * @param val the given element
   * @return the minimum
   */
  virtual int getMinCost (Variable *v, int val);
  /**
   * @pre The variable @a v should be not discretized
   *
   * compute the minimum cost of the constraint, having that the specified variable is assigned to the lower bound
   * @param v the specified variable
   * @return the minimum
   */
  virtual int getMinLbCost (Variable *v);
  /**
   * @pre The variable @a v should be not discretized
   *
   * compute the minimum cost of the constraint, having that the specified variable is assigned to the upper bound
   * @param v the specified variable
   * @return the minimum
   */
  virtual int getMinUbCost (Variable *v);
  /**
   * handle the consequence of an increase of the left bound of the variable @a v
   * @param v the variable of which the left bound has increased
   */
  //virtual void reviseFromLb (Variable *v);
  /**
   * handle the consequence of an increase of the right bound of the variable @a v
   * @param v the variable of which the right bound has increased
   */
  //virtual void reviseFromUb (Variable *v);
  /**
   * handle the consequence of an assignment of the variable @a v
   * @param v the variable that has been assigned
   */
  //virtual void reviseFromAssignment (Variable *v);

  /**
   * compute the cost of the given support
   * @param s the support
   * @return the cost
   */
  virtual int getCost (Support *s);
  /**
   * get the contribution of this constraint to the global cost when all variables are assigned
   * @param values the values of the involved variables
   * @return the cost of this constraint
   */
  virtual int getFinalCost (int *values);
};

#endif
