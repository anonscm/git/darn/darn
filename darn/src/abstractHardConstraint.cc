// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "abstractHardConstraint.h"


AbstractHardConstraint::AbstractHardConstraint (int i, int mc, int nv) : AbstractConstraint(i, mc, nv) {}

AbstractHardConstraint::~AbstractHardConstraint () {}

void AbstractHardConstraint::revise () {}

void AbstractHardConstraint::revise (int v) {}

void AbstractHardConstraint::revise (Variable *v) {
  revise(getVariableIndex(v));
}

void AbstractHardConstraint::reviseFromLb (int v) {}

void AbstractHardConstraint::reviseFromLb (Variable *v) {
  reviseFromLb(getVariableIndex(v));
}

void AbstractHardConstraint::reviseFromUb (int v) {}

void AbstractHardConstraint::reviseFromUb (Variable *v) {
  reviseFromUb(getVariableIndex(v));
}

void AbstractHardConstraint::reviseFromAssignment (Variable *v) {
  reviseFromAssignment(getVariableIndex(v));
}

void AbstractHardConstraint::reviseFromAssignment (int v) {}

bool AbstractHardConstraint::checkLbSupport (int v) {
  bool c = true;
  if (!lbSupports[v]->isSet()) {
    return false;
  }
  for (int i = 0; i < nbVariables; i++) {
    if (!getDomain(i)->contains(lbSupports[v]->getSupport(i))) {
      return false;
    }
  }
  if (traceLevel > 1) {
    c = getConsistency(lbSupports[v]);
    if (!c) {
      ostringstream s;
      s << "Error! The lbSupport of " << getVariable(v)->getName() << " is not consistent anymore ( ";
      for (int i = 0; i < nbVariables; i++) {
        s << getVariable(i)->print() << " = " << lbSupports[v]->getSupport(i) << "  ";
      }
      trace (1, s.str() + ")!");
    }
  }
  return (c);
}

bool AbstractHardConstraint::checkUbSupport (int v) {
  bool c = true;
  if (!ubSupports[v]->isSet()) {
    return false;
  }
  for (int i = 0; i < nbVariables; i++) {
    if (!getDomain(i)->contains(ubSupports[v]->getSupport(i))) {
      return false;
    }
  }
  if (traceLevel > 1) {
    c = getConsistency(ubSupports[v]);
    if (!c) {
      ostringstream s;
      s << "Error! The ubSupport of " << getVariable(v)->getName() << " is not consistent anymore ( ";
      for (int i = 0; i < nbVariables; i++) {
        s << getVariable(i)->print() << " = " << ubSupports[v]->getSupport(i) << "  ";
      }
      trace (1, s.str() + ")!");
    }
  }
  return (c);
}

bool AbstractHardConstraint::getConsistency (Support *s) {
  return false;
}

void AbstractHardConstraint::getTrace (int start, int *values, bool **backtrace) { }

void AbstractHardConstraint::save () {
  for (int i = 0; i < nbVariables; i++) {
    lbSupports[i]->save();
    ubSupports[i]->save();
  }
}

void AbstractHardConstraint::restore () {
  for (int i = 0; i < nbVariables; i++) {
    lbSupports[i]->restore();
    ubSupports[i]->restore();
  }
}
