// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef NCPAIR_H_INCLUDED
#define NCPAIR_H_INCLUDED

#include "values.h"
#include "softConstraint.h"

/**
 * The NonCanonicPair is a Nary (Binary) Constraint. It forces two nucleotides to match through a given (non canonic) way.
 */
class NonCanonicPair : public SoftConstraint {

protected:
  /**
   * the interaction of the first nucleotide
   */
  Interaction interaction1;
  /**
   * the interaction of the second nucleotide
   */
  Interaction interaction2;
  /**
   * the orientation of the nucleotides
   */
  Orientation orientation;
  /**
   * the isostericity family
   */
  int family;
  /**
   * isostericity values
   */
  IsostericityValues *isostericityValues;
  /**
   * computes the minimum cost of the function (including the costs of the projections), given the bounds of the variables
   * take linear time
   * @param min1 the lower bound of the first variable
   * @param max1 the upper bound of the first variable
   * @param min2 the lower bound of the second variable
   * @param max2 the upper bound of the second variable
   * @return the minimum cost
   */
  int computeCost (int min1, int max1, int min2, int max2);
  /**
   * computes the cost of the function (forgetting the costs of the projections), given the values of the variables
   * @param i1 the value of the first variable
   * @param i2 the value of the second variable
   * @return the cost
   */
  int computeCost (int i1, int i2);


public:
  /**
   * the constructor
   * @param v1 the first variable (the lower bound of the first sequence)
   * @param v2 the second variable (the upper bound of the first sequence)
   * @param i the index of this constraint
   * @param o whether the constraint is optional
   * @param it1 the interaction of the first nucleotide
   * @param it2 the interaction of the second nucleotide
   * @param ori the orientation of the nucleotides
   * @param f the isostericity family
   * @param e2c the convertor from errors to costs
   * @param iv the isostericity values
   */
  NonCanonicPair (Variable *v1, Variable *v2, int i, bool o, Interaction it1, Interaction it2, Orientation ori, int f, ErrorsToCosts *e2c, IsostericityValues *iv);

  /**
   * get the minimum of the function cost
   * @return the minimum
   */
  virtual int getMinCost ();
  /**
   * @pre The variable @a v should be discretized
   *
   * get the minimum of the function cost, having that the given variable has been assigned to the given value
   * @param v the given variable
   * @param val the given element
   * @return the minimum
   */
  virtual int getMinCost (Variable *v, int val);
  /**
   * @pre The variable @a v should be not discretized
   *
   * compute the minimum cost of the constraint, having that the specified variable is assigned to the lower bound
   * @param v the specified variable
   * @return the minimum
   */
  virtual int getMinLbCost (Variable *v);
  /**
   * @pre The variable @a v should be not discretized
   *
   * compute the minimum cost of the constraint, having that the specified variable is assigned to the upper bound
   * @param v the specified variable
   * @return the minimum
   */
  virtual int getMinUbCost (Variable *v);

  /**
   * compute the cost of the given support
   * @param s the support
   * @return the cost
   */
  virtual int getCost (Support *s);
  /**
   * get the contribution of this constraint to the global cost when all variables are assigned
   * @param values the values of the involved variables
   * @return the cost of this constraint
   */
  virtual int getFinalCost (int *values);
};

#endif
