// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "nonCanonicPair.h"


NonCanonicPair::NonCanonicPair (Variable *v1, Variable *v2, int i, bool o, Interaction it1, Interaction it2, Orientation ori, int f, ErrorsToCosts *e2c, IsostericityValues *iv) : SoftConstraint(i, e2c, o, 2, v1, v2) {
  simple = false;
  interaction1 = it1;
  interaction2 = it2;
  orientation = ori;
  family = f;
  isostericityValues = iv;
  name = "non canonic pair";
}

int NonCanonicPair::computeCost (int i1, int i2) {
  if (isostericityValues->getMatchCost(interaction1, interaction2, orientation, sequence->getNucleotide(i1), sequence->getNucleotide(i2), family)) {
    return 0;
  }
  else if (isostericityValues->getMatchCost(interaction1, interaction2, orientation, sequence->getNucleotide(i1), sequence->getNucleotide(i2), ALLFAMILIES)) {
    return min2(getMaxCost()+1, isostericityValues->getNotInFamilyValue());
  }
  return getMaxCost()+1;
}

int NonCanonicPair::computeCost (int lb1, int ub1, int lb2, int ub2) {
  if ((!unaryConstraints[0]->isAssigned()) && (!unaryConstraints[1]->isAssigned())) {
    return 0;
  }
  DomainIterator di1 = unaryConstraints[0]->getDomainIterator();
  DomainIterator di2 = unaryConstraints[1]->getDomainIterator();
  int min = getMaxCost()+1, mem = getMem(), mem1, mem2;
  int value;
  currentSupport->unset();

  di1.setTo(lb1);
  for (; *di1 < ub1; ++di1) {
    mem1 = getMemory(variables[0], *di1);
    di2 = unaryConstraints[1]->getDomainIterator();
    di2.setTo(lb2);
    for (; *di2 < ub2; ++di2) {
      mem2 = getMemory(variables[1], *di2);
      value = computeCost(*di1, *di2) - mem1 - mem2 - mem;
      if (value < min) {
        currentSupport->setSupport(0, *di1);
        currentSupport->setSupport(1, *di2);
        min = value;
      }
    }
  }
  return min;
}

int NonCanonicPair::getMinCost (Variable *v, int val) {
  int ind = getVariableIndex(v);
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  currentSupport->unset();
  switch (ind) {
    case 0:
      lb1 = val;
      ub1 = val+1;
      break;
    case 1:
      lb2 = val;
      ub2 = val+1;
      break;
  }
  return computeCost(lb1, ub1, lb2, ub2);
}

int NonCanonicPair::getMinLbCost (Variable *v) {
  int ind = getVariableIndex(v);
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  currentSupport->unset();
  switch (ind) {
    case 0:
      ub1 = lb1+1;
      break;
    case 1:
      ub2 = lb2+1;
      break;
  }
  return computeCost(lb1, ub1, lb2, ub2);
}

int NonCanonicPair::getMinUbCost (Variable *v) {
  int ind = getVariableIndex(v);
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  currentSupport->unset();
  switch (ind) {
    case 0:
      lb1 = ub1-1;
      break;
    case 1:
      lb2 = ub2-1;
      break;
  }
  return computeCost(lb1, ub1, lb2, ub2);
}

int NonCanonicPair::getMinCost () {
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  currentSupport->unset();
  return computeCost(lb1, ub1, lb2, ub2);
}

int NonCanonicPair::getCost (Support *s) {
  int val1 = s->getSupport(0), val2 = s->getSupport(1);
  int result = computeCost(val1, val2) - getMemory(variables[0], val1) - getMemory(variables[1], val2) - getMem();
  return result;
}

int NonCanonicPair::getFinalCost (int *values) {
  int val1 = values[0];
  int val2 = values[1];
  return min2(getMaxCost()+1, computeCost(val1, val2));
}
