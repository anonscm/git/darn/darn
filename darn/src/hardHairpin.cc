// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "hardHairpin.h"

HardHairpin::HardHairpin (Variable *v1, Variable *v2, int i, int ml, int mml, int mc, InteractionValues *iv) : HardConstraint (i, mc, 2, v1, v2) {
  simple = false;
  minLength = max2(ml, HAIRPINMINLENGTH);
  maxLength = min2(mml, HAIRPINMAXLENGTH);
  interactionValues = iv;

      // build and set init values to the matrix
  matrix = new int*[maxLength+1];
  for (int i = 0; i <= maxLength; i++) {
    matrix[i] = new int[maxLength+1];
    if (i+static_cast<int>(LOOPMINLENGTH)-2 < maxLength+1) {
      matrix[i][i+static_cast<int>(LOOPMINLENGTH)-2] = interactionValues->getJunctionCost();
      if (i+static_cast<int>(LOOPMINLENGTH)-1 < maxLength+1) {
        matrix[i][i+static_cast<int>(LOOPMINLENGTH)-1] = interactionValues->getJunctionCost();
      }
    }
  }
  hardness = minLength;
  name = "hard hairpin";
}

int *HardHairpin::isConsistent (int var, int val, int *values) {
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  switch (var) {
    case 0:
      lb1 = val;
      ub1 = val+1;
      break;
    case 1:
      lb2 = val;
      ub2 = val+1;
      break;
  }
  return (isConsistent(lb1, ub1, lb2, ub2, values));
}

int *HardHairpin::isConsistent (int lb1, int ub1, int lb2, int ub2, int *values) {
  int start1 = max2(lb1, lb2-(maxLength-1));
  int stop1 = min2(ub1-1, (ub2-1)-(minLength-1));
  int start2, stop2;
  DomainIterator di1 = unaryConstraints[0]->getDomainIterator();

      // check all the possible values of the first variable
  for (di1.setTo(start1); *di1 <= stop1; ++di1) {

    start2 = max2(lb2, *di1+(minLength-1));
    stop2 = min2(ub2-1, *di1+(maxLength-1));
    DomainIterator di2 = unaryConstraints[1]->getDomainIterator();

        // knowing the value of the first variable, check the values of the second variable
    for (di2.setTo(start2); *di2 <= stop2; ++di2) {

      if (isConsistent(*di1, *di2)) {
        values[0] = *di1;
        values[1] = *di2;
        return values;
      }
    }
  }
  return NULL;
}

bool HardHairpin::isConsistent (int i1, int i2) {
  int l = (i2+1) - i1;
  int d;
  int match, value, min;
  int startJunction, stopJunction, junctionValue;
  Nucleotide *n1 = sequence->getNucleotide(i1);
  Nucleotide *n2 = sequence->getNucleotide(i2);

      // make the two nucleotides match
  if (interactionValues->getMatchCost(n1, n2) != 0) {
    return maxCost+1;
  }

      // Nussinov algorithm
  for (d = LOOPMINLENGTH; d < l; d++) {
    min = maxCost+1;
    for (int i = 0; i < l-d; i++) {
      int j = i + d;
      match = interactionValues->getMatchCost(sequence->getNucleotide(i1+i), sequence->getNucleotide(i1+j));
      
          // compute the cost of the junction
      startJunction = i + LOOPMINLENGTH + 1;
      stopJunction = j - LOOPMINLENGTH - 1;
      junctionValue = maxCost+1;
      for (int k = startJunction; k+1 <= stopJunction; k++) {
        if (junctionValue > matrix[i][k] + matrix[k+1][j]) {
          junctionValue = matrix[i][k] + matrix[k+1][j];
        }
      }

      value = min4(matrix[i+1][j-1]+match, matrix[i][j-1]+interactionValues->getInsertionCost(), matrix[i+1][j]+interactionValues->getInsertionCost(), junctionValue+interactionValues->getJunctionCost());
      matrix[i][j] = value;
      if (min > value) {
        min = value;
      }
    }
    if (min-interactionValues->getJunctionCost() > maxCost) {
      return false;
    }
  }
  return (matrix[0][l-1]-interactionValues->getJunctionCost() <= maxCost);
}

void HardHairpin::revise () {
  for (int i = 0; i < 4; i++) {
    revise(i);
  }
}

void HardHairpin::revise (int v) {
  reviseFromLb(v);
  reviseFromUb(v);
}

void HardHairpin::reviseFromLb (int v) {
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int buffer[2], *values;
  DomainIterator di = unaryConstraints[v]->getDomainIterator();
  if (checkLbSupport(v)) {
    return;
  }
  lbSupports[v]->unset();
  if (v == 0) {
    if (lb2 < lb1 + (minLength-1)) {
      unaryConstraints[1]->setLb(lb1 + (minLength-1));
    }
  }
  else {
    if (lb1 < lb2 - (maxLength-1)) {
      unaryConstraints[0]->setLb(lb2 - (maxLength-1));
    }
  }
  for (; *di < ub1; ++di) {
    values = isConsistent(v, *di, buffer);
    if (values != NULL) {
      for (int i = 0; i < 2; i++) {
        lbSupports[v]->setSupport(i, values[i]);
      }
      unaryConstraints[v]->setLb(*di);
      revise(1-v);
      return;
    }
  }
  unaryConstraints[v]->setLb(unaryConstraints[v]->getUb());
}

void HardHairpin::reviseFromUb (int v) {
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  int buffer[2], *values;
  DomainIterator di = unaryConstraints[v]->getReverseDomainIterator();
  if (checkUbSupport(v)) {
    return;
  }
  ubSupports[v]->unset();
  if (v == 0) {
    if (ub2-1 > ub1-1 + (maxLength-1)) {
      unaryConstraints[1]->setUb(ub1-1 + (maxLength-1) +1);
    }
  }
  else {
    if (ub1-1 > ub2-1 - (minLength-1)) {
      unaryConstraints[0]->setUb(ub2-1 - (minLength-1) +1);
    }
  }
  for (; *di >= lb2; --di) {
    values = isConsistent(v, *di, buffer);
    if (values != NULL) {
      for (int i = 0; i < 2; i++) {
        lbSupports[v]->setSupport(i, values[i]);
      }
      unaryConstraints[v]->setUb(*di+1);
      revise(1-v);
      return;
    }
  }
  unaryConstraints[v]->setUb(unaryConstraints[v]->getLb());
}

void HardHairpin::reviseFromAssignment (int v) {
  int lb1 = unaryConstraints[0]->getLb();
  int ub1 = unaryConstraints[0]->getUb();
  int lb2 = unaryConstraints[1]->getLb();
  int ub2 = unaryConstraints[1]->getUb();
  int buffer[4], *values;
  if ((checkLbSupport(v)) || (checkUbSupport(v))) {
    return;
  }
  lbSupports[v]->unset();
  ubSupports[v]->unset();
  if (v == 0) {
    if (lb2 < lb1 + (minLength-1)) {
      unaryConstraints[1]->setLb(lb1 + (minLength-1));
    }
    if (ub2-1 > ub1-1 + (maxLength-1)) {
      unaryConstraints[1]->setUb(ub1-1 + (maxLength-1) +1);
    }
  }
  else {
    if (lb1 < lb2 - (maxLength-1)) {
      unaryConstraints[0]->setLb(lb2 - (maxLength-1));
    }
    if (ub1-1 > ub2-1 - (minLength-1)) {
      unaryConstraints[0]->setUb(ub2-1 - (minLength-1) +1);
    }
  }
  values = isConsistent(v, unaryConstraints[v]->getLb(), buffer);
  if (values != NULL) {
    for (int i = 0; i < 2; i++) {
      lbSupports[v]->setSupport(i, values[i]);
      ubSupports[v]->setSupport(i, values[i]);
    }
    revise(1-v);
    return;
  }
  unaryConstraints[v]->setUb(unaryConstraints[v]->getLb());
}

bool HardHairpin::getConsistency (Support *s) {
  int val1 = s->getSupport(0);
  int val2 = s->getSupport(1);
  int buffer[2], *values;
  values = isConsistent(val1, val2, buffer);
  return (values != NULL);
}

bool HardHairpin::printSpace (int var, int place) {
  return (((var == 0) && (place == 0)) || ((var == 1) && (place == 1)));
}

void HardHairpin::getTrace (int start, int *values, bool **backtrace) {
  int i1 = values[0];
  int i2 = values[1];
  int l = (i2+1) - i1;
  int d;
  int match;
  int startJunction, stopJunction, junctionValue, junctionIndex;
  int i, j;
  stack< pair<int, int> > positions;

  // fill the matrix
  for (d = LOOPMINLENGTH; d < l; d++) {
    for (i = 0; i < l-d; i++) {
      j = i + d;
      match = interactionValues->getMatchCost(sequence->getNucleotide(i1+i), sequence->getNucleotide(i1+j));
      
      // compute the cost of the junction
      startJunction = i + LOOPMINLENGTH + 1;
      stopJunction = j - LOOPMINLENGTH - 1;
      junctionValue = maxCost+1;
      for (int k = startJunction; k+1 <= stopJunction; k++) {
        if (junctionValue > matrix[i][k] + matrix[k+1][j]) {
          junctionValue = matrix[i][k] + matrix[k+1][j];
        }
      }

      matrix[i][j] = min4(matrix[i+1][j-1]+match, matrix[i][j-1]+interactionValues->getInsertionCost(), matrix[i+1][j]+interactionValues->getInsertionCost(), junctionValue+interactionValues->getJunctionCost());
    }
  }

  // backtrace
  positions.push(pair<int, int>(0, l-1));
  while (! positions.empty()) {
    i = positions.top().first;
    j = positions.top().second;
    positions.pop();

    if ((j >= i + static_cast<int>(LOOPMINLENGTH))) {

      // compute junction cost
      startJunction = i + LOOPMINLENGTH + 1;
      stopJunction = j - LOOPMINLENGTH - 1;
      junctionValue = maxCost+1;
      junctionIndex = -1;
      for (int k = startJunction; k+1 <= stopJunction; k++) {
        if (junctionValue > matrix[i][k] + matrix[k+1][j]) {
          junctionValue = matrix[i][k] + matrix[k+1][j];
          junctionIndex = k;
        }
      }
      junctionValue += interactionValues->getJunctionCost();

      if (matrix[i][j] == matrix[i+1][j] + interactionValues->getInsertionCost()) {
        positions.push(pair<int, int>(i+1, j));
      }
      else if (matrix[i][j] == matrix[i][j-1] + interactionValues->getInsertionCost()) {
        positions.push(pair<int, int>(i, j-1));
      }
      else if (matrix[i][j] == junctionValue) {
        positions.push(pair<int, int>(i, junctionIndex));
        positions.push(pair<int, int>(junctionIndex+1, j));
      }
      else {
        if (matrix[i][j] == matrix[i+1][j-1]) {
          backtrace[i1 + (i+1) - start][i1 + (j-1) - start] = true;
        }
        positions.push(pair<int, int>(i+1, j-1));
      }
    }
  }
}
