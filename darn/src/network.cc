// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "network.h"


Network::Network (int nv, int mCost) : exploredNodes(0), nbUndominatedSolutions(0) {
  nbTotVariables = nv;
  top = new Top(mCost);
  variableContainer = new VariableContainer(nbTotVariables);
  unaryConstraints = new DefaultUnarySoftConstraint*[nbTotVariables];
  //variableOrderer = new SimpleVariableOrderer(unaryConstraints, variableContainer);
  //variableOrderer = new DegVariableOrderer(unaryConstraints, variableContainer);
  //variableOrderer = new SizeDegVariableOrderer(unaryConstraints, variableContainer);
  variableOrderer = new HardnessVariableOrderer(unaryConstraints, variableContainer);
  zeroArityConstraint = new AbstractSoftConstraint(-1, new ErrorsToCosts(mCost));
  variables = new Variable*[nbTotVariables];
  assignmentHardRevision = new Revision(nbTotVariables);
  lbHardRevision = new Revision(nbTotVariables);
  ubHardRevision = new Revision(nbTotVariables);
  lbComplicatedHardRevision = new Revision(nbTotVariables);
  ubComplicatedHardRevision = new Revision(nbTotVariables);
  assignmentComplicatedHardRevision = new Revision(nbTotVariables);
  assignmentSoftRevision = new Revision(nbTotVariables);
  lbSoftRevision = new Revision(nbTotVariables);
  ubSoftRevision = new Revision(nbTotVariables);
  holeSoftRevision = new Revision(nbTotVariables);
  lbComplicatedSoftRevision = new Revision(nbTotVariables);
  ubComplicatedSoftRevision = new Revision(nbTotVariables);
  holeComplicatedSoftRevision = new Revision(nbTotVariables);
  assignmentComplicatedSoftRevision = new Revision(nbTotVariables);
  sRecorder = new SolutionRecorder(unaryConstraints, mCost);
  backtrack = new SimpleBacktrack();
  //backtrack = new SimpleGraphBasedBacktrack(nbTotVariables, variableContainer);
  firstWay = true;
}

Network::~Network () {
  /*
  delete variableContainer;
  delete[] unaryConstraints;
  delete variableOrderer;
  delete zeroArityConstraint;
  delete[] variables;
  delete assignmentHardRevision;
  delete lbHardRevision;
  delete ubHardRevision;
  delete assignmentComplicatedHardRevision;
  delete lbComplicatedHardRevision;
  delete ubComplicatedHardRevision;
  delete assignmentSoftRevision;
  delete lbSoftRevision;
  delete ubSoftRevision;
  delete holeSoftRevision;
  delete assignmentComplicatedSoftRevision;
  delete lbComplicatedSoftRevision;
  delete ubComplicatedSoftRevision;
  delete holeComplicatedSoftRevision;
  delete sRecorder;
  delete backtrack;
  */
}

void Network::reset () {
  exploredNodes = 0;
  variableContainer->reset();
  setAllRevision();
  if ((!sequenceBoth) || (firstWay)) {
    sRecorder->reset();
  }
  backtrack->reset();
  for (int i = 0; i < nbTotVariables; i++) {
    unaryConstraints[i]->reset();
    variables[i]->reset();
  }
  zeroArityConstraint->reset();
  top->reset();
  for (unsigned int i = 0; i < softConstraints.size(); i++) {
    softConstraints[i]->reset();
  }
  for (unsigned int i = 0; i < hardConstraints.size(); i++) {
    hardConstraints[i]->reset();
  }
  sRecorder->reset();
}

void Network::reverseSequence () {
  sequence->reverse();
  firstWay = false;
  reset();
}

Variable *Network::makeVariable (string name, Sequence *s) {
  static int pushedVariables = 0;
  if (pushedVariables >= nbTotVariables) {
    trace(1, "Error! Trying to add a new Variable while the maximum number has been exceeded!");
    return NULL;
  }
  Variable *v = new Variable(pushedVariables, name, s);
  variables[pushedVariables] = v;
  pushedVariables++;
  makeUnaryConstraint(v);
  return v;
}

DefaultUnarySoftConstraint* Network::makeUnaryConstraint (Variable *v) {
  static int pushedVariables = 0;
  DefaultUnarySoftConstraint *uc = new DefaultUnarySoftConstraint(v, v->getIndex(), new ErrorsToCosts(zeroArityConstraint->getMaxCost()));
  if (pushedVariables >= nbTotVariables) {
    trace(1, "Error! Trying to add a new UnaryConstraint while the maximum number has been exceeded!");
    return NULL;
  }
  unaryConstraints[pushedVariables] = uc;
  pushedVariables++;
  return uc;
}

Pattern* Network::makePattern (Variable *v, string p, ErrorsToCosts *e2c, bool o) {
  Pattern *pat = new Pattern(v, softConstraints.size(), o, p, e2c, ValuesReader::getMatchingValues());
  addSoftConstraint(pat);
  return pat;
}

Pattern2* Network::makePattern2 (Variable *v1, Variable *v2, string p, ErrorsToCosts *e2c, bool o) {
  Pattern2 *pat = new Pattern2(v1, v2, softConstraints.size(), o, p, e2c, ValuesReader::getMatchingValues());
  addSoftConstraint(pat);
  return pat;
}

Distance* Network::makeDistance (Variable *v1, Variable *v2, int d1, int d2, int d3, int d4, int c1, ErrorsToCosts *e2c, bool o) {
  Distance *d = new Distance(v1, v2, softConstraints.size(), o, d1, d2, d3, d4, c1, e2c);
  addSoftConstraint(d);
  return d;
}

Composition* Network::makeComposition (Variable *v1, Variable *v2, Nucleotide *n1, Nucleotide *n2, int t1, int t2, Relation r, int mc, ErrorsToCosts *e2c, bool o) {
  Composition *c = new Composition(v1, v2, softConstraints.size(), o, n1, n2, t1, t2, r, mc, e2c);
  addSoftConstraint(c);
  return c;
}

NonCanonicPair* Network::makeNonCanonicPair (Variable *v1, Variable *v2, Interaction int1, Interaction int2, Orientation ori, int f, ErrorsToCosts *e2c, bool o) {
  NonCanonicPair *p = new NonCanonicPair(v1, v2, softConstraints.size(), o, int1, int2, ori, f, e2c, ValuesReader::getIsostericityValues());
  addSoftConstraint(p);
  return p;
}

Helix* Network::makeHelix (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int hl, ErrorsToCosts *e2c, bool w, bool o) {
  Helix *h = new Helix(v1, v2, v3, v4, softConstraints.size(), o, hl, e2c, ((w)? ValuesReader::getInteractionValuesWithWobble(): ValuesReader::getInteractionValues()));
  addSoftConstraint(h);
  return h;
}

HelixSimple* Network::makeHelixSimple (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int msl, int mmsl, int mll, int mmll, ErrorsToCosts *e2c, bool w, bool insert, bool o) {
  HelixSimple *h = new HelixSimple(v1, v2, v3, v4, softConstraints.size(), o, msl, mmsl, mll, mmll, e2c, ((w)? ValuesReader::getInteractionValuesWithWobble(): ValuesReader::getInteractionValues()), insert);
  addSoftConstraint(h);
  return h;
}

Repetition* Network::makeRepetition (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int ms, int mms, int md, int mmd, ErrorsToCosts *e2c, bool insert, bool o) {
  Repetition *r = new Repetition(v1, v2, v3, v4, softConstraints.size(), o, ms, mms, md, mmd, e2c, ValuesReader::getMatchingValues(), insert);
  addSoftConstraint(r);
  return r;
}

Hairpin* Network::makeHairpin (Variable *v1, Variable *v2, int ml, int mml, ErrorsToCosts *e2c, bool w, bool o) {
  Hairpin *h = new Hairpin(v1, v2, softConstraints.size(), ml, mml, o, e2c, ((w)? ValuesReader::getInteractionValuesWithWobble(): ValuesReader::getInteractionValues()));
  addSoftConstraint(h);
  return h;
}

Duplex* Network::makeDuplex (Variable *v1, Variable *v2, Variable *v3, Variable *v4, ErrorsToCosts *e2c, Sequence *s, bool w, bool o) {
  Duplex *d = new Duplex(v1, v2, v3, v4, softConstraints.size(), o, e2c, s, ((w)? ValuesReader::getInteractionValuesWithWobble(): ValuesReader::getInteractionValues()));
  addSoftConstraint(d);
  return d;
}

void Network::addSoftConstraint (SoftConstraint *c) {
  softConstraints.push_back(c);
}

HardPattern* Network::makeHardPattern (Variable *v, string p, int mc) {
  HardPattern *pat = new HardPattern(v, hardConstraints.size(), p, mc, ValuesReader::getMatchingValues());
  addHardConstraint(pat);
  return pat;
}

HardPattern2* Network::makeHardPattern2 (Variable *v1, Variable *v2, string p, int mc) {
  HardPattern2 *pat = new HardPattern2(v1, v2, hardConstraints.size(), p, mc, ValuesReader::getMatchingValues());
  addHardConstraint(pat);
  return pat;
}

HardDistance* Network::makeHardDistance (Variable *v1, Variable *v2, int d1, int d2) {
  HardDistance *d = new HardDistance(v1, v2, hardConstraints.size(), d1, d2);
  addHardConstraint(d);
  return d;
}

HardComposition* Network::makeHardComposition (Variable *v1, Variable *v2, Nucleotide *n1, Nucleotide *n2, int t, Relation r) {
  HardComposition *c = new HardComposition(v1, v2, hardConstraints.size(), n1, n2, t, r);
  addHardConstraint(c);
  return c;
}

HardNonCanonicPair* Network::makeHardNonCanonicPair (Variable *v1, Variable *v2, Interaction i1, Interaction i2, Orientation o, int f, int mc) {
  HardNonCanonicPair *p = new HardNonCanonicPair(v1, v2, hardConstraints.size(), i1, i2, o, f, mc, ValuesReader::getIsostericityValues());
  addHardConstraint(p);
  return p;
}

HardHelix* Network::makeHardHelix (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int hl, int mc, bool w) {
  HardHelix *h = new HardHelix(v1, v2, v3, v4, hardConstraints.size(), hl, mc, ((w)? ValuesReader::getInteractionValuesWithWobble(): ValuesReader::getInteractionValues()));
  addHardConstraint(h);
  return h;
}

HardHelixSimple* Network::makeHardHelixSimple (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int msl, int mmsl, int mll, int mmll, int mc, bool w, bool insert) {
  HardHelixSimple *h = new HardHelixSimple(v1, v2, v3, v4, hardConstraints.size(), msl, mmsl, mll, mmll, mc, ((w)? ValuesReader::getInteractionValuesWithWobble(): ValuesReader::getInteractionValues()), insert);
  addHardConstraint(h);
  return h;
}

HardRepetition* Network::makeHardRepetition (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int ms, int mms, int md, int mmd, int mc, bool insert) {
  HardRepetition *r = new HardRepetition(v1, v2, v3, v4, hardConstraints.size(), ms, mms, md, mmd, mc, ValuesReader::getMatchingValues(), insert);
  addHardConstraint(r);
  return r;
}
HardHairpin* Network::makeHardHairpin (Variable *v1, Variable *v2, int ml, int mml, int mc, bool w) {
  HardHairpin *h = new HardHairpin(v1, v2, hardConstraints.size(), ml, mml, mc, ((w)? ValuesReader::getInteractionValuesWithWobble(): ValuesReader::getInteractionValues()));
  addHardConstraint(h);
  return h;
}

HardDuplex* Network::makeHardDuplex (Variable *v1, Variable *v2, Variable *v3, Variable *v4, int mc, Sequence *s, bool w) {
  HardDuplex *d = new HardDuplex(v1, v2, v3, v4, hardConstraints.size(), mc, s, ((w)? ValuesReader::getInteractionValuesWithWobble(): ValuesReader::getInteractionValues()));
  addHardConstraint(d);
  return d;
}

void Network::addHardConstraint (HardConstraint *c) {
  hardConstraints.push_back(c);
}

void Network::setAAC () {
  int currentIndex;
  Variable *currentVariable = NULL;
  HardConstraint *currentHardConstraint;
  SoftConstraint *currentSoftConstraint;
  int c0;

  while ((!assignmentHardRevision->isEmpty()) || (!lbHardRevision->isEmpty()) || (!ubHardRevision->isEmpty()) || (!assignmentComplicatedHardRevision->isEmpty()) || (!lbComplicatedHardRevision->isEmpty()) || (!ubComplicatedHardRevision->isEmpty()) || (!assignmentSoftRevision->isEmpty()) || (!lbSoftRevision->isEmpty()) || (!ubSoftRevision->isEmpty()) || (!holeSoftRevision->isEmpty())|| (!assignmentComplicatedSoftRevision->isEmpty()) || (!lbComplicatedSoftRevision->isEmpty()) || (!ubComplicatedSoftRevision->isEmpty()) || (!holeComplicatedSoftRevision->isEmpty())) {
    
    c0 = zeroArityConstraint->getMinCost();
    
    if (!assignmentHardRevision->isEmpty()) {
      currentIndex = assignmentHardRevision->getCurrentElement();
      assignmentHardRevision->removeCurrentElement();
      currentVariable = variables[currentIndex];
      for (int i = 0; i < currentVariable->getNbHardConstraints(); i++) {
        currentHardConstraint = (HardConstraint*) currentVariable->getHardConstraint(i);
        if (currentHardConstraint->isSimple()) {
          currentHardConstraint->reviseFromAssignment(currentVariable);
        }
      }
    }
    else if (!lbHardRevision->isEmpty()) {
      currentIndex = lbHardRevision->getCurrentElement();
      lbHardRevision->removeCurrentElement();
      currentVariable = variables[currentIndex];
      for (int i = 0; i < currentVariable->getNbHardConstraints(); i++) {
        currentHardConstraint = (HardConstraint*) currentVariable->getHardConstraint(i);
        if (currentHardConstraint->isSimple()) {
          currentHardConstraint->reviseFromLb(currentVariable);
        }
      }
    }
    else if (!ubHardRevision->isEmpty()) {
      currentIndex = ubHardRevision->getCurrentElement();
      ubHardRevision->removeCurrentElement();
      currentVariable = variables[currentIndex];
      for (int i = 0; i < currentVariable->getNbHardConstraints(); i++) {
        currentHardConstraint = (HardConstraint*) currentVariable->getHardConstraint(i);
        if (currentHardConstraint->isSimple()) {
          currentHardConstraint->reviseFromUb(currentVariable);
        }
      }
    }
    else if (!assignmentComplicatedHardRevision->isEmpty()) {
      currentIndex = assignmentComplicatedHardRevision->getCurrentElement();
      assignmentComplicatedHardRevision->removeCurrentElement();
      currentVariable = variables[currentIndex];
      for (int i = 0; i < currentVariable->getNbHardConstraints(); i++) {
        currentHardConstraint = (HardConstraint*) currentVariable->getHardConstraint(i);
        if (!currentHardConstraint->isSimple()) {
          currentHardConstraint->reviseFromAssignment(currentVariable);
        }
      }
    }
    else if (!lbComplicatedHardRevision->isEmpty()) {
      currentIndex = lbComplicatedHardRevision->getCurrentElement();
      lbComplicatedHardRevision->removeCurrentElement();
      currentVariable = variables[currentIndex];
      for (int i = 0; i < currentVariable->getNbHardConstraints(); i++) {
        currentHardConstraint = (HardConstraint*) currentVariable->getHardConstraint(i);
        if (!currentHardConstraint->isSimple()) {
          currentHardConstraint->reviseFromLb(currentVariable);
        }
      }
    }
    else if (!ubComplicatedHardRevision->isEmpty()) {
      currentIndex = ubComplicatedHardRevision->getCurrentElement();
      ubComplicatedHardRevision->removeCurrentElement();
      currentVariable = variables[currentIndex];
      for (int i = 0; i < currentVariable->getNbHardConstraints(); i++) {
        currentHardConstraint = (HardConstraint*) currentVariable->getHardConstraint(i);
        if (!currentHardConstraint->isSimple()) {
          currentHardConstraint->reviseFromUb(currentVariable);
        }
      }
    }
    else if (!assignmentSoftRevision->isEmpty()) {
      currentIndex = assignmentSoftRevision->getCurrentElement();
      assignmentSoftRevision->removeCurrentElement();
      currentVariable = variables[currentIndex];
      for (int i = 0; i < currentVariable->getNbSoftConstraints(); i++) {
        currentSoftConstraint = (SoftConstraint*) currentVariable->getSoftConstraint(i);
        if (currentSoftConstraint->isSimple()) {
          currentSoftConstraint->reviseFromAssignment(currentVariable);
        }
      }
    }
    else if (!lbSoftRevision->isEmpty()) {
      currentIndex = lbSoftRevision->getCurrentElement();
      lbSoftRevision->removeCurrentElement();
      currentVariable = variables[currentIndex];
      for (int i = 0; i < currentVariable->getNbSoftConstraints(); i++) {
        currentSoftConstraint = (SoftConstraint*) currentVariable->getSoftConstraint(i);
        if (currentSoftConstraint->isSimple()) {
          currentSoftConstraint->reviseFromLb(currentVariable);
        }
      }
    }
    else if (!ubSoftRevision->isEmpty()) {
      currentIndex = ubSoftRevision->getCurrentElement();
      ubSoftRevision->removeCurrentElement();
      currentVariable = variables[currentIndex];
      for (int i = 0; i < currentVariable->getNbSoftConstraints(); i++) {
        currentSoftConstraint = (SoftConstraint*) currentVariable->getSoftConstraint(i);
        if (currentSoftConstraint->isSimple()) {
          currentSoftConstraint->reviseFromUb(currentVariable);
        }
      }
    }
    else if (!holeSoftRevision->isEmpty()) {
      currentIndex = holeSoftRevision->getCurrentElement();
      currentVariable = variables[currentIndex];
      for (int i = 0; i < currentVariable->getNbSoftConstraints(); i++) {
        currentSoftConstraint = (SoftConstraint*) currentVariable->getSoftConstraint(i);
        if (currentSoftConstraint->isSimple()) {
          currentSoftConstraint->reviseFromHole(currentVariable, holeSoftRevision->getCurrentValue());
        }
      }
      holeSoftRevision->removeCurrentElement();
    }
    else if (!assignmentComplicatedSoftRevision->isEmpty()) {
      currentIndex = assignmentComplicatedSoftRevision->getCurrentElement();
      assignmentComplicatedSoftRevision->removeCurrentElement();
      currentVariable = variables[currentIndex];
      for (int i = 0; i < currentVariable->getNbSoftConstraints(); i++) {
        currentSoftConstraint = (SoftConstraint*) currentVariable->getSoftConstraint(i);
        if (!currentSoftConstraint->isSimple()) {
          currentSoftConstraint->reviseFromAssignment(currentVariable);
        }
      }
    }
    else if (!lbComplicatedSoftRevision->isEmpty()) {
      currentIndex = lbComplicatedSoftRevision->getCurrentElement();
      lbComplicatedSoftRevision->removeCurrentElement();
      currentVariable = variables[currentIndex];
      for (int i = 0; i < currentVariable->getNbSoftConstraints(); i++) {
        currentSoftConstraint = (SoftConstraint*) currentVariable->getSoftConstraint(i);
        if (!currentSoftConstraint->isSimple()) {
          currentSoftConstraint->reviseFromLb(currentVariable);
        }
      }
    }
    else if (!ubComplicatedSoftRevision->isEmpty()) {
      currentIndex = ubComplicatedSoftRevision->getCurrentElement();
      ubComplicatedSoftRevision->removeCurrentElement();
      currentVariable = variables[currentIndex];
      for (int i = 0; i < currentVariable->getNbSoftConstraints(); i++) {
        currentSoftConstraint = (SoftConstraint*) currentVariable->getSoftConstraint(i);
        if (!currentSoftConstraint->isSimple()) {
          currentSoftConstraint->reviseFromUb(currentVariable);
        }
      }
    }
    else if (!holeComplicatedSoftRevision->isEmpty()) {
      currentIndex = holeComplicatedSoftRevision->getCurrentElement();
      currentVariable = variables[currentIndex];
      for (int i = 0; i < currentVariable->getNbSoftConstraints(); i++) {
        currentSoftConstraint = (SoftConstraint*) currentVariable->getSoftConstraint(i);
        if (!currentSoftConstraint->isSimple()) {
          currentSoftConstraint->reviseFromHole(currentVariable, holeComplicatedSoftRevision->getCurrentValue());
        }
      }
      holeComplicatedSoftRevision->removeCurrentElement();
    }
    currentVariable->getUnaryConstraint()->setANC();
    while (zeroArityConstraint->getMinCost() > c0) {
      for (int i = 0; i < nbTotVariables; i++) {
        unaryConstraints[i]->setANC();
      }
      c0 = zeroArityConstraint->getMinCost();
    }
  }
}

void Network::explore () {
  char mesg[255];
  try {
    setAllRevision();
    setAAC();
  }
  catch (inconsistency u) {
    sprintf(mesg, "%s\n%s: No solution!\n%s", ((webDisplay)? "\n<p>": ""), sequence->getName().c_str(), ((webDisplay)? "</p>\n": ""));
    trace (1, mesg);
    printSolutions();
    return;
  }

  trace(2, "\nStarting the scan...");

  try {
    explore(0);
  }
  catch (inconsistency u) {
	}

  trace(2, "...scanned\n");
  
  printSolutions();
}

void Network::explore (int depth) {
  int nextVariable;
  DefaultUnarySoftConstraint *uc;

  // we have found a solution
  if (depth == nbTotVariables) {
    sRecorder->addSolution();
    return;
  }

  // find next variable to assign
  nextVariable = variableOrderer->getNextVariable();
  variableContainer->assign(nextVariable);
  backtrack->notifyAssignment(nextVariable);
  uc = unaryConstraints[nextVariable];

  if (depth == 0) {
    trace(2, "First assigned variable: " + uc->getVariable()->getName());
  }

  try {
    // there is only one value in the domain left: quickly assign the variable to it
    if (uc->getUb() == uc->getLb()+1) {
      explore(depth+1);
      variableContainer->backtrack();
      backtrack->notifyBacktrack();
      return;
    }

    // scan all the values of the domain
    for (int val = uc->getLb(); val < uc->getUb(); val = uc->getLb()) {
      save();

      if ((depth == 0) && (traceLevel > 2)) {
        printf("%ith / %i nucleotide\r", val, sequence->getLength());
        fflush (stdout);
      }

      exploredNodes++;
      clearAllRevision();
      try {
        // assign the variable
        uc->assign(val);
        // possibly change the value of Top
        if (top->changeValue(sRecorder->getCurrentOptimalSolution())) {
          for (int i = 0; i < nbTotVariables; i++) {
            unaryConstraints[i]->setANC();
          }
        }
        setAAC();
        explore(depth+1);
      }
      catch (inconsistency u) {
        if (backtrack->doMoreBacktrack()) {
          variableContainer->backtrack();
          backtrack->notifyBacktrack();
          restore();
          return;
        }
      }
      restore();

      clearAllRevision();
      // remove the explored value from the domain
      uc->incLb(1);
      // possibly change the value of Top
      if (top->changeValue(sRecorder->getCurrentOptimalSolution())) {
        for (int i = 0; i < nbTotVariables; i++) {
          unaryConstraints[i]->setANC();
        }
      }
      setAAC();

      // there is only one value in the domain left: quickly assign the variable to it
      if (uc->getUb() == uc->getLb()+1) {
        explore(depth+1);
        variableContainer->backtrack();
        backtrack->notifyBacktrack();
        return;
      }
    }
  }
  catch (inconsistency u) {
    variableContainer->backtrack();
    backtrack->notifyBacktrack();
    throw inconsistency(u);
  }

  variableContainer->backtrack();
  backtrack->notifyBacktrack();
}

void Network::printSolutions () {
  char mesg[1024];
  char mesg2[50];

  if ((! gffFormat) && (! fastaFormat)) {
    if ((sRecorder->getNbUndominatedSolutions() > 0) || (traceLevel > 0)) {
      mesg2[0] = '\0';
      if (sequenceBoth) {
        if (firstWay) {
          sprintf(mesg2, " (plus strand)");
        }
        else {
          sprintf(mesg2, " (minus strand)");
        }
      }
      sprintf(mesg, "%s\n%s%s:\n%s", ((webDisplay)? "\n<p>": ""), sequence->getName().c_str(), mesg2, ((webDisplay)? "</p>\n": ""));
      trace(0, mesg);
    }
  }

  sRecorder->print(softConstraints, firstWay);
  nbUndominatedSolutions += sRecorder->getNbUndominatedSolutions();
  
  if ((! gffFormat) && (! fastaFormat)) {
    if ((sRecorder->getNbUndominatedSolutions() > 0) || (traceLevel > 0)) {
      sprintf(mesg, "\n%s\t\t\t(number of solutions: %i)%s", ((webDisplay)? "<p>\n": ""), sRecorder->getNbUndominatedSolutions(), ((webDisplay)? "\n</p>": ""));
      trace(0, mesg);
    }
    sprintf(mesg, "%s\t\t\t(number of undominated solutions: %i)%s", ((webDisplay)? "<p>\n": ""), sRecorder->getNbSolutions(), ((webDisplay)? "\n</p>": ""));
    trace(1, mesg);
    sprintf(mesg, "%s\t\t\t(explored nodes: %i)%s", ((webDisplay)? "<p>\n": ""), exploredNodes, ((webDisplay)? "\n</p>": ""));
    trace(1, mesg);
  }
}

int Network::getNbUndominatedSolutions () {
  return nbUndominatedSolutions;
}

void Network::setAllRevision () {
  lbHardRevision->setAll();
  ubHardRevision->setAll();
  lbComplicatedHardRevision->setAll();
  ubComplicatedHardRevision->setAll();
  lbSoftRevision->setAll();
  ubSoftRevision->setAll();
  lbComplicatedSoftRevision->setAll();
  ubComplicatedSoftRevision->setAll();
}

void Network::clearAllRevision () {
  lbHardRevision->clear();
  ubHardRevision->clear();
  lbComplicatedHardRevision->clear();
  ubComplicatedHardRevision->clear();
  assignmentHardRevision->clear();
  assignmentComplicatedHardRevision->clear();
  lbSoftRevision->clear();
  ubSoftRevision->clear();
  lbComplicatedSoftRevision->clear();
  ubComplicatedSoftRevision->clear();
  assignmentSoftRevision->clear();
  assignmentComplicatedSoftRevision->clear();
}

void Network::save () {
  for (int i = 0; i < nbTotVariables; i++) {
    unaryConstraints[i]->save();
  }
  for (unsigned int i = 0; i < softConstraints.size(); i++) {
    softConstraints[i]->save();
  }
  for (unsigned int i = 0; i < hardConstraints.size(); i++) {
    hardConstraints[i]->save();
  }
  zeroArityConstraint->save();
  top->save();
}

void Network::restore () {
  for (int i = 0; i < nbTotVariables; i++) {
    unaryConstraints[i]->restore();
  }
  for (unsigned int i = 0; i < softConstraints.size(); i++) {
    softConstraints[i]->restore();
  }
  for (unsigned int i = 0; i < hardConstraints.size(); i++) {
    hardConstraints[i]->restore();
  }
  zeroArityConstraint->restore();
  top->restore();
}
