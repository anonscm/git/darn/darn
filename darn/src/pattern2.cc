// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "pattern2.h"


Pattern2::Pattern2 (Variable *v1, Variable *v2, int i, bool o, string p, ErrorsToCosts *e2c, MatchingValues *mv) : SoftConstraint(i, e2c, o, 2, v1, v2) {
  sequence = v1->getSequence();
  pattern = new Sequence(p, false, false);
  reversePattern = pattern->getReverse();
  matchingValues = mv;
  directAutomaton = new Automaton(pattern, getMaxCost(), matchingValues);
  reverseAutomaton = new Automaton(reversePattern, getMaxCost(), matchingValues);
  automatons[DIRECT] = directAutomaton;
  automatons[REVERSE] = reverseAutomaton;
  name = "pattern";
}

int Pattern2::computeErrors (int lb1, int ub1, int lb2, int ub2) {
  int start, stop;
  int firstLetterStop;
  int lastLetterStart, lastLetterStop;
  int score, extraCost;
  int min = getMaxCost() + 1;
  
  if ((lb1 > ub1) || (lb2 > ub2)) {
    return errorsToCosts->getMaxErrors()+1;
  }
  
  start = max2(lb1, lb2 - ((pattern->getLength()-1 + errorsToCosts->getMaxErrors())/matchingValues->getInsertionCost()));
  stop = min2(ub2-1, (ub1-1) + ((pattern->getLength()-1 + errorsToCosts->getMaxErrors())/matchingValues->getInsertionCost()));
  firstLetterStop = ub1-1;
  lastLetterStart = lb2;
  lastLetterStop = ub2-1;

  // if the pattern is an accepted approximation of the empty word
  if ((start >= lastLetterStart) && (start <= lastLetterStop) && (static_cast<int>(pattern->getLength()) * matchingValues->getInsertionCost() < errorsToCosts->getMaxErrors()+1)) {
    min = pattern->getLength() * matchingValues->getInsertionCost();
  }

  // we scan the text
  automatons[DIRECT]->reset();
  for (int i = start; (i <= stop); i++) {
    // the word may not start outside the allowed bounds
    //extraCost = max2(0, (i-firstLetterStop+1)*matchingValues->getInsertionCost());
    extraCost = ((i > firstLetterStop)? getMaxCost()+1: 0);
    automatons[DIRECT]->setOffset(extraCost, i);

    automatons[DIRECT]->addLetter(sequence->getNucleotide(i));
    score = automatons[DIRECT]->getScore();

    if ((i >= lastLetterStart) && (i <= lastLetterStop) && (score < min)) {
      //score = checkCandidate(lb1, ub1, i);
      min = score;
      if (min == 0) {
        return 0;
      }
    }
  }
  return min;
}

int Pattern2::computeCost (int lb1, int ub1, int lb2, int ub2) {
  int lb1p = max2(lb1, unaryConstraints[0]->getLb()+1);
  int ub1p = min2(ub1, unaryConstraints[0]->getUb()-1);
  int lb2p = max2(lb2, unaryConstraints[1]->getLb()+1);
  int ub2p = min2(ub2, unaryConstraints[1]->getUb()-1);
  int memLb1, memUb1, memLb2, memUb2;
  int mem1, mem2;
  int mem = getMem();
  int memSum;
  int min = getMaxCost()+1;

  // no variable is discretized
  if ((!unaryConstraints[0]->isDiscretized()) && (!unaryConstraints[1]->isDiscretized())) {
    memLb1 = cost->getLbMem(0);
    memUb1 = cost->getUbMem(0);
    memLb2 = cost->getLbMem(1);
    memUb2 = cost->getUbMem(1);

    min = diffCost(errorsToCosts->getCostFor(computeErrors(lb1p, ub1p, lb2p, ub2p)), mem);
    if (lb1 == unaryConstraints[0]->getLb()) {
      memSum = sumCost(memLb1, mem);
      min = min2(min, diffCost(errorsToCosts->getCostFor(computeErrors(lb1, lb1+1, lb2p, ub2p)), memSum));
    }
    if (ub1 == unaryConstraints[0]->getUb()) {
      memSum = sumCost(memUb1, mem);
      min = min2(min, diffCost(errorsToCosts->getCostFor(computeErrors(ub1-1, ub1, lb2p, ub2p)), memSum));
    }
    if (lb2 == unaryConstraints[1]->getLb()) {
      memSum = sumCost(memLb2, mem);
      min = min2(min, diffCost(errorsToCosts->getCostFor(computeErrors(lb1p, ub1p, lb2, lb2+1)), memSum));
    }
    if (ub2 == unaryConstraints[1]->getUb()) {
      memSum = sumCost(memUb2, mem);
      min = min2(min, diffCost(errorsToCosts->getCostFor(computeErrors(lb1p, ub1p, ub2-1, ub2)), memSum));
    }
    if ((lb1 == unaryConstraints[0]->getLb()) && (lb2 == unaryConstraints[1]->getLb())) {
      memSum = sumCost(sumCost(memLb1, memLb2), mem);
      min = min2(min, diffCost(errorsToCosts->getCostFor(computeErrors(lb1, lb1+1, lb2, lb2+1)), memSum));
    }
    if ((lb1 == unaryConstraints[0]->getLb()) && (ub2 == unaryConstraints[1]->getUb())) {
      memSum = sumCost(sumCost(memLb1, memUb2), mem);
      min = min2(min, diffCost(errorsToCosts->getCostFor(computeErrors(lb1, lb1+1, ub2-1, ub2)), memSum));
    }
    if ((ub1 == unaryConstraints[0]->getUb()) && (lb2 == unaryConstraints[1]->getLb())) {
      memSum = sumCost(sumCost(memUb1, memLb2), mem);
      min = min2(min, diffCost(errorsToCosts->getCostFor(computeErrors(ub1-1, ub1, lb2, lb2+1)), memSum));
    }
    if ((ub1 == unaryConstraints[0]->getUb()) && (ub2 == unaryConstraints[1]->getUb())) {
      memSum = sumCost(sumCost(memUb1, memUb2), mem);
      min = min2(min, diffCost(errorsToCosts->getCostFor(computeErrors(ub1-1, ub1, ub2-1, ub2)), memSum));
    }
    return min;
  }

  // first variable is discretized
  if (!unaryConstraints[1]->isDiscretized()) {
    memLb2 = cost->getLbMem(1);
    memUb2 = cost->getUbMem(1);

    DomainIterator di1 = unaryConstraints[0]->getDomainIterator();
    for (di1.setTo(lb1); *di1 < ub1; ++di1) {
      mem1 = cost->getMemProfile(0, *di1);
      memSum = sumCost(mem1, mem);
      min = diffCost(errorsToCosts->getCostFor(computeErrors(*di1, *di1+1, lb2p, ub2p)), memSum);
      if (lb2 == unaryConstraints[1]->getLb()) {
        memSum = sumCost(mem1, sumCost(memLb2, mem));
        min = min2(min, diffCost(errorsToCosts->getCostFor(computeErrors(*di1, *di1+1, lb2, lb2+1)), memSum));
      }
      if (ub2 == unaryConstraints[1]->getUb()) {
        memSum = sumCost(mem1, sumCost(memUb2, mem));
        min = min2(min, diffCost(errorsToCosts->getCostFor(computeErrors(*di1, *di1+1, ub2-1, ub2)), memSum));
      }
      if (min == 0) {
        return 0;
      }
    }
    return min;
  }

  // second variable is discretized
  if (!unaryConstraints[0]->isDiscretized()) {
    memLb1 = cost->getLbMem(0);
    memUb1 = cost->getUbMem(0);

    DomainIterator di2 = unaryConstraints[1]->getDomainIterator();
    for (di2.setTo(lb2); *di2 < ub2; ++di2) {
      mem2 = cost->getMemProfile(1, *di2);
      memSum = sumCost(mem2, mem);
      min = diffCost(errorsToCosts->getCostFor(computeErrors(lb1p, ub1p, *di2, *di2+1)), memSum);
      if (lb1 == unaryConstraints[0]->getLb()) {
        memSum = sumCost(mem2, sumCost(memLb1, mem));
        min = min2(min, diffCost(errorsToCosts->getCostFor(computeErrors(lb1, lb1+1, *di2, *di2+1)), memSum));
      }
      if (ub1 == unaryConstraints[0]->getUb()) {
        memSum = sumCost(mem2, sumCost(memUb1, mem));
        min = min2(min, diffCost(errorsToCosts->getCostFor(computeErrors(ub1-1, ub1, *di2, *di2+1)), memSum));
      }
      if (min == 0) {
        return 0;
      }
    }
    return min;
  }

  // both variable are discretized
  DomainIterator di1 = unaryConstraints[0]->getDomainIterator();
  for (di1.setTo(lb1); *di1 < ub1; ++di1) {
    mem1 = cost->getMemProfile(0, *di1);

    DomainIterator di2 = unaryConstraints[1]->getDomainIterator();
    for (di2.setTo(lb2); *di2 < ub2; ++di2) {
      mem2 = cost->getMemProfile(1, *di2);

      memSum = sumCost(sumCost(mem1, mem2), mem);
      min = min2(min, diffCost(errorsToCosts->getCostFor(computeErrors(*di1, *di1+1, *di2, *di2+1)), memSum));
      if (min == 0) {
        return 0;
      }
    }
  }
  return min;
}

int Pattern2::getMinCost () {
  int lb1, ub1, lb2, ub2, result;
  currentSupport->unset();
  lb1 = unaryConstraints[0]->getLb();
  ub1 = unaryConstraints[0]->getUb();
  lb2 = unaryConstraints[1]->getLb();
  ub2 = unaryConstraints[1]->getUb();
  result = computeCost(lb1, ub1, lb2, ub2);
  return result;
}

int Pattern2::getMinCost (Variable *v, int val) {
  int i, lb1, ub1, lb2, ub2, result;
  currentSupport->unset();
  i = getVariableIndex(v);
  lb1 = unaryConstraints[0]->getLb();
  ub1 = unaryConstraints[0]->getUb();
  lb2 = unaryConstraints[1]->getLb();
  ub2 = unaryConstraints[1]->getUb();
  switch (i) {
    case 0:
      lb1 = val;
      ub1 = val+1;
      break;
    case 1:
      lb2 = val;
      ub2 = val+1;
      break;
  }
  result = computeCost(lb1, ub1, lb2, ub2);
  return result;
}

int Pattern2::getMinLbCost (Variable *v) {
  return getMinCost(v, v->getUnaryConstraint()->getLb());
}

int Pattern2::getMinUbCost (Variable *v) {
  return getMinCost(v, v->getUnaryConstraint()->getUb()-1);
}

int Pattern2::getCost (Support *s) {
  int i1 = s->getSupport(0);
  int i2 = s->getSupport(1);
  return (computeCost(i1, i1+1, i2, i2+1));
}

int Pattern2::getFinalCost (int *values) {
  int i1 = values[0];
  int i2 = values[1];
  int l2 = i2 - i1 + 1;
  int res;
  Sequence *s = sequence->getSubSequence(i1, l2);
  res = min2(getMaxCost()+1, errorsToCosts->getCostFor(getEditSubstitutionDistance(pattern, s, matchingValues)));
  delete s;
  return res;
}

