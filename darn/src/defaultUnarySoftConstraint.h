// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef UNCONS_H_INCLUDED
#define UNCONS_H_INCLUDED

#include "unaryCost.h"
#include "abstractSoftConstraint.h"


/**
 * The UnaryConstraint class stands for a WCSP's unary constraint. It implements a unary constraint with all costs equal to null, and all the mecanisms of standard propagation.  This class and the method getMinCost() should be overloaded in order to get a real unary constraint. It contains no information by itself, but have pointers to Domain and UnaryCost.
 */
class DefaultUnarySoftConstraint : public AbstractSoftConstraint {

  /**
   * the variable related to this constraint
   */
  Variable *variable;
  /**
   * the domain of the variable related to this constraint
   */
  Domain *domain;

public:
  /**
   * the constuctor (creates the UnaryCost)
   * @param v the variable constrained by this instance
   * @param i the index
   * @param e2c the convertor from errors to costs
   */
  DefaultUnarySoftConstraint (Variable *v, int i, ErrorsToCosts *e2c);
  /**
   * destructor
   */
  ~DefaultUnarySoftConstraint();

  /**
   * reset the constraint for a new search
   */
  virtual void reset();

  /**
   * @return the variable constrained by this instance
   */
  Variable *getVariable ();
  /**
   * @return the domain of the variable constrained by this instance
   */
  Domain *getDomain ();
  /**
   * @return its unaryCost
   */
  UnaryCost *getUnaryCost ();

  /**
   * @return whether the variable is discretized (ask to Domain)
   */
  bool isDiscretized ();
  /**
   * discretize the variable (ask to Domain), also notify the nary constraints
   */
  virtual void setDiscretized ();
  /**
   * assign the variable (ask to Domain and UnaryCost) also notify the nary constraints for discretizing
   * @param val the value the variable is assigned to
   */
  virtual void assign (int val);
  /**
   * @return whether the variable is assigned (ask to Domain)
   */
  virtual bool isAssigned ();
  /**
   * @return lower bound (ask to Domain)
   */
  virtual int getLb ();
  /**
   * @return upper bound (ask to Domain)
   */
  virtual int getUb ();
  /**
   * set the lower bound (ask to Domain)
   * @param l the new lower bound
   */
  virtual void setLb (int l);
  /**
   * set the upper bound (ask to Domain)
   * @param u the new upper bound
   */
  virtual void setUb (int u);
  /**
   * increase the lower bound (ask to Domain)
   * @param l the increase
   */
  virtual void incLb (int l);
  /**
   * decrease the upper bound (ask to Domain)
   * @param u the decrease
   */
  virtual void decUb (int u);
  /**
   * @pre The variable is discretized
   *
   * delete a value of the domain (ask to Domain)
   * @param i the value to be deleted
   */
  virtual void delValue (int i);
  /**
   * @return whether the domain is empty (ask to Domain)
   */
  bool isEmpty ();
  /**
   * check whether the given value is in the domain
   * @param v the checked value
   * @return whether the value is in the domain
   */
  bool contains (int v);
  /**
   * @return a new DomainIterator (ask to Domain)
   */
  DomainIterator getDomainIterator ();
  /**
   * @return a new DomainIterator that goes backward (ask to Domain)
   */
  DomainIterator getReverseDomainIterator ();
  /**
   * @return the cost of the lower bound (ask to UnaryCost)
   */
  virtual int getLbMem ();
  /**
   * @pre The variable is not discretized
   *
   * @return the cost of the upper bound (ask to UnaryCost)
   */
  virtual int getUbMem ();
  /**
   * @pre The variable is not discretized
   *
   * set the cost of the lower bound (ask to UnaryCost)
   * @param i the new lower bound's cost
   */
  virtual void setLbMem (int i);
  /**
   * @pre The variable is not discretized
   *
   * set the cost of the upper bound (ask to UnaryCost)
   * @param i the new upper bound's cost
   */
  virtual void setUbMem (int i);
  /**
   * @pre The variable is not discretized
   *
   * increase the cost of the lower bound (ask to UnaryCost)
   * @param i the increment
   */
  virtual void incLbMem (int i);
  /**
   * @pre The variable is not discretized
   *
   * increase the cost of the upper bound (ask to UnaryCost)
   * @param i the increment
   */
  virtual void incUbMem (int i);
  /**
   * @pre The variable is not discretized
   *
   * decrease the cost of the lower bound (ask to UnaryCost)
   * @param i the decrement
   */
  virtual void decLbMem (int i);
  /**
   * @pre The variable is not discretized
   *
   * decrease the cost of the upper bound (ask to UnaryCost)
   * @param i the decrement
   */
  virtual void decUbMem (int i);
  /**
   * @pre The variable is not discretized
   *
   * set the cost of the lower bound to zero (ask to UnaryCost)
   */
  virtual void resetLbMem ();
  /**
   * @pre The variable is not discretized
   *
   * set the cost of the upper bound to zero (ask to UnaryCost)
   */
  virtual void resetUbMem ();
  /**
   * @pre The variable is discretized
   *
   * get the unary cost of an element of the domain (ask to UnaryCost)
   * @param i the element
   * @return the cost
   */
  virtual int getMemProfile (int i);
  /**
   * @pre The variable is discretized
   *
   * set the unary cost of an element of the domain (ask to UnaryCost)
   * @param i the element
   * @param v the value
   */
  virtual void setMemProfile (int i, int v);
  /**
   * @pre The variable is discretized
   *
   * increase the unary cost of an element of the domain (ask to UnaryCost)
   * @param i the element
   * @param v the increase
   */
  virtual void incMemProfile (int i, int v);
  /**
   * @pre The variable is discretized
   *
   * decrease the unary cost of an element of the domain (ask to UnaryCost)
   * @param i the element
   * @param v the decrease
   */
  virtual void decMemProfile (int i, int v);
  /**
   * get the memory of the unary cost -- what has been projected from the unary constraint to the zero arity constraint (ask to UnaryCost)
   * @return the cost
   */
  virtual int getMem ();
  /**
   * set the memory of the unary cost -- what has been projected from the unary constraint to the zero arity constraint (ask to UnaryCost)
   * @param m the cost
   */
  virtual void setMem (int m);
  /**
   * increase the memory of the unary cost -- what has been projected from the unary constraint to the zero arity constraint) (ask to UnaryCost)
   * @param m the increase
   */
  virtual void incMem (int m);
  /**
   * decrease the memory of the unary cost -- what has been projected from the unary constraint to the zero arity constraint (ask to UnaryCost)
   * @param m the decrease
   */
  virtual void decMem (int m);
  /**
   * @warning To be overloaded by every unary constraint.
   *
   * compute the minimum cost of the unary constraint
   * @return the mininum, 0 by default in this class
   */
  virtual int getMinCost ();
  /**
   * compute the cost of the given support
   * @param s the given support
   * @return the cost
   */
  virtual int getCost (Support *s);

  /**
   * enforce the unary support
   */
  virtual void zeroInverse ();
  /**
   * enforce NC: enforce the unary support and prune the forbidden values
   * @return true if a value has been pruned
   */
  virtual bool setANC ();
  
  /**
   * print the name and the state of the constraint
   * @return a string containing the variable and @a mem
   */
  virtual string print ();

  /**
   * make the Domain and the UnaryCost save the current values
   */
  void save () ;
  /**
   * make the Domain and the UnaryCost restore the current values
   */
  void restore () ;
};

#endif
