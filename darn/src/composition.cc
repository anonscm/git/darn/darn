// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "composition.h"

Composition::Composition (Variable *v1, Variable *v2, int i, bool o, Nucleotide *n1, Nucleotide *n2, int t1, int t2, Relation r, int mc, ErrorsToCosts *e2c) : SoftConstraint(i, e2c, o, 2, v1, v2) {
  nucleotide1 = n1;
  nucleotide2 = n2;
  relation = r;
  threshold1 = t1;
  threshold2 = t2;
  minCost = mc;
  name = "composition";
}

bool Composition::isCorrectNucleotide (Nucleotide *n) {
  return ((n == nucleotide1) || (n == nucleotide2));
}

int Composition::getCost (int nbPositives, int nbTotal) {
  int ratio;
  float a, b;

  if (nbTotal == 0) {
    return 0;
  }
  ratio = (int) ((((float) nbPositives) / ((float) nbTotal)) * 100);
  if (relation == NOT_LESS_THAN) {
    if (ratio < threshold1) {
      return getMaxCost()+1;
    }
    if (ratio < threshold2) {
      a = (((float) minCost-getMaxCost()) / ((float) threshold2-threshold1));
      b = (((float) getMaxCost()*threshold2-minCost*threshold1) / ((float) threshold2-threshold1));
      return ((int) (a * ratio + b));
    }
    return minCost;
  }
  if (relation == NOT_GREATER_THAN) {
    if (ratio <= threshold1) {
      return minCost;
    }
    if (ratio <= threshold2) {
      a = (((float) getMaxCost()-minCost) / ((float) threshold2-threshold1));
      b = (((float) minCost*threshold2-getMaxCost()*threshold1) / ((float) threshold2-threshold1));
      return ((int) (a * ratio + b));
    }
    return getMaxCost()+1;
  }
  return getMaxCost()+1;
}

int Composition::computeCost (int i1, int i2) {
  int nbPositives = 0;
  int nbTotal = 0;
  int mem, mem1, mem2;

  for (int i = i1; i <= i2; i++) {
    nbTotal++;
    if (isCorrectNucleotide(sequence->getNucleotide(i))) {
      nbPositives++;
    }
  }
  mem = getMem();
  mem1 = getMemory(unaryConstraints[0]->getVariable(), i1);
  mem2 = getMemory(unaryConstraints[1]->getVariable(), i2);
  return diffCost(getCost(nbPositives, nbTotal), mem + mem1 + mem2);
}

int Composition::getMinCost () {
  int lb1, ub1, lb2, ub2;
  int result = 0;
  currentSupport->unset();

  lb1 = unaryConstraints[0]->getLb();
  ub1 = unaryConstraints[0]->getUb();
  lb2 = unaryConstraints[1]->getLb();
  ub2 = unaryConstraints[1]->getUb();
  if ((lb1+1 == ub1) && (lb2+1 == ub2)) {
    result = computeCost(lb1, lb2);
  }
  return result;
}

int Composition::getMinCost (Variable *v, int val) {
  int i, lb1, ub1, lb2, ub2;
  int result = 0;
  currentSupport->unset();

  i = getVariableIndex(v);
  lb1 = unaryConstraints[0]->getLb();
  ub1 = unaryConstraints[0]->getUb();
  lb2 = unaryConstraints[1]->getLb();
  ub2 = unaryConstraints[1]->getUb();
  switch (i) {
    case 0:
      lb1 = val;
      ub1 = val+1;
      break;
    case 1:
      lb2 = val;
      ub2 = val+1;
      break;
  }
  if ((lb1+1 == ub1) && (lb2+1 == ub2)) {
    result = computeCost(lb1, lb2);
  }
  return result;
}

int Composition::getMinLbCost (Variable *v) {
  int result = getMinCost(v, v->getUnaryConstraint()->getLb());
  return result;
}

int Composition::getMinUbCost (Variable *v) {
  int result = getMinCost(v, v->getUnaryConstraint()->getUb()-1);
  return result;
}

int Composition::getCost (Support *s) {
  int i1 = s->getSupport(0);
  int i2 = s->getSupport(1);
  return (computeCost(i1, i2));
}

int Composition::getFinalCost (int *values) {
  int start = values[0];
  int stop = values[1];
  int nbPositives = 0;
  int nbTotal = 0;
  
  for (int i = start; i <= stop; i++) {
    nbTotal++;
    if (isCorrectNucleotide(sequence->getNucleotide(i))) {
      nbPositives++;
    }
  }
  return getCost(nbPositives, nbTotal);
}
