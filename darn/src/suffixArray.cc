// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "suffixArray.h"


SuffixArray::SuffixArray (Sequence *s, InteractionValues *iv) : runs(-1) {
  int *tempSeq;
  int *tempSa;
  sequence = s;
  interactionValues = iv;
  size = sequence->getLength();
  maxSize = DUPLEXMAXLENGTH+1;
  fsStack = new FixedSizeStack(2 * maxSize);
  tempSeq = new int[size+3];
  tempSa = new int[size+3];
  suffixArray = new Entry*[size];
  for (unsigned int i = 0; i < size; i++) {
   tempSeq[i] = (sequence->getNucleotide(i)->getCode()) + 1;
  }
  tempSeq[size] = tempSeq[size+1] = tempSeq[size+2] = 0;
  buildSuffixArray(tempSeq, tempSa, size, NBNUCLEOTIDES);
  for (unsigned int i = 0; i < size; i++) {
    suffixArray[i] = new Entry;
    suffixArray[i]->suf = tempSa[i];
  }
  delete[] tempSeq;
  delete[] tempSa;
  setLcps();
  setChildren();
  setMinMax();
  setRuns();
  matrix = new int[maxSize];
}

inline int SuffixArray::stringCompare (string s1, string s2) {
  int length = min2(s1.length(), s2.length());
  for (int i = 0; i < length; i++) {
    if (s1[i] < s2[i]) {
      return -(i+1);
    }
    else if (s1[i] > s2[i]) {
      return (i+1);
    }
  }
  if (s1.length() > s2.length()) {
    return length+1;
  }
  else if (s1.length() == s2.length()) {
    return 0;
  }
  else {
    return -(length+1);
  }
}

inline int SuffixArray::stringCompare (string s1, string s2, int from, int to) {
  int l1 = min2(s1.length(), to);
  int l2 = min2(s2.length(), to);
  if (from > to) {
    return 0;
  }
  return stringCompare(s1.substr(from, l1-from), s2.substr(from, l2-from));
}

void SuffixArray::radixPass (const int *from, int *to, const int *text, const int n, const int radix) {
  // count occurrences
  int* c = new int[radix + 1];                  // counter array
  for (int i = 0; i <= radix; i++) {            // reset counters
    c[i] = 0;         
  }
  for (int i = 0;  i < n; i++) {                // count occurences
    c[text[from[i]]]++;
  }
  for (int i = 0, sum = 0; i <= radix; i++) {   // exclusive prefix sums
     int t = c[i];
     c[i] = sum;
     sum += t;
  }
  for (int i = 0;  i < n;  i++) {               // sort
    to[c[text[from[i]]]++] = from[i];
  }
  delete [] c;
}

// find the suffix array SA of s[0..n-1] in {1..radix}^n
// require s[n]=s[n+1]=s[n+2]=0, n>=2
void SuffixArray::buildSuffixArray (const int *text, int *sa, const int n, const int radix) {
  int n0=(n+2)/3, n1=(n+1)/3, n2=n/3, n02=n0+n2; 
  int* s12  = new int[n02 + 3];
  int* SA12 = new int[n02 + 3];
  int* s0   = new int[n0];
  int* SA0  = new int[n0];
  
  s12[n02] = s12[n02+1] = s12[n02+2] = 0; 
  SA12[n02] =SA12[n02+1] = SA12[n02+2] = 0; 


  // generate positions of mod 1 and mod  2 suffixes
  // the "+(n0-n1)" adds a dummy mod 1 suffix if n%3 == 1
  for (int i=0, j=0;  i < n+(n0-n1);  i++) {
    if (i%3 != 0) s12[j++] = i;
  }

  // lsb radix sort the mod 1 and mod 2 triples
  radixPass(s12 , SA12, text+2, n02, radix);
  radixPass(SA12, s12 , text+1, n02, radix);  
  radixPass(s12 , SA12, text  , n02, radix);

  // find lexicographic names of triples
  int name = 0, c0 = -1, c1 = -1, c2 = -1;
  for (int i = 0;  i < n02;  i++) {
    if ((text[SA12[i]] != c0) || (text[SA12[i]+1] != c1) || (text[SA12[i]+2] != c2)) { 
      name++;
      c0 = text[SA12[i]];
      c1 = text[SA12[i]+1];
      c2 = text[SA12[i]+2];
    }
    if (SA12[i] % 3 == 1) {                     // left half
      s12[SA12[i]/3] = name;
    } // left half
    else {                                      // right half
      s12[SA12[i]/3 + n0] = name;
    }
  }

  // recurse if names are not yet unique
  if (name < n02) {
    buildSuffixArray(s12, SA12, n02, name);
    // store unique names in s12 using the suffix array 
    for (int i = 0; i < n02; i++) {
      s12[SA12[i]] = i + 1;
    }
  }
  else {
    // generate the suffix array of s12 directly
    for (int i = 0; i < n02; i++) {
      SA12[s12[i] - 1] = i;
    }
  }
    
  // stably sort the mod 0 suffixes from SA12 by their first character
  for (int i = 0, j=0; i < n02; i++) {
    if (SA12[i] < n0) {
      s0[j++] = 3*SA12[i];
    }
  }
  radixPass(s0, SA0, text, n0, radix);

  // merge sorted SA0 suffixes and sorted SA12 suffixes
  for (int p=0, t = n0-n1, k = 0; k < n; k++) {
                                                // pos of current offset 12 suffix
    int i = (SA12[t] < n0 ? SA12[t] * 3 + 1 : (SA12[t] - n0) * 3 + 2);
                                                // pos of current offset 0 suffix
    int j = SA0[p];
    if ((SA12[t] < n0)? leq(text[i], s12[SA12[t] + n0], text[j], s12[j/3]): leq(text[i], text[i+1], s12[SA12[t]-n0+1], text[j], text[j+1], s12[j/3+n0])) {
      // suffix from SA12 is smaller
      sa[k] = i;
      t++;
      if (t == n02) {                           // done --- only SA0 suffixes left
        for (k++; p < n0; p++, k++) {
          sa[k] = SA0[p];
        }
      }
    }
    else { 
      sa[k] = j;
      p++; 
      if (p == n0)  {                           // done --- only SA12 suffixes left
        for (k++; t < n02; t++, k++) {
          sa[k] = (SA12[t] < n0 ? SA12[t] * 3 + 1 : (SA12[t] - n0) * 3 + 2);
        }
      }
    }  
  } 
  delete [] s12;
  delete [] SA12;
  delete [] SA0;
  delete [] s0; 
}

void SuffixArray::setLcps () {
  int value;
  suffixArray[0]->lcp = 0;
  for (unsigned int i = 1; i < size; i++) {
    for (value = 0; (suffixArray[i]->suf+value < static_cast<int>(sequence->getLength())) && (suffixArray[i-1]->suf+value < static_cast<int>(sequence->getLength())) && (sequence->getNucleotide(suffixArray[i]->suf+value)->isEqual(sequence->getNucleotide(suffixArray[i-1]->suf+value))); value++)
      ;
    suffixArray[i]->lcp = value;
    if (maxSize < static_cast<int>(value+1)) maxSize = value+1;
  }
}

void SuffixArray::setChildren () {
  stack<int> intervals;
  Entry *entry;
  int suf;

  intervals.push(0);
  for (unsigned int i = 1; i < size; i++) {
    entry = suffixArray[i];
    entry->next = -1;
    while (entry->lcp < suffixArray[intervals.top()]->lcp) {
      intervals.pop();
    }
    if (entry->lcp == suffixArray[intervals.top()]->lcp) {
      suffixArray[intervals.top()]->next = i;
      intervals.pop();
      intervals.push(i);
    }
    else if (entry->lcp > suffixArray[intervals.top()]->lcp) {
      intervals.push(i);
    }
  }
  while (!intervals.empty()) {
    intervals.pop();
  }

  suffixArray[0]->up = -1;
  suffixArray[0]->down = -1;
  suf = -1;
  intervals.push(0);
  for (unsigned int i = 1; i < size; i++) {
    entry = suffixArray[i];
    entry->down = -1;
    while (entry->lcp < suffixArray[intervals.top()]->lcp) {
      suf = intervals.top();
      intervals.pop();
      if ((entry->lcp <= suffixArray[intervals.top()]->lcp) && (suffixArray[intervals.top()]->lcp != suffixArray[suf]->lcp)) {
        suffixArray[intervals.top()]->down = suf;
      }
    }
    entry->up = suf;
    suf = -1;
    intervals.push(i);
  }
  while (intervals.size() > 1) {
    suf = intervals.top();
    intervals.pop();
    if (suffixArray[intervals.top()]->lcp != suffixArray[suf]->lcp) {
      suffixArray[intervals.top()]->down = suf;
    }
  }
}

pair <int, int> SuffixArray::setMinMax (int i, int j) {
  int min = size-1, max = 0;
  int i1, i2, leaf;
  pair<int, int> p;

  i1 = i;
  if (i == j) {
    min = suffixArray[i]->suf;
    suffixArray[i]->minIndex = -1;
    suffixArray[i]->maxIndex = -1;
    return pair<int, int>(min, min);
  }
  i2 = getLcpSuffix(i, j)-1;
 
  while (i1 >= 0) {
    if (i2 < 0) {
      i2 = j;
    }
    p = setMinMax(i1, i2);
    if (p.first < min) {
      min = p.first;
    }
    if (p.second > max) {
      max = p.second;
    }
    i1 = i2+1;
    if (i1 > j) {
      i1 = -1;
    }
    else {
      i2 = suffixArray[i1]->next-1;
    }
  }
  leaf = getLcpSuffix(i, j);
  suffixArray[leaf]->minIndex = min;
  suffixArray[leaf]->maxIndex = max;

  return pair<int, int>(min, max);
}

void SuffixArray::setMinMax () {
  int i1, i2;

  i1 = 0;
  while (i1 >= 0) {
    i2 = suffixArray[i1]->next-1;
    if (i2 < 0) {
      i2 = size-1;
    }
    setMinMax(i1, i2);
    i1 = i2+1;
    if (i1 > static_cast<int>(size)-1) {
      i1 = -1;
    }
  }
}

void SuffixArray::setRuns () {
  for (unsigned int i = 0; i < size; i++) {
    suffixArray[i]->runs = -1;
    suffixArray[i]->singleRuns = -1;
  }
}

inline int SuffixArray::getLcpSuffix (int i, int j) {
  int k = ((j >= static_cast<int>(size)-1)? -1: suffixArray[j+1]->up);
  int l = suffixArray[i]->down;
  if ((k != -1) && (i < k) && (k <= j)) {
    return k;
  }
  return l;
}

inline int SuffixArray::getLcp (int i, int j) {
  return suffixArray[getLcpSuffix(i, j)]->lcp;
}

inline bool SuffixArray::emptyJoin (int lb1, int ub1, int lb2, int ub2) {
  return ((lb1 > ub1) || (lb2 > ub2) || (lb1 > ub2) || (lb2 > ub1));
}

inline bool SuffixArray::isInside (int i, int lb, int ub) {
  return ((lb < ub) && (i >= lb) && (i < ub));
}

inline pair <int, int> SuffixArray::getIndexInterval (int i, int j) {
  int leaf, value;
  if (i == j) {
    value = suffixArray[i]->suf;
    return pair<int, int>(value, value+1);
  }
  leaf = getLcpSuffix(i, j);
  return pair<int, int>(suffixArray[leaf]->minIndex, suffixArray[leaf]->maxIndex+1);
}

  /**
   * get all the prefixes of @a s2 than are a @a nbMaxErrors-approximation of @a s1
   * if @a bothSides is true, check also if prefixes of @a s1 are approximations of @a s2
   * (if so, the distance is a negative integer)
   * it gives a stack of couples <size of the prefix, distance>
   */
void SuffixArray::getCandidates (Sequence *s1, Sequence *s2, int nbMaxErrors, bool bothSides) {
  int match;
  int minValue;
  int previous = 0, next, value = 0;
  int l1 = s1->getLength();
  int l2 = s2->getLength();
  Nucleotide *n1, *n2;

  fsStack->clear();
  
  if (nbMaxErrors == 0) {
    for (int i = 0; i < min2(l1, l2); i++) {
      if (interactionValues->getMatchCost(s1->getNucleotide(i), s2->getNucleotide(i)) == 0) {
        return;
      }
    }
    if ((bothSides) && (l1 >= l2)) {
      fsStack->push(0, -1);
    }
    else {
      fsStack->push(0, 0);
    }
    return;
  }
  if (l1 == 0) {
    value = 0;
    for (int i = 0; i < l2; i++) {
      if ((bothSides) && (i == l2-1)) {
        fsStack->push(0, -value-1);
      }
      else {
        fsStack->push(i, value);
      }
      value += interactionValues->getInsertionCost();
      if (value > nbMaxErrors) {
        return;
      }
    }
    return;
  }
  if (l1 == 1) {
    match = interactionValues->getInsertionCost();
    if (nbMaxErrors > 0) {
      fsStack->push(0, interactionValues->getInsertionCost());
    }
    value = 0;
    n1 = s1->getNucleotide(0);
    for (int i = 0; i < l2; i++) {
      n2 = s2->getNucleotide(i);
      if ((match != 0)) {
        match = min2(interactionValues->getMatchCost(n1, n2), match);
      }
      if (value + match > nbMaxErrors) {
        return;
      }
      if ((bothSides) && (i == l2-1)) {
        if ((i+1)*interactionValues->getInsertionCost() <= nbMaxErrors) {
          fsStack->push(0, -(i+1)*interactionValues->getInsertionCost()-1);
        }
        fsStack->push(1, -(value+match)-1);
      }
      else {
        fsStack->push(i+1, value+match);
      }
      value += interactionValues->getInsertionCost();
    }
    return;
  }
 
  for (int i = 1; i < l1+1; i++) {
    matrix[i] = i*interactionValues->getInsertionCost();
  }
  if (l1*interactionValues->getInsertionCost() <= nbMaxErrors) {
    fsStack->push(0, l1*interactionValues->getInsertionCost());
  }
  for (int j = 1; j < l2+1; j++) {
    previous = (j-1)*interactionValues->getInsertionCost();
    matrix[0] = j*interactionValues->getInsertionCost();
    minValue = matrix[0];
    n2 = s2->getNucleotide(j-1);
    for (int i = 1; i < l1+1; i++) {
      n1 = s1->getNucleotide(i-1);
      match = interactionValues->getMatchCost(n1, n2);
      next = matrix[i];
      value = min3(previous+match, matrix[i]+interactionValues->getInsertionCost(), matrix[i-1]+interactionValues->getInsertionCost());
      matrix[i] = value;
      previous = next;
      if (value < minValue) {
        minValue = value;
      }
    }
    if (minValue == nbMaxErrors+1) {
      return;
    }
    if ((value <= nbMaxErrors) && ((j != l2) || (!bothSides))) {
      fsStack->push(j, value);
    }
  }
  if (bothSides) {
    for (int i = 0; i < l1+1; i++) {
      value = matrix[i];
      if (value <= nbMaxErrors) {
        fsStack->push(i, -value-1);
      }
    }
  }
}

int SuffixArray::lookForApproximate (Sequence *pattern, int lbStart, int ubStart, int lbStop, int ubStop, int maxCost) {
  stack <StoredInterval> intervals;
  int pSize = pattern->getLength();
  int sSize = sequence->getLength();
  int firstLetterIndex = 0, lastLetterIndex = 0;
  int lcp = 0;
  int i, j, upperBound, lcpSuffix = 0, lb, ub;
  int patternIndex;
  int nbErrors;
  int extraIndex, extraErrors;
  pair<int, int> p;
  int nbMaxErrors = maxCost+1;
  
  lbStop--;
  ubStop--;
  ubStart++;
  ubStop++;

  runs++;

  intervals.push(StoredInterval(0, size-1, 0, 0, 0));
  while (!intervals.empty()) {
    i = intervals.top().bot;
    j = intervals.top().top;
    firstLetterIndex = intervals.top().lcp;
    patternIndex = intervals.top().patternIndex;
    nbErrors = intervals.top().nbErrors;
    intervals.pop();

    upperBound = j;

    if ((i == 0) && (j == static_cast<int>(size)-1)) {
      j = suffixArray[0]->next-1;
    }
    else {
      j = getLcpSuffix(i, j)-1;
    }
    if (j <= -1) {
      j = upperBound;
    }
    while (i <= upperBound) {
      p = getIndexInterval(i, j);
      lb = p.first;
      ub = p.second;
      if ((!emptyJoin(lb, ub, lbStart, ubStart)) && (!emptyJoin(lb+pSize-nbMaxErrors-1, ub+pSize+nbMaxErrors-1, lbStop, ubStop))) {
        if (i != j) {
          lcpSuffix = getLcpSuffix(i, j);
          lcp = getLcp(i, j);
          lastLetterIndex = lcp;

          if ((runs > suffixArray[lcpSuffix]->runs) || (nbErrors < suffixArray[lcpSuffix]->nbErrors) || (patternIndex != suffixArray[lcpSuffix]->patternSize)) {

            Sequence subSequence1 (*sequence, suffixArray[i]->suf+firstLetterIndex, lastLetterIndex-firstLetterIndex);
            Sequence subSequence2 (*pattern, patternIndex);
            getCandidates(&subSequence1, &subSequence2, nbMaxErrors, true);

            while (! fsStack->empty()) {
              extraIndex = fsStack->top()->first;
              extraErrors = fsStack->top()->second;
              fsStack->pop();

              if (extraErrors >= 0) {
                if ((nbErrors + extraErrors < nbMaxErrors) && (!emptyJoin(lb+pSize-nbMaxErrors-1, ub+pSize+nbMaxErrors-1, lbStop, ubStop))) {

                  suffixArray[lcpSuffix]->runs = runs;
                  suffixArray[lcpSuffix]->patternSize = patternIndex+extraIndex;
                  suffixArray[lcpSuffix]->nbErrors = nbErrors+extraErrors;

                  intervals.push(StoredInterval(i, j, lastLetterIndex, patternIndex+extraIndex, nbErrors+extraErrors));
                }
              }
              else {
                extraErrors = -extraErrors-1;
                if (nbErrors + extraErrors < nbMaxErrors) {
                  for (int k = i; k <= j; k++) {
                    if ((isInside(suffixArray[k]->suf, lbStart, ubStart)) && (isInside(suffixArray[k]->suf+firstLetterIndex+extraIndex-1, lbStop, ubStop))) {
                      nbMaxErrors = nbErrors+extraErrors;
                      if (nbMaxErrors == 0) {
                        return 0;
                      }
                    }
                  }
                }
                if ((firstLetterIndex+extraIndex == lastLetterIndex) && (nbErrors + extraErrors < nbMaxErrors-1)) {
                  for (int k = i; k <= j; k++) {
                    if (isInside(suffixArray[k]->suf, lbStart, ubStart)) {
                      for (int e = 1; e < nbMaxErrors-(nbErrors+extraErrors); e++) {
                        if (!isInside(suffixArray[k]->suf+lastLetterIndex+e-1, lbStop, ubStop)) {
                          break;
                        }
                        nbMaxErrors = nbErrors+extraErrors+e;
                      }
                    }
                  }
                }
              }
            }
          }
        }
        else {
          if ((runs > suffixArray[i]->singleRuns) || (patternIndex != suffixArray[i]->singlePatternSize) || (nbErrors < suffixArray[i]->singleNbErrors)) {

            suffixArray[i]->singleRuns = runs;
            suffixArray[i]->singlePatternSize = patternIndex;
            suffixArray[i]->singleNbErrors = nbErrors;

            Sequence subSequence1 (*pattern, patternIndex);
            Sequence subSequence2 (*sequence, suffixArray[i]->suf+firstLetterIndex, min2(sSize-(suffixArray[i]->suf+firstLetterIndex), pSize-patternIndex+(nbMaxErrors-nbErrors)));
            getCandidates(&subSequence1, &subSequence2, nbMaxErrors, false);

            while (! fsStack->empty()) {
              extraIndex = fsStack->top()->first;
              extraErrors = fsStack->top()->second;
              fsStack->pop();

              if ((nbErrors + extraErrors < nbMaxErrors) && (isInside(suffixArray[i]->suf+firstLetterIndex+extraIndex-1, lbStop, ubStop))) {
                nbMaxErrors = nbErrors+extraErrors;
                if (nbMaxErrors == 0) {
                  return 0;
                }
              }
            }
          }
        }
      }
      i = j+1;
      if (j < upperBound) {
        j = suffixArray[j+1]->next-1;
        if (j < 0) {
          j = upperBound;
        }
      }
    }
  }
  return nbMaxErrors;
}

bool SuffixArray::lookFor (Sequence *pattern, int lbStart, int ubStart, int lbStop, int ubStop, int maxCost, Support *support) {
  stack <StoredInterval> intervals;
  int firstLetterIndex = 0, lastLetterIndex = 0;
  int lcp = 0;
  int i, j, upperBound, lcpSuffix = 0, lb, ub;
  int patternIndex;
  int nbErrors;
  int extraIndex, extraErrors;
  pair<int, int> p;
  int nbMaxErrors = maxCost+1;
  int pSize = pattern->getLength();

  runs++;

  intervals.push(StoredInterval(0, size-1, 0, 0, 0));
  while (!intervals.empty()) {
    i = intervals.top().bot;
    j = intervals.top().top; 
    firstLetterIndex = intervals.top().lcp;
    patternIndex = intervals.top().patternIndex;
    nbErrors = intervals.top().nbErrors;
    intervals.pop();

    upperBound = j;

    if ((i == 0) && (j == static_cast<int>(size)-1)) {
      j = suffixArray[0]->next-1;
    }
    else {
      j = getLcpSuffix(i, j)-1;
    }
    if (j <= -1) {
      j = upperBound;
    }
    while (i <= upperBound) {
      p = getIndexInterval(i, j);
      lb = p.first;
      ub = p.second;
      if ((!emptyJoin(lb, ub, lbStart, ubStart)) && (!emptyJoin(lb+pSize-nbMaxErrors-1, ub+pSize+nbMaxErrors-1, lbStop, ubStop))) {
        if (i != j) {
          lcpSuffix = getLcpSuffix(i, j);
          lcp = getLcp(i, j);
          lastLetterIndex = lcp;

          if ((runs > suffixArray[lcpSuffix]->runs) || (nbErrors < suffixArray[lcpSuffix]->nbErrors) || (patternIndex != suffixArray[lcpSuffix]->patternSize)) {

            Sequence subsequence1 (*sequence, suffixArray[i]->suf+firstLetterIndex, lastLetterIndex-firstLetterIndex);
            Sequence subsequence2 (*pattern, patternIndex);
            getCandidates(&subsequence1, &subsequence2, nbMaxErrors, true);

            while (! fsStack->empty()) {
              extraIndex = fsStack->top()->first;
              extraErrors = fsStack->top()->second;
              fsStack->pop();

              if (extraErrors >= 0) {
                if ((nbErrors + extraErrors < nbMaxErrors) && (!emptyJoin(lb+pSize-nbMaxErrors-1, ub+pSize+nbMaxErrors-1, lbStop, ubStop))) {

                  suffixArray[lcpSuffix]->runs = runs;
                  suffixArray[lcpSuffix]->patternSize = patternIndex+extraIndex;
                  suffixArray[lcpSuffix]->nbErrors = nbErrors+extraErrors;

                  intervals.push(StoredInterval(i, j, lastLetterIndex, patternIndex+extraIndex, nbErrors+extraErrors));
                }
              }
              else {
                extraErrors = -extraErrors-1;
                if (nbErrors + extraErrors < nbMaxErrors) {
                  for (int k = i; k <= j; k++) {
                    if ((isInside(suffixArray[k]->suf, lbStart, ubStart)) && (isInside(suffixArray[k]->suf+firstLetterIndex+extraIndex-1, lbStop, ubStop))) {
                      if (support != NULL) {
                        support->setSupport(2, suffixArray[k]->suf);
                        support->setSupport(3, suffixArray[k]->suf+firstLetterIndex+extraIndex);
                      }
                      return true;
                    }
                  }
                }
              }
            }
          }
        }
        else {
          if ((runs > suffixArray[i]->singleRuns) || (patternIndex != suffixArray[i]->singlePatternSize) || (nbErrors < suffixArray[i]->singleNbErrors)) {

            suffixArray[i]->singleRuns = runs;
            suffixArray[i]->singlePatternSize = patternIndex;
            suffixArray[i]->singleNbErrors = nbErrors;

            Sequence subsequence1 (*pattern, patternIndex);
            Sequence subsequence2 (*sequence, suffixArray[i]->suf+firstLetterIndex, min2(sequence->getLength()-(suffixArray[i]->suf+firstLetterIndex), pSize-patternIndex+(nbMaxErrors-nbErrors)));
            getCandidates(&subsequence1, &subsequence2, nbMaxErrors, false);

            while (! fsStack->empty()) {
              extraIndex = fsStack->top()->first;
              extraErrors = fsStack->top()->second;
              fsStack->pop();

              if ((nbErrors + extraErrors < nbMaxErrors) && (isInside(suffixArray[i]->suf+firstLetterIndex+extraIndex-1, lbStop, ubStop))) {
                if (support != NULL) {
                  support->setSupport(2, suffixArray[i]->suf);
                  support->setSupport(3, suffixArray[i]->suf+firstLetterIndex+extraIndex);
                }
                return true;
              }
            }
          }
        }
      }
      i = j+1;
      if (j < upperBound) {
        j = suffixArray[j+1]->next-1;
        if (j < 0) {
          j = upperBound;
        }
      }
    }
  }
  if (support != NULL) {
    support->unset();
  }
  return false;
}

void SuffixArray::lookForAll (Sequence *pattern, int lbStart, int ubStart, int lbStop, int ubStop, int maxCost, int *values) {
  stack <StoredInterval> intervals;
  int firstLetterIndex = 0, lastLetterIndex = 0;
  int lcp = 0;
  int i, j, upperBound, lcpSuffix = 0, lb, ub;
  int patternIndex;
  int nbErrors;
  int extraIndex, extraErrors;
  pair<int, int> p;
  int nbMaxErrors = maxCost+1;
  int lb1 = ubStart+1, lb1Support = lbStart-1;
  int ub1 = lbStart-1, ub1Support = ubStart+1;
  int lb2 = ubStop+1, lb2Support = lbStop-1;
  int ub2 = lbStop-1, ub2Support = ubStop+1;
  int wordStart, wordStop;
  int pSize = pattern->getLength();
  int sSize = sequence->getLength();

  runs++;

  intervals.push(StoredInterval(0, size-1, 0, 0, 0));
  while (!intervals.empty()) {
    i = intervals.top().bot;
    j = intervals.top().top; 
    firstLetterIndex = intervals.top().lcp;
    patternIndex = intervals.top().patternIndex;
    nbErrors = intervals.top().nbErrors;
    intervals.pop();

    upperBound = j;

    if ((i == 0) && (j == static_cast<int>(size)-1)) {
      j = suffixArray[0]->next-1;
    }
    else {
      j = getLcpSuffix(i, j)-1;
    }
    if (j <= -1) {
      j = upperBound;
    }
    while (i <= upperBound) {
      p = getIndexInterval(i, j);
      lb = p.first;
      ub = p.second;
      if ((!emptyJoin(lb, ub, lbStart, ubStart)) && (!emptyJoin(lb+pSize-nbMaxErrors-1, ub+pSize+nbMaxErrors-1, lbStop, ubStop))) {
        if (i != j) {
          lcpSuffix = getLcpSuffix(i, j);
          lcp = getLcp(i, j);
          lastLetterIndex = lcp;

          if ((runs > suffixArray[lcpSuffix]->runs) || (nbErrors <= suffixArray[lcpSuffix]->nbErrors) || (patternIndex != suffixArray[lcpSuffix]->patternSize)) {

            Sequence subsequence1 (*sequence, suffixArray[i]->suf + firstLetterIndex, lastLetterIndex-firstLetterIndex); 
            Sequence subsequence2 (*pattern, patternIndex);
            getCandidates(&subsequence1, &subsequence2, nbMaxErrors, true);

            while (! fsStack->empty()) {
              extraIndex = fsStack->top()->first;
              extraErrors = fsStack->top()->second;
              fsStack->pop();

              if (extraErrors >= 0) {
                if ((nbErrors + extraErrors < nbMaxErrors) && (!emptyJoin(lb+pSize-nbMaxErrors-1, ub+pSize+nbMaxErrors-1, lbStop, ubStop))) {

                  suffixArray[lcpSuffix]->runs = runs;
                  suffixArray[lcpSuffix]->patternSize = patternIndex+extraIndex;
                  suffixArray[lcpSuffix]->nbErrors = nbErrors+extraErrors;

                  intervals.push(StoredInterval(i, j, lastLetterIndex, patternIndex+extraIndex, nbErrors+extraErrors));
                }
              }
              else {
                extraErrors = -extraErrors-1;
                if (nbErrors + extraErrors < nbMaxErrors) {
                  for (int k = i; k <= j; k++) {
                    if ((isInside(suffixArray[k]->suf, lbStart, ubStart)) && (isInside(suffixArray[k]->suf+firstLetterIndex+extraIndex-1, lbStop, ubStop))) {
                      wordStart = suffixArray[k]->suf;
                      wordStop = suffixArray[k]->suf+firstLetterIndex+extraIndex-1;
                      if (wordStart < lb1) {
                        lb1 = wordStart;
                        lb1Support = wordStop;
                      }
                      if (wordStart > ub1) {
                        ub1 = wordStart;
                        ub1Support = wordStop;
                      }
                      if (wordStop < lb2) {
                        lb2 = wordStop;
                        lb2Support = wordStart;
                      }
                      if (wordStop > ub2) {
                        ub2 = wordStop;
                        ub2Support = wordStart;
                      }
                    }
                  }
                }
              }
            }
          }
        }
        else {
          if ((runs > suffixArray[i]->singleRuns) || (patternIndex != suffixArray[i]->singlePatternSize) || (nbErrors <= suffixArray[i]->singleNbErrors)) {

            suffixArray[i]->singleRuns = runs;
            suffixArray[i]->singlePatternSize = patternIndex;
            suffixArray[i]->singleNbErrors = nbErrors;

            Sequence subsequence1 (*pattern, patternIndex);
            Sequence subsequence2 (*sequence, suffixArray[i]->suf+firstLetterIndex, min2(sSize-(suffixArray[i]->suf+firstLetterIndex), pSize-patternIndex+(nbMaxErrors-nbErrors)));
            getCandidates(&subsequence1, &subsequence2, nbMaxErrors, false);

            while (! fsStack->empty()) {
              extraIndex = fsStack->top()->first;
              extraErrors = fsStack->top()->second;
              fsStack->pop();

              if ((nbErrors + extraErrors < nbMaxErrors) && (isInside(suffixArray[i]->suf+firstLetterIndex+extraIndex-1, lbStop, ubStop))) {
                wordStart = suffixArray[i]->suf;
                wordStop = suffixArray[i]->suf+firstLetterIndex+extraIndex-1;
                if (wordStart < lb1) {
                  lb1 = wordStart;
                  lb1Support = wordStop;
                }
                if (wordStart > ub1) {
                  ub1 = wordStart;
                  ub1Support = wordStop;
                }
                if (wordStop < lb2) {
                  lb2 = wordStop;
                  lb2Support = wordStart;
                }
                if (wordStop > ub2) {
                  ub2 = wordStop;
                  ub2Support = wordStart;
                }
              }
            }
          }
        }
      }
      i = j+1;
      if (j < upperBound) {
        j = suffixArray[j+1]->next-1;
        if (j < 0) {
          j = upperBound;
        }
      }
    }
  }
  values[0] = lb1;
  values[1] = lb1Support;
  values[2] = ub1+1;
  values[3] = ub1Support;
  values[4] = lb2;
  values[5] = lb2Support;
  values[6] = ub2+1;
  values[7] = ub2Support;
}

int SuffixArray::lookForApproximate (Sequence *pattern, int lb, int ub, int maxCost) {
  int l1 = pattern->getLength();
  //int l2 = ub-lb+1;
  int l2 = ub-lb;
  int match, minValue;
  Nucleotide *n1, *n2;
  int previous = 0, next, value = 0;

  if (ub < lb) {
    return maxCost+1;
  }

  if (l1 == 0) {
    return l2;
  }
  if (l2 == 0) {
    return l1;
  }
  if (abs(l1-l2) > maxCost) {
    return maxCost+1;
  }
  for (int i = 1; i < l1+1; i++) {
    matrix[i] = i*interactionValues->getInsertionCost();
  }
  for (int j = 1; j < l2+1; j++) {
    previous = (j-1)*interactionValues->getInsertionCost();
    matrix[0] = j*interactionValues->getInsertionCost();
    minValue = matrix[0];
    n2 = sequence->getNucleotide(lb+j-1);
    for (int i = 1; i < l1+1; i++) {
      n1 = pattern->getNucleotide(i-1);
      match = interactionValues->getMatchCost(n1, n2);
      next = matrix[i];
      value = min3(previous+match, matrix[i]+interactionValues->getInsertionCost(), matrix[i-1]+interactionValues->getInsertionCost());
      matrix[i] = value;
      previous = next;
      if (value < minValue) {
        minValue = value;
      }
    }
    if (minValue == maxCost+1) {
      return maxCost+1;
    }
  }
  return matrix[l1];
}

string SuffixArray::print () {
  Entry *entry;
  ostringstream oss;
  oss << "index\tsuf\tlcp\tup\tdown\tnext\tminInd\tmaxInd" << endl;
  oss << "---" << endl;
  for (unsigned int i = 0; i < size; i++) {
    entry = suffixArray[i];
    oss << i << "\t" << entry->suf << "\t" << entry->lcp << "\t" << entry->up << "\t" << entry->down << "\t" << entry->next << "\t" << entry->minIndex << "\t" << entry->maxIndex << endl;
  }
  oss << "---" << endl << endl;
  return oss.str();
}

