// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef ABSHCONS_H_INCLUDED
#define ABSHCONS_H_INCLUDED

#include "abstractConstraint.h"
#include "support.h"


/**
 * Provide the minimum features to model a hard constraint.
 * Contains supports, safe backtracking features.
 */
class AbstractHardConstraint : public AbstractConstraint {

protected:
  /**
   * the supports of the lower bounds domains
   */
  Support **lbSupports;
  /**
   * the supports of the upper bounds domains
   */
  Support **ubSupports;

public:

  /**
   * constructor
   * @param i the index
   * @param mc the threshold, or maximum cost allowed by the constraint
   * @param nv the number of variables
   */
  AbstractHardConstraint (int i, int mc, int nv = 0);
  
  /**
   * empty destructor
   */
  virtual ~AbstractHardConstraint ();

  /**
   * revision of the constraint, when no information is given on what led to the revision
   */
  virtual void revise ();

  /**
   * revision of the constraint, due to a given variable
   * @param v the index of the given variable
   */
  virtual void revise (int v);

  /**
   * revision of the constraint, due to a given variable
   * @param v the given variable
   */
  virtual void revise (Variable *v);

  /**
   * revision of the constraint, when the lower bound of a variable has changed
   * @param v the index of the given variable
   */
  virtual void reviseFromLb (int v);

  /**
   * revision of the constraint, when the lower bound of a variable has changed
   * @param v the given variable
   */
  virtual void reviseFromLb (Variable *v);

  /**
   * revision of the constraint, when the upper bound of a variable has changed
   * @param v the index of the given variable
   */
  virtual void reviseFromUb (int v);

  /**
   * revision of the constraint, when the upper bound of a variable has changed
   * @param v the given variable
   */
  virtual void reviseFromUb (Variable *v);

  /**
   * revision of the constraint, when the upper bound of a variable has been assigned
   * @param v the index of the given variable
   */
  virtual void reviseFromAssignment (int v);

  /**
   * revision of the constraint, when the upper bound of a variable has been assigned
   * @param v the given variable
   */
  virtual void reviseFromAssignment (Variable *v);

  /**
   * verfiy that the support of the lower bound of a variable is still valid
   * @param v the index of the given variable
   */
  virtual bool checkLbSupport (int v);

  /**
   * verfiy that the support of the upper bound of a variable is still valid
   * @param v the index of the given variable
   */
  virtual bool checkUbSupport (int v);

  /**
   * verfiy that the support is still valid
   * @param s the support
   */
  virtual bool getConsistency (Support *s);

  /**
   * set the trace of this constraint to a trace matrix
   * @param start the start position of a candidate
   * @param values the set of positions taken by the variables
   * @param backtrace the trace matrix
   */
  virtual void getTrace (int start, int values[], bool **backtrace);

  /**
   * save the values of the constraint --- the supports
   */
  virtual void save ();

  /**
   * restore the values of the constraint --- the supports
   */
  virtual void restore ();
};


#endif
