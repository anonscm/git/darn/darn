// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef AUTOMAT_H_INCLUDED
#define AUTOMAT_H_INCLUDED

#include "common.h"
#include "values.h"

/**
 * states the left-to-right direction
 */
static const int DIRECT = 0;
/**
 * states the right-to-left direction
 */
static const int REVERSE = 1;

class AutomatonCell {
private:
  int value;
  int support;
public:
  AutomatonCell () {
    value = UNDEFINED;
    support = UNDEFINED;
  }
  AutomatonCell (int v) : value(v) {
    support = UNDEFINED;
  }
  AutomatonCell (int v, int s) : value(v), support(s) { }
  AutomatonCell (AutomatonCell &a) {
    value = a.value;
    support = a.support;
  }
  void setValue (int v) {
    value = v;
  }
  void setSupport (int s) {
    support = s;
  }
  int getValue () {
    return value;
  }
  int getSupport () {
    return support;
  }
  void copy (AutomatonCell &a) {
    value = a.getValue();
    support = a.getSupport();
  }
  void reset () {
    value = UNDEFINED;
    support = UNDEFINED;
  }
};

/**
 * This class implements an automaton that scans the text
 */

class Automaton {

protected:
  /**
   * a set a states of the automaton:
   *    @a columns[@a i] = @a j means that the last @a i letters are a @a j approximation of the pattern
   */
  AutomatonCell *columns;
  /**
   * useful variable, to remember some information
   */
  AutomatonCell previous;
  /**
   * the number of errors allowed to match the pattern
   */
  int maxError;
  /**
   * the sequence containing the pattern
   */
  Sequence *pattern;
  /**
   * the matching costs
   */
  MatchingValues *matchingValues;

public:
  /**
   * constructor
   * @param s the pattern sequence
   * @param me the number of errors allowed
   * @param mv the matching values
   */
  Automaton (Sequence *s, int me, MatchingValues *mv) : previous(0), maxError(me) {
    pattern = s;
    columns = new AutomatonCell[pattern->getLength()+1];
    matchingValues = mv;
  }

  /**
   * prepare the automaton for a new scan
   */
  void reset () {
    previous.reset();
    for (unsigned int i = 0; i <= pattern->getLength(); i++) {
      columns[i].setValue(maxError+1);
      columns[i].setSupport(UNDEFINED);
    }
  }

  /**
   * set the offset score (the score of the match will be at least @a i)
   * @param i the offset score
   * @param support the support for this alignement
   */
  void setOffset (int i, int support) {
    previous.setValue(i);
    previous.setSupport(support);
  }

  /**
   * add a new letter to the automaton
   * @param n the new letter
   */
  void addLetter (Nucleotide *n) {
    AutomatonCell val[3];
    int min;

    // the initial substitution
    val[0].setValue(previous.getValue() + matchingValues->getSubstitutionCost(pattern->getNucleotide(0), n));
    val[0].setSupport(previous.getSupport());
    previous.copy(columns[1]);
    columns[1].copy(val[0]);

    // induction 
    for (unsigned int i = 2; i < pattern->getLength(); i++) {
      val[0].setValue(previous.getValue() + matchingValues->getSubstitutionCost(pattern->getNucleotide(i-1), n));
      val[0].setSupport(previous.getSupport());

      val[1].setValue(columns[i].getValue() + matchingValues->getInsertionCost());
      val[1].setSupport(columns[i].getSupport());

      val[2].setValue(columns[i-1].getValue() + matchingValues->getInsertionCost());
      val[2].setSupport(columns[i-1].getSupport());

      previous.copy(columns[i]);
      columns[i].setValue(min3(val[0].getValue(), val[1].getValue(), val[2].getValue()));
      min = max3(val[0].getSupport(), val[1].getSupport(), val[2].getSupport());
      for (int j = 0; j < 3; j++) {
        if (val[j].getValue() == columns[i].getValue()) {
          min = min2(min, val[j].getSupport());
        }
      }
      columns[i].setSupport(min);
    }
    // the final substitution
    if (pattern->getLength() > 1) {
      val[0].setValue(previous.getValue() + matchingValues->getSubstitutionCost(pattern->getNucleotide(pattern->getLength()-1), n));
      val[0].setSupport(previous.getSupport());
    }
    columns[pattern->getLength()].copy(val[0]);
  }

  /**
   * get the current score
   * @return the score
   */
  int getScore () {
    return columns[pattern->getLength()].getValue();
  }

  /**
   * get the current support
   * @return the support
   */
  int getSupport () {
    return columns[pattern->getLength()].getSupport();
  }
};

#endif
