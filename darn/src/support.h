// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef  SUP_H_INCLUDED
#define  SUP_H_INCLUDED

#include "common.h"


/**
 * The SupportValues contains all Support needs to save and restore while backtracking.
 */
struct SupportValues {
  /**
   * the total number of variables involved
   */
  int nbVariables;
  /**
   * the support
   */
  int *values;
  /**
   * whether this support is empty
   */
  bool set;

  /**
   * constructor
   * @param nv the number of variables
   */
  SupportValues (int nv) : nbVariables(nv), set(false) {
    values = new int[nbVariables];
  }

  /**
   * destructor
   */
  ~SupportValues() {
    /*
    delete[] values;
    */
  }

  /**
   * whether a support has been previously found
   */
  bool isSet () {
    return set;
  }

  /**
   * set the support to be empty
   */
  void unset () {
    set = false;
  }

  /**
   * get a variable's value which is part of the support
   * @param v the variable
   * @return the value
   */
  int getSupport (int v) {
    if (!set) {
      trace(1, "Error! Trying to read a support which is not set!");
      return -1;
    }
    if ((v < 0) || (v >= nbVariables)) {
      trace(1, "Error! Trying to read a non existing support!");
      return -1;
    }
    return values[v];
  }

  /**
   * set a variable's value which is part of the support
   * @param v the variable
   * @param s the value
   */
  void setSupport (int v, int s) {
    if ((v < 0) || (v >= nbVariables)) {
      trace(1, "Error! Trying to read a non existing support!");
      return;
    }
    values[v] = s;
    set = true;
  }

  /**
   * copy the content of a support to this one
   * @param s the copied support
   */
  void copy (SupportValues *s) {
    set = s->set;
    if (set) {
      for (int i = 0; i < nbVariables; i++) {
        values[i] = s->values[i];
      }
    }
  }

  /**
   * print the content of the current instance
   * @param m the string to write into
   */
  void print (char *m) {
    char tmp[123];
    if (!set) {
      strcpy(m, "[empty]");
    }
    strcat(m, "[");
    sprintf(tmp, "%d", values[0]);
    strcat(m, tmp);
    for (int i = 1; i < nbVariables; i++) {
      strcat(m, "-");
      sprintf(tmp, "%d", values[i]);
      strcat(m, tmp);
    }
    strcat(m, "]");
  }
};


/**
 * The Support class. It stores the support of a value or the support of a constraint (for zero-inversibility).
 * It can be used by a nary constraint (not only binary).
 */
class Support {
  
private:
  /**
   * the total number of variables involved
   */
  int nbVariables;
  /**
   * the number of stored values
   */
  int nbStoredValues;
  /**
   * the stored values
   */
  SupportValues **memories;


public:
  /**
   * constructor
   * @param nv the number of variables of the constraint
   */
  Support (int nv) : nbVariables(nv), nbStoredValues(0) {
    memories = new SupportValues*[nbTotVariables+1];
    for (int i = 0; i < nbTotVariables+1; i++) {
      memories[i] = new SupportValues(nbVariables);
    }
  }
  /**
   * destructor
   */
  ~Support () {
    /*
    for (int i = 0; i < nbTotVariables; i++) {
      delete memories[i];
    }
    delete[] memories;
    */
  }
  /**
   * prepare the support for a new search
   */
  void reset () {
    nbStoredValues = 0;
    for (int i = 0; i < nbTotVariables+1; i++) {
      memories[i]->unset();
    }
  }
  /**
   * whether a support has been previously found
   */
  bool isSet () {
    return memories[nbStoredValues]->isSet();
  }
  /**
   * set the support to be empty
   */
  void unset () {
    memories[nbStoredValues]->unset();
  }
  /**
   * get a variable's value which is part of the support
   * @param v the variable
   * @return the value
   */
  int getSupport (int v) {
    return memories[nbStoredValues]->getSupport(v);
  }
  /**
   * set a variable's value which is part of the support
   * @param v the variable
   * @param s the value
   */
  void setSupport (int v, int s) {
    memories[nbStoredValues]->setSupport(v, s);
  }
  /**
   * copy the content of a support to this one
   * @param s the copied support
   */
  void copy (Support *s) {
    memories[nbStoredValues]->copy(s->memories[s->nbStoredValues]);
  }

  /**
   * make the support save the current values
   */
  void save () {
    if (nbStoredValues >= nbTotVariables) {
      trace(1, "Error! Illegal save of a support!");
    }
    memories[nbStoredValues+1]->copy(memories[nbStoredValues]);
    nbStoredValues++;
  }
  /**
   * make the support restore the current values
   */
  void restore () {
    if (nbStoredValues <= 0) {
      trace(1, "Error! Illegal restore of a support!");
    }
    nbStoredValues--;
  }
  
  /**
   * print the content of the current instance
   * @param m the string to write into
   */
  void print (char *m) {
    memories[nbStoredValues]->print(m);
  }

};


#endif
