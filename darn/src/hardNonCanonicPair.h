// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef HNCPAIR_H_INCLUDED
#define HNCPAIR_H_INCLUDED

#include "values.h"
#include "hardConstraint.h"


/**
 * This class forces two nucleotides to match, following a given interaction and orientation
 */
class HardNonCanonicPair : public HardConstraint {

protected:

  /**
   * the interaction of the first nucleotide
   */
  Interaction interaction1;
  /**
   * the interaction of the second nucleotide
   */
  Interaction interaction2;
  /**
   * the orientation of the nucleotides
   */
  Orientation orientation;
  /**
   * the isostericity family
   */
  int family;
  /**
   * isostericity values
   */
  IsostericityValues *isostericityValues;
  
  /**
   * whether the assignment is allowed
   * @param i1 the value of the first variable
   * @param i2 the value of the second variable
   * @return true, if the assignment is allowed
   */
  bool checkConsistency (int i1, int i2);

public:
  /**
   * the constructor
   * @param v1 the first variable (the lower bound of the first sequence)
   * @param v2 the second variable (the upper bound of the first sequence)
   * @param i the index of this constraint
   * @param it1 the interaction of the first nucleotide
   * @param it2 the interaction of the second nucleotide
   * @param ori the orientation of the nucleotides
   * @param f the isostericity family
   * @param mc the maximum cost
   * @param iv the isostericity values
   */
  HardNonCanonicPair (Variable *v1, Variable *v2, int i, Interaction it1, Interaction it2, Orientation ori, int f, int mc, IsostericityValues *iv);
  /**
   * revision of the constraint, when no information is given on what led to the revision
   */
  virtual void revise ();
  /**
   * revision of the constraint, due to a given variable
   * @param v the index of the given variable
   */
  virtual void revise (int v);
  /**
   * revision of the constraint, when the lower bound of a variable has changed
   * @param v the index of the given variable
   */
  virtual void reviseFromLb (int v);
  /**
   * revision of the constraint, when the upper bound of a variable has changed
   * @param v the given variable
   */
  virtual void reviseFromUb (int v);
  /**
   * revision of the constraint, when the upper bound of a variable has been assigned
   * @param v the given variable
   */
  virtual void reviseFromAssignment (int v);
  /**
   * verfiy that the support is still valid
   * @param s the support
   */
  virtual bool getConsistency (Support *s);
  /**
   * whether one should print a space before or after the variable @a var
   *  (useful for printing the results)
   * @param var the index of the variable
   * @param place 0: before, 1: after
   * @return true, if a space should be printed
   */
  virtual bool printSpace (int var, int place);
};

#endif
