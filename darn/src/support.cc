// Darn: RNA Motif Localization
// Copyright (C) 2010  INRA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "support.h"

Support::Support (int nv) : nbVariables(nv), set(false), nbStoredValues(0) {
  values = new int[nbVariables];
  memories = new SupportValues*[nbTotVariables];
  for (int i = 0; i < nbTotVariables; i++) {
    memories[i] = new SupportValues(nbVariables);
  }
}

Support::~Support () {
  /*
  for (int i = 0; i < nbTotVariables; i++) {
    delete memories[i];
  }
  delete[] memories;
  delete[] values;
  */
}

void Support::reset () {
  nbStoredValues = 0;
  set = false;
}

bool Support::isSet () {
  return set;
}

void Support::unset () {
  set = false;
}

int Support::getSupport (int v) {
  if (!set) {
    trace(1, "Error! Trying to read a support which is not set!");
    return -1;
  }
  if ((v < 0) || (v >= nbVariables)) {
    trace(1, "Error! Trying to read a non existing support!");
    return -1;
  }
  return values[v];
}

void Support::setSupport (int v, int s) {
  if ((v < 0) || (v >= nbVariables)) {
    trace(1, "Error! Trying to read a non existing support!");
    return;
  }
  values[v] = s;
  set = true;
}

void Support::copy (Support *s) {
  set = s->set;
  if (set) {
    for (int i = 0; i < nbVariables; i++) {
      values[i] = s->values[i];
    }
  }
}

void Support::save () {
  memories[nbStoredValues]->set = set;
  if (set) {
    for (int i = 0; i < nbVariables; i++) {
      memories[nbStoredValues]->values[i] = values[i];
    }
  }
  nbStoredValues++;
}

void Support::restore () {
  nbStoredValues--;
  set = memories[nbStoredValues]->set;
  if (set) {
    for (int i = 0; i < nbVariables; i++) {
      values[i] = memories[nbStoredValues]->values[i];
    }
  }
}

string Support::print () {
  ostringstream s;
  if (!set) {
    return "[empty]";
  }
  s << "[" << values[0];
  for (int i = 1; i < nbVariables; i++) {
    s << "-" << values[i];
  }
  s << "]";
  return s.str();
}
