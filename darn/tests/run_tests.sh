#!/bin/sh
# TEST SCRIPT FOR DARN

# How to use the script:
#  - go to the test directory (the one containing this script)
#  - update the 2 following 2 variables at the beginning of the script
#  - execute: run_tests.sh


# First, use the scrit to generate reference output files:
# This will generate the output reference files, called test<n>.ref

# Second, use the scrit to compare the current outputs to the reference outputs
# For each test, a message is displayed indicating if the test succeed or failed.
# If a test failed, use the following command to see the differences 
# between current and reference output: tkdiff test<n>.out test<n>.ref
# Note that current output test<n>.out is only kept when the test failed.

# Description of defined tests
# test1: -s sample.fa -d tRNA_Bacteria.des -u -t
# test2: -s sample.fa -d tRNA_Bacteria.des -n B -f P
# test3: -s sample.fa -d Lysine -n R -f F
# test4: -s sample.fa -d test_duplex.des -x test_duplex.fa -f G 
# test5: -s sample.fa -d test_duplex.des -x test_duplex.fa -v -c -e




############# UPDATE SCRIPT PARAMETERS 
export DARN_PATH="/home/cros/PROJECT/ARN/PROJETS/Darn/SRC/NEW/NEW-darn-CVS/src"
export DARN_OPTION_TEST=test
#export DARN_OPTION_TEST=generate_references




############# RUN tests
### test1
${DARN_PATH}/darn -s ../example/sample.fa -d ../descriptor/tRNA_Bacteria.des -o tmp.res -u -t
# just remove the last line because time spent in second varies
head -n -1 tmp.res > test1.out; rm tmp.res

if [ $DARN_OPTION_TEST = generate_references ] ; then
  mv  -f test1.out  test1.ref
  echo "Test1 reference file updated"
else
  diff test1.ref test1.out > tmp.res
  if [ -s "tmp.res" ] ; then
    echo "Test1 failed"
  else
    echo "Test1 succeed"; rm test1.out
  fi 
  rm tmp.res
fi


### test2
${DARN_PATH}/darn -s ../example/sample.fa -d ../descriptor/tRNA_Bacteria.des -o test2.out -n B -f P

if [ $DARN_OPTION_TEST = generate_references ] ; then
  mv  -f test2.out  test2.ref
  echo "Test2 reference file updated"
else
  diff test2.ref test2.out > tmp.res
  if [ -s "tmp.res" ] ; then
    echo "Test2 failed"
  else
    echo "Test2 succeed"; rm test2.out
  fi 
  rm tmp.res
fi


### test3
${DARN_PATH}/darn -s ../example/sample.fa -d ../descriptor/Lysine.des -o test3.out -n R -f F

if [ $DARN_OPTION_TEST = generate_references ] ; then
  mv  -f test3.out  test3.ref
  echo "Test3 reference file updated"
else
  diff test3.ref test3.out > tmp.res
  if [ -s "tmp.res" ] ; then
    echo "Test3 failed"
  else
    echo "Test3 succeed"; rm test3.out
  fi 
  rm tmp.res
fi


### test4
${DARN_PATH}/darn -s ../example/sample.fa -d test_duplex.des -x test_duplex.fa -o test4.out -f G 

if [ $DARN_OPTION_TEST = generate_references ] ; then
  mv  -f test4.out  test4.ref
  echo "Test4 reference file updated"
else
  diff test4.ref test4.out > tmp.res
  if [ -s "tmp.res" ] ; then
    echo "Test4 failed"
  else
    echo "Test4 succeed"; rm test4.out
  fi 
  rm tmp.res
fi


### test5
${DARN_PATH}/darn -s ../example/sample.fa -d test_duplex.des -x test_duplex.fa -o tmp.res -v -c -e
# just remove the last line because time spent in second varies
head -n -1 tmp.res > test5.out; rm tmp.res

if [ $DARN_OPTION_TEST = generate_references ] ; then
  mv  -f test5.out  test5.ref
  echo "Test5 reference file updated"
else
  diff test5.ref test5.out > tmp.res
  if [ -s "tmp.res" ] ; then
    echo "Test5 failed"
  else
    echo "Test5 succeed"; rm test5.out
  fi 
  rm tmp.res
fi



