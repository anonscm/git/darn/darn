/* routines from PS_dot.c */
extern int PS_rna_plot(char *string, char *structure, char *file);
/* write PostScript drawing of structure to file */
extern int PS_rna_plot_a(char *string, char *structure, char *file, char *pre, char *post);
extern int rna_plot_type;   /* 0= simple coordinates, 1= naview */

typedef struct cpair {
  int i,j,mfe;
  float p, hue, sat;
} cpair;
