<?php
  require("header.php");

  function die_script ($mesg) {
    echo "<p>";
    echo "  <b>" . $mesg . "</b>";
    echo "</p>";
    die();
  }

  function printResult ($title, $result) {
    echo"      <h3>\n";
    echo"        " . $title . "\n";
    echo"      </h3>\n";
    echo"      <div class=\"code\">\n";
    foreach ($result as $r)  {
      echo str_replace(".ps", ".png", $r) . "\n";
    }
    echo"      </div>\n";
  }

  $time = time();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/1998/REC-html40-19980424/strict.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Darn</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <div class="top">
      <div class="t1">
        Darn
      </div>
      <div class="t2">
        Non-protein-coding RNA detection
      </div>
    </div>
    <div class="main">
      <h1>
        Help?
      </h1>
        A problem? Please consult the <a href="Src/darn/doc/UserManual.pdf">Darn manual</a> online.
      <?php
        // the request is sent... retrieve the parameters
        if (isset($_GET["sent"])) {

          // write IP address for log
          $IP = $_SERVER['REMOTE_ADDR'];
          $hostname = gethostbyaddr($IP);
          $dumpFile = fopen($baseDir . $dumpFileName, "a") or die_script("Error! Cannot open dump file!");;
          fputs($dumpFile, date(DATE_RFC822) . " " . $hostname . " " . $IP . "\n");
          fclose($dumpFile);
          

          // main sequence
          // main sequence in remote computer
          if ($_FILES["uploadedSequence"]["size"] !== 0) {
            if ($_FILES["uploadedSequence"]["size"] > $MAX_FILE_SIZE) {
              die_script("Error! Cannot download such a big main sequence file!");
            }
            $sequenceFileName = tempnam($tmpDir, "seq_");
            move_uploaded_file($_FILES["uploadedSequence"]["tmp_name"], $sequenceFileName) or die_script("Error! Cannot get uploaded main sequence file! The file seems to be too large!");
            chmod($sequenceFileName, 0777);
          }

          // main sequence in database
          else if ($_POST["sequenceFile"] != "NONE") {
            $domain = $_POST["domain"];
            $organism = $_POST["organism"];
            $sequenceFile = $_POST["sequenceFile"];

            $sequenceFileName = $completeSequenceDir . $domain . "/" . $organism . "/" . $sequenceFile;
          }

          // main sequence in textarea
          else if ($_POST["sequence"] !== "") {
            $sequenceFileName = tempnam($tmpDir, "seq_");
            $sequenceFile = fopen($sequenceFileName, "w") or die_script("Error! Cannot open sequence temporary file!");
            fputs($sequenceFile, $_POST["sequence"]);
            fclose($sequenceFile);
            chmod($sequenceFileName, 0777);
          }
          // main sequence not found
          else {
            die_script("Error! No main sequence has been specified!");
          }

          // descriptor
          $descriptorFileName = tempnam($baseDir . $descriptorDir, "seq_");
          $descriptorFile = fopen($descriptorFileName, "w") or die_script("Error! Cannot open descriptor temporary file!");
          fputs($descriptorFile, str_replace("\\\"", "\"", $_POST["descriptor"])) or die_script("Error! Cannot write into descriptor temporary file!");
          fclose($descriptorFile);
          chmod($descriptorFileName, 0777);

          // duplex
          if ($_POST["duplexFile"] != "NONE") {
            $duplexFileName = $baseDir . $duplexDir . $_POST["duplexKind"] . "/" . $_POST["duplexFile"];
          }
          else if ($_POST["target"] !== "") {
            $duplexFileName = tempnam($tmpDir, "dup_");
            $duplexFile = fopen($duplexFileName, "w") or die_script("Error! Cannot open duplex temporary file!");
            fputs($duplexFile, $_POST["target"]);
            fclose($duplexFile);
            chmod($duplexFileName, 0777);
          }
          else {
            $duplexFileName = false;
          }
          $cmdLine = $baseDir . $srcDir . $darnDir . $darnName . " -d " . $descriptorFileName . " -s " . $sequenceFileName;
          if ($duplexFileName) {
            $cmdLine = $cmdLine . " -x " . $duplexFileName;
            if ((isset($_POST["direction"])) && ($_POST["direction"] == "2")) {
              $cmdLine .= " -v";
            }
          }

          // order by cost
          if (isset($_POST["orderByCost"])) {
            $cmdLine .= " -c";
          }

          // explanation
          if (isset($_POST["explanation"])) {
            $cmdLine .= " -e";
          }

          // gff format
          if (isset($_POST["gff"])) {
            $cmdLine .= " -f G";
          }

          // fasta format
          if (isset($_POST["fasta"])) {
            $cmdLine .= " -f F";
          }

          // secondary structure
          if (isset($_POST["structure"])) {
            $cmdLine .= " -t";
          }

          // alarm
          $cmdLine .= " -a 120";

          // strands
          if ($_POST["strand"] == "2") {
            $cmdLine .= " -n R";
          }
          if ($_POST["strand"] == "12") {
            $cmdLine .= " -n B";
          }

          // Web output
          $cmdLine .= " -w";

          if ((! isset($_POST["gff"])) && (! isset($_POST["fasta"]))) {
            // directory where to write images
            mkdir($baseDir . $imgDir . $time, 0777);
            chmod($baseDir . $imgDir . $time, 0777);
            $cmdLine .= " -y " . $baseDir . $imgDir . $time;
            $cmdLine .= " -z " . $imgDir . $time;
          }

          // execution
          exec($cmdLine, $output, $returnValue);
          if ($returnValue != 0) {
            echo "      <h1>Error in the program! If you think it a bug, please report it, including the command line:</h1>";
            echo "      <h2>" . $cmdLine . "</h2>";
          }
          printResult("Results:", $output); 

          if ((! isset($_POST["gff"])) && (! isset($_POST["fasta"]))) {
            // convert image files
            $dir = opendir($baseDir . $imgDir . $time) or die_script('Error! Cannot open drawing directory!');
            while($filePs = readdir($dir)) {
              if(($filePs != '.') && ($filePs != '..')) {
                $filePng = str_replace(".ps", ".png", $filePs);
                chmod($baseDir . $imgDir . $time . "/" . $filePs, 0777);
                $cmd = "convert ". $baseDir . $imgDir . $time . "/" . $filePs . " " . $baseDir . $imgDir . $time . "/" . $filePng;
                exec($cmd, $output, $returnValue);
                chmod($baseDir . $imgDir . $time . "/" . $filePng, 0777);
                if ($returnValue != 0) {
                  echo "      <h1>Error! Cannot convert $filePs to PNG file!</h1>";
                }
              }
            }
            closedir($dir);
          }
        }
        else {
      ?>

      <form method="post" enctype="multipart/form-data" action="<?php echo $pageName; ?>">

        <div class="bloc">
          <h1>
            Descriptor:
          </h1>
          <p>
            The selection of a descriptor file will fill in the following text area.
          </p>
          <p>
            You can edit the content of the text area.
          </p>
          <p>
            <select name="descriptorFile" onchange="javascript:this.form.descriptor.value='';this.form.submit();">
              <option value="NONE">Choose a descriptor here</option>
              <?php
                $dir = opendir($baseDir . $descriptorDir) or die('Error! Cannot open directory for descriptor files!');
                while (false !== ($file = readdir($dir))) {
                  if (substr(strrev($file), 0, 4) == "sed.") {
                    $name = substr($file, 0, strrpos($file, "."));
                    if (isset($_POST["descriptorFile"])) {
                      $fileName = $_POST["descriptorFile"];
                      if ($fileName == $file) {
                        $selected = " selected";
                      }
                      else {
                        $selected = "";
                      }
                   }
                   else {
                     $selected = "";
                   }
              ?>
              <option value="<?php echo $file; ?>"<?php echo $selected; ?>><?php echo $name; ?></option>
              <?php
                  }
                }
              ?>
            </select>
          </p>
          <p>
            <textarea name="descriptor" rows="15" cols="100"><?php
              if (trim($_POST["descriptor"]) != "") {
                echo stripslashes($_POST["descriptor"]);
              }
              else if (isset($_GET["getDescriptor"])) {
                $alignmentFileName = tempnam($tmpDir, "ali_");
                $alignmentFile = fopen($alignmentFileName, "wb") or die("Error! Cannot open alignment temporary file '$alignmentFileName'!");
                fputs($alignmentFile, str_replace("\r\n", "\n", $_POST["alignment"]));
                fclose($alignmentFile);
                $cmdLine = $baseDir . $srcDir . $motifDir . $motifName . " -i " . $alignmentFileName;
                exec($cmdLine, $output, $returnValue);
                if ($returnValue != 0) {
                  echo "Error in the program! Please report the bug.";
                }
                foreach ($output as $o) {
                  echo $o . "\n";
                }
              }
              else if (isset($_POST["descriptorFile"])) {
                $fileName = $_POST["descriptorFile"];
                if ($fileName !== "NONE") {
                  $fullFileName = $baseDir . $descriptorDir . $fileName;
                  $file = fopen($fullFileName, 'r') or die("Error! Cannot read file " . $fileName);
                  $descriptor = fread($file, filesize($fullFileName));
                  $file = fclose($file) or die("Error! Cannot close file " . $fileName);
                }
                echo $descriptor;
              }
            ?></textarea>
          </p>

          <h2>
            Sequence alignment:
          </h2>
          <p>
            Alternatively, you can paste an multiple alignment in the following text area using the Stockholm format (with the structure), and Darn will build a descriptor for this alignment.
          </p>
          <p>
            <textarea name="alignment" rows="10" cols="100"><?php
              if (isset($_POST["alignment"])) {
                echo "\n" . stripslashes($_POST["alignment"]);
              }
            ?></textarea>
          </p>
          <p>
            <input type="submit" value="Get descriptor!" onclick="javascript:this.form.descriptor.value='';this.form.action='<?php echo $pageName; ?>?getDescriptor=true';">
          </p>
        </div>

        <div class="bloc">
          <h1>
            Main sequence:
          </h1>
          <p>
            You can either choose a file in our database, paste your fasta sequence, or upload your fasta file.
          </p>
          <p>
            Select a sequence file here:
          </p>
          <p id="sequenceFile">
            <select name="domain" onchange="javascript:this.form.action='<?php echo $pageName; ?>#sequenceFile';this.form.submit();">
              <option value="NONE">Choose a domain here</option>
              <?php
                $dir = opendir($completeSequenceDir) or die('Error! Cannot open directory for sequence files!');
                while (false !== ($file = readdir($dir))) {
                  if (($file !== ".") && ($file !== "..")) {
                    $name = str_replace("_", " ", $file);
                    if (isset($_POST["domain"])) {
                      $fileName = $_POST["domain"];
                      if ($fileName == $file) {
                        $selected = " selected";
                      }
                      else {
                        $selected = "";
                      }
                    }
                    else {
                      $selected = "";
                    }
              ?>
              <option value="<?php echo $file; ?>"<?php echo $selected; ?>><?php echo $name; ?></option>
              <?php
                  }
                }
              ?>
            </select>
            &nbsp; &nbsp; &nbsp;
            <select name="organism" onchange="javascript:this.form.action='<?php echo $pageName; ?>#sequenceFile';this.form.submit();">
              <?php
                if (!isset($_POST["domain"]) || ($_POST["domain"] == "NONE")) {
              ?>
              <option value="NONE">Choose a domain first</option>
              <?php
                }
                else {
              ?>
              <option value="NONE">Choose an organism here</option>
              <?php
                  $domain = $_POST["domain"];
                  $dir = opendir($completeSequenceDir . "/". $domain ) or die('Error! Cannot open directory for sequence files!');
                  while (false !== ($file = readdir($dir))) {
                    if (($file !== ".") && ($file !== "..")) {
                      $name = str_replace("_", " ", $file);
                      if (isset($_POST["organism"])) {
                        $fileName = $_POST["organism"];
                        if ($fileName == $file) {
                          $selected = " selected";
                        }
                        else {
                          $selected = "";
                        }
                      }
                      else {
                        $selected = "";
                      }
              ?>
              <option value="<?php echo $file; ?>"<?php echo $selected; ?>><?php echo $name; ?></option>
              <?php
                    }
                  }
                }
              ?>
            </select>
            &nbsp; &nbsp; &nbsp;
            <select name="sequenceFile">
              <?php
                if (!isset($_POST["domain"]) || ($_POST["domain"] == "NONE")) {
              ?>
              <option value="NONE">Choose a domain first</option>
              <?php
                }
                else if (!isset($_POST["organism"]) || ($_POST["organism"] == "NONE")) {
              ?>
              <option value="NONE">Choose an organism first</option>
              <?php
                }
                else {
              ?>
              <option value="NONE">Choose a chromosome here</option>
              <?php
                  $domain = $_POST["domain"];
                  $organism = $_POST["organism"];
                  $dir = opendir($completeSequenceDir . $domain . "/" . $organism) or die('Error! Cannot open directory for sequence files!');
                  while (false !== ($file = readdir($dir))) {
                    if (($file !== ".") && ($file !== "..")) {
                      $name = str_replace("_", " ", substr($file, 0, strrpos($file, ".")));
                      if (isset($_POST["sequenceFile"])) {
                        $fileName = $_POST["sequenceFile"];
                        if ($fileName == $file) {
                          $selected = " selected";
                        }
                        else {
                          $selected = "";
                        }
                     }
                     else {
                       $selected = "";
                     }
              ?>
              <option value="<?php echo $file; ?>"<?php echo $selected; ?>><?php echo $name; ?></option>
              <?php
                    }
                  }
                }
              ?>
            </select>
          </p>
          <p>
            or paste your fasta file here:
          </p>
          <p>
            <textarea name="sequence" rows="10" cols="100"><?php
              if (isset($_POST["sequence"])) {
                echo "\n" . stripslashes($_POST["sequence"]);
                }
            ?></textarea>
          </p>
          <p>
            or upload a fasta file here (for technical reasons, the file size should not exceed <?php echo ($MAX_FILE_SIZE/1000000); ?> Mo):
          </p>
          <p>
            <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $MAX_FILE_SIZE; ?>" >
            <input name="uploadedSequence" type="file">
          </p>
        </div>

        <div class="bloc">
          <h1>
            Target sequence (if needed by the descriptor):
          </h1>
          <p>
            <span class="warn">Only if a duplex motif is given in the descriptor thanks to the T variables. Otherwise, you may get an error.</span>
          </p>
          <p>
            You may either choose a target in the following database, or paste your fasta sequence.
          </p>
          <p>
            Select a sequence here:
          </p>
          <p id="duplexFile">
            <select name="duplexKind" onchange="javascript:this.form.action='<?php echo $pageName; ?>#duplexFile';this.form.submit();">
              <option value="NONE">Choose a kind of duplex here</option>
              <?php
                $dir = opendir($baseDir . $duplexDir) or die('Error! Cannot open directory for duplex files!');
                while (false !== ($file = readdir($dir))) {
                  if (($file !== ".") && ($file !== "..")) {
                    $name = str_replace("_", " ", $file);
                    if (isset($_POST["duplexKind"])) {
                      $fileName = $_POST["duplexKind"];
                      if ($fileName == $file) {
                        $selected = " selected";
                      }
                      else {
                        $selected = "";
                      }
                    }
                    else {
                      $selected = "";
                    }
              ?>
              <option value="<?php echo $file; ?>"<?php echo $selected; ?>><?php echo $name; ?></option>
              <?php
                  }
                }
              ?>
            </select>
            &nbsp; &nbsp; &nbsp;
            <select name="duplexFile">
              <?php
                if (!isset($_POST["duplexKind"]) || ($_POST["duplexKind"] == "NONE")) {
              ?>
              <option value="NONE">Choose a duplex kind first</option>
              <?php
                }
                else {
              ?>
              <option value="NONE">Choose a duplex file here</option>
              <?php
                  $duplexKind = $_POST["duplexKind"];
                  $dir = opendir($baseDir . $duplexDir . "/" . $duplexKind) or die('Error! Cannot open directory for duplex files!'
    );
                  while (false !== ($file = readdir($dir))) {
                    if (substr(strrev($file), 0, 4) == "saf.") {
                      $name = str_replace("_", " ", substr($file, 0, strrpos($file, ".")));
                      if (isset($_POST["duplexFile"])) {
                        $fileName = $_POST["duplexFile"];
                        if ($fileName == $file) {
                          $selected = " selected";
                        }
                        else {
                          $selected = "";
                        }
                     }
                     else {
                       $selected = "";
                     }
              ?>
              <option value="<?php echo $file; ?>"<?php echo $selected; ?>><?php echo $name; ?></option>
              <?php
                    }
                  }
                }
              ?>
            </select>
          </p>
          <p>
            or paste your fasta file here:
          </p>
          <p>
            <textarea name="target" rows="10" cols="100"><?php
              if (isset($_POST["target"])) {
                echo "\n" . stripslashes($_POST["target"]);
              }
            ?></textarea>
          </p>
        </div>

        <div class="bloc">
          <h1>
            Options:
          </h1>
          <p>
            Please select the strand <em>on the main stem</em> you want to scan:
          </p>
          <p>
            <input type="radio" name="strand" value="1"<?php if ((isset($_POST["strand"])) && ($_POST["strand"] == "1")) echo " checked"; ?>> the positive strand
          </p>
          <p>
            <input type="radio" name="strand" value="2"<?php if ((isset($_POST["strand"])) && ($_POST["strand"] == "2")) echo " checked"; ?>> the negative strand
          </p>
          <p>
            <input type="radio" name="strand" value="12"<?php if ((!isset($_POST["strand"])) || ($_POST["strand"] == "12")) echo " checked"; ?>> both strands
          </p>

          <p>
            Please select which direction <em>on the target stem</em> you want to scan:
          </p>
          <p>
            <input type="radio" name="direction" value="2"<?php if ((!isset($_POST["direction"])) || ($_POST["direction"] == "2")) echo " checked"; ?>> from 3' to 5'
          </p>
          <p>
            <input type="radio" name="direction" value="1"<?php if ((isset($_POST["direction"])) && ($_POST["direction"] == "1")) echo " checked"; ?>> from 5' to 3'
          </p>

          <p>
            <input type="checkbox" name="gff"> Use GFF format to output solutions.
          </p>

          <p>
            <input type="checkbox" name="fasta"> Use FASTA format to output solutions.
          </p>

          <p>
            <input type="checkbox" name="orderByCost"> Give solutions with lower costs first.
          </p>

          <p>
            <input type="checkbox" name="explanation"> Give the costs given by the constraints.
          </p>

          <p>
            <input type="checkbox" name="structure"> Display secondary structure using parenthesis representation.
          </p>
        </div>

        <p>
          <input type="submit" value="GO!" onclick="javascript:this.form.action='<?php echo $pageName; ?>?sent=true';">
        </p>

        <p>
          <span class="warn">Important notice: since this web page is available to anyone, every job is interrupted after 2 minutes, and the solutions found so far are displayed. Please download the software for larger searches.</span>
        </p>
      </form>
      <?php
        }
      ?>
    <h1>
      Download Darn
    </h1>
    <p>
      You can also download a <a href="https://mulcyber.toulouse.inra.fr/frs/?group_id=15">local version</a> of Darn from a Forge.
    </p>
    </div>

    <div class="bot">
       <a href="mailto:matthias.zytnicki@no_spam@versailles.inra.fr">matthias.zytnicki@no_spam@versailles.inra.fr</a><br >
    <span class="warn">Last update February 10, 2010.</span>
    </div>
  </body>
</html>
