<?php
  $pageName            = "index.php";
  $baseDir             = "/var/www/Darn/";
  $tmpDir              = "/tmp/darn/";
  $descriptorDir       = "Db/Descriptors/";
  $completeSequenceDir = "/var/www/Darn/Db/Genomes/";
  $duplexDir           = "Db/Targets/";
  $srcDir              = "Src/";
  $darnDir             = "darn/src/";
  $motifDir            = "motif/";
  $darnName            = "darn";
  $motifName           = "motif";
  $imgDir              = "Img/";
  $dumpFileName        = "Dump/dump.txt";
  $MAX_FILE_SIZE       = 60000000;
?>
