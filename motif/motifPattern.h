#ifndef MOTIFPATTERN_H_INCLUDED
#define MOTIFPATTERN_H_INCLUDED

#include "motifTools.h"
#include "motifSequence.h"
#include "motifElement.h"

/**
 * The class represents a pattern
 */
class MotifPattern : public MotifElement {

protected:
  /**
   * the pattern
   */
  MotifSequence *pattern;
  /**
   * the beginning of the pattern
   */
  int startIndex;
  /**
   * the end of the pattern
   */
  int stopIndex;
  /**
   * the beginning in each sequence
   */
  MotifSharedValue *startIndices;
  /**
   * the end in each sequence
   */
  MotifSharedValue *stopIndices;
  /**
   * the number of errors (substitutions / insertions / deletions)
   */
  MotifSharedValue *errors;
  /**
   * the cost profile
   */
  int *scores;
  /**
   * the maximum number of errors observed
   */
  int maximumErrors;
  /**
   * whether the pattern is found in every sequence
   */
  bool optional;

  /**
   * compute the number of errors between a subsequence and @a pattern
   * @param the index of the organism
   * @param start the start position of the word
   * @param stop the stop position of the word
   * @return the number of errors
   */
  int getErrors (int org, int start, int stop);


public:
  /**
   * the constructor
   * @param no the number of organisms
   * @param s the pattern
   * @param start the start index of the stem
   * @param stop the end index of the stem
   */
  MotifPattern (int no, string s = "", int start = UNDEFINED, int stop = UNDEFINED);
  /**
   * empty destructor
   */
  ~MotifPattern ();
   /**
   * set @a patter
   * @param start the new start index
   */
  void setPattern (string s);
 /**
   * set @a startIndex
   * @param start the new start index
   */
  void setStartIndex (int start);
  /**
   * set @a stopIndex
   * @param stop the new start index
   */
  void setStopIndex (int stop);
  /**
   * get @a startIndex
   * @return the start index
   */
  int getStartIndex ();
  /**
   * get @a stopIndex
   * @return the stop index
   */
  int getStopIndex ();
  /**
   * get @a startIndices
   * @return the start indices
   */
  MotifSharedValue *getStartIndices ();
  /**
   * get @a stopIndices
   * @return the stop indices
   */
  MotifSharedValue *getStopIndices ();

  /**
   * set the @a pattern, @a errors
   */
  void setValues ();

  /**
   * whether the pattern is found in every sequence
   * @return an accessor to @a optional
   */
  bool isOptional();

  /**
   * whether one can express preferences on this constraint
   * @return true, if one can express preferences on this constraint
   */
  bool hasSoftPart();

  /**
   * export the element as a Darn cost function descriptor line
   * @return the descriptor line
   */
  string exportFunction();
  /**
   * export the maximum cost given by this constraint
   * @return the maximum cost
   */
  int getMaximumCost();
  /**
   * get the cost by this pattern on a given constraint
   * @param org the index of the sequence
   * @return a cost
   */
  int getCost(int org);
  /**
   * whether the given position is in the pattern
   * @param pos the given position
   * @return true, if the given position is in the pattern
   */
  bool isInside(int pos);

  /**
   * possibly print some information about this pattern
   * @return a string
   */
  string printInformation ();

  /**
   * print a representation of the stem
   * @return a string
   */
  string print ();
};

#endif

