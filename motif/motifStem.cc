#include "motifStem.h"

MotifStem::MotifStem (int no, MotifHelix *h, int start, int stop) : startIndex(start), stopIndex(stop), optional(false) {
  helix = h;
  startIndices = new MotifSharedValue(no);
  stopIndices = new MotifSharedValue(no);
  lengths = new MotifSharedValue(no);
  indels = new MotifSharedValue(no);
}

void MotifStem::setStartIndex (int start) {
  startIndex = start;
}

void MotifStem::setStopIndex (int stop) {
  stopIndex = stop;
}

int MotifStem::getStartIndex () {
  return startIndex;
}

int MotifStem::getStopIndex () {
  return stopIndex;
}

MotifSharedValue *MotifStem::getStartIndices () {
  return startIndices;
}

MotifSharedValue *MotifStem::getStopIndices () {
  return stopIndices;
}

MotifSharedValue *MotifStem::getLengths () {
  return lengths;
}

MotifSharedValue *MotifStem::getIndels () {
  return indels;
}

void MotifStem::grow (int size) {
  setStopIndex(getStopIndex() + size);
}

void MotifStem::setValues () {
  int values[nbOrganisms];
  int nbInsertions;
  float threshold;
  int nbValues;
  bool isGap;
  int v;
  bool found;

  for (int j = 0; j < nbOrganisms; j++) {
    // first set @a startIndices
    found = false;
    for (int i = startIndex; i <= stopIndex; i++) {
      v = UNDEFINED;
      if (!(*motifSequences[j])[i]->isGap()) {
        v = i;
        found = true;
        break;
      }
    }
    if (!found) {
      optional = true;
      startIndices->setValue(j, UNDEFINED);
    }
    else {
      startIndices->setValue(j, v);
    }

    // then set @a stopIndices
    found = false;
    for (int i = stopIndex; i >= startIndex; i--) {
      v = UNDEFINED;
      if (!(*motifSequences[j])[i]->isGap()) {
        v = i;
        found = true;
        break;
      }
    }
    if (!found) {
      optional = true;
      stopIndices->setValue(j, UNDEFINED);
    }
    else {
      stopIndices->setValue(j, v);
    }
  }
 
  // then set @a indels
  for (int j = 0; j < nbOrganisms; j++) {
    values[j] = 0;
  }
  for (int i = startIndex; i <= stopIndex; i++) {
    nbValues = 0;
    nbInsertions = 0;
    for (int j = 0; j < nbOrganisms; j++) {
      if ((i >= startIndices->getValue(j)) && (j <= stopIndices->getValue(j))) {
        nbValues++;
        if ((*motifSequences[j])[i]->isGap()) {
          nbInsertions++;
        }
      }
    }
    threshold = nbValues / 2.0;
    // @a isGap is true if the @j th column of an alignment is a gap
    isGap = (nbInsertions > threshold);
    for (int j = 0; j < nbOrganisms; j++) {
      if (isGap) {
        if (!(*motifSequences[j])[i]->isGap()) {
          values[j]++;
        }
      }
      else {
        if (((*motifSequences[j])[i]->isGap()) && (i >= startIndices->getValue(j)) && (i <= stopIndices->getValue(j))) {
          values[j]++;
        }
      }
    }
  }
  // finally calculate @a lengths
  for (int j = 0; j < nbOrganisms; j++) {
    v = 0;
    for (int i = startIndex; i <= stopIndex; i++) {
      if (!(*motifSequences[j])[i]->isGap()) {
        v++;
      }
    }
    lengths->setValue(j, v-1);
    indels->setValue(j, values[j]);
  }
}

void MotifStem::setHelix (MotifHelix *h) {
  helix = h;
}

MotifHelix *MotifStem::getHelix () {
  return helix;
}

bool MotifStem::isOptional () {
  return (optional);
}

bool MotifStem::isInside (int pos) {
  if (optional) {
    return false;
  }
  return ((pos >= startIndex) && (pos <= stopIndex));
}

string MotifStem::print () {
  char m[255];
  sprintf(m, "%i - %i", startIndex, stopIndex);
  return (string(m) + "   (startIndices: " + startIndices->print() + " -- stopIndices: " + stopIndices->print() + " -- lengths: " + lengths->print() + " -- indels: " + indels->print() + ")");
}
