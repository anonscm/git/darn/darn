#include "motifDistance.h"

MotifDistance::MotifDistance (int no, MotifElement *e1, MotifElement *e2) {
  element1 = e1;
  element2 = e2;
  startIndices = new MotifSharedValue(no);
  stopIndices = new MotifSharedValue(no);
  lengths = new MotifSharedValue(no);
  elements = new MotifElement*[2];
  elements[0] = e1;
  elements[1] = e2;
}

int MotifDistance::getCostFromDistance (int d) {
  double a1 = (c1 - c2) / (sd2 - sd1);
  double b1 = (c2*sd2 - sd1*c1) / (sd2 - sd1);
  double a2 = (c2 - c1) / (sd4 - sd3);
  double b2 = (c1*sd4 - sd3*c2) / (sd4 - sd3);

  float value;

  if (d < sd1) {
    value = c2;
  }
  else if (d < sd2) {
    value = a1*d + b1;
  }
  else if (d <= sd3) {
    value = c1;
  }
  else if (d <= sd4) {
    value = a2*d + b2;
  }
  else {
    value = c2;
  }
  return static_cast<int>(round(value));
}

void MotifDistance::setFirstElement (MotifElement *e) {
  element1 = e;
  elements[0] = e;
}

void MotifDistance::setSecondElement (MotifElement *e) {
  element2 = e;
  elements[1] = e;
}

void MotifDistance::setValues () {
  int l;
  startIndex = element1->getStopIndex();
  stopIndex = element2->getStartIndex();
  for (int i = 0; i < nbOrganisms; i++) {
    startIndices->setValue(i, element1->getStopIndices()->getValue(i));
    stopIndices->setValue(i, element2->getStartIndices()->getValue(i));
    l = 0;
    if (startIndices->getValue(i) <= stopIndices->getValue(i)) {
      for (int j = startIndices->getValue(i); j <= stopIndices->getValue(i); j++) {
        if (!(*motifSequences[i])[j]->isGap()) {
          l++;
        }
      }
    }
    else {
      for (int j = stopIndices->getValue(i); j <= startIndices->getValue(i); j++) {
        if (!(*motifSequences[i])[j]->isGap()) {
          l++;
        }
      }
      l = -l;
    }
    lengths->setValue(i, l);
  }
}

bool MotifDistance::hasSoftPart () {
  return (lengths->getMaximum() - lengths->getMinimum() > 1);
}

string MotifDistance::exportFunction (int maxCost) {
  char msg1[1023];
  char msg2[1023];
  string msg;
  double ln2 = log(2);
  double sqrtLn2 = sqrt(ln2);
  double sqrt2 = sqrt(2);
  bool isReverse = (lengths->getMinimum() < 0);
  int start = ((isReverse)? -lengths->getMaximum()-1: lengths->getMinimum()-1);
  int stop = ((isReverse)? -lengths->getMinimum()-1: lengths->getMaximum()-1);
  int s1 = ((isReverse)? element2->getStartVariableIndex(): element1->getStopVariableIndex());
  int s2 = ((isReverse)? element1->getStopVariableIndex(): element2->getStartVariableIndex());
  float m = ((isReverse)? -lengths->getAverage()-1: lengths->getAverage()-1);
  float s = lengths->getStandardDeviation();
  int sdi1, sdi2, sdi3, sdi4, ci1, ci2;

  sprintf(msg1, "SPACER[length=%i..%i](X%i,X%i)\n", start, stop, s1, s2);
  msg = msg1;

  if (!hasSoftPart()) {
    return msg;
  }

  if (maxCost <= 0) {
    maxCost = DEFAULT_SPACER_COST;
  }

  sd1 = (2*m*sqrtLn2 - 2*s*ln2*sqrt2 - s*sqrt2) / (2*sqrtLn2);
  sd2 = (2*s*sqrt2 + 2*m*sqrtLn2 - 2*s * ln2*sqrt2 - s*sqrt2) / (2*sqrtLn2);
  sd3 = (-2*s*sqrt2 + 2*m*sqrtLn2 + 2*s*ln2*sqrt2 + s*sqrt2) / (2*sqrtLn2);
  sd4 = (2*m*sqrtLn2 + 2*s*ln2*sqrt2 + s*sqrt2) / (2*sqrtLn2);
  c1 = 0;
  c2 = maxCost - log(s * sqrt(2 * pi));
  sdi1 = static_cast<int>(round(sd1));
  sdi2 = static_cast<int>(round(sd2));
  sdi3 = static_cast<int>(round(sd3));
  sdi4 = static_cast<int>(round(sd4));
  ci1 = static_cast<int>(round(c1));
  ci2 = static_cast<int>(round(c2));
  sprintf(msg2, "SPACER[length=%i..%i..%i..%i,costs=%i..%i,model=optional](X%i,X%i)\n", sdi1, sdi2, sdi3, sdi4, ci1, ci2, s1, s2);
  msg += msg2;
  return msg;
}

int MotifDistance::getMaximumCost () {
  int maxCost = getCost(lengths->getValue(0));

  for (int i = 1; i < nbOrganisms; i++) {
    maxCost = max2(maxCost, getCost(i));
  }
  return maxCost;
}

int MotifDistance::getCost(int org) {
  if (!hasSoftPart()) {
    return 0;
  }
  return getCostFromDistance(lengths->getValue(org));
}

string MotifDistance::print () {
  string s;
  char m[100];
  sprintf(m, "[%i-%i]  ", startIndex, stopIndex);
  s += m;
  s += "(startIndices: " + startIndices->print() + " -- stopIndices: " + stopIndices->print() + " -- lengths: " + lengths->print() + ")";
  return s;
}
