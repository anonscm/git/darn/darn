
#include <stdio.h>

/**
 * skip the letters in @a letter in the line @a text
 * @param text the line to be read
 * @param letter the letters to be skipped
 * @return a pointer to the next chararcter
 */
char* skipLetters (char *text, const char *letter) {
  for (int i = 0; text[i] != '\0'; i++) {
    bool skip = false;
    for (int j = 0; letter[j] != '\0'; j++) {
      if (text[i] == letter[j]) {
        skip = true;
        break;
      }
    }
    if (!skip) {
      return text+i;
    }
  }
  return NULL;
}

/**
 * skip the first word of a line
 * @param text the line
 * @return a pointer to the first white character
 */
char* skipWord (char *text) {
  for (int i = 0; text[i] != '\0'; i++) {
    if ((text[i] == ' ') || (text[i] == '\t')) {
      return text+i;
    }
  }
  return NULL;
}

/**
 * copy the first word of a line
 * @param text the line
 * @param a place where to write the word
 */
void copyWord (char *text, char *word) {
  int i;
  for (i = 0; (text[i] != '\0') && (text[i] != ' ') && (text[i] != '\t') && (text[i] != '\n') && (text[i] != '\r'); i++) {
    word[i] = text[i];
  }
  word[i] = '\0';
}

/**
 * get the length of a word
 * @param text the line
 * @return the length
 */
int getWordLength (char *text) {
  int i;
  for (i = 0; (text[i] != '\0') && (text[i] != ' ') && (text[i] != '\t') && (text[i] != '\n') && (text[i] != '\r'); i++) 
    ;
  return i;
}

/**
 * check if the first word starts with the second word
 * @param text a possibly long text
 * @param word the word to be found
 * @return true, if the first word starts with the second word
 */
bool startsWith (char *text, const char *word) {
  for (int i = 0; word[i] != '\0'; i++) {
    if (text[i] != word[i]) {
      return false;
    }
  }
  return true;
}

/**
 * checks whether the line is empty
 * @param text the line to be scanned
 * @return true, if the line is empty
 */
bool isEmptyLine (char *text) {
  if (skipLetters(text, " \t") == NULL) {
    return true;
  }
  if (startsWith(skipLetters(text, " \t"), "//")) {
    return true;
  }
  if ((startsWith(skipLetters(text, " \t"), "#")) && (!startsWith(skipLetters(text, " \t"), "#=GC SS")) && (!startsWith(skipLetters(text, " \t"), "#=GR SS_cons"))) {
    return true;
  }
  return false;
}

/**
 * read a line from a file
 * @param fileStream the file
 * @param line the buffer to writen the line into
 * @return true, if the file is read
 */
bool getLine (ifstream &fileStream, char line[]) {
  char c;
  bool cr = false;
  bool endOfLine = false;

  if (! fileStream.good()) {
    return false;
  }

  do {
    c = fileStream.get();
    if (! fileStream.good()) {
      *line = '\0';
      return true;
    }
    else if (c == '\r') {
      cr = true;
      *line = '\0';
      endOfLine = true;
    }
    else if (c == '\n') {
      *line = '\0';
      endOfLine = true;
    }
    else {
      *(line++) = c;
    }
  }
  while (! endOfLine);

  if (cr) {
    c = fileStream.get();
    if (c != '\n') {
      fileStream.unget();
    }
  }

  return true;
}

/**
 * parse the alignment file (find sequences and structure)
 * @param fileName the name of the file
 */
void scan (char *fileName) {
  char buffer[1023];
  char word[1023];
  char m[100];
  char *pChar;
  int cpt;
  bool isStockholm = false;
  char firstSequenceFound = false;
  char firstSequenceName[100];
  int nbRepetitions = 1;
  bool firstRepetitionRead = false;
  string *tmpSequences;

  // first pass: retrieve the length of the alignment and the number of organisms
  ifstream fileStream(fileName);
  if (!fileStream.is_open()) {
    sprintf(m, "Error! Cannot open file \"%s\" for input!", fileName);
    trace(10, m);
    return;
  }
  while (getLine(fileStream, buffer)) {
    if (!isEmptyLine(buffer)) {
      // catch the name of the first sequence
      if (!firstSequenceFound) {
        // copy the name
        pChar = skipLetters(buffer, " \t");
        copyWord(pChar, firstSequenceName);
        firstSequenceFound = true;
        // get the size of the sequence
        pChar = skipWord(pChar);
        pChar = skipLetters(pChar, " \t");
        sequenceLength = getWordLength(pChar);
        nbOrganisms = 1;
      }
      // the file has the Stockholm format
      else if ((!isStockholm) && ((startsWith(buffer, "#=GR SS_cons")) || (startsWith(buffer, "#=GC SS")))) {
        isStockholm = true;
      }
      // the first name is found once again
      else if ((firstSequenceFound) && (startsWith(skipLetters(buffer, " \t"), firstSequenceName))) {
        // extend the size of the alignment
        pChar = skipLetters(buffer, " \t");
        pChar = skipWord(pChar);
        pChar = skipLetters(pChar, " \t");
        sequenceLength += getWordLength(pChar);
        nbRepetitions++;
        firstRepetitionRead = true;
      }
      // count the number of organisms
      else if (!firstRepetitionRead) {
        nbOrganisms++;
      }
    }
  }
  if (!isStockholm) {
    nbOrganisms--;
  }

  // initialize the sequences
  tmpSequences = new string[nbOrganisms+1];
  motifSequences = new MotifSequence*[nbOrganisms];
  for (int i = 0; i < nbOrganisms; i++) {
    tmpSequences[i] = string();
    motifSequences[i] = new MotifSequence(sequenceLength);
  }
  tmpSequences[nbOrganisms] = string();
  motifStructure = new MotifStructure(sequenceLength);

  // second pass: retrieve the alignment and the structure
  fileStream.clear();
  fileStream.seekg(0, ios_base::beg);
  for (int i = 0; i < nbRepetitions; i++) {
    cpt = 0;
    while (getLine(fileStream, buffer)) {
      if (!isEmptyLine(buffer)) {
        if (cpt < nbOrganisms) {
          // should be an alignment...
          pChar = skipLetters(buffer, " \t");
          copyWord(pChar, word);
          motifSequences[cpt]->setName(word);
          pChar = skipWord(pChar);
          pChar = skipLetters(pChar, " \t");
          copyWord(pChar, word);
          tmpSequences[cpt] += word;
        }
        else {
          // should be the structure
          pChar = skipLetters(buffer, " \t");
          if (isStockholm) {
            if (startsWith(pChar, "#=GC SS_cons")) {
              pChar += strlen("#=GC SS_cons");
              pChar = skipLetters(pChar, " \t");
            }
            else if (startsWith(pChar, "#=GR SS_cons")) {
              pChar += strlen("#=GR SS_cons");
              pChar = skipLetters(pChar, " \t");
            } 
            else if (startsWith(pChar, "#=GC SS")) {
              pChar += strlen("#=GC SS");
              pChar = skipLetters(pChar, " \t");
            }
          }
          copyWord(pChar, word);
          tmpSequences[nbOrganisms] += word;
          break;
        }
        cpt++;
      }
    }
  }
  fileStream.close();

  for (int i = 0; i < nbOrganisms; i++) {
    motifSequences[i]->setSequence(tmpSequences[i]);
  }
  motifStructure->setSequence(tmpSequences[nbOrganisms]);
  delete[] tmpSequences;

  sprintf(m, "\tFile \"%s\" red.", fileName);
  trace(10, m);
}


