#ifndef MOTIFMAIN_H_INCLUDED
#define MOTIFMAIN_H_INCLUDED

#include "motifSequence.h"
#include "motifParametersReader.h"
#include "motifInputFileReader.h"
#include "motifStructureAnalyser.h"
#include "motifHelix.h"
#include "motifPattern.h"
#include "motifDistance.h"


MotifSequenceLetter *motifSequenceLetterA;
MotifSequenceLetter *motifSequenceLetterC;
MotifSequenceLetter *motifSequenceLetterG;
MotifSequenceLetter *motifSequenceLetterT;
MotifSequenceLetter *motifSequenceLetterM;
MotifSequenceLetter *motifSequenceLetterR;
MotifSequenceLetter *motifSequenceLetterW;
MotifSequenceLetter *motifSequenceLetterS;
MotifSequenceLetter *motifSequenceLetterY;
MotifSequenceLetter *motifSequenceLetterK;
MotifSequenceLetter *motifSequenceLetterN;
MotifSequenceLetter *motifSequenceLetterX;
MotifSequenceLetter *motifSequenceLetterGAP;
MotifSequenceLetter **motifSequenceLetters;

MotifStructureLetter *motifStructureLetterLEFT1;
MotifStructureLetter *motifStructureLetterRIGHT1;
MotifStructureLetter *motifStructureLetterLEFT2;
MotifStructureLetter *motifStructureLetterRIGHT2;
MotifStructureLetter *motifStructureLetterLEFT3;
MotifStructureLetter *motifStructureLetterRIGHT3;
MotifStructureLetter *motifStructureLetterLEFT4;
MotifStructureLetter *motifStructureLetterRIGHT4;
MotifStructureLetter *motifStructureLetterGAP;
MotifStructureLetter **motifStructureLetters;

char *inputFileName;
ostream *outputStream;

int nbOrganisms;
int sequenceLength;

MotifSequence **motifSequences;
MotifStructure *motifStructure;

char MotifSequenceLetter::characters[] = {'A', 'C', 'G', 'T', 'M', 'R', 'W', 'S', 'Y', 'K', 'N', 'X', '-'};
bool MotifSequenceLetter::matching[][NBSEQUENCELETTERS] = {
/*       A  C  G  T  M  R  W  S  Y  K  N  X  - */
/* A */ {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0},
/* C */ {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
/* G */ {0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0},
/* T */ {1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
/* M */ {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
/* R */ {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
/* W */ {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
/* S */ {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
/* Y */ {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
/* K */ {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
/* N */ {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
/* X */ {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
/* - */ {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};
bool MotifSequenceLetter::compatibility[][NBSEQUENCELETTERS] = {
/*       A  C  G  T  M  R  W  S  Y  K  N  X  - */
/* A */ {1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0},
/* C */ {0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0},
/* G */ {0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0},
/* T */ {0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0},
/* M */ {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
/* R */ {1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
/* W */ {1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0},
/* S */ {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
/* Y */ {0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0},
/* K */ {0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0},
/* N */ {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
/* X */ {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
/* - */ {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};


char MotifStructureLetter::characters[] = {'(', ')', '<', '>', '{', '}', '[', ']', '-'};

vector<MotifPattern*> motifPatterns;
vector<MotifHelix*> motifHelices;
vector<MotifDistance*> motifDistances;

int traceLevel;

bool information;

#endif
