#include "motifMain.h"

/**
 * The main class
 */
int main (int argc, char *argv[]) {
  int maxCost, cost = 0;
  int nbOptionalPatterns = 0;
  int nbOptionalHelices = 0;
  char m[511];
  char mScore[511];
  mScore[0] = '\0';

  motifSequenceLetterA = new MotifSequenceLetter('A');
  motifSequenceLetterC = new MotifSequenceLetter('C');
  motifSequenceLetterG = new MotifSequenceLetter('G');
  motifSequenceLetterT = new MotifSequenceLetter('T');
  motifSequenceLetterM = new MotifSequenceLetter('M');
  motifSequenceLetterR = new MotifSequenceLetter('R');
  motifSequenceLetterW = new MotifSequenceLetter('W');
  motifSequenceLetterS = new MotifSequenceLetter('S');
  motifSequenceLetterY = new MotifSequenceLetter('Y');
  motifSequenceLetterK = new MotifSequenceLetter('K');
  motifSequenceLetterN = new MotifSequenceLetter('N');
  motifSequenceLetterX = new MotifSequenceLetter('X');
  motifSequenceLetterGAP = new MotifSequenceLetter('-');

  motifSequenceLetters = new MotifSequenceLetter*[NBSEQUENCELETTERS];
  motifSequenceLetters[0] = motifSequenceLetterA;
  motifSequenceLetters[1] = motifSequenceLetterC;
  motifSequenceLetters[2] = motifSequenceLetterG;
  motifSequenceLetters[3] = motifSequenceLetterT;
  motifSequenceLetters[4] = motifSequenceLetterM;
  motifSequenceLetters[5] = motifSequenceLetterR;
  motifSequenceLetters[6] = motifSequenceLetterW;
  motifSequenceLetters[7] = motifSequenceLetterS;
  motifSequenceLetters[8] = motifSequenceLetterY;
  motifSequenceLetters[9] = motifSequenceLetterK;
  motifSequenceLetters[10] = motifSequenceLetterN;
  motifSequenceLetters[11] = motifSequenceLetterX;
  motifSequenceLetters[12] = motifSequenceLetterGAP;

  motifStructureLetterLEFT1 = new MotifStructureLetter('(');
  motifStructureLetterLEFT2 = new MotifStructureLetter('<');
  motifStructureLetterLEFT3 = new MotifStructureLetter('{');
  motifStructureLetterLEFT4 = new MotifStructureLetter('[');
  motifStructureLetterRIGHT1 = new MotifStructureLetter(')');
  motifStructureLetterRIGHT2 = new MotifStructureLetter('>');
  motifStructureLetterRIGHT3 = new MotifStructureLetter('}');
  motifStructureLetterRIGHT4 = new MotifStructureLetter(']');
  motifStructureLetterGAP = new MotifStructureLetter('-');

  motifStructureLetters = new MotifStructureLetter*[MotifStructureLetter::nbMotifStructureLetters];
  motifStructureLetters[0] = motifStructureLetterLEFT1;
  motifStructureLetters[1] = motifStructureLetterRIGHT1;
  motifStructureLetters[2] = motifStructureLetterLEFT2;
  motifStructureLetters[3] = motifStructureLetterRIGHT2;
  motifStructureLetters[4] = motifStructureLetterLEFT3;
  motifStructureLetters[5] = motifStructureLetterRIGHT3;
  motifStructureLetters[6] = motifStructureLetterLEFT4;
  motifStructureLetters[7] = motifStructureLetterRIGHT4;
  motifStructureLetters[8] = motifStructureLetterGAP;

  readParameters(argc, argv);

  scan(inputFileName);

  // find patterns
  findPatterns();
  for (unsigned int i = 0; i < motifPatterns.size(); i++) {
    motifPatterns[i]->setValues();
    if (motifPatterns[i]->isOptional()) {
      nbOptionalPatterns++;
    }
  }
  // find helices
  findHelices();
  for (unsigned int i = 0; i < motifHelices.size(); i++) {
    motifHelices[i]->setValues();
    if (motifHelices[i]->isOptional()) {
      nbOptionalHelices++;
    }
  }
  // find spacers
  findDistances();
  for (unsigned int i = 0; i < motifDistances.size(); i++) {
    motifDistances[i]->setValues();
  }

  // get temporary top value
  maxCost = 0;
  for (int j = 0; j < nbOrganisms; j++) {
    cost = 0;
    for (unsigned int i = 0; i < motifPatterns.size(); i++) {
      cost += motifPatterns[i]->getCost(j);
    }
    for (unsigned int i = 0; i < motifHelices.size(); i++) {
      cost += motifHelices[i]->getCost(j);
    }
    maxCost = max2(maxCost, cost);
  }

  // print header
  sprintf(m, "VARIABLES X_VAR = 1..%lu\n", 2*((motifPatterns.size()-nbOptionalPatterns) + 2*(motifHelices.size()-nbOptionalHelices)));
  string s(m);

  // print soft constraints
  for (unsigned int i = 0; i < motifDistances.size(); i++) {
    s += motifDistances[i]->exportFunction(maxCost);
  }
  for (unsigned int i = 0; i < motifPatterns.size(); i++) {
    if (!motifPatterns[i]->isOptional()) {
      s += motifPatterns[i]->exportFunction();
    }
  }
  for (unsigned int i = 0; i < motifHelices.size(); i++) {
    if (!motifHelices[i]->isOptional()) {
      s += motifHelices[i]->exportFunction();
    }
  }

  // recompute top value
  maxCost = 0;
  for (int j = 0; j < nbOrganisms; j++) {
    cost = 0;
    for (unsigned int i = 0; i < motifDistances.size(); i++) {
      cost += motifDistances[i]->getCost(j);
    }
    for (unsigned int i = 0; i < motifPatterns.size(); i++) {
      cost += motifPatterns[i]->getCost(j);
    }
    for (unsigned int i = 0; i < motifHelices.size(); i++) {
      cost += motifHelices[i]->getCost(j);
    }
    maxCost = max2(maxCost, cost);
  }

  if (information) {
    int index = 0;
    string sSequences[nbOrganisms];
    string sGiven;
    string sPrimary;
    string sSecondary;
    bool found;
    while (index < motifStructure->getSize()) {
      for (int i = 0; i < nbOrganisms; i++) {
        sSequences[i] = "            ";
      }
      sGiven     = "GIVEN SEC.: ";
      sPrimary   = "PRIMARY:    ";
      sSecondary = "SECONDARY:  ";
      while ((sGiven.size() < LINE_SIZE) && (index < motifStructure->getSize())) {

        // print alignment
        for (int i = 0; i < nbOrganisms; i++) {
          sSequences[i] += (*motifSequences[i])[index]->print();
        }

        // print given secondary structure
        sGiven += motifStructure->getStructure(index)->print();

        // print computed primary structure
        found = false;
        for (unsigned int i = 0; i < motifPatterns.size(); i++) {
          if (motifPatterns[i]->isInside(index)) {
            found = true;
            if (! motifStructure->getPureSequence(index)->isGap()) {
              sPrimary += motifStructure->getPureSequence(index)->print();
            }
            else {
              sPrimary += tolower(motifStructure->getSequence(index)->print());
            }
          }
          if (found) {
            break;
          }
        }
        if (! found) {
          sPrimary += '-';
        }

        // print computed secondary structure
        found = false;
        for (unsigned int i = 0; i < motifHelices.size(); i++) {
          switch (motifHelices[i]->isInside(index)) {
            case -1:
              sSecondary += '(';
              found = true;
              break;
            case 1:
              sSecondary += ')';
              found = true;
              break;
          }
          if (found) {
            break;
          }
        }
        if (! found) {
          sSecondary += '-';
        }
        index++;
      }
      for (int i = 0; i < nbOrganisms; i++) {
        trace(1, sSequences[i]);
      }
      trace(0, sGiven);
      trace(0, sPrimary);
      trace(0, sSecondary);
      trace(0, "");
    }

    if (traceLevel > 1) {
      for (unsigned int i = 0; i < motifHelices.size(); i++) {
        s = motifHelices[i]->printInformation();
        if (s != "") {
          trace(2, s.c_str());
        }
      }
      for (unsigned int i = 0; i < motifPatterns.size(); i++) {
        s = motifPatterns[i]->printInformation();
        if (s != "") {
          trace(2, s.c_str());
        }
      }
    }
    return 0;
  }

  
  sprintf(mScore, "TOP_VALUE = %i", maxCost+1);

  trace(0, mScore);
  trace(0, s);
  return 0;
}
