#ifndef MOTIFCONSANA_H_INCLUDED
#define MOTIFCONSANA_H_INCLUDED

#include <typeinfo>

#include "motifCommon.h"
#include "motifPattern.h"
#include "motifHelix.h"
#include "motifDistance.h"

/**
 * find the patterns in the consensus
 */
void findPatterns () {
  string word;
  MotifPattern *pattern;
  bool inWord = false;
  bool sureWord = false;
  vector<MotifPattern *> purePatterns;
  vector<MotifPattern *> patterns;
  unsigned int purePatternsIndex;
  unsigned int patternsIndex;
  int wordStart = NOT_FOUND;
  int wordStop = NOT_FOUND;
  int gapSize = NOT_FOUND;
  int ambiguousSize = NOT_FOUND;

  // deal with words with no error
  for (int pos = 0; pos < motifStructure->getSize(); pos++) {
    
    // not a gap
    if (!(motifStructure->getPureSequence(pos)->isGap())) {
      // not N
      if (motifStructure->getPureSequence(pos) != motifSequenceLetterN) {
        wordStop = pos;
        if (! inWord) {
          sureWord = false;
          inWord = true;
          wordStart = pos;
        }
        // not ambiguous
        if (!(motifStructure->getPureSequence(pos)->isAmbiguous())) {
          if (! sureWord) {
            sureWord = true;
          }
        }
      }
    }
    // a gap
    else {
      if ((inWord) && (sureWord)) {
        word = "";
        for (int i = wordStart; i <= wordStop; i++) {
          word += motifStructure->getPureSequence(i)->print();
          motifStructure->getSequence()->cancel(i);
        }
        pattern = new MotifPattern(nbOrganisms, word);
        purePatterns.push_back(pattern);
        pattern->setStartIndex(wordStart);
        pattern->setStopIndex(wordStop);
      }
      inWord = false;
      sureWord = false;
    }
  }
  // possibly close the last word
  if ((inWord) && (sureWord)) {
    for (int i = wordStart; i <= wordStop; i++) {
      word += motifStructure->getPureSequence(i)->print();
      motifStructure->getSequence()->cancel(i);
    }
    pattern = new MotifPattern(nbOrganisms, word);
    purePatterns.push_back(pattern);
    pattern->setStartIndex(wordStart);
    pattern->setStopIndex(wordStop);
  }

  // deal with word with errors
  inWord = false;
  for (int pos = 0; pos < motifStructure->getSize(); pos++) {
    
    // normal letter
    if ((!(motifStructure->getSequence(pos)->isAmbiguous())) && (!(motifStructure->getSequence(pos)->isGap()))) {
      gapSize = 0;
      ambiguousSize = 0;
      wordStop = pos;

      // open a new word
      if (!inWord) {
        pattern = new MotifPattern(nbOrganisms);
        patterns.push_back(pattern);
        pattern->setStartIndex(pos);
        inWord = true;
      }
    }

    // gap
    else if (motifStructure->getSequence(pos)->isGap()) {
      if (inWord) {
        gapSize++;
        if (gapSize > MAXGAPSIZE) {
          inWord = false;
          pattern->setStopIndex(wordStop);
          word = "";
          for (int i = pattern->getStartIndex(); i <= wordStop; i++) {
            if (! motifStructure->getSequence(i)->isGap()) {
              word += motifStructure->getSequence(i)->print();
            }
          }
          pattern->setPattern(word);
        }
      }
    }

    // canceled letter
    else if (motifStructure->getSequence(pos)->isCanceled()) {
      if (inWord) {
        inWord = false;
        pattern->setStopIndex(wordStop);
        word = "";
        for (int i = pattern->getStartIndex(); i <= wordStop; i++) {
          if (! motifStructure->getSequence(i)->isGap()) {
            word += motifStructure->getSequence(i)->print();
          }
        }
        pattern->setPattern(word);
      }
    }

    // ambiguous letter
    else if (motifStructure->getSequence(pos)->isAmbiguous()) {
      if (inWord) {
        ambiguousSize++;
        if (ambiguousSize > MAXAMBIGUOUSSIZE) {
          inWord = false;
          pattern->setStopIndex(wordStop);
          word = "";
          for (int i = pattern->getStartIndex(); i <= wordStop; i++) {
            if (! motifStructure->getSequence(i)->isGap()) {
              word += motifStructure->getSequence(i)->print();
            }
          }
          pattern->setPattern(word);
        }
      }
    }
  }

  // possibly close the last word
  if (inWord) {
    pattern->setStopIndex(wordStop);
    word = "";
    for (int i = pattern->getStartIndex(); i <= wordStop; i++) {
      if (! motifStructure->getSequence(i)->isGap()) {
        word += motifStructure->getSequence(i)->print();
      }
    }
    pattern->setPattern(word);
  }

  // sort the two kinds of patterns
  purePatternsIndex = 0;
  patternsIndex = 0;
  while ((purePatternsIndex < purePatterns.size()) && (patternsIndex < patterns.size())) {
    if (purePatterns[purePatternsIndex]->getStartIndex() < patterns[patternsIndex]->getStartIndex()) {
      motifPatterns.push_back(purePatterns[purePatternsIndex]);
      purePatternsIndex++;
    }
    else {
      motifPatterns.push_back(patterns[patternsIndex]);
      patternsIndex++;
    }
  }
  while (purePatternsIndex < purePatterns.size()) {
    motifPatterns.push_back(purePatterns[purePatternsIndex]);
    purePatternsIndex++;
  }
  while (patternsIndex < patterns.size()) {
    motifPatterns.push_back(patterns[patternsIndex]);
    patternsIndex++;
  }
}

/**
 * find the helices in the consensus
 */
void findHelices () {
  MotifHelix *helix;
  MotifStem *stem1 = NULL;
  MotifStem *stem2 = NULL;
  bool inLeftStem = false;
  int leftStemStop = NOT_FOUND;
  int rightStemStart = NOT_FOUND;
  int gapSize = NOT_FOUND;
  int rightPos = NOT_FOUND;

  // read the sequence
  for (int leftPos = 0; leftPos < motifStructure->getSize(); leftPos++) {
    // left stem of an helix
    if (motifStructure->getStructure(leftPos)->isLeftBrace()) {
      rightPos = motifStructure->getPair(leftPos);

      // create a new helix
      if (! inLeftStem) {
        helix = new MotifHelix(nbOrganisms);
        motifHelices.push_back(helix);
        stem1 = helix->getFirstStem();
        stem2 = helix->getSecondStem();
        stem1->setStartIndex(leftPos);
        stem2->setStopIndex(rightPos);
        inLeftStem = true;
      }
      // if the right stem is not OK, end the helix
      else if ((rightStemStart <= rightPos) || (rightStemStart - rightPos > MAXGAPSIZEHELIX+1)) {
        stem1->setStopIndex(leftStemStop);
        stem2->setStartIndex(rightStemStart);
        inLeftStem = false;
      }
      // if the stems are OK
      leftStemStop = leftPos;
     rightStemStart = rightPos;
      gapSize = 0;
    }
    else {
      if (inLeftStem) {
        gapSize++;
        // if the left stem is not OK, end the helix
        if (gapSize > MAXGAPSIZEHELIX) {
          stem1->setStopIndex(leftStemStop);
          stem2->setStartIndex(rightStemStart);
          inLeftStem = false;
        }
      }
    }
  }
  if (inLeftStem) {
    stem1->setStopIndex(leftStemStop);
    stem2->setStartIndex(rightStemStart);
  }
}

/**
 * find the distances in the consensus
 */
void findDistances () {
  int nbMotifElements = 0;
  vector<MotifElement *> motifElements1;
  vector<MotifElement *> motifElements2;
  int minValue, index = NOT_FOUND;
  MotifDistance *distance;

  // put all non optional patterns and stems in one vector
  for (unsigned int i = 0; i < motifPatterns.size(); i++) {
    if (!motifPatterns[i]->isOptional()) {
      motifElements1.push_back(motifPatterns[i]);
      nbMotifElements++;
    }
  }
  for (unsigned int i = 0; i < motifHelices.size(); i++) {
    if (!motifHelices[i]->isOptional()) {
      motifElements1.push_back(motifHelices[i]->getFirstStem());
      motifElements1.push_back(motifHelices[i]->getSecondStem());
      nbMotifElements += 2;
    }
  }

  // sort the patterns and stems by increasing order
  for (int i = 0; i < nbMotifElements; i++) {
    minValue = sequenceLength+1;
    for (int j = 0; j < nbMotifElements; j++) {
      if ((motifElements1[j] != NULL) && (motifElements1[j]->getStartIndex() < minValue)) {
        minValue = motifElements1[j]->getStartIndex();
        index = j;
      }
    }
    motifElements2.push_back(motifElements1[index]);
    motifElements2[i]->setStartVariableIndex(2*i + 1);
    motifElements2[i]->setStopVariableIndex(2*i + 2);
    motifElements1[index] = NULL;
  }

  // create the distance constraints
  for (int i = 0; i < nbMotifElements-1; i++) {
    // if the two elements are the stems of an helix,
    // do not add a distance, since it is recorded as a loop
    if ((typeid(*motifElements2[i]) != typeid(MotifStem)) || (typeid(*motifElements2[i+1]) != typeid(MotifStem)) || (static_cast<MotifStem*>(motifElements2[i])->getHelix() != static_cast<MotifStem*>(motifElements2[i+1])->getHelix())) {
      distance = new MotifDistance(nbOrganisms, motifElements2[i], motifElements2[i+1]);
      motifDistances.push_back(distance);
    }
  }
}

#endif
