#ifndef MOTIFDISTANCE_H_INCLUDED
#define MOTIFDISTANCE_H_INCLUDED

#include "motifSequence.h"
#include "motifElement.h"

/**
 * The class represents a distance between two elements of structure
 */
class MotifDistance {

protected:
  /**
   * the first element of structure
   */
  MotifElement *element1;
  /**
   * the second element of structure
   */
  MotifElement *element2;
  /**
   * both elements of structure
   */
  MotifElement **elements;
  /**
   * the beginning of the stem
   */
  int startIndex;
  /**
   * the end of the stem
   */
  int stopIndex;
  /**
   * the beginning in each sequence
   */
  MotifSharedValue *startIndices;
  /**
   * the end in each sequence
   */
  MotifSharedValue *stopIndices;
  /**
   * the different lengths
   */
  MotifSharedValue *lengths;

  /**
   * the first allowed hard distance
   */
  int d1;
  /**
   * the last allowed hard distance
   */
  int d2;
  /**
   * the first allowed soft distance
   */
  double sd1;
  /**
   * the second soft distance
   */
  double sd2;
  /**
   * the third soft distance
   */
  double sd3;
  /**
   * the last allowed soft distance
   */
  double sd4;
  /**
   * the minimum soft cost
   */
  double c1;
  /**
   * the maximum soft cost
   */
  double c2;

  /**
   * get the cost given by this constraint, given a distance
   * @param d a distance
   * @return a cost
   */
  int getCostFromDistance (int d);

public:
  /**
   * the constructor
   * @param no the number of organisms
   * @param e1 the first element
   * @param e2 the second element
   */
  MotifDistance (int no, MotifElement *e1 = NULL, MotifElement *e2 = NULL);
  /**
   * set @a element1
   * @param e the first element of structure
   */
  void setFirstElement (MotifElement *e);
  /**
   * set @a element2
   * @param e the second element of structure
   */
  void setSecondElement (MotifElement *e);

  /**
   * set the @a startIndex, @a stopIndex, @a startIndices, @a stopIndices, @a lengths
   */
  void setValues ();

  /**
   * whether one can express preferences on this constraint
   * @return true, if one can express preferences on this constraint
   */
  bool hasSoftPart();
  /**
   * export the element as a Darn cost function descriptor line
   * @param maxCost the maximum cost of the network
   * @return the descriptor line
   */
  string exportFunction(int maxCost);
  /**
   * export the maximum cost given by this constraint
   * @return the maximum cost
   */
  int getMaximumCost();
  /**
   * get the cost by this distance on a given constraint
   * @param org the index of the sequence
   * @return a cost
   */
  int getCost(int org);

  /**
   * print a representation of the stem
   * @return a string
   */
  string print ();
};

#endif

