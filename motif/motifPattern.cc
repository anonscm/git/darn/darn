#include "motifPattern.h"

MotifPattern::MotifPattern (int no, string s, int start, int stop) : startIndex(start), stopIndex(stop), optional(false) {
  startIndices = new MotifSharedValue(no);
  stopIndices = new MotifSharedValue(no);
  errors = new MotifSharedValue(no);
  if (s != "") {
    pattern = new MotifSequence(s.length());
    pattern->setSequence(s);
  }
}

MotifPattern::~MotifPattern () {

}

int MotifPattern::getErrors(int org, int start, int stop) {
  if ((start == UNDEFINED) || (stop == UNDEFINED)) {
    return pattern->getSize();
  }
  
  MotifSequence seq(motifSequences[org], start, stop-start+1);
  seq.removeGaps();

  int l1 = seq.getSize();
  int l2 = pattern->getSize();
  int matrix[l1][l2];
  int val1, val2, val3;

  for (int i = 1; i < l1; i++) {
    matrix[i][0] = MAX_POSSIBLE_COST;
  }
  for (int j = 1; j < l2; j++) {
    matrix[0][j] = MAX_POSSIBLE_COST;
  }
  matrix[0][0] = 0;
  for (int i = 1; i < l1; i++) {
    for (int j = 1; j < l2; j++) {
      val1 = matrix[i-1][j-1] + ((seq[i-1]->isCompatible((*pattern)[j-1]))? 0: 1);
      val2 = matrix[i][j-1] + 1;
      val3 = matrix[i-1][j] + 1;
      matrix[i][j] = min3(val1, val2, val3);
    }
  }
  val1 = matrix[l1-1][l2-1] + ((seq[l1-1]->isCompatible((*pattern)[l2-1]))? 0: 1);

  return val1;
}

void MotifPattern::setPattern (string s) {
  pattern = new MotifSequence(s.length());
  pattern->setSequence(s);
}
    
void MotifPattern::setStartIndex (int start) {
  startIndex = start;
}

void MotifPattern::setStopIndex (int stop) {
  stopIndex = stop;
}

int MotifPattern::getStartIndex () {
  return startIndex;
}

int MotifPattern::getStopIndex () {
  return stopIndex;
}

MotifSharedValue *MotifPattern::getStartIndices () {
  return startIndices;
}

MotifSharedValue *MotifPattern::getStopIndices () {
  return stopIndices;
}

void MotifPattern::setValues () {
  double sum;
  int minScore, maxScore, nbScores;
  int *quantiles;
  string s;
  bool found;
  int nbNonAmbiguousNucleotides = 0;

  // set @a startIndices
  for (int j = 0; j < nbOrganisms; j++) {
    found = false;
    for (int i = startIndex; i <= stopIndex; i++) {
      if (!(*motifSequences[j])[i]->isGap()) {
        startIndices->setValue(j, i);
        found = true;
        break;
      }
    }
    if (! found) {
      startIndices->setValue(j, UNDEFINED);
      optional = true;
    }
  }

  // set @a stopIndices
  for (int j = 0; j < nbOrganisms; j++) {
    found = false;
    for (int i = stopIndex; i >= startIndex; i--) {
      if (!(*motifSequences[j])[i]->isGap()) {
        stopIndices->setValue(j, i);
        found = true;
        break;
      }
    }
    if (! found) {
      stopIndices->setValue(j, UNDEFINED);
      optional = true;
    }
  }

  // set @a errors
  for (int j = 0; j < nbOrganisms; j++) {
    errors->setValue(j, getErrors(j, startIndices->getValue(j), stopIndices->getValue(j)));
  }
  
  for (int i = 0; i < pattern->getSize(); i++) {
    if (! (*pattern)[i]->isAmbiguous()) {
      nbNonAmbiguousNucleotides++;
    }
  }
  if ((errors->getMaximum() >= (nbNonAmbiguousNucleotides / 2)) && (errors->getMaximum() > 0)) {
    optional = true;
  }

  if ((!hasSoftPart()) || (optional)) {
    return;
  }

  // get the scores
  quantiles = errors->getQuantiles();
  scores = new int[errors->getMaximum()+1];
  for (int i = 0; i <= errors->getMaximum(); i++) {
    if (quantiles[i] == 0) {
      scores[i] = UNDEFINED;
    }
    else {
      sum = 0;
      for (int j = 0; j <= i; j++) {
        sum += choice(j, i) * choice(i-j, pattern->getSize()) * pw(pattern->getSize()+1, j);
      }
      scores[i] = static_cast<int>(round(i * log(NBNUCLEOTIDES) + log(sum) - log(quantiles[i]) + log(nbOrganisms)));
    }
  }

  // complete scores for unknown observations
  if (scores[0] == UNDEFINED) {
    scores[0] = 0;
  }
  minScore = 0;
  nbScores = UNDEFINED;
  for (int i = 0; i <= errors->getMaximum(); i++) {
    if (scores[i] == UNDEFINED) {
      if (nbScores == UNDEFINED) {
        nbScores = 1;
      }
      else {
        nbScores++;
      }
    }
    else {
      if (nbScores == UNDEFINED) {
        minScore = i;
      }
      else {
        maxScore = i;
        for (int j = minScore+1; j < maxScore; j++) {
          scores[j] = static_cast<int>(round((scores[maxScore]-scores[minScore])*(static_cast<float>(j-minScore)/(nbScores+1)))) + scores[minScore];
        }
        nbScores = UNDEFINED;
        minScore = i;
      }
    }
  }

  // reset minimum score to 0
  minScore = scores[0];
  if (minScore != 0) {
    for (int i = 0; i <= errors->getMaximum(); i++) {
      scores[i] -= minScore;
    }
  }

  // possibly rectify costs when cost function is not monotonous
  for (int i = 0; i < errors->getMaximum(); i++) {
    scores[i+1] = max2(scores[i], scores[i+1]);
  }
}

bool MotifPattern::isOptional () {
  return (optional);
}

bool MotifPattern::hasSoftPart () {
  return (errors->getMaximum() > 0);
}

string MotifPattern::exportFunction () {
  char mTmp[511];
  string m;

  if (optional) {
    return string("");
  }
  
  // print the constraint
  sprintf(mTmp, "PATTERN[word=%s,errors=%i,model=%s](X%i,X%i)", pattern->print().c_str(), errors->getMaximum(), ((hasSoftPart())? "soft": "hard"), startVariableIndex, stopVariableIndex);
  m = mTmp;
  if (hasSoftPart()) {
    m += " {";
    sprintf(mTmp, "%i", scores[0]);
    m += mTmp;
    for (int i = 1; i <= errors->getMaximum(); i++) {
      sprintf(mTmp, ",%i", scores[i]);
      m += mTmp;
    }
    m += "}";
  }
  m += "\n";
  return m;
}

int MotifPattern::getMaximumCost () {
  if ((!hasSoftPart()) || (optional)) {
    return 0;
  }
  return scores[errors->getMaximum()];
}

int MotifPattern::getCost (int org) {
  if ((!hasSoftPart()) || (optional)) {
    return 0;
  }
  return scores[getErrors(org, startIndices->getValue(org), stopIndices->getValue(org))];
}

bool MotifPattern::isInside (int pos) {
  if (optional) {
    return false;
  }
  return ((pos >= startIndex) && (pos <= stopIndex));
}

string MotifPattern::printInformation () {
  char m[511];
  char mTmp[511];
  if (! optional) {
    return string("");
  }
  if (errors->getMaximum() >= MAX_POSSIBLE_COST) {
    sprintf(mTmp, "does not appear %i time(s)", errors->getCard(MAX_POSSIBLE_COST));
  }
  else {
    sprintf(mTmp, "contains up to %i error(s) (%i time(s), e.g. in organism #%i)", errors->getMaximum(), errors->getCard(errors->getMaximum()), errors->getArgMaximum());
  }
  sprintf(m, "Pattern '%s' %i-%i is optional for it %s", pattern->print().c_str(), startIndex, stopIndex, mTmp);
  return string(m);
}

string MotifPattern::print () {
  string s (pattern->print());
  char m[100];
  sprintf(m, ": %i-%i", startIndex, stopIndex);
  s += m;
  s += "(start: " + startIndices->print() + " -- stop: " + stopIndices->print() + " -- errors: " + errors->print() + ")";
  return s;
}
