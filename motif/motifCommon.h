#ifndef MOTIFCOM_H_INCLUDED
#define MOTIFCOM_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <stack>
#include <cmath>
using namespace std;

class MotifSequenceLetter;
class MotifSequence;
class MotifStructureLetter;
class MotifStructure;
class MotifPattern;
class MotifHelix;
class MotifDistance;


// Global variables
extern char *inputFileName;
extern ostream *outputStream;
extern MotifSequenceLetter *motifSequenceLetterA;
extern MotifSequenceLetter *motifSequenceLetterC;
extern MotifSequenceLetter *motifSequenceLetterG;
extern MotifSequenceLetter *motifSequenceLetterT;
extern MotifSequenceLetter *motifSequenceLetterM;
extern MotifSequenceLetter *motifSequenceLetterR;
extern MotifSequenceLetter *motifSequenceLetterW;
extern MotifSequenceLetter *motifSequenceLetterS;
extern MotifSequenceLetter *motifSequenceLetterY;
extern MotifSequenceLetter *motifSequenceLetterK;
extern MotifSequenceLetter *motifSequenceLetterN;
extern MotifSequenceLetter *motifSequenceLetterX;
extern MotifSequenceLetter *motifSequenceLetterGAP;
extern MotifSequenceLetter **motifSequenceLetters;
extern MotifStructureLetter *motifStructureLetterLEFT1;
extern MotifStructureLetter *motifStructureLetterRIGHT1;
extern MotifStructureLetter *motifStructureLetterLEFT2;
extern MotifStructureLetter *motifStructureLetterRIGHT2;
extern MotifStructureLetter *motifStructureLetterLEFT3;
extern MotifStructureLetter *motifStructureLetterRIGHT3;
extern MotifStructureLetter *motifStructureLetterLEFT4;
extern MotifStructureLetter *motifStructureLetterRIGHT4;
extern MotifStructureLetter *motifStructureLetterGAP;
extern MotifStructureLetter **motifStructureLetters;
extern int nbOrganisms;
extern int sequenceLength;
extern MotifSequence **motifSequences;
extern MotifStructure *motifStructure;
extern vector<MotifPattern*> motifPatterns;
extern vector<MotifHelix*> motifHelices;
extern vector<MotifDistance*> motifDistances;
extern int traceLevel;
extern bool information;


/**
 * the number of nucleotides
 */
static const int NBNUCLEOTIDES = 4;
/**
 * the number of extended nucleotides
 */
static const int NBSEQUENCELETTERS = 13;
/**
 * the maximum number of contiguous ambiguous nucleotides in a pattern constraint
 */
static const int MAXAMBIGUOUSSIZE = 2;
/**
 * the maximum number of contiguous gaps in a pattern constraint
 */
static const int MAXGAPSIZE = 2;
/**
 * the maximum number of contiguous gaps in a helix constraint
 */
static const int MAXGAPSIZEHELIX = 0;
/**
 * useful constant, to be returned when a value could be found
 */
static const int NOT_FOUND = -1;
/**
 * useful constant, to be returned when a value is not known
 */
static const int UNDEFINED = -1;
/**
 * if the presence rate of a nucleotide is above this theshold, there is consensus
 */
static const float CONSENSUS_THRESHOLD = 0.80;
/**
 * the minimum ratio no gap / nb sequences in order to take part in a word
 */
static const float QUORUM = 0.66;
/**
 * the maximum possible cost given by a constraint
 */
static const int MAX_POSSIBLE_COST = 10000;
/**
 * the maximum cost of a spacer function when no information is available
 */
static const int DEFAULT_SPACER_COST = 10;
/**
 * the maximum number of characters in a line
 */
static const unsigned int LINE_SIZE = 100;
/**
 * pi
 */
static const double pi = 4 * (atan(1.0));
/**
 * where the costs are stored
 */
static const char costFile[] = "costs.dat";

static inline void trace (int level, string message) {
  if (level < traceLevel) (*outputStream) << message << endl;
}

static inline int min2 (const int i1, const int i2) {
  return ((i1 < i2)? i1: i2);
}

static inline int min3 (const int i1, const int i2, const int i3) {
  return min2(min2(i1, i2), i3);
}

static inline int min4 (const int i1, const int i2, const int i3, const int i4) {
  return min2(min2(i1, i2), min2(i3, i4));
}

static inline int max2 (const int i1, const int i2) {
    return ((i1 > i2)? i1: i2);
}

static inline int max3 (const int i1, const int i2, const int i3) {
    return max2(max2(i1, i2), i3);
}

static inline int max4 (const int i1, const int i2, const int i3, const int i4) {
    return max2(max2(i1, i2), max2(i3, i4));
}

static inline int argMin2 (const int i1, const int i2) {
  return ((i2 < i1)? 1: 0);
}

static inline int argMin3 (const int i1, const int i2, const int i3) {
  if (i2 < i1) {
    return ((i3 < i2)? 2: 1);
  }
  else {
    return ((i3 < i1)? 2: 0);
  }
}

static inline int argMin4 (const int i1, const int i2, const int i3, const int i4) {
  if (i2 < i1) {
    if (i3 < i2) {
      return ((i4 < i3)? 3: 2);
    }
    else {
      return ((i4 < i2)? 3: 1);
    }
  }
  else {
    if (i3 < i1) {
      return ((i4 < i3)? 3: 2);
    }
    else {
      return ((i4 < i1)? 3: 0);
    }
  }
}

#endif
