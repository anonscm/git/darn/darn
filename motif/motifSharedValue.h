#ifndef MOTIFSHAREDVALUE_H_INCLUDED
#define MOTIFSHAREDVALUE_H_INCLUDED

#include "motifCommon.h"

/**
 * The class stores an integer value that is shared by the description of several organisms (such as the size of an helix).
 */
class MotifSharedValue {

protected:
  /**
   * the number of values
   */
  int nbValues;
  /**
   * the values
   */
  int *values;
  /**
   * quantiles of the distribution
   */
  int *quantiles;

public:
  /**
   * the constructor
   * @param nv the number of values
   */
  MotifSharedValue (int nv) : nbValues(nv) {
    values = new int[nbValues];
    quantiles = NULL;
  }

  /**
   * set this instance as the sum of two other instances
   * @param v1 another instance
   * @param v2 another instance
   */
  void sum (MotifSharedValue &v1, MotifSharedValue &v2) {
    for (int i = 0; i < nbValues; i++) {
      setValue(i, v1.values[i] + v2.values[i]);
    }
  }

  /**
   * set this instance as the sum of three other instances
   * @param v1 another instance
   * @param v2 another instance
   * @param v3 another instance
   */
  void sum (MotifSharedValue &v1, MotifSharedValue &v2, MotifSharedValue &v3) {
    for (int i = 0; i < nbValues; i++) {
      setValue(i, v1.values[i] + v2.values[i] + v3.values[i]);
    }
  }

  /**
   * set a value
   * @param i the index of the value
   * @param v the value itself
   */
  void setValue (int i, int v) {
    if (i > nbValues) {
      trace(1, "Error! Trying to set an out of range value in MotifSharedValue!");
      return;
    }
    values[i] = v;
  }

  /**
   * get a value
   * @param i the index of the value
   * @return the @a i-th value
   */
  int getValue (int i) {
    if (i > nbValues) {
      trace(1, "Error! Trying to get an out of range value in MotifSharedValue!");
      return UNDEFINED;
    }
    return values[i];
  }

  /**
   * @pre the values should be set
   *
   * compute the minimum of the stored values
   * @return the minimum
   */
  int getMinimum () {
    int m;
    int i = 0;
    for (i = 0; i < nbValues; i++) {
      if (values[i] != UNDEFINED) {
        break;
      }
    }
    if (i >= nbValues) {
      return UNDEFINED;
    }
    for (m = values[i]; i < nbValues; i++) {
      if (values[i] != UNDEFINED) {
        m = min2(m, values[i]);
      }
    }
    return m;
  }

  /**
   * @pre the values should be set
   *
   * compute the value reaching the minimum of the stored values
   * @return the value reaching the minimum
   */
  int getArgMinimum () {
    int m;
    int arg;
    int i = 0;
    for (i = 0; i < nbValues; i++) {
      if (values[i] != UNDEFINED) {
        arg = i;
        break;
      }
    }
    if (i >= nbValues) {
      return UNDEFINED;
    }
    for (m = values[i]; i < nbValues; i++) {
      if (values[i] != UNDEFINED) {
        if (values[i] < m) {
          m = values[i];
          arg = i;
        }
      }
    }
    return arg;
  }

  /**
   * @pre the values should be set
   *
   * compute the maximum of the stored values
   * @return the maximum
   */
  int getMaximum () {
    int m;
    int i = 0;
    for (i = 0; i < nbValues; i++) {
      if (values[i] != UNDEFINED) {
        break;
      }
    }
    if (i >= nbValues) {
      return UNDEFINED;
    }
    for (m = values[i]; i < nbValues; i++) {
      if (values[i] != UNDEFINED) {
        m = max2(m, values[i]);
      }
    }
    return m;
  }

  /**
   * @pre the values should be set
   *
   * compute the value reaching the maximum of the stored values
   * @return the value reaching the maximum
   */
  int getArgMaximum () {
    int m;
    int arg;
    int i = 0;
    for (i = 0; i < nbValues; i++) {
      if (values[i] != UNDEFINED) {
        arg = i;
        break;
      }
    }
    if (i >= nbValues) {
      return UNDEFINED;
    }
    for (m = values[i]; i < nbValues; i++) {
      if (values[i] != UNDEFINED) {
        if (values[i] > m) {
          m = values[i];
          arg = i;
        }
      }
    }
    return arg;
  }

  /**
   * @pre the values should be set
   *
   * compute the number of times the given value is reached
   * @param v a value
   * @return the number of times @a v is reached
   */
  int getCard (int v) {
    int nb = 0;
    for (int i = 0; i < nbValues; i++) {
      if (min2(values[i], MAX_POSSIBLE_COST) == v) {
        nb++;
      }
    }
    return nb;
  }

  /**
   * @pre the values should be set
   *
   * compute the average of the stored values
   * @return the average
   */
  float getAverage () {
    int nb = nbValues;
    float a = 0.0;
    for (int i = 0; i < nbValues; i++) {
      if (values[i] == UNDEFINED) {
        nb--;
      }
      else {
        a += values[i];
      }
    }
    if (nb == 0) {
      return UNDEFINED;
    }
    return (a / nb);
  }

  /**
   * @pre the values should be set
   *
   * compute the standard deviation of the stored values
   * @return the standard deviation
   */
  float getStandardDeviation () {
    int nb = nbValues;
    float a = 0.0;
    float average = getAverage();
    for (int i = 0; i < nbValues; i++) {
      if (values[i] == UNDEFINED) {
        nb--;
      }
      else {
        a += (average - values[i]) * (average - values[i]);
      }
    }
    if (nb == 0) {
      return UNDEFINED;
    }
    return sqrt(a / nb);
  }

  /**
   * @pre the values should be set
   *
   * compute the quantiles of the distribution
   * @return the quantiles of the distribution
   */
  int *getQuantiles () {
    int max = getMaximum();
  
    if (max == UNDEFINED) {
      return NULL;
    }

    if (quantiles != NULL) {
      return quantiles;
    }
    
    quantiles = new int[max+1];

    for (int j = 0; j <= max; j++) {
      quantiles[j] = 0;
    }

    for (int i = 0; i < nbValues; i++) {
      quantiles[values[i]]++;
    }

    return quantiles;
  }

  /**
   * print a representation of the stem
   * @return a string
   */
  string print () {
    char m[255];
    sprintf(m, "%i", values[0]);
    for (int i = 1; i < nbValues; i++) {
      sprintf(m, "%s, %i", m, values[i]);
    }
    return (string(m));
  }
};

#endif
