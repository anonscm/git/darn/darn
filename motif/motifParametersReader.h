#ifndef MOTIFPARAM_H_INCLUDED
#define MOTIFPARAM_H_INCLUDED


#include <stdio.h>
#include <string.h>

#include "motifCommon.h"


/**
 * Write an help message
 */
void writeHelpMessage () {
  cout << "motif -i file1 [-o file2] [-d] [-h] [-V[V]]" << endl;
  cout << "\tfile1: the file that contains the alignment" << endl;
  cout << "\tfile2: the file where to write the result (default: standard output)" << endl;
  cout << "\td: only display information on the generated descriptor" << endl;
  cout << "\th: this message" << endl;
  cout << "\tv: verbosity" << endl;
}

/**
 * Read the command line
 * @param argc the number of parameters in the command line
 * @param argv the parameter
 * @return true if the program should exit
 */
bool readParameters (int argc, char *argv[]) {
  int argRed = 1;
  if (argc <= 2) {
    writeHelpMessage();
    exit(0);
  }
  outputStream = &cout;
  traceLevel = 1;
  information = false;
  while (argRed < argc) {
    if (!strcmp(argv[argRed], "-h") || (!strcmp(argv[argRed], "--help"))) {
      writeHelpMessage();
      return true;
    }
    else if (!strcmp(argv[argRed], "-i")) {
      inputFileName = argv[argRed+1];
      argRed += 2;
    }
    else if (!strcmp(argv[argRed], "-o")) {
      outputStream = new ofstream(argv[argRed+1]);
      argRed += 2;
    }
    else if (!strcmp(argv[argRed], "-d")) {
      argRed += 1;
      information = true;
    }
    else if (!strcmp(argv[argRed], "-V")) {
      traceLevel = 10;
      argRed += 1;
    }
    else if (!strcmp(argv[argRed], "-VV")) {
      traceLevel = 100;
      argRed += 1;
    }
    else {
      cerr << "Error ! Cannot understand parameter '" << argv[argRed] << "'!" << endl;
      argRed += 1;
    }
  }
  trace(10, "Parameters red.");
  return false;
}

#endif
