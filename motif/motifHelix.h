#ifndef MOTHELIX_H_INCLUDED
#define MOTHELIX_H_INCLUDED

#include "motifTools.h"
#include "motifStem.h"

/**
 * The class represents an helix
 */
class MotifHelix : public MotifElement {

protected:
  /**
   * the first stem
   */
  MotifStem *firstStem;
  /**
   * the second stem
   */
  MotifStem *secondStem;
  /**
   * both stems
   */
  MotifStem **stems;
  /**
   * the distance between the stems
   */
  MotifSharedValue *distances;
  /**
   * the overall number of errors
   */
  MotifSharedValue *errors;
  /**
   * the score profile
   */
  int *scores;
  /**
   * whether the helix is found in every sequence
   */
  bool optional;

  /**
   * get the number of matching errors between two different regions of the sequence
   * @param org the organism index
   * @param start1 the start position of the first region
   * @param stop1 the stop position of the first region
   * @param start1 the start position of the second region
   * @param stop2 the stop position of the second region
   * @return the number of errors (Levenshtein distance)
   */
  int getErrors (int org, int start1, int stop1, int start2, int stop2);

public:
  /**
   * the constructor
   * @param no the number of organisms
   */
  MotifHelix (int no);
  /**
   * get @a firstStem
   * @return the first stem
   */
  MotifStem *getFirstStem ();
  /**
   * get @a secondStem
   * @return the second stem
   */
  MotifStem *getSecondStem ();
  /**
   * get one of the two stem
   * @param i the index of the stem (0 or 1)
   * @return the stem
   */
  MotifStem *getStem (int i);
  /**
   * set the @a distances and @a mismatches values, and pass the message to the stems
   */
  void setValues ();
  /**
   * get @a startIndex
   * @return the start index
   */
  int getStartIndex ();
  /**
   * get @a stopIndex
   * @return the stop index
   */
  int getStopIndex ();
  /**
   * get @a startIndices
   * @return the start indices
   */
  MotifSharedValue *getStartIndices ();
  /**
   * get @a stopIndices
   * @return the stop indices
   */
  MotifSharedValue *getStopIndices ();

  /**
   * whether the helix is found in every sequence
   * @return an accessor to @a optional
   */
  bool isOptional();
  
  /**
   * whether one can express preferences on this constraint
   * @return true, if one can express preferences on this constraint
   */
  bool hasSoftPart();

  /**
   * export the element as a Darn cost function descriptor line
   * @return the descriptor line
   */
  string exportFunction();
  /**
   * export the maximum cost given by this constraint
   * @return the maximum cost
   */
  int getMaximumCost();
  /**
   * get the cost by this helix on a given constraint
   * @param org the index of the sequence
   * @return a cost
   */
  int getCost(int org);
  /**
   * whether the given position is in the pattern
   * @param pos the given position
   * @return -1, if the given position is in the first stem,
   *    1 if it is in the second stem,
   *    0 otherwise
   */
  int isInside(int pos);


  /**
   * possibly print some information about this pattern
   * @return a string
   */
  string printInformation ();
  /**
   * print a representation of the helix
   * @return a string
   */
  string print ();
};

#endif
