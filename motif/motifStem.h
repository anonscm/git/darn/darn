#ifndef MOTIFSTEM_H_INCLUDED
#define MOTIFSTEM_H_INCLUDED

#include "motifSequence.h"
#include "motifElement.h"


class MotifHelix;

/**
 * The class represents the stem of an helix
 */
class MotifStem : public MotifElement {

protected:
  /**
   * the beginning of the stem
   */
  int startIndex;
  /**
   * the end of the stem
   */
  int stopIndex;
  /**
   * the beginning in each sequence
   */
  MotifSharedValue *startIndices;
  /**
   * the end in each sequence
   */
  MotifSharedValue *stopIndices;
  /**
   * the different lengths
   */
  MotifSharedValue *lengths;
  /**
   * the number of insertions / deletions
   */
  MotifSharedValue *indels;
  /**
   * the helix that owns this stems
   */
  MotifHelix *helix;
  /**
   * whether the stem is found in every sequence
   */
  bool optional;

public:
  /**
   * the constructor
   * @param no the number of organisms
   * @param m the helix that owns the stem
   * @param start the start index of the stem
   * @param stop the end index of the stem
   */
  MotifStem (int no, MotifHelix *h, int start = UNDEFINED, int stop = UNDEFINED);
  /**
   * set @a startIndex
   * @param start the new start index
   */
  void setStartIndex (int start);
  /**
   * set @a stopIndex
   * @param stop the new start index
   */
  void setStopIndex (int stop);
  /**
   * get @a startIndex
   * @return the start index
   */
  int getStartIndex ();
  /**
   * get @a stopIndex
   * @return the stop index
   */
  int getStopIndex ();
  /**
   * get @a startIndices
   * @return the start indices
   */
  MotifSharedValue *getStartIndices ();
  /**
   * get @a stopIndices
   * @return the stop indices
   */
  MotifSharedValue *getStopIndices ();
  /**
   * get @a lengths
   * @return the lengths
   */
  MotifSharedValue *getLengths ();
  /**
   * get @a indels
   * @return the indels
   */
  MotifSharedValue *getIndels ();
  /**
   * make the stem grow
   * @param size the amount of nucleotides the stem should grow
   */
  void grow (int size = 1);

  /**
   * set the @a lengths and @a indels values
   */
  void setValues ();


  /**
   * set the helix that owns this stem
   * @param h an helix
   */
  void setHelix (MotifHelix *h);
  /**
   * get the helix that owns this stem
   * @return the helix
   */
  MotifHelix *getHelix ();

  /**
   * whether the stem is found in every sequence
   * @return an accessor to @a optional
   */
  bool isOptional();
  /**
   * whether the given position is in the stem
   * @param pos the given position
   * @return true, if the given position is in the stem
   */
  bool isInside (int pos);

  /**
   * export the element as a MilPat hard constraint descriptor line
   * @return the descriptor line
   */
  string exportHard();

  /**
   * print a representation of the stem
   * @return a string
   */
  string print ();
};

#endif
