#ifndef MOTIFSEQUENCE_H_INCLUDED
#define MOTIFSEQUENCE_H_INCLUDED

#include "motifCommon.h"


/**
 * This class represents a letter of a sequence alignment
 */
class MotifSequenceLetter {

public:

protected:
  /**
   * the structure that defines all the possible letters in a sequence (including '-')
   */
  enum Letter {A, C, G, T, M, R, W, S, Y, K, N, X, GAP};
  /**
   * the current letter
   */
  Letter letter;
  /**
   * ascii representation of the letter
   */
  static char characters[];
  /**
   * matching array
   */
  static bool matching[][NBSEQUENCELETTERS];
  /**
   * compatibility array
   */
  static bool compatibility[][NBSEQUENCELETTERS];

  /**
   * parse a character to a letter
   * @param c the character
   * @return a letter
   */
  static Letter getLetter (char c) {
    char m[100];
    switch (c) {
      case 'A':
      case 'a':
        return A;
      case 'C':
      case 'c':
        return C;
      case 'G':
      case 'g':
        return G;
      case 'U':
      case 'u':
      case 'T':
      case 't':
        return T;
      case 'M':
      case 'm':
        return M;
      case 'R':
      case 'r':
        return R;
      case 'W':
      case 'w':
        return W;
      case 'S':
      case 's':
        return S;
      case 'Y':
      case 'y':
        return Y;
      case 'K':
      case 'k':
        return K;
      case 'N':
      case 'n':
        return N;
      case 'X':
      case 'x':
        return X;
      case '-':
      case '_':
      case ':':
      case '.':
        return GAP;
      default:
        sprintf(m, "Error! An alignment contains an unreadable letter: (char #%i) '%c'!", (int) c, c);
        trace (1, m);
        return GAP;
    }
  }

public:
  /**
   * constructor with a character (which sould be 'A', 'C', 'G', 'T', 'U', 'N', 'X', '-' or '.')
   * @param c the character
   */
  MotifSequenceLetter (char c = '-') {
    letter = getLetter(c);
  }
  /**
   * the copy constructor
   * @param n the nucleotide to be copied
   * @return a copied letter
   */

  MotifSequenceLetter operator= (MotifSequenceLetter &n) {
    letter = n.letter;
    return *this;
  }

  /**
   * whether the nucleotide is "canceled"
   * @return true, if the nucleotide is "canceled"
   */
  bool isCanceled () {
    return (letter == X);
  }

  /**
   * whether the current letter is gap
   * @return true it is a gap
   */
  bool isGap () {
    return (letter == GAP);
  }

  /**
   * whether the current letter is ambiguous (not a real nucleotide)
   * @return true it is ambiguous
   */
  bool isAmbiguous () {
    return ((int) letter >= 4);
  }

  /**
   * whether the current letter is the same as the given one
   * @param n the given letter
   * @return true if @a n is the same letter
   */
  bool isEqual (MotifSequenceLetter *n) {
    return (letter == n->letter);
  }

  /**
   * test whether the given is the same as this one
   * @param n the given nucleotide
   * @return true if the given is the same as this one
   */
  bool isCompatible (MotifSequenceLetter *n) {
    return (compatibility[(int) letter][(int) n->letter]);
  }

  /**
   * test whether the given nucleotide can match the given one
   * @param n the given nucleotide
   * @return true if the nucleotides can match together
   */
  bool matchesWith (MotifSequenceLetter *n) {
    switch (letter) {
      case A:
        return ((n == motifSequenceLetterT) || (n == motifSequenceLetterN));
      case C:
        return ((n == motifSequenceLetterG) || (n == motifSequenceLetterN));
      case G:
        return ((n == motifSequenceLetterC) || (n == motifSequenceLetterT) || (n == motifSequenceLetterN));
      case T:
        return ((n == motifSequenceLetterA) || (n == motifSequenceLetterG) || (n == motifSequenceLetterN));
      case N:
        return true;
      case X:
        return false;
      case GAP:
        return false;
      default:
        return false;
    }
  }

  /**
   * print a trace of the letter
   * @return a character
   */
  char print () {
    return (characters[(int) letter]);
  }

  /**
   * get a code for the nucleotide
   */
  int getCode () {
    return (int) letter;
  }

  /**
   * get the corresponding letter (const)
   * @param c a character
   * @return a letter
   */
  static MotifSequenceLetter *getSequenceLetter (char c) {
    return motifSequenceLetters[(int) getLetter(c)];
  }

  /**
   * get a letter corresponding the vector of presence
   * @param presence a vector of size 4
   * @return a letter
   */
  static MotifSequenceLetter *getSequenceLetter (int *presence) {
    int nbDifferentNucleotides = 0;

    for (int i = NBNUCLEOTIDES; i < NBSEQUENCELETTERS; i++) {
       if (presence[i] > 0) {
         return motifSequenceLetterN;
       }
    }
    
    for (int i = 0; i < NBNUCLEOTIDES; i++) {
      if (presence[i] > 0) {
        nbDifferentNucleotides++;
      }
    }
    switch (nbDifferentNucleotides) {
      case 0:
        return motifSequenceLetterGAP;
      case 1:
        for (int i = 0; i < NBNUCLEOTIDES; i++) {
          if (presence[i] > 0) {
            return motifSequenceLetters[i];
          }
        }
      case 2:
        if (presence[0] > 0) {
          if (presence[1] > 0) {
            return motifSequenceLetterM;
          }
          else if (presence[2] > 0) {
            return motifSequenceLetterR;
          }
          else {
            return motifSequenceLetterW;
          }
        }
        else if (presence[1] > 0) {
          if (presence[2] > 0){
            return motifSequenceLetterS;
          }
          else {
            return motifSequenceLetterY;
          }

        }
        else {
          return motifSequenceLetterK;
        }
      default:
        return motifSequenceLetterN;
    }
  }
};



/**
 * This class represents a sequence of an alignment
 */
class MotifSequence {

protected:
  /**
   * the name of the sequence
   */
  char name[100];
  /**
   * the size of the sequence
   */
  int size;
  /**
   * the sequence itself
   */
  MotifSequenceLetter **sequence;

public:
  /**
   * constructor
   * @param s the size of the sequence
   */
  MotifSequence (int s) : size(s) {
    sequence = new MotifSequenceLetter*[size];
    name[0] = '\0';
  }

  MotifSequence (MotifSequenceLetter **ms, int s) : size(s) {
    sequence = ms;
    name[0] = '\0';
  }

  /**
   * copy part of a sequence
   * @param ms the original sequence
   * @param start the start position of the new sequence
   * @param s the size
   */
  MotifSequence (MotifSequence *ms, int start, int s) : size(s) {
    sequence = new MotifSequenceLetter*[size];
    for (int i = 0; i < size; i++) {
      sequence[i] = ms->sequence[start+i];
    }
    name[0] = '\0';
  }

  /**
   * set the name
   * @param n the name of the sequence
   */
  void setName (char *n) {
    char c;
    for (int i = 0; i < 99; i++) {
      c = n[i];
      name[i] = c;
      if (c == '\0') {
        return;
      }
    }
    name[100] = '\0';
  }

  /**
   * set the sequence
   * @param s the sequence, in ascii characters
   */
  void setSequence (string s) {
    for (int i = 0; i < size; i++) {
      sequence[i] = MotifSequenceLetter::getSequenceLetter(s[i]);
    }
  }

  /**
   * get the name of the sequence
   * @return the name
   */
  char *getName () {
    return name;
  }

  /**
   * get the size of the sequence
   * @return the size
   */
  int getSize () {
    return size;
  }

  /**
   * get an element of the sequence
   * @param i the index of the element
   * @return the element
   */
  MotifSequenceLetter *operator[] (int i) {
    if (i >= size) {
      trace(1, "Error, trying to get an out of range alignment letter!");
      return motifSequenceLetterGAP;
    }
    return sequence[i];
  }

  /**
   * "cancel" a nucleotide
   * @param i the index of the element
   */
  void cancel (int i) {
    if (i >= size) {
      trace(1, "Error, trying to get an out of range alignment letter!");
      return;
    }
    sequence[i] = motifSequenceLetterX;
  }

  /**
   * whether the nucleotide is "canceled"
   * @param i the index of the element
   * @return true, if the nucleotide is "canceled"
   */
  bool isCanceled (int i) {
    if (i >= size) {
      trace(1, "Error, trying to get an out of range alignment letter!");
      return false;
    }
    return (sequence[i]->isCanceled());
  }

  /**
   * remove the gaps of the sequence
   */
  void removeGaps () {
    int s = 0;
    int j = 0;
    MotifSequenceLetter **seq;
    for (int i = 0; i < size; i++) {
      if (!sequence[i]->isGap()) {
        s++;
      }
    }
    seq = new MotifSequenceLetter*[s];
    for (int i = 0; i < size; i++) {
      if (!sequence[i]->isGap()) {
        seq[j] = sequence[i];
        j++;
      }
    }
    delete[] sequence;
    sequence = seq;
    size = s;
  }

  /**
   * revert the sequence
   */
  void revert () {
    MotifSequenceLetter **seq = new MotifSequenceLetter*[size];
    for (int i = 0; i < size; i++) {
      seq[i] = sequence[size-1-i];
    }
    delete[] sequence;
    sequence = seq;
  }

  /**
   * print a representation of the sequence
   * @return a string
   */
  string print () {
    string s;
    for (int i = 0; i < size; i++) {
      s += sequence[i]->print();
    }
    return s;
  }
};



/**
 * This class represents a letter of a sequence structure
 */
class MotifStructureLetter {

public:
  /**
   * the number of different letters
   */
  static const int nbMotifStructureLetters = 9;

protected:
  /**
   * the structure that defines all the possible letters in a sequence (including '-')
   */
  enum Letter {LEFT1, RIGHT1, LEFT2, RIGHT2, LEFT3, RIGHT3, LEFT4, RIGHT4, GAP};
  /**
   * the current letter
   */
  Letter letter;
  /**
   * ascii representation of the letter
   */
  static char characters[];

  /**
   * parse a character to a letter
   * @param c the character
   * @return a letter
   */
  static Letter getLetter (char c) {
    char m[100];
    switch (c) {
      case '(':
        return LEFT1;
      case ')':
        return RIGHT1;
      case '<':
        return LEFT2;
      case '>':
        return RIGHT2;
      case '{':
        return LEFT3;
      case '}':
        return RIGHT3;
      case '[':
        return LEFT4;
      case ']':
        return RIGHT4;
      case '-':
      case '.':
      case ',':
      case '_':
      case ':':
        return GAP;
      default:
        sprintf(m, "Error! The structure contains an unreadable letter: (char #%i) '%c'!", (int) c, c);
        trace (1, m);
        return GAP;
    }
  }

public:
  /**
   * constructor with a character (which sould be '(', ')', '<', '>', '{', '}', '[', ']', '-' or '.')
   * @param c the character
   */
  MotifStructureLetter (char c = '-') {
    letter = getLetter(c);
  }

  /**
   * the copy constructor
   * @param n the nucleotide to be copied
   * @return a copied letter
   */
  MotifStructureLetter operator= (MotifStructureLetter &n) {
    letter = n.letter;
    return *this;
  }

  /**
   * whether the current letter is gap
   * @return true it is a gap
   */
  bool isGap () {
    return (letter == GAP);
  }

  /**
   * whether the current letter is the same as the given one
   * @param n the given letter
   * @return true if @a n is the same letter
   */
  bool isEqual (MotifStructureLetter *n) {
    return (letter == n->letter);
  }

  /**
   * print a trace of the letter
   * @return a character
   */
  char print () {
    return (characters[(int) letter]);
  }

  /**
   * get the corresponding letter (const)
   * @param c a character
   * @return a letter
   */
  static MotifStructureLetter *getMotifStructure (char c) {
    return motifStructureLetters[(int) getLetter(c)];
  }

  /**
   * check whether the current letter is a left brace
   * @return true, if the current letter is a left brace
   */
  bool isLeftBrace () {
    int index = (int) letter;
    return ((index < GAP) && (index % 2 == 0));
  }

  /**
   * check whether the current letter is a right brace
   * @return true, if the current letter is a right brace
   */
  bool isRightBrace () {
    int index = (int) letter;
    return ((index < GAP) && (index % 2 == 1));
  }

  /**
   * get the complement of a brace
   * @return the complement
   */
  MotifStructureLetter *getComplement () {
    int index = (int) letter;;
    char m[100];
    if (index >= GAP) {
      sprintf(m, "Error! Cannot get complement of '%c'", print());
      trace(1, m);
      return motifStructureLetterGAP;
    }
    if (index & 1 == 0) {
      return motifStructureLetters[index+1];
    }
    else {
      return motifStructureLetters[index-1];
    }
  }
};


/**
 * This class represents a sequence of an structure
 */
class MotifStructure {

protected:
  /**
   * the size of the sequence
   */
  int size;
  /**
   * the structure itself
   */
  MotifStructureLetter **structure;
  /**
   * if a letter is a member of a pair, gives the other member of the pair
   */
  int *pairs;
  /**
   * the consensus sequence
   */
  MotifSequenceLetter **sequence;
  /**
   * a structure that encapsulate the consensus sequence
   */
  MotifSequence *motifSequence;
  /**
   * the 100% consensus sequence
   */
  MotifSequenceLetter **pureSequence;
  /**
   * a structure that encapsulate the 100% consensus sequence
   */
  MotifSequence *motifPureSequence;
  /**
   * the rates of presence of the most common letter
  int **rates;
   */

  /**
   * explicitely set the association between pairs of braces
   */
  void setPairs () {
    MotifStructureLetter *currentLeftBrace, *currentRightBrace;
    stack<int> s;
    int pos;
    char m[100];
    for (int j = 0; j < size; j++) {
      pairs[j] = UNDEFINED;
    }
    for (int i = 0; i < 4; i++) {
      currentLeftBrace = motifStructureLetters[2*i];
      currentRightBrace = motifStructureLetters[2*i+1];
      for (int j = 0; j < size; j++) {
        if (structure[j] == currentLeftBrace) {
          s.push(j);
        }
        else if (structure[j] == currentRightBrace) {
          if (s.empty()) {
            sprintf(m, "Error! Structure structure sequence seems wrong: extra number of '%c'!", currentRightBrace->print());
            trace(1, m);
          }
          else {
            pos = s.top();
            s.pop();
            pairs[pos] = j;
            pairs[j] = pos;
          }
        }
      }
      if (!s.empty()) {
        sprintf(m, "Error! Structure structure sequence seems wrong: extra number of '%c'!", currentLeftBrace->print());
        trace(1, m);
        while (s.empty()) {
          s.pop();
        }
      }
    }
  }

  /**
   * set the consensus sequence
   */
  void setSequence () {
    int rates[size][NBSEQUENCELETTERS];
    MotifSequenceLetter *letter;
    bool found;

    for (int i = 0; i < size; i++) {
      sequence[i] = motifSequenceLetterN;

      // get the nucleotide distribution
      for (int j = 0; j < NBSEQUENCELETTERS; j++) {
        letter = motifSequenceLetters[j];
        rates[i][j] = 0;
        for (int k = 0; k < nbOrganisms; k++) {
          if ((*motifSequences[k])[i] == letter) {
            rates[i][j]++;
          }
        }
      }

      // set the sequence
      found = false;
      if (((float) rates[i][motifSequenceLetterGAP->getCode()] / ((float) nbOrganisms)) >= 0.5) {
        sequence[i] = motifSequenceLetterGAP;
      }
      else {
        for (int j = 0; j < NBNUCLEOTIDES; j++) {
          if (((float) rates[i][j]) / ((float) (nbOrganisms - rates[i][motifSequenceLetterGAP->getCode()])) > CONSENSUS_THRESHOLD) {
            sequence[i] = motifSequenceLetters[j];
            found = true;
            break;
          }
        }
        if (! found) {
          sequence[i] = MotifSequenceLetter::getSequenceLetter(rates[i]);
        }
      }

      // set the pure sequence
      if (rates[i][motifSequenceLetterGAP->getCode()] > 0) {
        pureSequence[i] = motifSequenceLetterGAP;
      }
      else {
        pureSequence[i] = MotifSequenceLetter::getSequenceLetter(rates[i]);
        if (! pureSequence[i]->isAmbiguous()) {
          sequence[i] = motifSequenceLetterX;
        }
      }
    }
    motifSequence = new MotifSequence(sequence, size);
    motifPureSequence = new MotifSequence(pureSequence, size);

    /*

      // get the gap distribution
      for (int k = 0; k < nbOrganisms; k++) {
        if ((*motifSequences[k])[i]->isGap()) {
          nbGaps++;
        }
        else if (((*motifSequences[k])[i] == motifSequenceLetterA) || ((*motifSequences[k])[i] == motifSequenceLetterC) || ((*motifSequences[k])[i] == motifSequenceLetterG) || ((*motifSequences[k])[i] == motifSequenceLetterT)) {
          nbTotal++;
        }
      }

      // we have a gap
      if (nbGaps > nbOrganisms / 2.0) {
        sequence[i] = motifSequenceLetterGAP;
        continue;
      }
      consensus = motifSequenceLetterN;

      // get the nucleotide distribution
      for (int j = 0; j < NBNUCLEOTIDES; j++) {
        letter = motifSequenceLetters[j];
        nbPositives = 0;
        presence[j] = false;
        for (int k = 0; k < nbOrganisms; k++) {
          if ((*motifSequences[k])[i] == letter) {
            presence[j] = true;
            nbPositives++;
          }
        }

        // one nucleotide has reached the consensus threshold
        if ((float) nbPositives / (float) nbTotal > CONSENSUS_THRESHOLD) {
          consensus = letter;
          rates[i] = static_cast<int>((((float) nbPositives / (float) nbTotal) * 100));
        }
      }

      // set the letter
      letter = MotifSequenceLetter::getSequenceLetter(presence);
      if (letter != motifSequenceLetterN) {
        sequence[i] = letter;
      }
      else {
        sequence[i] = consensus;
      }
    }
    motifSequence = new MotifSequence(sequence, size);
    */
  }


public:
  /**
   * constructor
   * @param s the size of the structure sequence
   */
  MotifStructure (int s) : size(s) {
    structure = new MotifStructureLetter*[size];
    pairs = new int[size];
    sequence = new MotifSequenceLetter*[size];
    pureSequence = new MotifSequenceLetter*[size];
  }

  /**
   * accessor to @a size
   * @return the size of the structure
   */
  int getSize () {
    return size;
  }

  /**
   * set the structure
   * @param s the structure, in ascii characters
   */
  void setSequence (string s) {
    for (int i = 0; i < size; i++) {
      structure[i] = MotifStructureLetter::getMotifStructure(s[i]);
    }
    setPairs();
    setSequence();
  }

  /**
   * get an element of the structure
   * @param i the index of the element
   * @return the element
   */
  MotifStructureLetter *getStructure (int i) {
    if (i >= size) {
      trace(1, "Error, trying to get an out of range structure letter!");
      return motifStructureLetterGAP;
    }
    return structure[i];
  }

  /**
   * get the consensus sequence
   * @return the sequence
   */
  MotifSequence *getSequence () {
    return motifSequence;
  }

  /**
   * get the 100% consensus sequence
   * @return the sequence
   */
  MotifSequence *getPureSequence () {
    return motifPureSequence;
  }

  /**
   * get an element of the consensus sequence
   * @param i the index of the element
   * @return the element
   */
  MotifSequenceLetter *getSequence (int i) {
    if (i >= size) {
      trace(1, "Error, trying to get an out of range sequence letter!");
      return motifSequenceLetterGAP;
    }
    return sequence[i];
  }

  /**
   * get an element of the 100% consensus sequence
   * @param i the index of the element
   * @return the element
   */
  MotifSequenceLetter *getPureSequence (int i) {
    if (i >= size) {
      trace(1, "Error, trying to get an out of range sequence letter!");
      return motifSequenceLetterGAP;
    }
    return pureSequence[i];
  }
  /**
   * get the percentage of probability (higher than @a CONSENSUS_THRESHOLD) of emission of the letter, if any
   * @param i the index of the element
   * @return the percentage, or UNDEFINED
  int getRate (int i) {
    if (i >= size) {
      trace(1, "Error, trying to get an out of range rate letter!");
      return UNDEFINED;
    }
    return rates[i];
  }
   */

  /**
   * get the nucleotide that matches with the given one
   * @param i the index of the given nucleotide
   * @return the index of the other nucleotide
   */
  int getPair (int i) {
    return pairs[i];
  }

  /**
   * print a representation of the structure
   * @return a string
   */
  string print () {
    string s1("Structure    : ");
    string s2("Sequence     : ");
    string s3("Pure sequence: ");
    for (int i = 0; i < size; i++) {
      s1 += structure[i]->print();
      s2 += sequence[i]->print();
      s3 += pureSequence[i]->print();
    }
    return s1 + "\n" + s2 + "\n" + s3;
  }
};


#endif
