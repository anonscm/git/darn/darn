#ifndef MOTIFELEMENT_H_INCLUDED
#define MOTIFELEMENT_H_INCLUDED

#include "motifCommon.h"
#include "motifSharedValue.h"

/**
 * This interface draws what an element of structure (Pattern, Helix and Stem) should contain, namely the bounds
 */
class MotifElement {

protected:
  /**
   * the variable index that gives the beginning of the element
   */
  int startVariableIndex;
  /**
   * the variable index that gives the end of the element
   */
  int stopVariableIndex;

public:
  /**
   * empty destructor
   */
  virtual ~MotifElement () { }
  /**
   * get the beginning index of the element
   * @return the index where the element starts
   */
  virtual int getStartIndex () = 0;
  /**
   * get the end index of the element
   * @return the index where the element ends
   */
  virtual int getStopIndex () = 0;
  /**
   * get the beginning indices of each sequence
   * @return the index where the elements start
   */
  virtual MotifSharedValue *getStartIndices () = 0;
  /**
   * get the beginning indices of each sequence
   * @return the index where the elements start
   */
  virtual MotifSharedValue *getStopIndices () = 0;

  /**
   * set @a startVariableIndex;
   * @param i the index variable stating where the element should start
   */
  virtual void setStartVariableIndex (int i) {
    startVariableIndex = i;
  }
  /**
   * set @a stopVariableIndex;
   * @param i the index variable stating where the element should stop
   */
  virtual void setStopVariableIndex (int i) {
    stopVariableIndex = i;
  }
  /**
   * accessor to @a startVariableIndex;
   * @return the index variable stating where the element starts
   */
  virtual int getStartVariableIndex () {
    return startVariableIndex;
  }
  /**
   * accessor to @a startVariableIndex;
   * @return the index variable stating where the element stops
   */
  virtual int getStopVariableIndex () {
    return stopVariableIndex;
  }
};

#endif

