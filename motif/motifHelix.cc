#include "motifHelix.h"

MotifHelix::MotifHelix (int no) : optional(false) {
  firstStem = new MotifStem(no, this);
  secondStem = new MotifStem(no, this);
  stems = new MotifStem*[2];
  stems[0] = firstStem;
  stems[1] = secondStem;
  distances = new MotifSharedValue(no);
  errors = new MotifSharedValue(no);
}

int MotifHelix::getErrors(int org, int start1, int stop1, int start2, int stop2) {
  if ((start1 == UNDEFINED) || (stop1 == UNDEFINED) || (start2 == UNDEFINED) || (stop2 == UNDEFINED)) {
    return max2(firstStem->getLengths()->getMaximum(), secondStem->getLengths()->getMaximum());
  }
  
  MotifSequence seq1(motifSequences[org], start1, stop1-start1+1);
  MotifSequence seq2(motifSequences[org], start2, stop2-start2+1);
  seq1.removeGaps();
  seq2.removeGaps();
  seq2.revert();
  int l1 = seq1.getSize();
  int l2 = seq2.getSize();
  int matrix[l1+1][l2+1];
  int val1, val2, val3;

  for (int i = 0; i <= l1; i++) {
    matrix[i][0] = i;
  }
  for (int j = 1; j <= l2; j++) {
    matrix[0][j] = j;
  }
  for (int i = 1; i <= l1; i++) {
    for (int j = 1; j <= l2; j++) {
      val1 = matrix[i-1][j-1] + ((seq1[i-1]->matchesWith(seq2[j-1]))? 0: 1);
      val2 = matrix[i][j-1] + 1;
      val3 = matrix[i-1][j] + 1;
      matrix[i][j] = min3(val1, val2, val3);
    }
  }
  return matrix[l1][l2];
}

MotifStem *MotifHelix::getFirstStem () {
  return firstStem;
}

MotifStem *MotifHelix::getSecondStem () {
  return secondStem;
}

MotifStem *MotifHelix::getStem (int i) {
  return stems[i];
}

void MotifHelix::setValues () {
  int v;
  int minScore, maxScore, nbScores;
  int *quantiles;
  double sum;
  int minStem;
  int maxStem;
  
  for (int i = 0; i < nbOrganisms; i++) {
    // first set @a distances
    v = 0;
    for (int j = firstStem->getStopIndex()+1; j <= secondStem->getStartIndex()-1; j++) {
      if (!(*motifSequences[i])[j]->isGap()) {
        v++;
      }
    }
    distances->setValue(i, v);
  }

  // then set the stems
  firstStem->setValues();
  secondStem->setValues();

  optional = (firstStem->isOptional()) || (secondStem->isOptional());

  // then set @a errors
  for (int i = 0; i < nbOrganisms; i++) {
    errors->setValue(i, getErrors(i, firstStem->getStartIndices()->getValue(i), firstStem->getStopIndices()->getValue(i), secondStem->getStartIndices()->getValue(i), secondStem->getStopIndices()->getValue(i)));
  }

  if (! optional) {
    // if the minimum size of the loop is not more than 1, optional
    if (distances->getMinimum() < 2) {
      optional = true;
    }
    // if the size of the stem is 1
    if ((max2(firstStem->getLengths()->getMaximum(), secondStem->getLengths()->getMaximum()) == 0)) {
      // if the maximum number of errors is positive, or the loop size is too big, optional
      if ((errors->getMaximum() > 0) || (distances->getMaximum() > 50)) {
        optional = true;
      }
    }
    // if the size of the stem is not 1 and the maximum number of errors is the minimum size of the stem minus 1, optional
    else if (errors->getMaximum() >= min2(firstStem->getLengths()->getMinimum(), secondStem->getLengths()->getMinimum())) {
      optional = true;
    }
  }
  
  if ((!hasSoftPart()) || (optional)) {
    return;
  }

  // get the scores
  quantiles = errors->getQuantiles();
  scores = new int[errors->getMaximum()+1];
  minStem = min2(firstStem->getLengths()->getMinimum(), secondStem->getLengths()->getMinimum()) + 1;
  maxStem = max2(firstStem->getLengths()->getMaximum(), secondStem->getLengths()->getMaximum()) + 1;
  for (int i = 0; i <= errors->getMaximum(); i++) {
    if (quantiles[i] == 0) {
      scores[i] = UNDEFINED;
    }
    else {
      sum = 0;
      for (int j = minStem; j <= maxStem; j++) {
        sum += pw(NBNUCLEOTIDES, j) * choice(i, j);
      }
      scores[i] = static_cast<int>(round(i * log(NBNUCLEOTIDES) + log(sum) - log(quantiles[i]) + log(nbOrganisms)));
    }
  }

  // complete scores for unknown observations
  if (scores[0] == UNDEFINED) {
    scores[0] = 0;
  }
  minScore = 0;
  nbScores = UNDEFINED;
  for (int i = 0; i <= errors->getMaximum(); i++) {
    if (scores[i] == UNDEFINED) {
      if (nbScores == UNDEFINED) {
        nbScores = 1;
      }
      else {
        nbScores++;
      }
    }
    else {
      if (nbScores == UNDEFINED) {
        minScore = i;
      }
      else {
        maxScore = i;
        for (int j = minScore+1; j < maxScore; j++) {
          scores[j] = static_cast<int>(round((scores[maxScore]-scores[minScore])*(static_cast<float>(j-minScore)/(nbScores+1)))) + scores[minScore];
        }
        nbScores = UNDEFINED;
        minScore = i;
      }
    }
  }

  // reset minimum score to 0
  minScore = scores[0];
  if (minScore != 0) {
    for (int i = 0; i <= errors->getMaximum(); i++) {
      scores[i] -= minScore;
    }
  }

  // possibly rectify costs when cost function is not monotonous
  for (int i = 0; i < errors->getMaximum(); i++) {
    scores[i+1] = max2(scores[i], scores[i+1]);
  }
}

int MotifHelix::getStartIndex () {
  return firstStem->getStartIndex();
}

int MotifHelix::getStopIndex () {
  return secondStem->getStopIndex();
}

MotifSharedValue *MotifHelix::getStartIndices () {
  return firstStem->getStartIndices();
}

MotifSharedValue *MotifHelix::getStopIndices () {
  return secondStem->getStopIndices();
}

bool MotifHelix::isOptional () {
  return (optional);
}

bool MotifHelix::hasSoftPart () {
  return (errors->getMaximum() > 0);
}

string MotifHelix::exportFunction () {
  char mTmp[512];
  string m;

  if (optional) {
    return string("");
  }

  // get the distributions
  int minStem = min2(firstStem->getLengths()->getMinimum(), secondStem->getLengths()->getMinimum()) + 1;
  int maxStem = max2(firstStem->getLengths()->getMaximum(), secondStem->getLengths()->getMaximum()) + 1;
  int minLoop = distances->getMinimum();
  int maxLoop = distances->getMaximum();

  // print the constraint
  sprintf(mTmp, "HELIX[stem=%i..%i,loop=%i..%i,errors=%i,%swobble=yes,model=%s](X%i,X%i,X%i,X%i)", minStem, maxStem, minLoop, maxLoop, errors->getMaximum(), ((MAXGAPSIZEHELIX > 0)? "indels=yes,": ""), ((hasSoftPart())? "soft": "hard"), firstStem->getStartVariableIndex(), firstStem->getStopVariableIndex(), secondStem->getStartVariableIndex(), secondStem->getStopVariableIndex());
  m = mTmp;
  if (hasSoftPart()) {
    m += " {";
    sprintf(mTmp, "%i", scores[0]);
    m += mTmp;
    for (int i = 1; i <= errors->getMaximum(); i++) {
      sprintf(mTmp, ",%i", scores[i]);
      m += mTmp;
    }
    m += "}";
  }
  m += "\n";
  return m;
}

int MotifHelix::getMaximumCost () {
  if ((!hasSoftPart() || (optional))) {
    return 0;
  }
  return scores[errors->getMaximum()];
}

int MotifHelix::getCost (int org) {
  if ((!hasSoftPart() || (optional))) {
    return 0;
  }
  return scores[getErrors(org, firstStem->getStartIndices()->getValue(org), firstStem->getStopIndices()->getValue(org), secondStem->getStartIndices()->getValue(org), secondStem->getStopIndices()->getValue(org))];
}

int MotifHelix::isInside (int pos) {
  if (optional) {
    return 0;
  }
  if (firstStem->isInside(pos)) {
    return -1;
  }
  if (secondStem->isInside(pos)) {
    return 1;
  }
  return 0;
}

string MotifHelix::printInformation () {
  char m[511];
  char mTmp[511];
  int minSize;
  if (! optional) {
    return string("");
  }
  if (errors->getMaximum() >= MAX_POSSIBLE_COST) {
    sprintf(mTmp, "does not appear %i time(s)", errors->getCard(MAX_POSSIBLE_COST));
  }
  else {
    sprintf(mTmp, "contains up to %i error(s) (%i time(s), e.g. organism #%i)", errors->getMaximum(), errors->getCard(errors->getMaximum()), errors->getArgMaximum());
  }
  minSize = min2(firstStem->getLengths()->getMinimum(), secondStem->getLengths()->getMinimum());
  sprintf(m, "Helix %i-%i %i-%i is optional for it %s and the min size is %i (%i time(s), e.g. in organism #%i)", firstStem->getStartIndex(), firstStem->getStopIndex(), secondStem->getStartIndex(), secondStem->getStopIndex(), mTmp, min2(firstStem->getLengths()->getMinimum(), secondStem->getLengths()->getMinimum())+1, firstStem->getLengths()->getCard(minSize) + secondStem->getLengths()->getCard(minSize), ((firstStem->getLengths()->getMinimum() <= secondStem->getLengths()->getMinimum())? firstStem->getLengths()->getArgMinimum(): secondStem->getLengths()->getArgMinimum()));
  return string(m);
}

string MotifHelix::print () {
  return ("--> " + firstStem->print() + "\n    " + secondStem->print() + "\n      (distances: " + distances->print() + " -- errors: " + errors->print() + ")");
}
