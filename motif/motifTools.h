#ifndef MOTIFTOOLS_H_INCLUDED
#define MOTIFTOOLS_H_INCLUDED

#include "motifCommon.h"

static double pw (int a, int b) {
  double res = 1.0;
  for (int i = 0; i < b; i++) {
    res *= a;
  }
  return res;
}

static double choice (int p, int n) {
  double top = 1.0, bot = 1.0;
  if (p > n) {
    return 0;
  }
  for (int i = n-p+1; i <= n; i++) {
    top *= i;
  }
  for (int i = 1; i <= p; i++) {
    bot *= i;
  }
  return (top / bot);
}

#endif
